bool getAllRecords(int startDirPageNo, vector<string> &allrecs){
	
	DM_FUNC_TRACE && cout << "DM.getAllRecords()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	char* recbuf = NULL;	
	int recSize;
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID curRID,nextRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == -1){
				//empty DE - goto next DE
				continue;
			}
			
			//read page in DE into BufferPool
			if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			
			if(DataPage != NULL){
				delete DataPage;
			}
			DataPage = new DataBasePage(datapagebuf);
			
			//read first sys-col RID
			if(!DataPage->firstRecord(curRID)){
				//empty DataBasePage.. goto next page
				continue;
			}
			
			nextRID.pageNo = curRID.pageNo;
			nextRID.slotNo = curRID.slotNo;
			
			//read records
			do{			
				//RECORD LEVEL
				
				curRID.pageNo = nextRID.pageNo;
				curRID.slotNo = nextRID.slotNo;
				
				if(recbuf != NULL){
					delete[] recbuf;
				}
				
				if(!DataPage->getRecord(curRID,recbuf,recSize)){
					//read error
					err = RECORD_READ_ERROR;
					goto ret;
				}			
				
				//memcopy buf into systabrec
				//memcpy((char*)&syscolrec,recbuf,recSize);
				
				//add record to allrecs
				allrecs.push_back(new string(recbuf,recSize));				
				
			
			}while(DataPage->nextRecord(curRID, nextRID));

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(recbuf != NULL){
		delete[] recbuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
