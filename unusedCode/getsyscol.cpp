bool DataManager::getSysColRecsForTable(string tabName,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns){
        DM_FUNC_TRACE && cout << "DM.getSysColRecsForTable()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* sysColDirPage = NULL;
	DataBasePage* DataPage = NULL;	
	
	int curDirPageNo;
	int curDataPageNo;
	
	char* syscolrecbuf = NULL;
	SysColumnRecord syscolrec;
	int recSize;
	
	RID curRID,nextRID;
	
	int totalColsFound = 0;
	
	
	if(dbHeader.getSystemColumnPointer() != SYS_COL_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	curDirPageNo = SYS_COL_START_PAGE_NUM;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(sysColDirPage != NULL){
			delete sysColDirPage;
		}
		sysColDirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!sysColDirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == -1){
				//empty DE - goto next DE
				continue;
			}
			
			//read page in DE into BufferPool
			if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			
			if(DataPage != NULL){
				delete DataPage;
			}
			DataPage = new DataBasePage(datapagebuf);
			
			//read first sys-col RID
			if(!DataPage->firstRecord(curRID)){
				//empty DataBasePage.. goto next page
				continue;
			}
			
			nextRID.pageNo = curRID.pageNo;
			nextRID.slotNo = curRID.slotNo;
			
			//read records
			do{			
				//RECORD LEVEL
				
				curRID.pageNo = nextRID.pageNo;
				curRID.slotNo = nextRID.slotNo;
				
				if(syscolrecbuf != NULL){
					delete[] syscolrecbuf;
				}
				
				if(!DataPage->getRecord(curRID,syscolrecbuf,recSize)){
					//read error
					err = RECORD_READ_ERROR;
					goto ret;
				}			
				
				//memcopy buf into systabrec
				memcpy((char*)&syscolrec,syscolrecbuf,recSize);
				//Process
				cout << endl;
				if(tabName.compare(syscolrec.tableName) == 0){
					syscolrec.dump();
					totalColsFound ++;
					//check if required no. of columns are found.
					if(totalColsFound == noOfColumns)
						break;
				}
				
			
			}while(DataPage->nextRecord(curRID, nextRID));
			
		        if(totalColsFound == noOfColumns)
				break;
		
		}while( sysColDirPage->getNextDirectoryEntry(curDE, nextDE) );
		
		if(totalColsFound == noOfColumns)
			break;
		
	}while((curDirPageNo = sysColDirPage->getNextDirectoryPage()) != -1);
	
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(syscolrecbuf != NULL){
		delete[] syscolrecbuf;
	}
	if(sysColDirPage != NULL){
		delete sysColDirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
