#ifndef SETTINGS_H
#define SETTINGS_H
class settings{
	public:
	static int verbose;
	static int bufferPageSizeInBytes;
	static int noOfPagesInBuffer;
	static char filename[255];

	static void init(){		
		verbose = 1;
		bufferPageSizeInBytes = 2;
		noOfPagesInBuffer = 1;
		//filename  = "dump.txt";
		sprintf(filename,"%s","dump.txt");
	}

};

int settings::verbose;
int settings::bufferPageSizeInBytes;
int settings::noOfPagesInBuffer;
char settings::filename[];

#endif
