#include <iostream>
#include "pagedataentries.h"

#ifndef SYSTABLEDIRECTORY_H
#define SYSTABLEDIRECTORY_H


class SysTableDirectory{
	public:
	        char *pageData;
		SysTableDirectory();
		SysTableDirectory(char *);
		int getNextTableDirectory();
                bool setNextTableDirectory(int );
                int getPrevTableDirectory();
                bool setPrevTableDirectory(int );
                short getNoOfTableDirectoryEntries();
                bool setNoOfTableDirectoryEntries(short );
		bool insertDirectoryEntry(DirectoryEntry );
                bool removeDirectoryEntry(int );
};

#endif
