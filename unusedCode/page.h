#include "./../settings.h"

#ifndef PAGE_H
#define PAGE_H

//class declaration of Page and its interfaces
class Page{

   public :
      unsigned char* pageData;  

      Page(){

	pageData = new unsigned char[settings::bufferPageSizeInBytes];
        for(int i = 0; i < settings::bufferPageSizeInBytes; i++){
            pageData[i] = false;
        }

      }     
   
};

#endif
