#include <iostream>

#ifndef SYSTEMTABLE_H
#define SYSTEMTABLE_H

class SystemTable{
   public:
          char *pageData;
          SysTable();      
          int getNextPage();
          bool setNextPage(int );
          short getNoOfSlots();
          bool setNoOfSlots(short );
          short getNoOfSlotsOccupied();
          bool setNoOfSlotsOccupied(short );
          short getNoOfFreeSlots();
          bool setNoOfFreeSlots(short );
          bool resetAllSlots();
          bool removeTable(short );
          
};

#endif
