#include "./../settings.h"
#include "page.h"
#include "ctime"

#ifndef FRAME_H
#define FRAME_H

class frame{

    private:

    int dirty; //Tells if the page has been modified
    int inUse; //Whether page is being used by someone or not
    int priority; //Priority of this page
    time_t lastUsedTime; //Last time when this frame was used to read/write
    int frameNo; //Number of this frame

    public:

    int pageNo; //Page number used to identify the page
    Page page; //actual content of the page       

    frame(){
        
        dirty = 0;
        pageNo = -1;
	inUse = 0;
        priority = -1;
	lastUsedTime = time (NULL);
	frameNo = -1;
        //verbose && cout << "Creating a new page\n";

    }  

    ~frame(){            
    }

    void setInUse(){
    	inUse = 1;
	lastUsedTime = time (NULL);
    }

    void clearInUse(){
	inUse = 0;
	lastUsedTime = time (NULL);
    }

    bool isInUse(){
	if(inUse == 1)
		return true;
	else
		return false;
    }

    void setDirty(){
	dirty = 1;
    }

    void clearDirty(){
	dirty = 0;
    }

    bool isDirty(){
	if(dirty == 1)
		return true;
	else
		return false;
    }

    void setPriority(int pri){
	priority = pri;
    }
    
    int getPriority(){
	return (int)priority;
    }

    time_t getLastUsedTime(){
	return lastUsedTime;
    }

    int getFrameNo(){
	return frameNo;
    }

    void setFrameNo(int frameNum){
	frameNo = frameNum;
    }

};
#endif
