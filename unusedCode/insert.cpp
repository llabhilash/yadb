//insertRecordIntoDataPage(char record,int reclen,int startPage)-This method inserts the record into the datapage given a 
//                  record ,its size(reclen) , and the directory page.

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include "./../BufferManager/buffermanager.h"
#include "./../Common/Settings.h"
#include "./../Common/DataType.h"
#include "./../Common/Priorities.h"
#include "./../Common/ErrorCodes.h"
#include "./../Datastructures/alldatastructures.h"
#include "./../Datastructures/pagedataentries.h"
#include "datamanager.h"
#ifndef SLOT_DIR_SIZE
#define SLOT_DIR_SIZE 4
#endif


bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader){
	int err = NO_ERROR;
        bool recInsert = false;
	int dirPageNo = startPage;
	char *dirData,*pageData;
	int dbPageNo = -1;
	DirectoryEntry curDirEntry,nextDirEntry;
	//check all directory entries of all directory for the required space to insert record..
	while(true){
	        //Break from this loop once if a freepage to insert a record is found or scanning of
	        //all Directory pages is complete.
	        if(dirPageNo == -1){
		        break;//No more directory page left to scan for DEs.
	        }
		dirData = new char[PAGE_SIZE_IN_BYTES];
		if(!bm.read(dirPageNo,dirData,DIR_PAGE_PRIORITY)){
			//err = DIRPAGE_READ_ERROR;
			//goto ret;
		}
		DirectoryPage dirPage = DirectoryPage(dirData);
		
		//check if a DE is present.
		if(!dirPage.getFirstDirectoryEntry(curDirEntry)){
		        //No DE in this directory page.So,scan the next directory page.
		        dirPageNo = dirPage.getNextDirectoryPage();
		}else{
		        while(true){
		        //scan each DEs to find the required space.
		        //Break from this while loop once if found a freepage or scanning all DEs is completed.
				if(curDirEntry.freeSpace >= d){
					dbPageNo = curDirEntry.pageNo;
					//dirEntry = curDirEntry;
					break;//PageNo containing free space to insert record found.
				}else{
					if(!dirPage.getNextDirectoryEntry(curDirEntry,nextDirEntry)){
					//No DEs was found in this directory Page having the required freespace.
					//So,scan next Directory Page.
					        dirPageNo = dirPage.getNextDirectoryPage();
						delete[] dirData;//remove allocated memory  for directory page;
						break;
			                }else{
				                curDirEntry = nextDirEntry;
			                }
				}
			}//end of second while
			if(dbPageNo != -1){
				break;//if a PageNo containing free space to insert record found,then break from loop.
			}
		}//end of else
	}//end of first while().
	//Check if a pageNo containing required space was found.If not found means no DE entry was found containing
	//required amount of freespace.So,create a new DE by creating a new datapage(DataBasePage).
	if(dbPageNo == -1){
		//Insert a new DE by creating a new page.
		int newPageNo;
		//DirectoryEntry curDirEntry;
		dirPageNo = startPage;//try to start to insert a new page from the first directory page.
		pageData = new char[PAGE_SIZE_IN_BYTES];
		if(!getFreePage(pageData, newPageNo,dbHeader)){
			//err = FREEPAGE_READ_ERROR;
			//goto ret;
		}
		
		curDirEntry.pageNo = newPageNo;
		while(true){
		        if(dirPageNo == -1){
			        break;//All directory pages are full.Need to create new Directory Page.
			        //Need to Implement the case where a new directory Page has to be created.
			        //For now assuming that we wont face a case where a new Directory Page has to be created.
		        }
			dirData = new char[PAGE_SIZE_IN_BYTES];
			if(!bm.read(dirPageNo,dirData,DIR_PAGE_PRIORITY)){
				//err = DIRPAGE_READ_ERROR;
				//goto ret;
			}
			DirectoryPage dirPage = DirectoryPage(dirData);
			//Try to insert the directory Entry for the new page.
			if(!dirPage.insertDirectoryEntry(curDirEntry)){
				//Cannot insert any more DEs in this Directory Page.
			        dirPageNo = dirPage.getNextDirectoryPage();
			        delete[] dirData;
			}else{
				break;
			}
		}
		//Since dirData is not deleted if we could insert a new DE in the same Directory page.
		//we are using it to create a new DirectoryPage object.
		//P.S. the previous instance of DirectoryPage cannot be used outside  the while loop.
		//Initiale the new database page.
		DataBasePage dbPage = DataBasePage(pageData);
		dbPage.init();
		DirectoryPage dirPage = DirectoryPage(dirData);
		RID rid;
		rid.pageNo = curDirEntry.pageNo;
		if(!dbPage.insertRecord(record,reclen,rid)){
		        //err = DATABASEPAGE_WRITE_ERROR;
		        //goto ret;
		}else{
		        curDirEntry.freeSpace = dbPage.getTotalFreeSpace();
		        dirPage.updateDirectoryEntry(curDirEntry);
			bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
			bm.write(newPageNo,pageData,DB_PAGE_PRIORITY);
		}
		VERBOSE && cout<<"Record inserted in to page: "<<rid.pageNo<<" ,slotNo :"<<rid.slotNo<<endl;
	
	}
	//Insert the record into the page=dbPageNo and update its corresponding DE entry in the DirectoryPage=dirPageNo.
	//After inserting a record write back to both dirPage and dbPage into the buffer.
	pageData = new char[PAGE_SIZE_IN_BYTES];
	if(!bm.read(dbPageNo,pageData,DB_PAGE_PRIORITY)){
		//err = DATABASEPAGE_READ_ERROR;
		//goto ret;
	}
	DataBasePage dbPage = DataBasePage(pageData);
	DirectoryPage dirPage = DirectoryPage(dirData);
	RID rid;
	rid.pageNo = curDirEntry.pageNo;
	if(!dbPage.insertRecord(record,reclen,rid)){
	        //err = DATABASEPAGE_WRITE_ERROR;
	        //goto ret;
	}else{
	        curDirEntry.freeSpace = dbPage.getTotalFreeSpace();
	        dirPage.updateDirectoryEntry(curDirEntry);
		bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
		bm.write(dbPageNo,pageData,DB_PAGE_PRIORITY);
	}
	VERBOSE && cout<<"Record inserted in to page: "<<rid.pageNo<<" ,slotNo :"<<rid.slotNo<<endl;
	
	ret:
	printErrorCode(err);
	delete[] dirData,pageData;//remove allocated memory
	return err==NO_ERROR?true:false;
}
