#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include "./../BufferManager/buffermanager.h"
#include "./../Common/Settings.h"
#include "./../Common/DataType.h"
#include "./../Common/Priorities.h"
#include "./../Common/ErrorCodes.h"
#include "./../Datastructures/alldatastructures.h"
#ifndef SLOT_DIR_SIZE
#define SLOT_DIR_SIZE 4
#endif



using namespace std;

class DataManager{
	public:
	       bool insertRecordIntoDataPage(char *,int ,int);
	       DataManager(){}
};

bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage){
        int err = NO_ERROR;
        bool recInsert = false;
	int dirPageNo = startPage;
	char *dirData,*pageData;
	int 
	while(true){
		dirData = new char[PAGE_SIZE_IN_BYTES];
		if(!bm.read(dirPageNo,dirData,DIR_PAGE_PRIORITY){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		DirectoryPage dirPage = DirectoryPage(dirData);
		DirectoryEntry curDirEnrty,nextDirEntry;
		short totalDE = dirPage.getNoOfDirectoryEntries();
		if(!dirPage.getFirstDirectoryEntry(curDirEntry)){
		        err = DIRENTRY_READ_ERROR;
			goto ret;
		}
			
		if(curDirEntry.freeSpace < (short)(reclen + SLOT_DIR_SIZE){
			while(true){
				if(!getNextDirectoryEntry(curDirEntry,nextDirEntry)){
				//No more DE present.
					break;
				}
				curDirEntry = nextDirEntry;
				if(curDirEntry.freespace >= (short)(reclen + SLOT_DIR_SIZE){
					int dbPageNo = curDirEntry.pageNo;
					pageData = new char[PAGE_SIZE_IN_BYTES];
					if(!bm.read(dirPageNo,dirData,DB_PAGE_PRIORITY)){
						err = DATABASEPAGE_READ_ERROR;
						goto ret;
					}
					DataBasePage dbPage = DataBasePage(pageData);
					RID rid;
					rid.pageNo = curDirEntry.pageNo;
					if(!dbPage.insertRecord(record,reclen,rid)){
					        err = DATABASEPAGE_WRITE_ERROR;
					        goto ret;
					}else{
					        curDirEntry.freespace = dbPage.getTotalFreeSpace();
					        dirPage.updateDirectoryEntry(curDirEntry);
						bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
						bm.write(dbPageNo,pageData,DB_PAGE_PRIORITY);
						recInsert = true;
						break;
					}
				}
			}//end of while
		}else{         //end of if
			int dbPageNo = curDirEntry.pageNo;
			pageData = new char[PAGE_SIZE_IN_BYTES];
			if(!bm.read(dirPageNo,dirData,DB_PAGE_PRIORITY)){
				err = DATABASEPAGE_READ_ERROR;
				goto ret;
			}
			DataBasePage dbPage = DataBasePage(pageData);
			RID rid;
			rid.pageNo = curDirEntry.pageNo;
			if(!dbPage.insertRecord(record,reclen,rid)){
			        err = DATABASEPAGE_WRITE_ERROR;
			        goto ret;
			}else{
			        curDirEntry.freespace = dbPage.getTotalFreeSpace();
			        dirPage.updateDirectoryEntry(curDirEntry);
			        bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
				bm.write(dbPageNo,pageData,DB_PAGE_PRIORITY);
				recInsert = true;
			}
		}//end of 
		if(!recInsert){
			int newPageNo;
			pageData = new char[PAGE_SIZE_IN_BYTES];
			if(!getFreePage(pageData, newPageNo, DBHeader &)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			DirectoryEntry newDirEntry;
			newDirEntry.pageNo = newPageNo;
			newDirEnrty.freeSpace = 8000;//initialize it to some number ,later it would be updated.
			if(dirPage.insertDirectoryEntry(newDirEntry)){
			        DataBasePage dbPage = DataBasePage(pageData);
			        dbPage.init();
				RID rid;
				rid.pageNo = newDirEntry.pageNo;
				if(!dbPage.insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}else{
				        newDirEntry.freespace = dbPage.getTotalFreeSpace();
				        dirPage.updateDirectoryEntry(newDirEntry);
					bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
					bm.write(newPageNo,pageData,DB_PAGE_PRIORITY);
					recInsert = true;
				}
			}else{
				int nextPageNo = dirPage.getNextDirectoryPage();
				if(nextPageNo == -1){
					int newPageNo;
					pageData = new char[PAGE_SIZE_IN_BYTES];
					if(!getFreePage(pageData,newPageNo, DBHeader &)){
						err = FREEPAGE_READ_ERROR;
						goto ret;
					}
					dirPage.setNextDirectoryPage(newPage);
					DirectoryPage newDirPage = DirectoryPage(pageData);
					newDirPage.init();
					bm.write(dirPageNo,dirData,DIR_PAGE_PRIORITY);
					bm.write(newPageNo,pageData,DIR_PAGE_PRIORITY);
					dirPageNo = newPageNo;
				}else{
					dirPageNo = nextPageNo;
				}
			}
			
		}else{
			break;
		}
		
	}//first while
	
	
	ret:
	printErrorCode(err);
	delete[] dirData,pageData;
	return err==NO_ERROR?true:false;
}
int main(){
	DataManager DM = DataManager();
}
