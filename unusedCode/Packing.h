//Packing of data into a byte stream, each of the values delimited by 0xff 

#include "DataType.h"
#include <string.h>
#include <malloc.h>
#include <sstream>

#ifndef PACKING_H
#define PACKING_H


bool convert_to_type(string str, int type,void* &var){
	bool error = false;
	string str1;
	char remove = '/';
	char* ctmp;
	int i=0;
	//cout<<"In string data: "<<str<<endl;
	int ival; short sval; float fval; long lval; double dval;  
	switch(type){
					case INTTYPE:	//cout<<"Converting from string: "<<str<<" to int: ";
								ival = atoi((str).c_str());
								//cout<<ival<<endl;
								if(ival == 0) error = true;
								var = (void*)new int(ival);
								break;
					case SHORTTYPE://	cout<<"Converting from string: "<<str<<" to short: ";
								sval = (short)atoi((str).c_str());
								//cout<<sval<<endl;
								
								if(sval == 0) error = true;
								var = (void*)new short(sval);
								break;

					case DATETYPE: //cout<<"Converting from string: "<<str<<" to Date: ";
								str1 = str;
								for(string::iterator it = str1.begin(); it!=str1.end();){
							     	 if( (*it) == remove )
							     	    it = str1.erase( it );
								      else
								      it++;
								}
								ival = atoi(str1.c_str());
								//cout<<ival<<endl;
								if(ival == 0) error = true;
								var = (void*)new int(ival); 
								break;
							
			     	case FLOATTYPE: //cout<<"Converting from string: "<<str<<" to Float: "; 
			     				fval = (float)atof((str).c_str());
	         						//cout<<fval<<endl;
								if(fval == 0) error = true;
								var = (void*)new float(fval);//(void*)ftmp;
								//values[i] = fval;
								break;
							
					case LONGTYPE: //cout<<"Converting from string: "<<str<<" to Long: ";
								lval = atol((str).c_str());
								//cout<<lval<<endl;
								if(lval == 0) error = true;
								var = (void*)new long(lval);
								break;
														
					case DOUBLETYPE: //cout<<"Converting from string "<<str<<" to Double: ";
								  dval = strtod((str).c_str(), NULL);
								  //cout<<dval<<endl;
								  if(dval == 0) error = true;
								  var = (void*)new double(dval);
								  break;
							
					case VARCHARTYPE:  //cout<<"string item:"<<endl;
								i=0;
								var = (char *)((str).c_str());
								break;		
							
				}
				return error;
}

bool unpack(vector<string> &datavalues,vector<int> types, const char* buf, int size){
	bool error = true;
	int *ival = NULL; float *fval = NULL; long *lval = NULL; double *dval = NULL; int *date = NULL; char* str = NULL;short* sval = NULL;
	int pos = 0;
	string strtmp;

	int itmp = 0;
	int ivalue; float fvalue; long lvalue; double dvalue; int datevalue; short svalue;
	for(int i=0;i<types.size();i++){	
		stringstream out("");
		
			   switch( types[i] ){
			
				case INTTYPE:  ival = new int;
							memcpy(ival,&buf[pos],INT_SIZE);
							pos+=INT_SIZE;
							//cout<<"Int value: "<<*ival<<endl;
							out << *ival;
							strtmp = out.str();
							//out.flush();
							//out("");
							datavalues.push_back(strtmp);
							delete ival;
							break;
				case FLOATTYPE:fval = new float;
							memcpy(fval,&buf[pos],FLOAT_SIZE);
							pos+=FLOAT_SIZE;
							//cout<<"Float value: "<<*fval<<endl;
							out << *fval;
							strtmp = out.str();
							out.flush();
							//out.("");
							datavalues.push_back(strtmp);							
							delete fval;
							break;
				case SHORTTYPE:	sval = new short;
							memcpy(sval,&buf[pos],SHORT_SIZE);
							pos+=SHORT_SIZE;
							//cout<<"Short value: "<<*sval<<endl;
							out << *sval;
							strtmp = out.str();
							//out.flush();
							//out.("");
							datavalues.push_back(strtmp);							
							delete sval;
							break;
				case LONGTYPE:	lval = new long;
							memcpy(lval,&buf[pos],LONG_SIZE);
							pos+=LONG_SIZE;
							//cout<<"Long value: "<<*lval<<endl;
							out << *lval;
							strtmp = out.str();
							out.flush();
							datavalues.push_back(strtmp);
							delete lval;
							break;
				case DOUBLETYPE: dval = new double;
							memcpy(dval,&buf[pos],DOUBLE_SIZE);
							pos+=DOUBLE_SIZE;							
							//cout<<"Double value: "<<*dval<<endl;
							out << *dval;
							strtmp = out.str();
							out.flush();
							datavalues.push_back(strtmp);
							delete dval;
							break;
				case VARCHARTYPE: str = NULL;
							itmp=pos;
							while(buf[itmp] != -57 && itmp < size)
								itmp++;
							str = new char[itmp-pos+1];
							memcpy(str,&buf[pos],(itmp-pos));
							str[itmp-pos] = '\0';
							pos=(itmp + 1);
							cout<<"String value: "<<str<<" Nxt item pos: "<< pos<<endl;
							strtmp = str;
							datavalues.push_back(strtmp);
							delete[] str;
							break;
				case DATETYPE: date = new int;
							memcpy(date,&buf[pos],INT_SIZE);
							pos+=INT_SIZE;
							//cout<<"Date value: "<<*date<<endl;
							out << *date;
							strtmp = out.str();
							out.flush();
							datavalues.push_back(strtmp);
							delete date;
							break;
				} 	
			}
}

bool pack_data(parsedQuery* pq, char* &buf, int &set_size){

	int i = 0;
	int size = 0;
	int var_size = 0;
	int *ival = NULL; float *fval  = NULL; long *lval = NULL; double *dval = NULL; int *date = NULL; char* str = NULL;short* sval = NULL;
	vector<string> values = pq->getDataValues();
	char* cstr;
	string strtmp;
	char u = -57; //delimiter for varchar
	vector<int> types = pq->getTypes();
	for(int i=0;i<pq->noOfColumns;i++){	
		switch(types[i]){
			case INTTYPE: size += INT_SIZE;
						break;
			case FLOATTYPE:size += FLOAT_SIZE;
						break;
			case SHORTTYPE:size += SHORT_SIZE;
						break;
			case LONGTYPE:	size += LONG_SIZE;
						break;
			case DOUBLETYPE:size += DOUBLE_SIZE;
						break;
			case VARCHARTYPE: strtmp = values[i];
						size += (strtmp.length() + 1);
						break;
			case DATETYPE: size += DATE_SIZE;
						break;			
		}
	}	
	//buf = (char*)calloc(size + 1,sizeof(char));
	cout<<"Buf Size: "<<size<<endl;
	buf = new char[size];
	int pos = 0;
	void* _value;
	bool error = false;
	for(int i=0;i<pq->noOfColumns;i++){	
		cout<<"Data "<<i+1<<". "<<values[i]<<endl;
		switch( types[i] ){
			
			case INTTYPE:  if(!convert_to_type(values[i],INTTYPE,_value)){
						ival = (int *)_value;
						memcpy(&buf[pos], ival, INT_SIZE);
						pos+=INT_SIZE;
						cout<<"Pos: "<< pos <<endl;
						}
						error = true;						
						//buf = memcpy(buf, ival, INT_SIZE);
						break;
			case FLOATTYPE:if(!convert_to_type(values[i],FLOATTYPE,_value)){
						fval = (float *)_value;
						memcpy(&buf[pos], fval, FLOAT_SIZE);
						pos+=FLOAT_SIZE;
						cout<<"Pos: "<< pos <<endl;
						}
						error = true;												
						//buf = memcpy(buf, fval, FLOAT_SIZE);
						break;
			case SHORTTYPE:if(!convert_to_type(values[i],SHORTTYPE,_value)){
						sval = (short *)_value;
						memcpy(&buf[pos], sval, SHORT_SIZE);
						pos+=SHORT_SIZE;
						cout<<"Pos: "<< pos <<endl;	
						}
						error = true;
						//buf = memcpy(buf, sval, SHORT_SIZE);
						break;
			case LONGTYPE:	if(!convert_to_type(values[i],LONGTYPE,_value)){
						lval = (long *)_value;
						memcpy(&buf[pos], lval, LONG_SIZE);
						pos+=LONG_SIZE;
						cout<<"Pos: "<< pos <<endl;
						}
						error = true;
						//buf = memcpy(buf, lval, LONG_SIZE);
						break;
			case DOUBLETYPE: if(!convert_to_type(values[i],DOUBLETYPE,_value)){
						dval = (double *)_value;
						memcpy(&buf[pos], dval, DOUBLE_SIZE);
						pos+=DOUBLE_SIZE;
						cout<<"Pos: "<< pos <<endl;	
						}
						error = true;				
						break;
			case VARCHARTYPE:if(!convert_to_type(values[i],VARCHARTYPE,_value)){
						strtmp = (char *)_value;
						strtmp = strtmp + u ;
						cout<<"String packed: "<<strtmp<<" Size: "<<strtmp.length()<<endl;
						memcpy(&buf[pos], strtmp.c_str(), strtmp.length() );
						pos+= (strtmp.length()  );
						cout<<"Pos: "<< pos <<endl;
						strtmp = "";
						}
						error = true;
						break;
			case DATETYPE: if(!convert_to_type(values[i],DATETYPE,_value)){
						date = (int *)_value;
						memcpy(&buf[pos], date, DATE_SIZE);
						pos+=DATE_SIZE;
						cout<<"Pos: "<< pos <<endl;
						}
						error = true;
						break;
			}		
		}
	
	set_size = size ;
	//buf[pos+1] = '\0';
	return error;		

}


#endif
