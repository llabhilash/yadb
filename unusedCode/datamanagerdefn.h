using namespace std;

#ifndef DATAMANAGER_DEFN_H
#define DATAMANAGER_DEFN_H

//=======================================================================
DataManager::DataManager(){}
//=======================================================================
DataManager::~DataManager(){}
//=======================================================================
bool DataManager::init(int ramSizeToUseInBytes){
		
	if(!bm.init(ramSizeToUseInBytes)){
		cout << "Cannot initialize buffer manager\n";
		return false;
	}
	
	return true;
}
//=======================================================================
bool DataManager::shutdown(){
	return bm.shutdown();
}
//=======================================================================

bool DataManager::getFreePage(char* &pageBuf, int& pageNum, DBHeader& dbheader){
	
	int err = NO_ERROR;
	char *freePageBuf = new char[PAGE_SIZE_IN_BYTES];
	FreePage *freePage = NULL;
	int nextFreePage;
	DM_FUNC_TRACE && cout<<"DM.getFreePage()\n";
	string dummy;
	
	//check if dbheader has free page pointer == -1		
	if(dbheader.getFreePagePointer() == -1){
	
		//expand heapfile
		
		if(!bm.expandHeapFile(dbheader.getLastPage())){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
				
		//set current Last Page + 1 as free page
		if(!dbheader.setFreePagePointer(dbheader.getLastPage() + 1)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		
		//increment noOfPages by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setNoOfPages(dbheader.getNoOfPages() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		//increment LastPage by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setLastPage(dbheader.getLastPage() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}	
		
	}
	
	//read free page pointer into buffer pool
	pageNum = dbheader.getFreePagePointer();
	if(!bm.read(pageNum, freePageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	freePage = new FreePage(freePageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);
	
	DM_FUNC_TRACE && cout << "Setting new free page pointer in DBHeader : " << nextFreePage << endl;
	//cin >> dummy;
	
	if(!bm.read(pageNum, pageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}	
	/*//Reset the free page int the dbheader......
	freePage = new FreePage(pageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);*/
	ret: 

	DM_FUNC_TRACE && printErrorCode(err);
	DM_FUNC_TRACE && cout << "Sending AS FreePage : " << pageNum << endl;
	return err==NO_ERROR?true:false;
	
}
//=======================================================================
bool DataManager::handleShowSchemas(){

	int err = NO_ERROR;
	DM_FUNC_TRACE && cout<<"DM.handleShowSchemas()\n";

	vector<string> schemas;

	//find all directories in FILENAME_PATH and add them to schemas
	DIR *dp;
	struct dirent *dirp;
	
	if((dp  = opendir(FILENAME_PATH)) == NULL) {
        	cout << "Error(" << errno << ") opening " << FILENAME_PATH << endl;
        	err = READ_ERROR;
        	goto ret;
    	}

    	while ((dirp = readdir(dp)) != NULL) {
    		
    		//skip .,..,.svn
    		if(string(dirp->d_name).compare(".") == 0 || string(dirp->d_name).compare("..") == 0 || string(dirp->d_name).at(0) == '.'){
    			continue;
    		}
    	
        	schemas.push_back(string(dirp->d_name));
        	cout << dirp->d_name<<"\n";
    	}
    	
   	closedir(dp);
   	
   	ret: 
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleCreateTable(parsedQuery &pq){

	int err = NO_ERROR;
	RID rid;
	
	DM_FUNC_TRACE && cout << "DM.handleCreateTable()\n";
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	//create sys_table record for tablename
	SysTableRecord sysTabRec, tempSysTabRec;
	sprintf(sysTabRec.tableName,"%s",pq.getTable().c_str());	//Add Table name
	sysTabRec.totalRows = 0;					//Intially there are 0 rows
	//sysTabRec.createTime;
	//sysTabRec.updateTime;
	sysTabRec.startPage = -1;//Page where the first DP for this table is
	sysTabRec.totalDP = 1;						//There are no DPs yet
	sysTabRec.totalColumns = pq.getNoOfColumns();			//Total number of columns for this table
	
	vector<string> columns;		//COLUMNS captured
	vector<int> types;			//COLUMN DATATYPES captured
	vector<bool> notNulls;		//COLUMN is NULL ?
	vector<bool> pks;				//COLUMN is PrimaryKey ?
	vector<bool> unique;			//COLUMN is Unique ?
	vector<int> maxsizes;			//COLUMN Max Size
	vector<defaultValue> defaultValues = (vector<defaultValue>)pq.getDefaultValues();
	SysColumnRecord* sysColRecs = NULL;
	DBHeader* dbHeader = NULL;
	DirectoryPage* dirpage = NULL;
	
	char* rec = NULL;
	int recSize;
	
	RID tempRID;
	
	//DUMP
	//sysTabRec.dump();
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	
	//get sys tab record for the table
	if(getSysTabRecForTable(pq.getTable(), tempSysTabRec, (*dbHeader), tempRID)){
		//Table already exists
		err = INVALIDTABLENAME;
		cout << "Table '" << pq.getTable() << "' already exists\n";
		goto ret;
	}
	
	//check if column name is duplicate
	columns = pq.getColumns();		//COLUMNS captured
	for(int i = 0; i < pq.getNoOfColumns();i++){
		for(int j=i+1; j < pq.getNoOfColumns(); j++){
			//check current col name all prev col names
			if(columns[i].compare(columns[j]) == 0){
				err = INVALID;
				cout << "Column names for a table have to be unique\n";
				goto ret;
			}
		}
	}
	
	//create new DirPage for this table
	if(!getFreePage(dirpagebuf, sysTabRec.startPage, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(dirpage != NULL)
		delete dirpage;
	dirpage = new DirectoryPage(dirpagebuf);
	dirpage->init();
	
	//write back to BM
	if(!bm.write(sysTabRec.startPage, dirpagebuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	//convert systabrec
	if(!convertSysTabRecToRecord(sysTabRec, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	
	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemTablePointer(), (*dbHeader), rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	delete[] rec;
	
	//create sys_col records for each column
	sysColRecs = new SysColumnRecord[pq.getNoOfColumns()];		//Create as many sys col records as columns
	
	columns = pq.getColumns();		//COLUMNS captured
	types = pq.getTypes();			//COLUMN DATATYPES captured
	notNulls = pq.getNotNulls();		//COLUMN is NULL ?
	pks = pq.getPKs();				//COLUMN is PrimaryKey ?
	unique = pq.getUnique();			//COLUMN is Unique ?
	maxsizes = pq.getMaxSize();			//COLUMN Max Size
	
	for(int i=0; i < pq.getNoOfColumns(); i++){
		sprintf(sysColRecs[i].columnName,"%s",columns[i].c_str());		//Add column name
		sprintf(sysColRecs[i].tableName,"%s",pq.getTable().c_str());		//Add table name
		sysColRecs[i].colPos = i;						//Column order in table
		sysColRecs[i].dataType = types[i];					//Column data type
		sysColRecs[i].isNotNull = notNulls[i];					//Column is Nullable ?
		sysColRecs[i].isPrimary = pks[i];					//Column is Primary ?		
		sysColRecs[i].isUnique = unique[i];					//Column is Unique ?
		sysColRecs[i].isDeleted = false;					//Column is Deleted ?
		sysColRecs[i].maxSize	= maxsizes[i];					//max size for the Column
		if(defaultValues[i].isDefault)
			strcpy(sysColRecs[i].defaultvalue,(defaultValues[i].value).c_str());
		else
			strcpy(sysColRecs[i].defaultvalue,"NULL");						//Default Value is NULL if no user defined value
		
		//check for constraint in compatibility
		//if col is primary, then set unique and not null to true
		if(pks[i]){
			sysColRecs[i].isNotNull = true;
			sysColRecs[i].isUnique = true;
		}
		
		//sysColRecs[i].dump();
		
		if(!convertSysColRecToRecord(sysColRecs[i], rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		
             	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }

		delete[] rec;
		
/*             	if(!insertRecordIntoDataPage((char *)&sysColRecs[i],sizeof(SysColumnRecord),
                	dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }
*/	     
	}
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	delete[] sysColRecs;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleUseSchema(parsedQuery &pq){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleUseSchema()\n";
	
	if(!bm.setSchemaFile((char *)pq.getDatabase().c_str())){
		err = USE_SCHEMA_ERROR;
		goto ret;
	}
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleShowTables(parsedQuery &){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowTables()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	cout << endl;
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysTabRec(systabrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}

		//systabrec.dump();
		cout << endl << systabrec.tableName << endl;
	}
	cout << endl;

	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================
bool DataManager::handleCreateIndex(parsedQuery &pq){
	
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleCreateIndex()\n";
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* indexbuf = new char[PAGE_SIZE_IN_BYTES];
	char* leafbuf = new char[PAGE_SIZE_IN_BYTES];
	char* rec= NULL;
	int recSize;
	int keySize = 0;
	
	DBHeader* dbHeader = NULL;
	SysTableRecord sysTabRec;
	RID systabRID, dummyRID;
	
	vector<SysColumnRecord> sysColumns;
	vector<RID> sysColRIDs;
	vector<int> colTypes;
	char* record = NULL;
	int size;

	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> recAttributes;

	IndexPage* indexpage = NULL;
	LeafPage* leafpage = NULL;
	
	
	vector<string> indexCols = pq.getColumns();
	vector<int> indexKeyTypes;
	vector<string> key;
	int count = 0, indexpageno, leafpageno;
	SysIndexRecord tempsysindexrec;
	vector<SysIndexRecord> sysIndexRecs;
	vector<int> sysColOrigPos;
	vector<RID> sysIndexRIDs;
	
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbHeader), systabRID)){
		//Table already exists
		err = INVALIDTABLENAME;
		cout << "Table '" << pq.getTable() << "' does not exist\n";
		goto ret;
	}	
		
	//get sys col records for the table
	if(!getSysColRecsForTable(pq.getTable(), sysColumns, (*dbHeader), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

	for(int i=0; i<sysColumns.size(); i++){
		colTypes.push_back(sysColumns[i].dataType);	
	}
	
	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	
	//check if index has duplicate columns
	for(int i=0; i<indexCols.size(); i++){
		for(int j=i+1; j<indexCols.size(); j++){
			if(indexCols[i].compare(indexCols[j]) == 0){
				err = INVALID;
				cout << "Column list must be unique\n";
				goto ret;
			}
		}	
	}
	
	
	
	//check column name validity
	for(int i = 0; i < indexCols.size(); i++){		
			
		for(int j=0; j<sysColumns.size(); j++){
			
			if(sysColumns[j].isDeleted)
			continue;
		
			if(indexCols[i].compare(to_string(sysColumns[j].columnName)) == 0){
				count++;
				indexKeyTypes.push_back(sysColumns[j].dataType);
				//store orignal position
				sysColOrigPos.push_back(sysColumns[j].colPos);
				//add to sysColumns[j].maxSize to keySize
				keySize = keySize + sysColumns[j].maxSize;
				break;				
			}
		}
	}
	
	//if count != indexCols.size().. it means not all columns in column list is valid
	if(count != indexCols.size()){
		err = INVALID;
		cout << "INVALID Column(s) in column list\n";
		goto ret;
	}
	
	//check if index name exists
	for(int i=0; i<sysIndexRecs.size(); i++){
		if(pq.getIndexName().compare(to_string(sysIndexRecs[i].indexName)) == 0){
			//duplicate index
			err = 	INVALID;
			cout << "Index '" << pq.getIndexName() << "' already exists on table '" << pq.getTable() << "'\n";
			goto ret;
		}
	}
	
	//compute actual keySize.
	keySize = keySize + ((indexCols.size() + 1)*sizeof(short));
	//create dummy pages for new index	
	if(!getFreePage(indexbuf, indexpageno, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	//create dummy pages for new leaf ( ROOT )	
	if(!getFreePage(leafbuf, leafpageno, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	indexpage = new IndexPage(indexbuf);
	indexpage->setPageType(INDEXHEADER);
	indexpage->setMaximumNodeEntries(4);
	indexpage->setKeySize(keySize);
	indexpage->setIndexerRoot(leafpageno);
	DM_FUNC_TRACE && indexpage->dump();
	
	leafpage = new LeafPage(leafbuf);
	leafpage->setPageType(LEAFNODE);
	leafpage->setMaximumNodeEntries(4);
	leafpage->setTotalKeysOccupied(0);
	leafpage->setKeySize(keySize);
	leafpage->setNextLeaf(-1);
	leafpage->setPrevLeaf(-1);
	DM_FUNC_TRACE && leafpage->dump(indexKeyTypes);
	
	//write back leafpage to BM.
	if(!bm.write(leafpageno, leafbuf, INDEX_ROOT_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	//write back index to BM
	if(!bm.write(indexpageno, indexbuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	
	//create sysindexrec and insert
	sprintf(tempsysindexrec.indexName,"%s",pq.getIndexName().c_str());
	sprintf(tempsysindexrec.tableName,"%s",pq.getTable().c_str());
	tempsysindexrec.startPage = indexpageno;
	tempsysindexrec.totalIndexCols = indexCols.size();
	for(int i=0; i<indexCols.size(); i++){
		tempsysindexrec.colOrigPos = sysColOrigPos[i];
		tempsysindexrec.colIndexPos = i;
		
		//convert index-rec to rec
		if(!convertSysIndexRecToRecord(tempsysindexrec, rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}	
		
             	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemIndexPointer(), (*dbHeader), dummyRID)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }

		delete[] rec;
				
	}

	
	//get all records and insert them into this index
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}

	//insert every record to index
	for(int i=0; i<allrecs.size(); i++){

		recAttributes.clear();

		//unpack the record
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}
		

		//construct key
		key = vector<string> (indexCols.size(),""); // dummy initial values for both key and key types
		

		DM_FUNC_TRACE && cout << "Inserting Key : ( ";
		for(int j=0; j<indexCols.size(); j++){
		
			//write the attribute at colOrigPos to the key in the colIndexPos
			key[j] = recAttributes[sysColOrigPos[j]];
			DM_FUNC_TRACE && cout << key[j] << " , ";
		}
		DM_FUNC_TRACE && cout << " )\n";
		
		//now the key for index is built..  pack it and insert
		
		//pack the key
		if(record != NULL)
			delete[] record;
		if(!pack::pack_data(key, record, size, indexKeyTypes)){
			err = PACK_ERROR;
			goto ret;
		}
		
		//insert the key
		if(!handleIndexInsert(indexpageno, indexKeyTypes, record, allRIDs[i], (*dbHeader))){
			err = RECORD_INSERT_ERROR;
			goto ret;
		}
	}

	
	//writeback header
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(dbHeader != NULL)
		delete dbHeader;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;	
	if(indexbuf != NULL)
		delete[] indexbuf;
	if(leafbuf != NULL)
		delete[] leafbuf;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================
bool DataManager::handleCreateSchema(parsedQuery &pq){

	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleCreateSchema()\n";

	char *filename = new char[pq.getDatabase().length()];
	char* rec= NULL;
	char null[] = "NULL";
	int recSize;
	strcpy(filename,pq.getDatabase().c_str());
        FILE *fp;
	settings::init();
	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);
	//cout<<filewithpath<<endl;
	if((fp = fopen(filewithpath,"r")) != NULL){
		cout<<"Can't create database '"<<filename<<"'; database exists"<<endl;
		fclose(fp);
		return false;
	}
        if((fp = fopen(filewithpath,"w")) == NULL){
	        cout<<"Can't create database '"<<filename<<"';database exists"<<endl;
	        return false;
        }
        fclose(fp);
        char *data = new char[PAGE_SIZE_IN_BYTES];
        DBHeader header = DBHeader(data);
        header.setNoOfPages(6);
        header.setLastPage(SYS_INDEX_START_PAGE_NUM + 2);
	header.setFreePagePointer(-1);
	header.setSystemTablePointer(SYS_TAB_START_PAGE_NUM);
	header.setSystemColumnPointer(SYS_COL_START_PAGE_NUM);	
	header.setSystemIndexPointer(SYS_INDEX_START_PAGE_NUM);			
        BufferManager Buff;
        Buff.init(100000);
        Buff.setSchemaFile(filename);
        Buff.write(DB_HEADER_PAGE_NUM,data,DB_HEADER_PRIORITY);
        delete[] data;
        data = new char[PAGE_SIZE_IN_BYTES];
        DirectoryPage dirPage = DirectoryPage(data);
        dirPage.init();
	DirectoryEntry de,de1;
	
	char *sysData = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage dbp = DataBasePage(sysData);
	SysTableRecord sysTab;
	RID rid;
	rid.pageNo = 4;
	//SYS_TABLE
	sprintf(sysTab.tableName,"%s","SYS_TABLE");
	sysTab.totalRows = 3;
	sysTab.startPage = SYS_TAB_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 5;
	dbp.init();
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	//SYS_COLUMN
	sprintf(sysTab.tableName,"%s","SYS_COLUMN");
	sysTab.totalRows = 19;
	sysTab.startPage = SYS_COL_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 10;	
	
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	
	//SYS_INDEX
	sprintf(sysTab.tableName,"%s","SYS_INDEX");
	sysTab.totalRows = 0;
	sysTab.startPage = SYS_INDEX_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 4;	
	
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	
	de.pageNo = SYS_INDEX_START_PAGE_NUM + 1;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_TAB_START_PAGE_NUM,data,DIR_PAGE_PRIORITY);
	Buff.write(de.pageNo,sysData,DB_PAGE_PRIORITY);
	
	
	//write dummy sys_index dirpage
	delete[] data;
	data = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
	dirPage.init();
	//write back to BM
	if(!Buff.write(SYS_INDEX_START_PAGE_NUM, data, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	delete[] data;
	delete[] sysData;
	data = new char[PAGE_SIZE_IN_BYTES];
	sysData = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
        dirPage.init();
	dbp = DataBasePage(sysData);
	SysColumnRecord sysCol;
	rid.pageNo = SYS_INDEX_START_PAGE_NUM + 2;
	//SYS_TABLE->tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 0;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = VARCHARTYPE;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	dbp.init();
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_TABLE->totalRows
	sprintf(sysCol.columnName,"%s","totalRows");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 1;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	////SYS_TABLE->createTime
	/*
        sprintf(sysCol.columnName,"%s","createTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->updateTime
	sprintf(sysCol.columnName,"%s","updateTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);*/
	//SYS_TABLE->startPage.
	
	sprintf(sysCol.columnName,"%s","startpage");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_TABLE->totalDP
	sprintf(sysCol.columnName,"%s","totalDP");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_TABLE->totalColumns
	sprintf(sysCol.columnName,"%s","totalColumns");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 4;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> COLUMN_NAME
	sprintf(sysCol.columnName,"%s","columnName");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_COLUMNNAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> TABLE_NAME
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> COL_POS
	sprintf(sysCol.columnName,"%s","colPos");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> dataType
	sprintf(sysCol.columnName,"%s","dataType");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 3;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_NULLABLE
	sprintf(sysCol.columnName,"%s","isNotNull");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 4;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_PRIMARY
	sprintf(sysCol.columnName,"%s","isPrimary");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 5;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_UNIQUE
	sprintf(sysCol.columnName,"%s","isUnique");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 6;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_COLUMN -> IS_DELETED
	sprintf(sysCol.columnName,"%s","isDeleted");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 7;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> MAX_SIZE
	sprintf(sysCol.columnName,"%s","maxSize");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 8;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> DEFAULT_VALUE
	sprintf(sysCol.columnName,"%s","defaultValue");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 9;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_DATATYPE_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	
	//SYS_INDEX -> indexName
	sprintf(sysCol.columnName,"%s","indexName");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> colOrigPos
	sprintf(sysCol.columnName,"%s","colOrigPos");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> colIndexPos
	sprintf(sysCol.columnName,"%s","colIndexPos");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 3;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> startPage
	sprintf(sysCol.columnName,"%s","startPage");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 4;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
        
	de.pageNo = SYS_INDEX_START_PAGE_NUM + 2;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_COL_START_PAGE_NUM,data,SYS_PRIORITY);
	Buff.write(de.pageNo,sysData,DB_PAGE_PRIORITY);
	
	Buff.shutdown();
	
	
        cout<<"Schema '"<<filename<<"' created successfully"<<endl;
        
        ret:
        delete[] filename;
        delete[] sysData;
        delete[] data;        
        
        DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}

//=======================================================================
bool DataManager::handleShowColumns(parsedQuery &){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowColumns()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysColRec(syscolrec, (char*)allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		cout << endl;
		syscolrec.dump();
	}

	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}

//=======================================================================
bool DataManager::getSysTabRecForTable(string tabName, SysTableRecord &sysrec, DBHeader &dbHeader, RID &rid){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysTabRecForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and check
		//memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysTabRec(systabrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}

		if(tabName.compare(systabrec.tableName) == 0){
			sysrec = systabrec;
			rid = allRIDs[i];
			goto ret;
		}
	}

	//no table found
	err = INVALIDTABLENAME;
	cout << "Table '" << tabName << "' does not exist" << endl;

	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================
bool DataManager::getSysColRecsForTable(string tabName,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns, vector<RID> &sysColRIDs){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysColRecsForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	//RID tempRID;

	bool swapped = false;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec,syscolrec1;
	SysColumnRecord* tempSysCol = NULL; 
	
	int i=0, n=0;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		
		tempSysCol = new SysColumnRecord;
		if(!convertRecordToSysColRec((*tempSysCol), (char*)allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		//cout << endl;
		//syscolrec.dump();
		if(tabName.compare((*tempSysCol).tableName) == 0){
			sysColumns.push_back((*tempSysCol));
			sysColRIDs.push_back(allRIDs[i]);
		}
	}

	/*procedure bubbleSort( A : list of sortable items ) defined as:
  n := length( A )
  do
    swapped := false
    n := n - 1
    for each i in 0 to n  do:
      if A[ i ] > A[ i + 1 ] then
        swap( A[ i ], A[ i + 1 ] )
        swapped := true
      end if
    end for
  while swapped
end procedure

*/
        n = sysColumns.size();
	do{
		swapped = false;
		n = n-1;
		for(i = 0; i < n ; i++){
			syscolrec = sysColumns[i];
			syscolrec1 = sysColumns[i + 1];
			if(syscolrec.colPos > syscolrec1.colPos){				
				//swap			
				swap(sysColumns[i],sysColumns[i + 1]);				
				swap(sysColRIDs[i],sysColRIDs[i+1]);				
				swapped = true;
			}
		}
	}while(swapped);


	ret:
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

bool DataManager::getSysIndexRecsForTable(string tabName,vector<SysIndexRecord> &sysIndexRecs,DBHeader &dbHeader, vector<RID> &sysIndexRIDs){
	
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysIndexRecsForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;

	SysIndexRecord sysindexrec;
	
	int i=0;

	if(!getAllRecords(dbHeader.getSystemIndexPointer(), allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}
	
	for(i = 0; i<allrecs.size(); i++){
	
		//convert each rec to sysindex rec
		//check if table name is same.. then add to sysindexrecs
		if(!convertRecordToSysIndexRec(sysindexrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if(tabName.compare(to_string(sysindexrec.tableName)) == 0){
			//add sysindex rec
			sysIndexRecs.push_back(sysindexrec);
			sysIndexRIDs.push_back(allRIDs[i]);
		}
	
	}
	
	ret:
	allRIDs.clear();
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}



//=======================================================================
bool DataManager::handleSelect(parsedQuery &pq){

	DM_FUNC_TRACE && cout << "DM.handleSelect()\n";
	int err = NO_ERROR;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	
	vector<int> colTypes;
	string str;
	
	vector<string> condition_array = (vector<string>)pq.getConditionArray();
	vector<string> colNames = (vector<string>)pq.getColumns();
	vector<bool>	displayCol; // contains the col indexs of the columns to display
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	RID rid;
	
	
	string displayHead, displayRecord;
	
	vector<string*> finalRecords;
	string* strptr = NULL;
	int* getAttrIndex;
	int i=0, j=0;
	int colsToAdd;
	char* recbuf = NULL;
	vector<string> recAttributes;
	vector<string> finalRecAttributes;
	vector<int> displayIndex; //Required for the order in which the records must be diplayed
	vector<int> maxSizes; //Max Sizes of Data values of each of the columns
	int var_col = 0;
	void* att_value = NULL;
	bool RecPassedCond = false;
	bool condition = false;
	bool condition_mismatch = false;
	bool columnexists;
	bool displayAll;
	
	double dval1,dval2;
	double* result = NULL;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	
	for(i=0; i<sysColRecs.size(); i++){
		displayCol.push_back(false);
		colTypes.push_back(sysColRecs[i].dataType);
		maxSizes.push_back(5);
		if(strlen(sysColRecs[i].columnName)>maxSizes[i])
			maxSizes[i] = strlen(sysColRecs[i].columnName) ;
		displayHead = "";
		displayHead += "(" + sysColRecs[i].getTypeName(sysColRecs[i].dataType) + ")";
		if(displayHead.size() >maxSizes[i])
				maxSizes[i] = displayHead.size();
	}
	
	displayAll = false;
	//populate column types
	for(i=0; i<colNames.size(); i++){
		//cout<<"entered1"<<endl;
		if(displayAll)
		break;
		
		if(!colNames[i].compare("*")){
			//cout<<"entered2"<<endl;
			displayAll = true;
		}

		columnexists = false;
		for(j=0;j<sysColRecs.size();j++){

			if( (displayAll || !colNames[i].compare(sysColRecs[j].columnName)) && !sysColRecs[j].isDeleted){
				//cout<<"entered3"<<endl;
				columnexists = true;
				displayCol[j] = true;
			}
		}
		if(!columnexists){
			err = COLUMN_NOT_DEFINED;
			goto ret;
		}
	}

	//cout<<"Getting All Records by table scan"<<endl;	
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}

	//Where Condition not entered, Just Display the Values
	//if(!pq.conditionExists){
	
	//Display only those columns whch are specified
	
	//cout<<"Unpacking all Records and filling finalRecords"<<endl;
	for(j=0; j<allrecs.size(); j++){
		
		recAttributes.clear();
		finalRecAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[j].c_str(), recsizes[j])){
			err = UNPACK_ERROR;
			goto ret;
		}
		//columns to add
		colsToAdd = sysColRecs.size() - recAttributes.size();
		strptr = (string*)new string[sysColRecs.size()];

		for(i=0;i<recAttributes.size();i++){
			
			if(recAttributes[i].size()+2 > maxSizes[i])
			maxSizes[i] = recAttributes[i].size()+2;
			strptr[i] = recAttributes[i];
			finalRecAttributes.push_back(recAttributes[i]);
		}

		displayRecord.clear();
		displayRecord = "| ";
		
		if(colsToAdd > 0){
		
			//cout<<"Adding values to finalRecords for the deleted columns and column with default values"<<endl;
			//Add values to final records for  the deleted columns and column with default values
			for(i=recAttributes.size();i<sysColRecs.size();i++){
			
				if(sysColRecs[i].isDeleted){
					strptr[i] = "NULL";
					finalRecAttributes.push_back("NULL");
					continue;
				}
				strptr[i] = sysColRecs[i].defaultvalue;
				if(strlen(sysColRecs[i].defaultvalue)+2 > maxSizes[i])
				maxSizes[i] = strlen(sysColRecs[i].defaultvalue)+2;
				finalRecAttributes.push_back(sysColRecs[i].defaultvalue);
			}
		}
		finalRecords.push_back(strptr);
		err = evaluate_expression(pq.getConditionArray(),finalRecAttributes,sysColRecs,condition);
		if(err != NO_ERROR )
		{
			goto ret;
		}
		if( condition ){
			displayIndex.push_back(j);
		}
	}
	
	//cout<<"Setting finalRecords to diplay"<<endl;
	for(j=0; j<sysColRecs.size(); j++){
		if(displayCol[j]){
			cout<<setw(maxSizes[j]+1)<<sysColRecs[j].columnName;
			cout<<setw(0);
		}
	}
	cout << endl;

	for(j=0; j<sysColRecs.size(); j++){
		displayHead = "";
		if(displayCol[j]){
			displayHead += "(" + sysColRecs[j].getTypeName(sysColRecs[j].dataType) + ")";
			cout<<setw(maxSizes[j]+1)<<displayHead;
			cout<<setw(0);
		}
	}
	cout << endl;

	for( j=0;j<displayIndex.size();j++){
	
		recAttributes.clear();
		strptr = finalRecords[displayIndex[j]];
		for(i=0;i<sysColRecs.size();i++){
		
			if(displayCol[i]){
			cout<<"|";
			cout<<setw(maxSizes[i] )<<strptr[i];
			}
		}
		cout<<endl;
	}
	
	cout << "\nNumber of Rows in Result Set : " << displayIndex.size() << endl;
	//}
	
	for(i=0;i<finalRecords.size();i++){
		strptr = finalRecords[i];
		delete[] strptr;
	}
	
	finalRecords.clear();
	
	ret:
	if(dbhead != NULL)
		delete dbhead;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//=======================================================================
bool DataManager::getAllRecords(int startDirPageNo, vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs){
	
	DM_FUNC_TRACE && cout << "DM.getAllRecords()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	char* recbuf = NULL;	
	int recSize;
	
	string* tempstring = NULL;
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID curRID,nextRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == -1){
				//empty DE - goto next DE
				continue;
			}
			
			//read page in DE into BufferPool
			if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			
			if(DataPage != NULL){
				delete DataPage;
			}
			DataPage = new DataBasePage(datapagebuf);
			
			
			curRID.pageNo = curDataPageNo;
			//read first sys-col RID
			if(!DataPage->firstRecord(curRID)){
				//empty DataBasePage.. goto next page
				continue;
			}
			
			nextRID.pageNo = curRID.pageNo;
			nextRID.slotNo = curRID.slotNo;
			
			//read records
			do{			
				//RECORD LEVEL
				
				curRID.pageNo = nextRID.pageNo;
				curRID.slotNo = nextRID.slotNo;
				
				if(recbuf != NULL){
					delete[] recbuf;
				}
				
				if(!DataPage->getRecord(curRID,recbuf,recSize)){
					//read error
					err = RECORD_READ_ERROR;
					goto ret;
				}			
				
				//memcopy buf into systabrec
				//memcpy((char*)&syscolrec,recbuf,recSize);
				
				//add record to allrecs
				if(tempstring != NULL)
					delete tempstring;
				tempstring = new string(recbuf,recSize);
				allrecs.push_back((*tempstring));				
				recsizes.push_back(recSize);
				allRIDs.push_back(curRID);
			
			}while(DataPage->nextRecord(curRID, nextRID));

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(recbuf != NULL){
		delete[] recbuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	if(tempstring != NULL)
		delete tempstring;	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================

bool DataManager::updateRecordFromDataPage(char* rec, int reclen, RID &rid, int startPage, DBHeader & dbHeader){
	int err = NO_ERROR;
        DM_FUNC_TRACE && cout << "DM.updateRecordFromDataPage()" << endl;
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage* DataPage = NULL;
	
	//cout<<"Trying to read pageno: "<<rid.pageNo<<endl;
	if(!bm.read(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_READ_ERROR;
		goto ret;
	}	

	DataPage = new DataBasePage(datapagebuf);
	
	if(DM_FUNC_TRACE){
		cout<<"Before updating pageno: "<<rid.pageNo<<endl;
		DataPage->dump();
		//cin >> dummy;
	}
	
	if(!DataPage->updateRecord(rec,reclen,rid))
	{
		//update not possible, then delete old record and insert new record
			DM_FUNC_TRACE && cout<<"Update not possible, possibly because size(old_rec) != size(update_rec)"<<endl;
			//delete old record
			if(!deleteRecord(startPage,rid,dbHeader))
			{
				//DM's delete Record Error
				err = DATAMANAGER_DELETE_RECORD_ERROR;
				goto ret;
			}
			
			//old record deleted, now insert new record
			if(!insertRecordIntoDataPage(rec,reclen,startPage,dbHeader,rid))
			{
				//DM's insert record into DataPage error
				err = DATAMANAGER_INSERT_RECORD_INTO_DATAPAGE_ERROR;
				goto ret;
			}
	}//updated record
	
	if(!bm.write(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_WRITE_ERROR;
		goto ret;
	}
	
	if(datapagebuf != NULL)
		delete[] datapagebuf;
		
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];

	if(!bm.read(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_READ_ERROR;
		goto ret;
	}	

	DataPage = new DataBasePage(datapagebuf);
	
	if(DM_FUNC_TRACE){
		cout<<"After updating pageno: "<<rid.pageNo<<endl;
		DataPage->dump();
		//cin >> dummy;
	}
					
	ret:
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================



//insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader)-This method inserts a record
//           into the database given a record ,its length ,start directory page and DBHeader instance.
//=========================================================================
bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader ,RID &recRid){

	DM_FUNC_TRACE && cout << "DM.insertRecordIntoDataPage()\n";
	
	string dummy = "";

	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
        char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	RID rid;
	int newPageNo;
      
	//Try and insert into existing DE
	//DM_FUNC_TRACE && cout << "Try and insert into existing DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(!dirpage->getFirstDirectoryEntry(curDE)){
			continue;
		}
		
		nextDE = curDE;
		do{
			curDE = nextDE;
			//DM_FUNC_TRACE && cout << "Looking for free space in a DE in page : " << curDE.pageNo <<",next =  " << nextDE.pageNo << endl;
			//DM_FUNC_TRACE && cin >> dummy;			
			if(curDE.freeSpace >= (short)(reclen + SLOT_DIR_SIZE)){
				if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = DATABASEPAGE_READ_ERROR;
					goto ret;
				}
				rid.pageNo = curDE.pageNo;
				if(datapage != NULL){
					delete datapage;
				}
				datapage = new DataBasePage(datapagebuf);
				
				if(DM_FUNC_TRACE){
					cout << "Before inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
				if(!datapage->insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}
				//databasepage dump
				if(DM_FUNC_TRACE){
					cout << "After inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				curDE.freeSpace = datapage->getTotalFreeSpace();
				if(DM_FUNC_TRACE){ 
					//cout << "Before updating DE to DirPage\n";
					//dirpage->dump();
				}
				
				dirpage->updateDirectoryEntry(curDE);
				
				bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
				bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
				
				if(DM_FUNC_TRACE){
					cout << "After updating DE to DirPage\n";
					dirpage->dump();
				}
				
				DM_FUNC_TRACE && cout << "Adding to existing DE, in DirPage : " << curDP << endl;
				DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
				
				//cin >> dummy;
				recRid.pageNo = curDE.pageNo;
				recRid.slotNo =rid.slotNo;
				goto ret;
				
			}
		}while(dirpage->getNextDirectoryEntry(curDE,nextDE));
		
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//All DEs are full
	
	//Try and insert a new DE	
	DM_FUNC_TRACE && cout << "Try and insert new DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	curDP = startPage;
	
	do{
	
	
	        //cout<<"ALL are out########################################"<<endl;
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->isAnyDEEmpty()){
		
			DM_FUNC_TRACE && cout << "Inserting new DE in to DirPage : " << curDP << endl;
			//DM_FUNC_TRACE && cin >> dummy;
		        
		        if(datapagebuf != NULL){
		        	delete[] datapagebuf;
		        }
			datapagebuf = new char[PAGE_SIZE_IN_BYTES];
			
			if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			
			curDE.pageNo = newPageNo;
			//dirpage->insertDirectoryEntry(curDE);
			RID rid;
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			datapage->init();
			
			if(!datapage->insertRecord(record,reclen,rid)){
			        err = DATABASEPAGE_WRITE_ERROR;
			        goto ret;
			}
		        curDE.freeSpace = datapage->getTotalFreeSpace();
		        //dirpage->updateDirectoryEntry(curDE);
		        
		        DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
			DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "Before updating DE to DirPage\n";
				//dirpage->dump();
			}
		        
		        dirpage->insertDirectoryEntry(curDE);
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "After updating DE to DirPage\n";
				//dirpage->dump();
			}
			
			bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
			bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);

			
			
			DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
			recRid.pageNo = curDE.pageNo;
			recRid.slotNo =rid.slotNo;
			//cin >> dummy;
			
			goto ret;
			
			
		}
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//ALL DEs in all DPs are full
	///creation of new DP
	
	//add a new DP to the end of the chain
	//Find the last DP in the chain
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	
	//dirpage has the last DP
	//curDP has the last DP page num
	//create a new DP, add link to it
	if(lastdirpagebuf != NULL){
		delete[] lastdirpagebuf;
	}
	lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	if(datapagebuf != NULL){
        	delete[] datapagebuf;
        }
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	curDE.pageNo = newPageNo;
	lastdirpage->insertDirectoryEntry(curDE);
	
	rid.pageNo = curDE.pageNo;
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	
	if(!datapage->insertRecord(record,reclen,rid)){
	        err = DATABASEPAGE_WRITE_ERROR;
	        goto ret;
	}
	
        curDE.freeSpace = datapage->getTotalFreeSpace();
        dirpage->updateDirectoryEntry(curDE);
	bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);	//write out current last page
	bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY);	//write out new data page
	bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);	//write out new last dir page
	
	
	DM_FUNC_TRACE && cout << "Adding new DirPage : " << dirpage->getNextDirectoryPage() << " , Current Last DirPage : "<< curDP << endl;
	DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
	DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
	DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
	//cin >> dummy;
	recRid.pageNo = curDE.pageNo;
	recRid.slotNo =rid.slotNo;
	goto ret;

	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(datapagebuf != NULL)
		delete[] datapagebuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;//remove allocated memory
	if(lastdirpagebuf != NULL)
		delete[] lastdirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(lastdirpage != NULL)
		delete lastdirpage;
	if(datapage != NULL)
		delete datapage;
	return err==NO_ERROR?true:false;
}
//===========================================================================================

bool DataManager::insertRecordsIntoTable(int startPage,DBHeader &  dbHeader,vector<RID> & recordRIDs, vector<int> types, vector<string*> recordStrings){


	DM_FUNC_TRACE && cout << "DM.insertRecordIntoDataPage()\n";
	DM_FUNC_TRACE && cout<<"No of records to insert:"<<recordStrings.size()<<endl;
	string dummy = "";
	
	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
        char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	char* record;
	int reclen;
	RID rid;
	int noOfRecs = recordStrings.size();
	int newPageNo ,i=0, j=0;
	int curRecord = 0;
	vector<string> attributes;
	string* recordStr;
	char* recordbuf;
	int bufsize;
	vector<char*> charRecords;
	vector<int> charRecSizes;
	
	for(i=0;i<recordStrings.size();i++){
		recordStr = (string*)recordStrings[i];
		for(j=0;j<types.size();j++)
			attributes.push_back(recordStr[j]);
		if(!pack::pack_data(attributes,recordbuf,bufsize,types)){
			err = CONVERT_ERROR;
			goto ret;
		}
		charRecords.push_back(recordbuf);
		charRecSizes.push_back(bufsize);	
	}
	
	/*for(i=0;i<records.size();i++){
		recAttributes.clear();
		if(!pack::unpack_data(recAttributes,types,records[i],recordSizes[i])){
			err = PACK_ERROR;
			goto ret;
		}
		cout<<"Unpacked "<<i+1<<"'records values are:";
		for(j=0;j<recAttributes.size();j++)
		cout<<"\t"<<recAttributes[j];
		cout<<endl;
	}*/
	
	record = (char*)charRecords[curRecord];
	reclen = charRecSizes[curRecord];
	
	/*for(i=0;i<recordSizes.size();i++)
		recordslen += recordSizes[i];
	*/
	//Try and insert into existing DE
	//DM_FUNC_TRACE && cout << "Try and insert into existing DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(!dirpage->getFirstDirectoryEntry(curDE)){
			continue;
		}
		
		nextDE = curDE;
		do{
			curDE = nextDE;
			//DM_FUNC_TRACE && cout << "Looking for free space in a DE in page : " << curDE.pageNo <<",next =  " << nextDE.pageNo << endl;
			//DM_FUNC_TRACE && cin >> dummy;			
			
			if(curDE.freeSpace >= (short)(reclen + SLOT_DIR_SIZE)){
			
				if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = DATABASEPAGE_READ_ERROR;
					goto ret;
				}
				rid.pageNo = curDE.pageNo;
				if(datapage != NULL){
					delete datapage;
				}
				datapage = new DataBasePage(datapagebuf);
				
				if(DM_FUNC_TRACE){
					cout << "Before inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
				
				do{
				
					if(!datapage->insertRecord(record,reclen,rid)){
					        err = DATABASEPAGE_WRITE_ERROR;
					        goto ret;
					}
					curRecord++;
					rid.pageNo = curDE.pageNo;
					recordRIDs.push_back(rid);
					curDE.freeSpace = datapage->getTotalFreeSpace();						
					if(curRecord == noOfRecs)
						break;
					record = (char*)charRecords[curRecord];
					reclen = charRecSizes[curRecord];
					//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
										
				}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
				
				cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;	
				//databasepage dump
				if(DM_FUNC_TRACE){
					cout << "After inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				curDE.freeSpace = datapage->getTotalFreeSpace();
				if(DM_FUNC_TRACE){ 
					//cout << "Before updating DE to DirPage\n";
					//dirpage->dump();
				}
				
				dirpage->updateDirectoryEntry(curDE);
				
				bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
				bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
				
				if(DM_FUNC_TRACE){
					cout << "After updating DE to DirPage\n";
					dirpage->dump();
				}
				
				DM_FUNC_TRACE && cout << "Adding to existing DE, in DirPage : " << curDP << endl;
				DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
				
				//cin >> dummy;
				if(curRecord == noOfRecs)
					goto ret;
				
			}
		}while(dirpage->getNextDirectoryEntry(curDE,nextDE));
		
		
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//All DEs are full
	
	//Try and insert a new DE	
	DM_FUNC_TRACE && cout << "Try and insert new DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	curDP = startPage;
	
	insertNewDE:
	do{
	
	        //cout<<"ALL are out########################################"<<endl;
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		//Error in this scope when loop is reentered, to enter new DE into existing DP
		while(dirpage->isAnyDEEmpty()){
		
			cin >> dummy;
			DM_FUNC_TRACE && cout << "Inserting new DE in to DirPage : " << curDP << endl;
			//DM_FUNC_TRACE && cin >> dummy;
		        
		        if(datapagebuf != NULL){
		        	delete[] datapagebuf;
		        }
			datapagebuf = new char[PAGE_SIZE_IN_BYTES];
			
			//Error here whn loop is reentered, free page returned is same as the previously returned page no.
			if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			
			curDE.pageNo = newPageNo;
			//dirpage->insertDirectoryEntry(curDE);
			RID rid;
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			datapage->init();
			
			cout<<"trying to insert records to page:"<<newPageNo<<endl;
			do{
			
				if(!datapage->insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}
				curRecord++;
				rid.pageNo = curDE.pageNo;
				recordRIDs.push_back(rid);
				curDE.freeSpace = datapage->getTotalFreeSpace();
				//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;				
				if(curRecord == noOfRecs)
					break;
				record = (char*)charRecords[curRecord];
				reclen = charRecSizes[curRecord];
				//DM_FUNC_TRACE && cout<<"FreeSpace of page:"<<curDE.pageNo<<" :"<<curDE.freeSpace<<" size of nxt record in page: "<<reclen+SLOT_DIR_SIZE<<endl;
			}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
			
			cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;
		        //dirpage->updateDirectoryEntry(curDE);
		        
		        DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
			DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "Before updating DE to DirPage\n";
				//dirpage->dump();
			}
		        
		        dirpage->insertDirectoryEntry(curDE);
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "After updating DE to DirPage\n";
				//dirpage->dump();
			}
			
			bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
			bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);

			//cin >> dummy;
			if(curRecord == noOfRecs)
					goto ret;			
			
		}
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//ALL DEs in all DPs are full
	///creation of new DP
	
	//add a new DP to the end of the chain
	//Find the last DP in the chain
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	
	//dirpage has the last DP
	//curDP has the last DP page num
	//create a new DP, add link to it
		
	if(lastdirpagebuf != NULL){
		delete[] lastdirpagebuf;
	}
	lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	if(datapagebuf != NULL){
        	delete[] datapagebuf;
        }
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	curDE.pageNo = newPageNo;
	lastdirpage->insertDirectoryEntry(curDE);
	
	rid.pageNo = curDE.pageNo;
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	DM_FUNC_TRACE && cout<<"trying to insert records to page:"<<newPageNo<<endl;
	do{
	
		if(!datapage->insertRecord(record,reclen,rid)){
		        err = DATABASEPAGE_WRITE_ERROR;
		        goto ret;
		}
		curRecord++;
		rid.pageNo = curDE.pageNo;
		recordRIDs.push_back(rid);
		curDE.freeSpace = datapage->getTotalFreeSpace();						
		if(curRecord == noOfRecs)
			break;
		record = (char*)charRecords[curRecord];
		reclen = charRecSizes[curRecord];
		//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
		
	}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
	
	DM_FUNC_TRACE && cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;
       dirpage->updateDirectoryEntry(curDE);
	bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);	//write out current last page
	bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY);	//write out new data page
	bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);	//write out new last dir page
	
	DM_FUNC_TRACE && cout << "Adding new DirPage : " << dirpage->getNextDirectoryPage() << " , Current Last DirPage : "<< curDP << endl;
	DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
	DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;

	if(curRecord != noOfRecs){
		curDP = dirpage->getNextDirectoryPage();
		goto insertNewDE;
	}	
	//cin >> dummy;
	//curDP now holds the Newly inserted last directory page num
	//and dirpage holds the directory page object pointing to this page.
/*	curDP = dirpage->getNextDirectoryPage();
	
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;
		
	dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
		err = DIRPAGE_READ_ERROR;
		goto ret;
	}
	
	if(dirpage != NULL){
		delete dirpage;
	}
	dirpage = new DirectoryPage(dirpagebuf);
*/	
	goto ret;
		
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(datapagebuf != NULL)
		delete[] datapagebuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;//remove allocated memory
	if(lastdirpagebuf != NULL)
		delete[] lastdirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(lastdirpage != NULL)
		delete lastdirpage;
	if(datapage != NULL)
		delete datapage;
	return err==NO_ERROR?true:false;
}

//===========================================================================================

bool DataManager::handleInsert(parsedQuery &pq){
        int err = NO_ERROR;
        int i = 0, j = 0, noCols = 0;
        int startPage;
        int size;
        DM_FUNC_TRACE && cout << "DM.handleInsert()\n";
        char *record = NULL;
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	RID rid;
	
	SysColumnRecord syscolrec;
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColumns;
        vector<RID> sysColRIDs;
        vector<int> types = pq.getTypes();
        vector<int> toTypes;
        vector<string> values = pq.getDataValues();
        vector<string> finalValues;
        vector<string> key;
        vector<int> keyTypes;
        
        vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
        
        vector<RID> sysIndexRIDs;
        vector<string> indexNames;
        bool flag = false;
        string tempstring;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *dirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	if(!getSysColRecsForTable(pq.getTable(),sysColumns,(*dbHeader),systabrec.totalColumns, sysColRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	noCols = 0;
	//check for columns type.
	for(j=0, i = 0 ; j < systabrec.totalColumns ; j++ ){
	        syscolrec = sysColumns[j];
		/*if(types[i] != syscolrec.dataType && types[i] != NULLTYPE){
			cout << "Column type mismatch " << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}*/
		
		/*if( j >= pq.getNoOfColumns() ){
			//Then there are some columns for which values have not been specified
			toTypes.push_back(syscolrec.dataType);
			finalValues.push_back(syscolrec.defaultvalue);
			continue;
		}*/
		
		//if column is deleted, add dummy column and null data
		if(syscolrec.isDeleted){
			//add as nulltype and null data
			toTypes.push_back(NULLTYPE);
			finalValues.push_back("NULL");
			//goto next sys coll
			continue;
		}
		
		//check if null is not allowed and column is not deleted
		if(types[i] == NULLTYPE && syscolrec.isNotNull && !syscolrec.isDeleted){
			cout << "Column '" << syscolrec.columnName << "' cannot take NULL values" << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}
		
		//user specified null value
		if(!values[i].compare("uNULL")){
			toTypes.push_back(NULLTYPE);
			finalValues.push_back("NULL");
			i++;
			noCols++;
			continue;
		}
		
		//check valid conversion		
		if(types[i] == NULLTYPE){
			//check if default value exists
			if(!strcmp(syscolrec.defaultvalue,"NULL"))
				toTypes.push_back(NULLTYPE);
			else
				toTypes.push_back(syscolrec.dataType);
			values[i] = (char*)syscolrec.defaultvalue;			
		}else{
			//check if recognised datatype can be converted into the type in syscol
			if(!checkDataType(types[i], syscolrec.dataType)){
				cout << "Datatype mismatch for column '" << syscolrec.columnName << "'" << endl;
				err = COLTYPE_MISMATCH;
				goto ret;	
			}
			toTypes.push_back(syscolrec.dataType);
			
		}
		
		//Check if the DataType is Varchar, if yes then check size
		if(types[i] == VARCHARTYPE){
			if(values[i].size() > syscolrec.maxSize){
				err = SIZE_MISMATCH;
				goto ret;	
			}	
		}
		
		noCols++;
		finalValues.push_back(values[i]);
		//next column
		i++;
	}
	
	if( finalValues.size() != j){
		cout << "Column count doesn't match " << endl;
		err = TOTALCOL_MISMATCH;
		goto ret;
	}
	
	//i represents the count of number of non deleted columns	
	//check for no of undeleted columns. 
	if(pq.getNoOfColumns() > noCols){
		cout << "Column count doesn't match " << endl;
		err = TOTALCOL_MISMATCH;
		goto ret;
	}
	
	startPage = systabrec.startPage;
	
	if(!pack::pack_data(finalValues, record, size, toTypes)){
		err = PACK_ERROR;
		goto ret;
	}
	
	//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
	if(!insertRecordIntoDataPage(record,size,startPage,(*dbHeader),rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	
	DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$"<<endl;
	DM_FUNC_TRACE && cout <<"pageNo "<<rid.pageNo<<"    slotNo"<<rid.slotNo<<endl;
	DM_FUNC_TRACE && cout <<"$$$$$$$$$$$$$$$$$"<<endl;
	
	//Add record to all Indexes on this table
	
	//find out list of unique index names
	for(int i=0; i<sysIndexRecs.size(); i++){
		flag = false;
		for(int j=0; j<indexNames.size(); j++){
			if(indexNames[j].compare(to_string(sysIndexRecs[i].indexName)) == 0){
				flag = true;
				break;
			}
		}
		
		if(flag) continue;
		
		//add index name to unique list
		indexNames.push_back(to_string(sysIndexRecs[i].indexName));
	}
	
	//now we have unique list of index names
	//for each of this index... insert the key
	
	for(int i=0; i<indexNames.size(); i++){	
	
		tempSysIndexRecs.clear();
		tempstring.clear();
		tempstring = indexNames[i];
		key.clear();
		keyTypes.clear();
		
		
		DM_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << endl;
		
		for(int j=0; j<sysIndexRecs.size(); j++){
			//find all index recs matching indexNames[i]
			if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
				tempSysIndexRecs.push_back(sysIndexRecs[j]);
			}
			
		}
		
		//now we have index recs for one index name
		key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
		keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
		
		
		for(int j=0; j<tempSysIndexRecs.size(); j++){
		
			//write the attribute at colOrigPos to the key in the colIndexPos
		
			key[tempSysIndexRecs[j].colIndexPos] = finalValues[tempSysIndexRecs[j].colOrigPos];
			keyTypes[tempSysIndexRecs[j].colIndexPos] = toTypes[tempSysIndexRecs[j].colOrigPos];
			
			DM_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColumns[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColumns[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
		}
		
		//now the key for one index is built..  pack it and insert
		//cout << "Press a key\n";
		//cin  >> tempstring;
		
		
		//pack the key
		if(record != NULL)
			delete[] record;
		if(!pack::pack_data(key, record, size, keyTypes)){
			err = PACK_ERROR;
			goto ret;
		}
		
		//insert the key
		if(!handleIndexInsert(tempSysIndexRecs[0].startPage,keyTypes,record,rid,(*dbHeader))){
			err = RECORD_INSERT_ERROR;
			goto ret;
		}
		
	}
	
	
	
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(record != NULL){
		delete[] record;
	}
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(dirPage != NULL){
		delete dirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//==========================================================================================

bool DataManager::handleDropSchema(parsedQuery &pq){

	int err = NO_ERROR;	
        DM_FUNC_TRACE && cout << "DM.handleDropSchema()\n";
	
	if(!bm.deleteSchemaFile((char *)pq.getDatabase().c_str())){
		err = DROP_SCHEMA_ERROR;
		goto ret;
	}
	
	
        ret:
        
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//==========================================================================================

//handleDropTable()-This method drops the table if it is present else returns error message.
bool DataManager::handleDropTable(parsedQuery &pq){
	int err = NO_ERROR;
        int i = 0;
        int startPage;
        int size;
        int curDirPageNo;
        DM_FUNC_TRACE && cout << "DM.handleDropTable()\n";
        
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	RID rid;
	
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
        
        DirectoryEntry curDE,nextDE;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *DirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}        
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	startPage = systabrec.startPage;
	curDirPageNo = startPage;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			
			if(!markAsFreePage(curDirPageNo,(*dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;

		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			
			if(!markAsFreePage(curDE.pageNo,(*dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		//mark this DP as freePage
		if(!markAsFreePage(curDirPageNo,(*dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	//delete table entry in SYSTAB
	if(!deleteRecord(SYS_TAB_START_PAGE_NUM,rid,(*dbHeader))){
		err = DATAMANAGER_DELETE_RECORD_ERROR;
		goto ret;
	}
	//delete columns entry in SYSCOLUMN
	for(i = 0; i < sysColRIDs.size() ; i++){
		if(!deleteRecord(SYS_COL_START_PAGE_NUM,sysColRIDs[i],(*dbHeader))){
			err = DATAMANAGER_DELETE_RECORD_ERROR;
			goto ret;
		}
	}
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//===========================================================================================

//markAsFreePage()-This method makes the page,pageNo as freepage and adds it to the DBHeader freepage list.
bool DataManager::markAsFreePage(int pageNo, DBHeader& dbHeader){
	DM_FUNC_TRACE && cout << "DM.markAsFreePage()";
        int err = NO_ERROR;
	int pagePtr = dbHeader.getFreePagePointer();
	char *data = new char[PAGE_SIZE_IN_BYTES];
	FreePage freePage = FreePage(data);
	freePage.setNextFreePage(pagePtr);
	
	if(!bm.write(pageNo,data,FREE_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	dbHeader.setFreePagePointer(pageNo);
	
	ret: 
        if(data != NULL){
	        delete[] data;
        }
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}

//===========================================================================================================


//deleteRecord()-This method deletes the record in the datapage given its RID.Also it updates corresponding
//                DE entry inthe Directory Page.
bool DataManager::deleteRecord(const int &startDirPageNo ,const RID &rid,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.deleteRecord()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID tempRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == rid.pageNo){
				//page corresponding to rid was found.
				//delete the record in that page and updata its corresponding DE value..
				
				//read page in DE into BufferPool
				if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = READ_ERROR;
					goto ret;
				}
				
				if(DataPage != NULL){
					delete DataPage;
				}
				DataPage = new DataBasePage(datapagebuf);
				DataPage->deleteRecord(rid);
				//check if there are any valid records.
				if(!DataPage->firstRecord(tempRID)){
					if(!markAsFreePage(curDataPageNo,dbHeader)){
						err = FREEPAGE_READ_ERROR;
						goto ret;
					}
					DirPage->updateDirectoryEntry(curDE,true);
					//goto ret;
				}else{
					//update DE in Directory Page.
					curDE.freeSpace = DataPage->getTotalFreeSpace();
					DirPage->updateDirectoryEntry(curDE,false);
					if(!bm.write(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
						err = WRITE_ERROR;
						goto ret;
					}
				}
				if(!bm.write(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
					err = WRITE_ERROR;
					goto ret;
				}
				goto ret;	
			}
			
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
		
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================
bool DataManager::handleDescTable(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDescTable()\n";
	int err = NO_ERROR;

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	RID rid;
	int i;	
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

		
	//print the sys cols for this table
	for(i=0; i<sysColRecs.size(); i++){
		cout << endl;
		sysColRecs[i].dump();	
	}
	cout << endl;
	
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================
bool DataManager::checkDataType(int fromDT, int toDT){

	bool err = NO_ERROR;

	//check if fromDT can be converted to toDT	
	switch(fromDT){
					case INTTYPE:
	 							if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != INTTYPE)
	 								err = INVALID;
								break;
								
					case DATETYPE:
								if(toDT != DATETYPE)
	 								err = INVALID;
								break;
								
					case FLOATTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != INTTYPE && toDT != FLOATTYPE)
	 								err = INVALID;
								break;
								
					case LONGTYPE:
								if(toDT != INTTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != LONGTYPE)
	 								err = INVALID;
								break;
								
					case DOUBLETYPE:
								if(toDT != LONGTYPE && toDT != INTTYPE && toDT != FLOATTYPE && toDT != DOUBLETYPE)
	 								err = INVALID;
								break;
								
					case VARCHARTYPE:
								if(toDT != VARCHARTYPE)
	 								err = INVALID;
								break;
								
					case SHORTTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE 
									&& toDT != INTTYPE && toDT != SHORTTYPE)
	 								err = INVALID;
								break;
								
					case BOOLTYPE:
								if(toDT != BOOLTYPE)
	 								err = INVALID;
								break;
	}
	
	
	return err==NO_ERROR?true:false;
}


//================================================================================================

//================================================================================================
bool DataManager::handleDelete(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDelete()\n";
	int err = NO_ERROR;
	

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	RID systabrid;	
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	int i;


	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), systabrid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//get all records for this table
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		if(!deleteRecord(sysTabRec.startPage, allRIDs[i], (*dbhead))){
			err = DATAMANAGER_DELETE_RECORD_ERROR;
			goto ret;
		}
	}
	
	allrecs.clear();
	allRIDs.clear();
	recsizes.clear();
	
	ret:	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}


//================================================================================================
bool DataManager::handleIndexInsert(const int &indexerpage,const vector<int> &keyType,char* const &key,const RID &rid,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.handleIndexInsert()" << endl;
        int err = NO_ERROR;
        int root;
	char * pageData = new char[PAGE_SIZE_IN_BYTES];
	char *newpageData = NULL;
	int newpageNo;
	IndexPage * indexpage = NULL;
	IndexNode node;
	BranchPage * branchpage = NULL;
	bool split = false;
	if(!bm.read(indexerpage,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(pageData);
	root = indexpage->getIndexerRoot();
	node.key = new char[(int) indexpage->getKeySize()];
	insertIntoIndex(root,keyType ,key ,node ,rid ,split,dbHeader);
	if(split){
	        DM_FUNC_TRACE && cout << "Split occured so create a new root" << endl;
	        DM_FUNC_TRACE && cout << "Key to be inserted : " << node.key << endl;
		newpageData = new char[PAGE_SIZE_IN_BYTES];
		if(!getFreePage(newpageData,newpageNo,dbHeader)){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		branchpage = new BranchPage(newpageData);
		branchpage->setPageType(BRANCHNODE);
		branchpage->setMaximumNodeEntries(indexpage->getMaximumNodeEntries());
		branchpage->setTotalKeysOccupied(0);
		branchpage->setKeySize(indexpage->getKeySize());
		branchpage->insert(keyType ,node.key ,node ,split);
		branchpage->dump(keyType);
		indexpage->setIndexerRoot(newpageNo);
		indexpage->dump();
		bm.write(indexerpage,pageData,DIR_PAGE_PRIORITY);
		bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
		
	}
	indexpage->dump();
	ret:
	if(newpageData != NULL)
		delete newpageData;
	if(branchpage != NULL)
		delete branchpage;
	if(pageData != NULL)
		delete pageData;
	if(indexpage != NULL)
		delete indexpage;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}
//================================================================================================

bool DataManager::insertIntoIndex(int &pageNo,const vector<int> &keyType ,char* const &key ,IndexNode &node ,const RID &rid ,bool &split,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.insertIntoIndex()" << endl;
        int err = NO_ERROR;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        BranchPage *branchpage = NULL;
        int nextPtr;
        char *newpageData = new char[PAGE_SIZE_IN_BYTES];
        int newpageNo;
        
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        
	indexpage = new IndexPage(pageData);
	if(indexpage->getPageType() == LEAFNODE){
		leafpage = new LeafPage(pageData);
		leafpage->insert(keyType,key,rid,split);
		if(split){
			DM_FUNC_TRACE && cout << "Split to be made in leaf" << endl;
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			leafpage->split(keyType,node,newpageData,key,rid,pageNo);
			leafpage->setNextLeaf(newpageNo);
			bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
			bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
			DM_FUNC_TRACE && cout << "Split done in leaf" << endl;
			split = true;
			DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Leaf$$$$$$$$$$$$$$$$$" << endl;
			leafpage->dump(keyType);
			goto ret;
		}
		leafpage->dump(keyType);
		bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
		split = false;
		goto ret;
	}
	
	branchpage = new BranchPage(pageData);
	branchpage->findNextNode(keyType ,key,nextPtr);
	insertIntoIndex(nextPtr,keyType,key,node,rid,split,dbHeader);
	if(split){
		branchpage->insert(keyType,node.key,node,split);
		if(split){
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			branchpage->split(keyType,node,newpageData,node.key);
			bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
			bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
			split = true;
			DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Branch$$$$$$$$$$$$$$$$$" << endl;
			branchpage->dump(keyType);
			goto ret;
		}
		branchpage->dump(keyType);
		bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
		split = false;
		goto ret;
	}
	ret:
	if(pageData != NULL)
		delete pageData;
	if(newpageData != NULL)
		delete newpageData;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(branchpage != NULL)
		delete branchpage;
		
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}

//=============================================================================================================
bool DataManager::handleDropIndex(parsedQuery &pq){
	
	DM_FUNC_TRACE && cout<<"DM.handleDropIndex()\n";
	int err = NO_ERROR;

	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbHeader = NULL;

	char* indexpagebuf = new char[PAGE_SIZE_IN_BYTES];
	IndexPage* indexPage = NULL;

	vector<int> indexAllPageNums;

	vector<SysIndexRecord> sysIndexRecs;
	vector<SysIndexRecord> tempSysIndexRecs;
	vector<RID> sysIndexRIDs;
	vector<RID> tempSysIndexRIDs;
	bool invalidIndex = true;
	bool duplicate = false;
	
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), tempSysIndexRecs, (*dbHeader), tempSysIndexRIDs)){
		err = INVALID;
		goto ret;
	}

	//check if such an index exists
	for(int i=0; i<tempSysIndexRecs.size(); i++){
		if(pq.getIndexName().compare(to_string(tempSysIndexRecs[i].indexName)) == 0){
			//index exists.. collect only 'this' index records
			invalidIndex = false;
			sysIndexRecs.push_back(tempSysIndexRecs[i]);
			sysIndexRIDs.push_back(tempSysIndexRIDs[i]);
		}
	}

	if(invalidIndex){
		//index does not exist
		err = INVALID;
		cout << "Index '" << pq.getIndexName() << "' on table '" << pq.getTable() << "' does not exist\n";
		goto ret;

	}

	//index exists
	if(!bm.read(sysIndexRecs[0].startPage, indexpagebuf, INDEX_ROOT_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexPage = new IndexPage(indexpagebuf);

	//collect all index page nums
	if(!getIndexPageNumbersRecursively(indexPage->getIndexerRoot(), indexAllPageNums)){
		err = INVALID;
		goto ret;
	}

	//mark each of the pages in indexAllPageNums as free pages
	for(int i=0; i<indexAllPageNums.size(); i++){
		
		duplicate = false;
		for(int j=0; j<i; j++){
			if(indexAllPageNums[i] == indexAllPageNums[j])
			{
				duplicate = true;
				break;			
			}
		}

		if(!duplicate && !markAsFreePage(indexAllPageNums[i],(*dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
	}

	//mark index page as free page
	if(!markAsFreePage(sysIndexRecs[0].startPage,(*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	
	//delete sys index records
	for(int i=0; i<sysIndexRecs.size(); i++){		
		if(!deleteRecord(SYS_INDEX_START_PAGE_NUM, sysIndexRIDs[i], (*dbHeader))){
			err = DATAMANAGER_DELETE_RECORD_ERROR;
			goto ret;
		}
	}

	//write back db header page
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}

	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbHeader != NULL)
		delete dbHeader;
	if(indexpagebuf != NULL)
		delete[] indexpagebuf;

	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::getIndexPageNumbersRecursively(int curPageNum, vector<int> &pageNums){

	DM_FUNC_TRACE && cout<<"DM.getIndexPageNumbersRecursively()\n";
	int err = NO_ERROR;
	
	BranchPage* branch = NULL;
	char* pagebuf = new char[PAGE_SIZE_IN_BYTES];

	vector<int> childPageNums;

	//read the current page..
	if(!bm.read(curPageNum, pagebuf, INDEX_INTERMEDIATE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//assume its a branch by default
	branch = new BranchPage(pagebuf);
	
	//check page type
	if(branch->getPageType() == LEAFNODE){
		//add this page to page num
		pageNums.push_back(curPageNum);
	}else {

		if(branch->getPageType() == BRANCHNODE){

			if(!branch->getAllChildPageNumbers(childPageNums)){
				err = INVALID;
				goto ret;
			}

			//call this function for each of the child page nums
			for(int i=0; i<childPageNums.size(); i++){
				if(!getIndexPageNumbersRecursively(childPageNums[i], pageNums)){
					err = INVALID;
					goto ret;
				}
				pageNums.push_back(childPageNums[i]);
			}

			
		}else{
			err = INVALID;
			goto ret;
		}

	}

	//if its a leaf delete it
	//if its a branch.. get other nodes
	

	ret:
	childPageNums.clear();
	if(branch != NULL)
		delete branch;
	if(pagebuf != NULL)
		delete[] pagebuf;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::handleAlterTable(parsedQuery &pq){

	DM_FUNC_TRACE && cout<<"DM.handleAlterTable()\n";
	int err = NO_ERROR;
	
	//column Names, Column Types, Add or Delete indices, Unique, Not Nullable, PKs
	vector<string> columns(pq.getColumns());
	vector<int> types(pq.getTypes()); //Columns to add
	vector<string> deleteCols(pq.getDeleteCols()); //Columns to Delete
	vector<bool> notNulls(pq.getNotNulls());			//COLUMN is NULL ?
	vector<bool> pks(pq.getPKs());				//COLUMN is PrimaryKey ?
	vector<bool> unique(pq.getUnique());			//COLUMN is Unique ?
	vector<defaultValue> defaultValues(pq.getDefaultValues()); //Contains default values
	vector<int> maxsizes(pq.getMaxSize());
	int i,j;
	
	cout<<"Entered handleAlterTable"<<endl;
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"Columns to add"<<endl;
	for(i=0;i<columns.size();i++)
		cout<<"\t"<<columns[i]<<" Type: "<<types[i]<<endl;
	cout<<"Columns to delete"<<endl;
	for(i=0;i<deleteCols.size();i++)
		cout<<"\t"<<deleteCols[i]<<endl;	
	cout<<"Default Vaules specified"<<endl;
	for(i=0;i<defaultValues.size();i++){
		if(defaultValues[i].isDefault)
			cout<<"Default value:"<<defaultValues[i].value<<endl;
		else
			cout<<"Default value: NULL"<<endl;
	}
	
	cout<<"No of Columns: "<<pq.noOfColumns<<endl;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];	
       DBHeader* dbHeader = NULL;
       DirectoryPage *dirPage = NULL;
	DirectoryPage *dirpage = NULL;
	DataBasePage *dbpage = NULL;
       int updatedNoOfColumns;
       RID rid, sysTabRID;
	SysTableRecord systabrec,sysTabRec;
	vector<SysColumnRecord> syscolrecs_prev;
	vector<SysColumnRecord> syscolrecs;
      	SysColumnRecord* sysColRecs = NULL;
      	SysTableRecord tmpsystabrec;
       vector<SysColumnRecord> sysColumns;
       vector<RID> sysColRIDs;
       vector<int> toTypes;
       char* rec= NULL;
	int recSize;
	bool exists;

	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//Get SysTableRecord for "table-name" from Sys Table Page
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//Needed to update totalColumns of systab entry corresponding to table
	sysTabRID = rid;
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), syscolrecs_prev, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	//If columns.size() != 0 , Add columns to SysCols
	if(columns.size() != 0){
	
		//check for adding duplicate columns
		for(i=0; i<syscolrecs_prev.size(); i++){
			for(j=0;j<columns.size();j++){
				if(!(columns[j]).compare(syscolrecs_prev[i].columnName)){
					err = COLUMN_EXISTS;
					goto ret;
				}
				if(!defaultValues[j].isDefault && notNulls[j]){
					err = COLUMN_NULLABLE;
					goto ret;
				}	
			}	
		}
		sysColRecs = new SysColumnRecord[columns.size()];		//Create as many sys col records as columns
		
		for(i=0; i < pq.getNoOfColumns(); i++){
			sprintf(sysColRecs[i].columnName,"%s",columns[i].c_str());		//Add column name
			sprintf(sysColRecs[i].tableName,"%s",pq.getTable().c_str());		//Add table name
			sysColRecs[i].colPos = i + systabrec.totalColumns;				//Column order in table
			sysColRecs[i].dataType = types[i];							//Column data type
			sysColRecs[i].isNotNull = notNulls[i];							//Column is Nullable ?
			sysColRecs[i].isPrimary = pks[i];								//Column is Primary ?		
			sysColRecs[i].isUnique = unique[i];							//Column is Unique ?
			sysColRecs[i].isDeleted = false;								//Column is Deleted ?
			sysColRecs[i].maxSize	= maxsizes[i];						//max size for the Column
			if(defaultValues[i].isDefault)
			strcpy(sysColRecs[i].defaultvalue,(defaultValues[i].value).c_str());
			else
			strcpy(sysColRecs[i].defaultvalue,"NULL");						//Default Value is NULL if no user defined value
			
			//check for constraint in compatibility
			//if col is primary, then set unique and not null to true
			if(pks[i]){
				sysColRecs[i].isNotNull = true;
				sysColRecs[i].isUnique = true;
			}

			if(!convertSysColRecToRecord(sysColRecs[i], rec, recSize)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			
			if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
				err = RECORD_INSERT_ERROR;
				goto ret;
		       }
		       DM_FUNC_TRACE && cout<<"Column "<<columns[i]<<" added"<<endl;
			
		       DM_FUNC_TRACE && cout<<"No problem deleting record"<<endl;
		}//Columns have been added to SysCol Page
		
		//Now create a updated systab record for the given tablename
		updatedNoOfColumns = systabrec.totalColumns + pq.getNoOfColumns();
		DM_FUNC_TRACE && cout<<"Updated no. of Columns:"<<updatedNoOfColumns;
		strcpy(sysTabRec.tableName,systabrec.tableName);
		sysTabRec.startPage = systabrec.startPage;
		sysTabRec.totalRows = systabrec.totalRows;
		sysTabRec.totalDP = systabrec.totalDP;
		sysTabRec.totalColumns = updatedNoOfColumns;
		
		if(rec != NULL)
		delete[] rec;
		
		//convert systabrec
		if(!convertSysTabRecToRecord(sysTabRec, rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}
		convertRecordToSysTabRec(tmpsystabrec, rec, recSize);
		
		cout<<"No of columns after converting to sys col record:"<<tmpsystabrec.totalColumns<<endl;
		
		if(!updateRecordFromDataPage(rec, recSize,sysTabRID,systabrec.startPage,(*dbHeader))){
			err = UPDATE_RECORD_FROM_DATAPAGE_ERROR;
			goto ret;
		}
		
	}//Columns are added
	
	if(deleteCols.size() != 0){
		
		sysColRIDs.clear();
		syscolrecs.clear();
		
		//Get SysTableRecord for "table-name" from Sys Table Page
		if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
		        err = INVALIDTABLENAME;
		        goto ret;
		}
		
		if(!getSysColRecsForTable(pq.getTable(), syscolrecs, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
			//Column Read Error
			err = INVALID;
			goto ret;
		}	
		
		//Check if the given columns exists
		for(i=0;i<deleteCols.size();i++){
			exists = false;
			for(j=0; j<syscolrecs_prev.size();j++)
				if(!deleteCols[i].compare(syscolrecs[j].columnName)){
					exists = true;
					break;
				}
			if(!exists){
				err = COLUMN_NOT_DEFINED;
				goto ret;
			}
			
			//Modify syscolrecs[j] and Update corresponding sysColRecord
			syscolrecs[j].isDeleted = true;
			if(rec != NULL)
				delete[] rec;
				
			if(!convertSysColRecToRecord(syscolrecs[j], rec, recSize)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			if(!updateRecordFromDataPage(rec, recSize,sysColRIDs[j], dbHeader->getSystemColumnPointer(),(*dbHeader))){
				err = UPDATE_RECORD_FROM_DATAPAGE_ERROR;
				goto ret;
			}
		}
	}
	
	ret:	
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbHeader != NULL)
		delete dbHeader;	
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//===============================================================================

bool DataManager::handleUpdate(parsedQuery &pq){

	DM_FUNC_TRACE && cout<<"DM.handleUpdate()\n";
	int err = NO_ERROR;
	
	//column Names, Column Types, Add or Delete indices, Unique, Not Nullable, PKs
	vector<string> condition_array = (vector<string>)pq.getConditionArray();
	vector<string> colNames = (vector<string>)pq.getColumns();	
	vector<string> dataValues(pq.getDataValues());
	int i=0,j=0;
	
	cout<<"Entered handleUpdate"<<endl;
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"Columns "<<endl;
	for(i=0;i<colNames.size();i++)
		cout<<"\t"<<colNames[i]<<" : "<< dataValues[i]<<endl;
		
	cout<<"No of Columns: "<<pq.noOfColumns<<endl;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	
	vector<int> colTypes;
	string str;
	
	vector<bool>	displayCol; // contains the col indexs of the columns to display
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> recAttributes;
	vector<string> updateAttributes;
	bool condition;
	RID rid,sysTabRecRID;
	bool exists;
	int updatedSize;
	char* updatedRec;	
	vector<int> updateIndex;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	sysTabRecRID = rid;
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}
	
	for(j=0;j<sysColRecs.size();j++){
		updateAttributes.push_back("");
		updateIndex.push_back(j);		
		colTypes.push_back(sysColRecs[j].dataType);
	}	
	
	for(i=0;i<pq.getNoOfColumns();i++){
		exists = false;
		for(j=0;j<sysColRecs.size();j++){
			if(!colNames[i].compare(sysColRecs[j].columnName)){
				updateAttributes[j] = dataValues[i];
				updateIndex[j] = -1;
				exists = true;
			}
		}
		if(!exists){
			err = COLUMN_NOT_DEFINED;
			goto ret;
		}
	}
	
	for(i=0;i<allrecs.size();i++){
		recAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}
		
		err = evaluate_expression(pq.getConditionArray(),recAttributes,sysColRecs,condition);
		if(err != NO_ERROR )
		{
			goto ret;
		}
		
		if(condition){
			//Update Record i
			rid = allRIDs[i];
			cout<<"Updating Record with values:";
			for(j=0;j<sysColRecs.size();j++){
				if(updateIndex[j] != -1)
					updateAttributes[j] = recAttributes[j];
				cout<<updateAttributes[j]<<"  ";
			}
			cout<<endl;
			
			if(!pack::pack_data(updateAttributes,updatedRec,updatedSize,colTypes)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			updateRecordFromDataPage((char*)allrecs[i].c_str(),allrecs[i].size(),rid,sysTabRec.startPage,(*dbhead));
		}
	}
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}
//===============================================================================

bool DataManager::handleAddRecords(parsedQuery &pq){

	DM_FUNC_TRACE && cout << "DM.handleAddRecods()\n";
	int err = NO_ERROR;
	
	int i =0,j=0;
	
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"No of Records to Add: "<<pq.getNoOfRecords()<<endl;
	cout<<"Record Type: ";
	if(pq.getRecordType() == DEFAULT)
		cout<<"DEFAULT"<<endl;
	else if(pq.getRecordType() == RANGE){
		cout<<"RANGE"<<endl;
		cout<<"Start of Range: "<<pq.getStartRange()<<endl;
	}	
	else
		cout<<"RANDOM"<<endl;

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	SysTableRecord sysTabRec;
	RID rid;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	char* record;
	int recSize = 0;
	vector<string> recAttributes;
	vector<char*> records; //contains list of records to add
	vector<int> recordSizes; 
	vector<int> types;
	vector<RID> recordRIDs;
	int randno;
	char* randstr;
	string randString,tempstr;
	int rangeno;
	char* rangestr;
	string rangeString;
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	char* tmpcstr;
	vector<string*> recordStrings;
	string* attributes;
	int k = 0;
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	record = NULL;
	/*for(i=0;i<pq.getNoOfRecords();i++)
		records.push_back(record);
	*/	
	types.clear();
	for(i=0;i<sysColRecs.size();i++)
		types.push_back(sysColRecs[i].dataType);
		
	if(pq.getRecordType() == DEFAULT){
		//Enter default values records to table
		DM_FUNC_TRACE && cout <<"Entering Default values"<<endl;
		records.clear();
		recAttributes.clear();
		for(i=0;i<sysColRecs.size();i++){
			if(!sysColRecs[i].isDeleted && !strcmp(sysColRecs[i].defaultvalue,"NULL") && sysColRecs[i].isNotNull ){
				err = COLUMN_NOT_NULLABLE;
				goto ret;
			}
			if(sysColRecs[i].isDeleted)
				recAttributes.push_back("NULL");
			else
				recAttributes.push_back(sysColRecs[i].defaultvalue);	
		}
		
		for(i=0;i<pq.getNoOfRecords();i++){
			attributes = (string*)new string[recAttributes.size()];
			for(k=0;k<recAttributes.size();k++)
				attributes[k] = recAttributes[k];
			recordStrings.push_back(attributes);
		}
		
		DM_FUNC_TRACE && cout <<"No of records: "<<recordStrings.size()<<endl;			
	}
	else if(pq.getRecordType() == RANDOM){
		//insert pq.getNoOfRecords() records of type RANDOM
		//initialize the seed
		DM_FUNC_TRACE && cout <<"Entering Random values"<<endl;		
		srand( time(NULL) );
		for(i=0;i<pq.getNoOfRecords();i++){
			recAttributes.clear();
			randno = rand() % 10000 + 1; //Random no b/w: 1 to 10000
			sprintf(randstr,"%d",randno);
			randString = randstr;
			attributes = (string*)new string[sysColRecs.size()];
			
			for(j=0;j<sysColRecs.size();j++){
			
				if(sysColRecs[i].isDeleted){
					recAttributes.push_back("NULL");
					attributes[j] = "NULL";
				}	
				else if(sysColRecs[i].dataType == INTTYPE || sysColRecs[i].dataType == SHORTTYPE || sysColRecs[i].dataType == LONGTYPE || sysColRecs[i].dataType == FLOATTYPE || sysColRecs[i].dataType == DOUBLETYPE){
					recAttributes.push_back(randString);
					attributes[j] = randString;
				}	
				else if(sysColRecs[i].dataType == BOOLTYPE){
					if(randno % 2 ==1){
						recAttributes.push_back("1");
						attributes[j] = "1";
						}
					else{
						recAttributes.push_back("0");
						attributes[j] = "0";
						}
				}
				else if(sysColRecs[i].dataType == VARCHARTYPE){
					tempstr = "word" + randString;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
				else if(sysColRecs[i].dataType == DATETYPE){
					sprintf(randstr,"%04d%02d%02d",(randno % 9999 + 1),(randno % 12 + 1),(randno % 31 + 1));
					tempstr = randstr;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
				else if(sysColRecs[i].dataType == TEXTTYPE){
					tempstr = "word" + randString;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
			}
			
			recordStrings.push_back(attributes);
		/*	tmpcstr = (char*)new char[recSize];
			strcpy(tmpcstr,record);
			records.push_back(tmpcstr);
			recordSizes.push_back(recSize);
		*/
		}
	}
	else if(pq.getRecordType() == RANGE){
		//insert pq.getNoOfRecords() records into table of type RANGE
		//if range value is specified, insert values starting from that range till range+pq.getNoOfRecords()
		//else find max value in the unique column, or else if varchar is present find max num whr value = "wordnum".
		DM_FUNC_TRACE && cout <<"Entering Range values"<<endl;		
		if(pq.getStartRange() != -1){
			//Range value specified
			rangeno = pq.getStartRange();
			for(i=0;i<pq.getNoOfRecords();i++){
				recAttributes.clear();
				rangeno = rangeno + i;
				sprintf(rangestr,"%d",rangeno);
				rangeString = rangestr;
				attributes = (string*)new string[sysColRecs.size()];
				
				for(j=0;j<sysColRecs.size();j++){
				
					if(sysColRecs[i].isDeleted){
						recAttributes.push_back("NULL");
						attributes[j] = "NULL";
					}	
					else if(sysColRecs[i].dataType == INTTYPE || sysColRecs[i].dataType == SHORTTYPE || sysColRecs[i].dataType == LONGTYPE || sysColRecs[i].dataType == FLOATTYPE || sysColRecs[i].dataType == DOUBLETYPE){
						recAttributes.push_back(rangeString);
						attributes[j] = rangeString;
					}	
					else if(sysColRecs[i].dataType == BOOLTYPE){
						if(rangeno % 2 ==1){
							recAttributes.push_back("1");
							attributes[j] = "1";
						}	
						else{
							recAttributes.push_back("0");
							attributes[j] = "0";
						}	
					}
					else if(sysColRecs[i].dataType == VARCHARTYPE){
						tempstr = "word" + rangeString;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
					else if(sysColRecs[i].dataType == DATETYPE){
						sprintf(rangestr,"%04d%02d%02d",(rangeno % 9999 + 1),(rangeno % 12 + 1),(rangeno % 31 + 1));
						tempstr = rangestr;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
					else if(sysColRecs[i].dataType == TEXTTYPE){
						tempstr = "word" + rangeString;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
				}
				recordStrings.push_back(attributes);
			/*	recSize = 0;
				if(!pack::pack_data(recAttributes,record,recSize,types)){
						err = PACK_ERROR;
						goto ret;
				}
				tmpcstr = (char*)new char[recSize];
				strcpy(tmpcstr,record);
				records.push_back(tmpcstr);
				recordSizes.push_back(recSize);
			*/	
			}
		}
		else {
			//Range value not specified, find the range from the records in table
			getAllRecords(sysTabRec.startPage,allrecs,recsizes,allRIDs);
			
			//try to find max values for each unique col, and insert records frm that values on wards for each col
			
		}
	}
	DM_FUNC_TRACE && cout <<"No of records:"<<records.size()<<endl;		
	//insert pq.getNoOfRecords() into table with
	if(!insertRecordsIntoTable(sysTabRec.startPage,(*dbhead),recordRIDs,types,recordStrings)){
		err = INSERT_RECORDS_TO_TABLE;
		goto ret;
	}
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);	
	return err==NO_ERROR?true:false;
}
//===============================================================================

bool DataManager::convertRecordToSysColRec(SysColumnRecord &syscolrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysColRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //column name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col pos
	colTypes.push_back(INTTYPE); //data type
	colTypes.push_back(BOOLTYPE); //is not null
	colTypes.push_back(BOOLTYPE); //is unique
	colTypes.push_back(BOOLTYPE); //is primary
	colTypes.push_back(BOOLTYPE); //is deleted
	colTypes.push_back(INTTYPE); //max size
	colTypes.push_back(VARCHARTYPE); //default value
	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//syscolrec.columnname
	memcpy(&syscolrec.columnName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	//table name
	memcpy(&syscolrec.tableName, recAttributes[1].c_str(), (recAttributes[1].size() + 1));
	
	//col pos
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.colPos = *(short*)att_value;
	delete (short*)att_value;
	
	//data type
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.dataType = *(int*)att_value;
	delete (int*)att_value;
	
	//is not null
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isNotNull = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is primary
	if( !pack::convert_to_type(recAttributes[5], colTypes[5], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isPrimary = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is Unique
	if( !pack::convert_to_type(recAttributes[6], colTypes[6], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isUnique = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is deleted
	if( !pack::convert_to_type(recAttributes[7], colTypes[7], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isDeleted = *(bool*)att_value;
	delete (bool*)att_value;
	
	//max size
	if( !pack::convert_to_type(recAttributes[8], colTypes[8], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.maxSize = *(int*)att_value;
	delete (int*)att_value;
	
	
	//default value
	memcpy(&syscolrec.defaultvalue, recAttributes[9].c_str(), (recAttributes[9].size() + 1));
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysColRecToRecord(SysColumnRecord syscolrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysColRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //column name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col pos
	colTypes.push_back(INTTYPE); //data type
	colTypes.push_back(BOOLTYPE); //is not null
	colTypes.push_back(BOOLTYPE); //is primary
	colTypes.push_back(BOOLTYPE); //is unique
	colTypes.push_back(BOOLTYPE); //is deleted
	colTypes.push_back(INTTYPE); //max size
	colTypes.push_back(VARCHARTYPE); //default value
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	//add column name	
	recAttributes.push_back((to_string(syscolrec.columnName)));
	
	//add table name
	recAttributes.push_back((to_string(syscolrec.tableName)));
	
	//add col pos
	recAttributes.push_back(to_string(syscolrec.colPos));

	//add data type
	recAttributes.push_back(to_string(syscolrec.dataType));
	
	//add is not null
	recAttributes.push_back(to_string(syscolrec.isNotNull));	
	
	//add is primary
	recAttributes.push_back(to_string(syscolrec.isPrimary));	
	
	//add is unique
	recAttributes.push_back(to_string(syscolrec.isUnique));	
	
	//add is deleted
	recAttributes.push_back(to_string(syscolrec.isDeleted));
	
	//add max size
	recAttributes.push_back(to_string(syscolrec.maxSize));	
	
	//add default value
	recAttributes.push_back(to_string(syscolrec.defaultvalue));
	
	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertRecordToSysTabRec(SysTableRecord &systabrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysTabRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(INTTYPE); //total rows
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(INTTYPE); //total DP
	colTypes.push_back(INTTYPE); //total columns

	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//systabrec.tablename
	memcpy(&systabrec.tableName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	
	//total rows
	if( !pack::convert_to_type(recAttributes[1], colTypes[1], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalRows = *(int*)att_value;
	delete (int*)att_value;
	
	
	//start page
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.startPage = *(int*)att_value;
	delete (int*)att_value;
	
	//total DP
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalDP = *(int*)att_value;
	delete (int*)att_value;
	
	//no of cols
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalColumns = *(int*)att_value;
	delete (int*)att_value;
		
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysTabRecToRecord(SysTableRecord systabrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysTabRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(INTTYPE); //total rows
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(INTTYPE); //total DP
	colTypes.push_back(INTTYPE); //total columns
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	//add table name	
	recAttributes.push_back((to_string(systabrec.tableName)));
	
	//add total rows
	recAttributes.push_back((to_string(systabrec.totalRows)));
	
	//add start page
	recAttributes.push_back((to_string(systabrec.startPage)));
	
	//add total DP
	recAttributes.push_back((to_string(systabrec.totalDP)));
	
	//add total columns
	recAttributes.push_back((to_string(systabrec.totalColumns)));
	
	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysIndexRecToRecord(SysIndexRecord sysindexrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysIndexRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //index name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col orig pos
	colTypes.push_back(SHORTTYPE); //col index pos
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(SHORTTYPE); //no of index cols
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	
	//add index name	
	recAttributes.push_back((to_string(sysindexrec.indexName)));
	
	//add table name	
	recAttributes.push_back((to_string(sysindexrec.tableName)));
	
	//add col orig pos
	recAttributes.push_back((to_string(sysindexrec.colOrigPos)));
	
	//add col index pos
	recAttributes.push_back((to_string(sysindexrec.colIndexPos)));
	
	//add start page
	recAttributes.push_back((to_string(sysindexrec.startPage)));

	//add no of index cols
	recAttributes.push_back((to_string(sysindexrec.totalIndexCols)));
	

	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertRecordToSysIndexRec(SysIndexRecord &sysindexrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysIndexRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //index name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col orig pos
	colTypes.push_back(SHORTTYPE); //col index pos
	colTypes.push_back(INTTYPE); //col index pos
	colTypes.push_back(SHORTTYPE); //total index cols
	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//sysindexrec.tablename
	memcpy(&sysindexrec.indexName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	//systabrec.tablename
	memcpy(&sysindexrec.tableName, recAttributes[1].c_str(), (recAttributes[1].size() + 1));
	
	
	//col orig pos
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.colOrigPos = *(short*)att_value;
	delete (short*)att_value;
	
	//col index pos
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.colIndexPos = *(short*)att_value;
	delete (short*)att_value;
	
	//col start page
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.startPage = *(int*)att_value;
	delete (int*)att_value;
	
	//total index cols
	if( !pack::convert_to_type(recAttributes[5], colTypes[5], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	delete (short*)att_value;
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=============================================================================================================



template <class X>
string DataManager::to_string(const X& x){
	stringstream s;
	s << x;
	return s.str();
}

#endif
