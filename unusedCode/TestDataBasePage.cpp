#include <iostream>
#include "databasepage.cpp"
#include "./../Common/Settings.h"
#include "pagedataentries.h"

using namespace std;

int main(){
        settings::init();
	char *temp = new char[PAGE_SIZE_IN_BYTES];
	char *recPtr ="aaaaaaaa";
	char *retPtr = NULL;// = new char[8];
	int size;
	cout<<recPtr<<endl;
	DataBasePage page = DataBasePage(temp);
	SlotDirEntry dirEntry;
	RID recID;
	recID.pageNo = 1;
	page.init();
	dirEntry = page.getCFSTuple();
	cout<<"Page Size =  "<<PAGE_SIZE_IN_BYTES<<endl;
	cout<<"TFS =  "<<page.getTotalFreeSpace()<<endl;
	cout<<"Total Slots = "<<page.getNoOfSlots()<<endl;
	cout<<"Dir Entry "<<dirEntry.offset<<"   "<<dirEntry.length<<endl;
	cout<<page.firstRecord(recID)<<endl;
	cout<<page.insertRecord(recPtr,8,recID)<<endl;
	cout<<page.insertRecord(recPtr,8,recID)<<endl;
	cout<<page.insertRecord(recPtr,8,recID)<<endl;
	cout<<"Rec ID "<<recID.pageNo<<"  "<<recID.slotNo<<endl;
	dirEntry = page.getCFSTuple();
	cout<<"TFS =  "<<page.getTotalFreeSpace()<<endl;
	cout<<"Total Slots = "<<page.getNoOfSlots()<<endl;
	cout<<"Dir Entry "<<dirEntry.offset<<"   "<<dirEntry.length<<endl;
	RID newID;
	newID.pageNo = 1;
	cout<<page.firstRecord(newID)<<endl;
	cout<<page.nextRecord(newID,recID)<<endl;
	cout<<"New ID "<<newID.pageNo<<"  "<<newID.slotNo<<endl;
	cout<<page.getRecord(newID,retPtr,size)<<" "<<newID.slotNo<<endl;
	cout<<(char *)retPtr<<"  "<<size<<endl;
	char * rec = NULL;
	cout<<page.getRecord(recID,rec,size)<<"  "<<recID.slotNo<<endl;
	cout<<(char *)retPtr<<"  "<<size<<endl;
	cout<<page.nextRecord(recID,newID)<<endl;
	cout<<page.getRecord(newID,retPtr,size)<<" "<<newID.slotNo<<endl;
	cout<<(char *)retPtr<<"  "<<size<<endl;
	cout<<page.deleteRecord(recID)<<endl;
	page.firstRecord(recID);
}
