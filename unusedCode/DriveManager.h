#include <fstream>
#include <iostream>
#include "./../Datastructures/frame.h"
#include "./../settings.h"

using namespace std;

#ifndef DM_CPP
#define DM_CPP
static int readPage(int pageNo, frame* tempframe){

            ifstream heapfile (settings::filename, ios::binary | ios::in);

            if(heapfile.is_open()){

            //seek beginning of page - pageNum
                heapfile.seekg((pageNo) * settings::bufferPageSizeInBytes,ios::beg);

                heapfile.read((char*)tempframe->page.pageData,settings::bufferPageSizeInBytes);

                settings::verbose && cout << heapfile.gcount() << " bytes read" << endl;

                if(heapfile.gcount() == 0){
                    heapfile.close();
                    return -1;
                }

                heapfile.close();


            }else{
                cout << "Unable to read from heapfile\n";
                return -1;
            }

            return 0;

}

static void writePage(int pageNo, frame* tempframe){

            fstream heapfile (settings::filename, ios::binary | ios::in | ios::out);

            if(heapfile.is_open()){

                heapfile.seekp(pageNo * settings::bufferPageSizeInBytes, ios::beg);

                if(heapfile.rdstate() & heapfile.failbit){
                    cout << "Page " << pageNo << " could not be found\n";
                    heapfile.close();
                    exit(0);
                    return;
                }

                heapfile.write((char*)tempframe->page.pageData,settings::bufferPageSizeInBytes);

                heapfile.close();

            }else{
                cout << "Unable to dump to file\n";
                exit(0);
            }
}
#endif
