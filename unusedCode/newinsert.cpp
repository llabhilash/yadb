bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader){
	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
        char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	RID rid;
	int newPageNo;
      
	//Try and insert into existing DE
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(!dirpage->getFirstDirectoryEntry(curDE)){
			continue;
		}
		
		nextDE = curDE;		
		do{
			curDE = nextDE;
			
			if(curDE.freeSpace >= (short)(reclen + SLOT_DIR_SIZE)){
				if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = DATABASEPAGE_READ_ERROR;
					goto ret;
				}				
				rid.pageNo = curDE.pageNo;
				if(datapage != NULL){
					delete datapage;
				}
				datapage = new DataBasePage(datapagebuf);
				
				if(!dbPage.insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}
				
				curDE.freeSpace = datapage->getTotalFreeSpace();
				dirpage->updateDirectoryEntry(curDirEntry);
				bm.write(curDP,datapagebuf,DIR_PAGE_PRIORITY);
				bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
				goto ret;
				
			}
		}(while(!dirpage->getNextDirectoryEntry(curDE,nextDE));
		
	}(while((curDP = dirpage->getNextDirectoryPage()) != -1 ));
	
	//All DEs are full
	
	//Try and insert a new DE	
	curDP = startPage;
	
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->isAnyDEEmpty()){
		        
		        if(datapagebuf != NULL){
		        	delete datapagebuf;
		        }
			datapagebuf = new char[PAGE_SIZE_IN_BYTES];
			
			if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			
			curDE.pageNo = newPageNo;
			dirpage->insertDirectoryEntry(curDE);
			RID rid;
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			datapage->init();
			
			if(!dbPage.insertRecord(record,reclen,rid)){
			        err = DATABASEPAGE_WRITE_ERROR;
			        goto ret;
			}
		        curDE.freeSpace = datapage->getTotalFreeSpace();
		        dirpage->updateDirectoryEntry(curDirEntry);
			bm.write(curDP,datapagebuf,DIR_PAGE_PRIORITY);
			bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
			goto ret;
			
			
		}
	}(while((curDP = dirpage->getNextDirectoryPage()) != -1 )));
	
	//ALL DEs in all DPs are full
	///creation of new DP
	
	//add a new DP to the end of the chain
	//Find the last DP in the chain
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	
	//dirpage has the last DP
	//curDP has the last DP page num
	//create a new DP, add link to it
	if(lastdirpagebuf != NULL){
		delete[] lastdirpagebuf;
	}
	lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	if(datapagebuf != NULL){
        	delete datapagebuf;
        }
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	curDE.pageNo = newPageNo;
	lastdirpage->insertDirectoryEntry(curDE);
	rid;
	rid.pageNo = curDE.pageNo;
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	
	if(!dbPage.insertRecord(record,reclen,rid)){
	        err = DATABASEPAGE_WRITE_ERROR;
	        goto ret;
	}
	
        curDE.freeSpace = datapage->getTotalFreeSpace();
        dirpage->updateDirectoryEntry(curDirEntry);
	bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);	//write out current last page
	bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY);	//write out new data page
	bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);	//write out new last dir page
	goto ret;

	
	
	ret:
	printErrorCode(err);
	if(datapagebuf != NULL)
		delete[] datapagebuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;//remove allocated memory
	if(lastdirpage != NULL)
		delete[] lastdirpage;
	return err==NO_ERROR?true:false;
}
