#include "BufferManager/BufferManager.h"
#include <iostream>
#include "settings.h"

void getIntInput(int* temp){
    while(!(std::cin >> *temp))
    {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                cout << "Invalid Input\nRe-enter : ";
    }
}

void getCharInput(char* temp){
    while(!(std::cin >> *temp))
    {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                cout << "Invalid Input\nRe-enter : ";
    }
}

int main(int argc, char* argv){

	settings::init();

        BufferManager bm;
        frame* temp;

        int choice = 1;
        int pageNo = -1;
        char toWrite = 'a';

        while(true){

            cout << "\n\n1 : Set verbose mode \n";
            cout << "2 : Read a page ( disk ==> buffer )\n";
            cout << "3 : Write a page (reads internally) ( buffer ==> disk )\n";
	    cout << "4 : Set inUse OFF for a page (frame which contains the page, may be used as a replacement)\n";
	    cout << "5 : Commit Buffer to disk\n";
            cout << "0 : Exit\n";
            cout << "Choice : ";

            if(!(std::cin >> choice))
            {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                //cout << "Invalid choice\nChoice : ";
                choice = -1;
                break;
            }


            switch(choice){

                case 1 :    bm.verboseMode();
                            break;

                case 2 :    cout << "Page number to read : ";                            
                            getIntInput(&pageNo);
                            temp = bm.read(pageNo);
                            break;

                case 3 :    cout << "Page number to write : ";
                            getIntInput(&pageNo);

			    temp = bm.read(pageNo);
			    //cout << "Frame number to write : ";
			    //int frameNo;
			    //getIntInput(&frameNo);
			    //temp = &bufferPool[frameNo]);

                            cout << "Character to write : ";
                            getCharInput(&toWrite);
                           

                            for(int i=0; i < settings::bufferPageSizeInBytes; i++){
                                temp->page.pageData[i] = toWrite;
                            }
			    temp->setInUse();
                            temp->setDirty();
			    temp->pageNo = pageNo;
                            //bm.write(pageNo,temp);
                            break;

		case 4 :    cout << "Page number : ";
			    getIntInput(&pageNo);
			    temp = bm.read(pageNo);
			    temp->clearInUse();
			    cout << "Frame " << temp->getFrameNo()<< " is not in use\n";
			    break;

		case 5 :    bm.commitBuffer();
			    break;

                case 0 :    exit(0);
                            break;

                default :   cout << "Invalid choice\n\n";
                            continue;

            }

        }
}
