bool DataManager::handleShowColumns(parsedQuery &){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowColumns()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		cout << endl;
		syscolrec.dump();
	}



	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
