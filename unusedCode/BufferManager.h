#include <string>
#include <limits>
#include <fstream>
#include <iostream>
#include <ctime>
#include "DriveManager.h"
#include "./../settings.h"

using namespace std;

#ifndef BM_CPP
#define BM_CPP

class BufferManager{

        frame* bufferPool;

        int no_of_hits;
        int no_of_reads;



	private:

	void write(int PageNo, frame* temp){
		
	    if(!temp->isDirty()){
		settings::verbose && cout << "Frame containing Page " << PageNo << " not dirty\n";
		return;
	    }
            writePage(PageNo,temp);
            temp->clearDirty();
	    temp->pageNo = PageNo;
        }


        public:

        frame* getNewPage(){
            frame* temp = new frame();
            return temp;
        }


        void setBufferPoolSize(){

            int tempsize;

            cout << "By default, " << settings::noOfPagesInBuffer << " pages are kept in buffer pool\n";

            cout << "No of pages in Buffer Pool ( 0 = use default ) : ";

            cin >> tempsize;

            cin.ignore(numeric_limits<int>::max(), '\n');

            if (!cin || cin.gcount() != 1 || tempsize < 0){
                cout << "Not a valid numeric value\n";
                tempsize = settings::noOfPagesInBuffer;
            }else
                if(tempsize == 0){
                    tempsize = settings::noOfPagesInBuffer;
                }


            settings::noOfPagesInBuffer = tempsize;

            settings::verbose && cout << "BufferPool = " << settings::noOfPagesInBuffer << " pages\n";

        }

        void initBufferPool(){
            bufferPool = new frame[settings::noOfPagesInBuffer];
	    for(int i=0; i<settings::noOfPagesInBuffer;i++){
		bufferPool[i].setFrameNo(i);		
	    }
            settings::verbose && cout << "Buffer Pool initialized\n";
        }

        void clearBufferPool(){
            delete[] bufferPool;
            settings::verbose && cout << "Buffer Pool cleared\n";
        }



    public:

        BufferManager(){
            setBufferPoolSize();
            initBufferPool();
            no_of_hits = 0;
            no_of_reads = 0;
        }

        ~BufferManager(){
            clearBufferPool();
        }


        //Set verbose mode
        void verboseMode(){

            cout << "Set Verbose Mode (0 - No ,  1 - Yes) : ";

            cin >> settings::verbose;

            cin.ignore(numeric_limits<int>::max(), '\n');

            if (!cin || cin.gcount() != 1)
                cout << "Not a numeric value\n";
            else{

                settings::verbose && cout << "Verbose Mode Set\n";

                !settings::verbose && cout << "Verbose Mode Cleared\n";


            }

        }


        

        frame* read(int PageNo){            

	    no_of_reads++;

            frame* readframe;

            int frameno = checkPageInBuffer(PageNo);

            if(frameno != -1){

		//Hurrah Its a Hit !!!

                readframe = &bufferPool[frameno];
		
                no_of_hits++;
                settings::verbose && cout << "Hit Ratio (" << no_of_hits << "/" << no_of_reads << ") : " << ((float)no_of_hits/no_of_reads) << endl;

                if(settings::verbose){
                        cout << "Read from buffer : \n";
                        cout.write((char*)readframe->page.pageData,settings::bufferPageSizeInBytes);
                        cout << endl;
                }

		readframe->setInUse();

                return readframe;

            }

	    //page not found in buffer

            //Replacement policy
            int victimFrameNo = getVictimFrameNo();
	    if(victimFrameNo == -1){
		cout << "\nUnable to find victim frame\n";
		exit(0);
	    }
		

	    if(settings::verbose){
		cout << "Replacement Victim Frame Details\nFrame : " << victimFrameNo << endl;
		cout << "LastUsed at : " << bufferPool[victimFrameNo].getLastUsedTime()/60 << " seconds from epoch\n";
		cout << "Priority : " << bufferPool[victimFrameNo].getPriority() << endl;
		cout << "Dirty : " << bufferPool[victimFrameNo].isDirty() << endl;
	    }

	    //if frame is dirty write back
	    if(bufferPool[victimFrameNo].isDirty()){
		write(bufferPool[victimFrameNo].pageNo,&bufferPool[victimFrameNo]);
	    }


	    readframe = &bufferPool[victimFrameNo];


            if(readPage(PageNo,readframe) == -1){

		//invalid pageno read
                cout << "Invalid page number. Page " << PageNo << " not found\n";
		no_of_reads--;
		PageNo = -1; //invalid page

            }else{

                if(settings::verbose){
                        cout << "Read from heapfile : \n";
                        cout.write((char*)readframe->page.pageData,settings::bufferPageSizeInBytes);
                        cout << endl;
                }

		readframe->setInUse();
            }

            readframe->pageNo = PageNo;
	    
            return readframe;

        }

        int checkPageInBuffer(int PageNo){
            for(int i=0; i< settings::noOfPagesInBuffer; i++){
                if(bufferPool[i].pageNo == PageNo){                    
                    return i;
                }
            }

            return -1;
        }

	int getVictimFrameNo(){

		//find frames which are not dirty and not in use. Find one frame which has lowest lastUsedTime and least priority
		int victimFrameNo = -1;
		time_t lastUsed = time (NULL) + 10000; //advance time, so that any frames time will be before that
		int priority = -1;
		
		for(int frameNo = 0; frameNo < settings::noOfPagesInBuffer; frameNo++){
			//cout << "\nCurrent\nFrame : " << victimFrameNo << " Priority : " << priority << " LastUsed : " << lastUsed << endl;
			//cout << "=======checking for frame " << frameNo << endl;
			//cout << "=======inUse : " << bufferPool[frameNo].isInUse() << " LastUsed : " << bufferPool[frameNo].getLastUsedTime() << " Priority : " << bufferPool[frameNo].getPriority() << endl;

			if(!bufferPool[frameNo].isInUse()){
			
				if( ( bufferPool[frameNo].getPriority() <= priority ) && ( bufferPool[frameNo].getLastUsedTime() < lastUsed ) ){

					//cout << "=======Candidate : frame " << frameNo << endl;

					priority = bufferPool[frameNo].getPriority();
					lastUsed = bufferPool[frameNo].getLastUsedTime();
					victimFrameNo = frameNo;
				}
			}

		}

		return victimFrameNo;
	}

	void commitBuffer(){
		for(int i=0; i < settings::noOfPagesInBuffer; i++){
			if(bufferPool[i].isDirty()){
				if(settings::verbose){
					cout << "Writing Frame " << i << " to disk\n";
				}
				write(bufferPool[i].pageNo,&bufferPool[i]);
				bufferPool[i].clearDirty();
			}
		}
	}
};
#endif
