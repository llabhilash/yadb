#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include "./../BufferManager/buffermanager.h"
#include "./../Common/Settings.h"
#include "./../Common/Priorities.h"
#include "./../Common/DataType.h"
#include "./../Common/SysPagesStartPos.h"
#include "./../Datastructures/alldatastructures.h"


using namespace std;

class DataManager{
	public:
	       bool handleCreateSchema(char *);
	       DataManager(){}
};

bool DataManager::handleCreateSchema(char * filename){
	FILE *fp;
	settings::init();
	//string 
	//add path to schema file
	cout<<FILENAME_SIZE<<"    "<<filename<<endl;
	char filewithpath[FILENAME_SIZE+10];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);
	cout<<filewithpath<<endl;
	if((fp = fopen(filewithpath,"r")) != NULL){
		cout<<"Can't create database '"<<filename<<"'; database exists"<<endl;
		fclose(fp);
		return false;
	}
        if((fp = fopen(filewithpath,"w")) == NULL){
	        cout<<"Can't create database '"<<filename<<"';database exists"<<endl;
	        return false;
        }
        fclose(fp);
        char *data = new char[PAGE_SIZE_IN_BYTES];
        DBHeader header = DBHeader(data);
        header.setNoOfPages(7);
        header.setLastPage(6);
	header.setFreePagePointer(-1);
	header.setSystemTablePointer(SYS_TAB_START_PAGE_NUM);
	header.setSystemColumnPointer(SYS_COL_START_PAGE_NUM);	
	header.setSystemIndexPointer(SYS_INDEX_START_PAGE_NUM);			
        BufferManager Buff;
        Buff.init(100000);
        cout<<Buff.setSchemaFile(filename)<<endl;
        Buff.write(DB_HEADER_PAGE_NUM,data,DB_HEADER_PRIORITY);
        delete[] data;
        data = new char[PAGE_SIZE_IN_BYTES];
        DirectoryPage dirPage = DirectoryPage(data);
        dirPage.init();
	DirectoryEntry de,de1;
	
	char *sysData = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage dbp = DataBasePage(sysData);
	SysTableRecord sysTab;
	RID rid;
	rid.pageNo = 4;
	//SYS_TABLE
	sprintf(sysTab.tableName,"%s","SYS_TABLE");
	sysTab.totalRows = 2;
	sysTab.startPage = SYS_TAB_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 7;
	dbp.init();
	dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	//SYS_COLUMN
	sprintf(sysTab.tableName,"%s","SYS_COLUMN");
	sysTab.totalRows = 8;
	sysTab.startPage = SYS_COL_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 6;
	dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	//cout<<"slotNo   "<<rid.slotNo<<endl;
	de.pageNo = 4;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_TAB_START_PAGE_NUM,data,DIR_PAGE_PRIORITY);
	Buff.write(4,sysData,DB_PAGE_PRIORITY);
	
	delete[] data,sysData;
	data = new char[PAGE_SIZE_IN_BYTES];
	sysData = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
        dirPage.init();
	dbp = DataBasePage(sysData);
	SysColumnRecord sysCol;
	rid.pageNo = 5;
	//SYS_TABLE->tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 0;
	sysCol.isNullable = false;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.dataType = VARCHARTYPE;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	dbp.init();
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalRows
	sprintf(sysCol.columnName,"%s","totalRows");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 1;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	////SYS_TABLE->createTime
        sprintf(sysCol.columnName,"%s","createTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->updateTime
	sprintf(sysCol.columnName,"%s","updateTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->startPage.
	sprintf(sysCol.columnName,"%s","startpage");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 4;
	sysCol.isNullable = false;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalDP
	sprintf(sysCol.columnName,"%s","totalDP");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 5;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalColumns
	sprintf(sysCol.columnName,"%s","totalColumns");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 6;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> COLUMN_NAME
	sprintf(sysCol.columnName,"%s","COLUMN_NAME");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.maxSize = MAX_COLUMNNAME_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> TABLE_NAME
	sprintf(sysCol.columnName,"%s","TABLE_NAME");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> COL_POS
	sprintf(sysCol.columnName,"%s","COLUMN_POS");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = SHORT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> dataType
	sprintf(sysCol.columnName,"%s","DATA_TYPE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 3;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = SHORT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_NULLABLE
	sprintf(sysCol.columnName,"%s","IS_NULLABLE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 4;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNullable = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_PRIMARY
	sprintf(sysCol.columnName,"%s","IS_PRIMARY");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 5;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNullable = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_UNIQUE
	sprintf(sysCol.columnName,"%s","IS_UNIQUE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 6;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNullable = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> MAX_SIZE
	sprintf(sysCol.columnName,"%s","MAX_SIZE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 7;
	sysCol.dataType = INTTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = INT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> DEFAULT_VALUE
	sprintf(sysCol.columnName,"%s","DEFAULT_VALUE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 8;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNullable = false;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.maxSize = MAX_DATATYPE_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
        
	de.pageNo = 5;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_COL_START_PAGE_NUM,data,SYS_PRIORITY);
	Buff.write(5,sysData,DB_PAGE_PRIORITY);
	
	
	dbp = DataBasePage(sysData);
        Buff.shutdown();
        Buff.init(100000);
        cout<<Buff.setSchemaFile(filename)<<endl;
        char *temp = new char[PAGE_SIZE_IN_BYTES];
        Buff.read(DB_HEADER_PAGE_NUM,temp,DB_HEADER_PRIORITY);
        DBHeader header1 = DBHeader(temp);
        header1.dump();
        delete[] temp;
        temp = new char[PAGE_SIZE_IN_BYTES];
        Buff.read(SYS_TAB_START_PAGE_NUM,temp,SYS_PRIORITY);
        DirectoryPage dp1 = DirectoryPage(temp);
        cout<<"Next Directory page  "<<dp1.getNextDirectoryPage()<<endl;
        cout<<"No of dir entries  "<<dp1.getNoOfDirectoryEntries()<<endl;
        dp1.getFirstDirectoryEntry(de1);
        cout<<"DE pageno  "<<de1.pageNo<<"  freespace"<<de1.freeSpace<<endl;
        dp1.dump();
        delete[] temp;
        temp = new char[PAGE_SIZE_IN_BYTES];
        Buff.read(4,temp,DB_PAGE_PRIORITY);
        DataBasePage dbp1 = DataBasePage(temp);
        SlotDirEntry se;
        cout<<"TFS = "<<dbp1.getTotalFreeSpace()<<endl;
        se = dbp1.getCFSTuple();
        cout<<"CFS ="<<se.length<<" Offset "<<se.offset<<endl;
        cout<<dbp1.getNoOfSlots()<<endl;
        dbp1.firstRecord(rid);
        char *rec;
        int len;
        dbp1.getRecord(rid,rec,len);
        cout<<len<<"  "<<sizeof(SysTableRecord)<<endl;
        memcpy((char *)&sysTab,rec,len);
        cout<<sysTab.tableName<<"   "<<sysTab.totalRows<<"  "<<sysTab.startPage<<"  ";
        cout<<sysTab.totalDP<<"  "<<sysTab.totalColumns<<endl;
        RID ri;
        delete[] rec;
        dbp1.nextRecord(rid,ri);
        dbp1.getRecord(ri,rec,len);
        memcpy((char *)&sysTab,rec,len);
        cout<<sysTab.tableName<<"   "<<sysTab.totalRows<<"  "<<sysTab.startPage<<"  ";
        cout<<sysTab.totalDP<<"  "<<sysTab.totalColumns<<endl;
        dbp1.dump();
        delete[] temp;
        
        temp = new char[PAGE_SIZE_IN_BYTES];
        Buff.read(5,temp,DB_PAGE_PRIORITY);
        dbp1 = DataBasePage(temp);
        dbp1.dump();
        dbp1.firstRecord(rid);
        dbp1.getRecord(rid,rec,len);
        memcpy((char *)&sysCol,rec,len);
        sysCol.dump();
        while(dbp1.nextRecord(rid,ri)){
	        dbp1.getRecord(ri,rec,len);    
	        memcpy((char *)&sysCol,rec,len);
	        rid = ri;
	        sysCol.dump();
        }
        return true;
}

int main(){
	DataManager DM = DataManager();
	char *filename = new char[20];
        cout<<"Enter filename"<<endl;
        cin>>filename;
        cout<<filename<<endl;
        DM.handleCreateSchema(filename);

}
