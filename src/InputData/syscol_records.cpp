
//SYS_COLUMN -> COLUMN_NAME
sprintf(sysCol.colName,"%s","COLUMN_NAME");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 0;
syscol.dataType = VARCHARTYPE;
syscol.isNullable = false;
syscol.isPrimary = true;
syscol.isUnique = false;
syscol.maxSize = MAX_COLUMNNAME_SIZE;

//SYS_COLUMN -> TABLE_NAME
sprintf(sysCol.colName,"%s","TABLE_NAME");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 1;
syscol.dataType = VARCHARTYPE;
syscol.isNullable = false;
syscol.isPrimary = true;
syscol.isUnique = false;
syscol.maxSize = MAX_TABLENAME_SIZE;

//SYS_COLUMN -> COL_POS
sprintf(sysCol.colName,"%s","COLUMN_POS");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 2;
syscol.dataType = SHORTTYPE;
syscol.isNullable = false;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = SHORT_SIZE;

//SYS_COLUMN -> dataType
sprintf(sysCol.colName,"%s","DATA_TYPE");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 3;
syscol.dataType = SHORTTYPE;
syscol.isNullable = false;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = SHORT_SIZE;

//SYS_COLUMN -> IS_NULLABLE
sprintf(sysCol.colName,"%s","IS_NULLABLE");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 4;
syscol.dataType = BOOLTYPE;
syscol.isNullable = true;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = BOOL_SIZE;

//SYS_COLUMN -> IS_PRIMARY
sprintf(sysCol.colName,"%s","IS_PRIMARY");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 5;
syscol.dataType = BOOLTYPE;
syscol.isNullable = true;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = BOOL_SIZE;

//SYS_COLUMN -> IS_UNIQUE
sprintf(sysCol.colName,"%s","IS_UNIQUE");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 6;
syscol.dataType = BOOLTYPE;
syscol.isNullable = true;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = BOOL_SIZE;

//SYS_COLUMN -> MAX_SIZE
sprintf(sysCol.colName,"%s","MAX_SIZE");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 7;
syscol.dataType = INTTYPE;
syscol.isNullable = false;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = INT_SIZE;

//SYS_COLUMN -> DEFAULT_VALUE
sprintf(sysCol.colName,"%s","DEFAULT_VALUE");
sprintf(sysCol.tableName,"%s","SYS_COLUMN");
syscol.colPos = 8;
syscol.dataType = VARCHARTYPE;
syscol.isNullable = false;
syscol.isPrimary = false;
syscol.isUnique = false;
syscol.maxSize = MAX_DATATYPE_SIZE;
