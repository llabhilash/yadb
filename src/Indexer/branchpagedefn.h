//branchpagedefn.h
//This file contains the function definations of methods of class BranchPage.
//////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BRANCHPAGEDEFN_H
#define BRANCHPAGEDEFN_H

#define PAGETYPE_START_POS (PAGE_SIZE_IN_BYTES -1 -sizeof(short) + 1)
#define FANOUT_START_POS (PAGETYPE_START_POS -sizeof(short))
#define TOTALKEYS_START_POS (FANOUT_START_POS -sizeof(short))
#define KEYSIZE_START_POS (TOTALKEYS_START_POS -sizeof(short))

using namespace std;

//BranchPage(char* &data)-Constructor for the class LeafPage.
BranchPage::BranchPage(char* &data){
	pageData = data;
}


//setPageType(const short &pageType)-This method is used to set the type of the page.i.e., whether it is a leafnode.
//or a intermidiate/root node
bool BranchPage::setPageType(const short &pageType){
	memcpy(&pageData[PAGETYPE_START_POS],(char *)&pageType,sizeof(short));
	return true;
}

//getPageType()-This method returns the type of the page i.e, whether its a leaf node or an intermidiate node.
short BranchPage::getPageType(){
	short pageType;
	memcpy((char *) &pageType,&pageData[PAGETYPE_START_POS],sizeof(short));
	return pageType;
}

//setMaximumNodeEntries(const short  &maxValue)- This method sets the maximum number of node entries i.e.,fanout.
bool BranchPage::setMaximumNodeEntries(const short  &maxValue){
	memcpy(&pageData[FANOUT_START_POS],(char *) &maxValue,sizeof(short));
	return true;
}

//getMaximumNodeEntries()-This method returns the fanout of the node.
short BranchPage::getMaximumNodeEntries(){
	short fanout;
	memcpy((char *) &fanout,&pageData[FANOUT_START_POS],sizeof(short));
	return fanout;
}


//setTotalKeysOccupied(const short &totalKeys)-This method sets the total Number of keys currently present in that node.
bool BranchPage::setTotalKeysOccupied(const short &totalKeys){
	memcpy(&pageData[TOTALKEYS_START_POS],(char *) &totalKeys,sizeof(short));
	return true;
}


//getTotalKeysOccupied()-This method returns the total number of keys currently present in that node
short BranchPage::getTotalKeysOccupied(){
	short totalKeys;
	memcpy((char *) &totalKeys,&pageData[TOTALKEYS_START_POS],sizeof(short));
	return totalKeys;
}


//setKeySize(const short &size)-This method sets the size of the indexkey.In case of composite key it should give the 
//size of the key after the key has been packed.
bool BranchPage::setKeySize(const short &size){
	memcpy(&pageData[KEYSIZE_START_POS],(char *) &size,sizeof(short));
	return true;
}


//getKeySize()-This method returns the size of the key used in indexing.
short BranchPage::getKeySize(){
	short keySize;
	memcpy((char *) &keySize ,&pageData[KEYSIZE_START_POS],sizeof(short));
	return keySize;
}


//insert(const vector<int> &keyType , const char* &key ,const RID &rid ,IndexNode &node)-This method inserts the
//tuple <rid ,key > into the leafpage in an indexed manner(sorted order).
//Here keyType is a vector containing the datatype of a key in case of composite key its consists of multiple rows of
//datatype arranged inthe order as it is to be indexed.Key represents the packed version of the key.
bool BranchPage::insert(const vector<int> &keyType , char* const &key ,const IndexNode &node ,bool &split){
        DM_FUNC_TRACE && cout << "BranchPage.insert()" << endl;
	int i;
	short totalKeys;
	bool result = false;
	int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
	
	if(getTotalKeysOccupied() == getMaximumNodeEntries()){
	//Split the BranchNode into Two.
		split = true;
		return false;
	}
	for(i = (getTotalKeysOccupied() -1 ) ; i >= 0 ; i --){
		if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
		if(result){
			break;
		}
		memcpy(&pageData[(i + 1) * (sizeof(int) + getKeySize())],record,recordSize);
	}
	if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	//construct the tuple<pageid,keyvalue,pageid>
	memcpy(record,(char *) &node.leftPtr ,sizeof(int));
	memcpy(&record[sizeof(int)],key,getKeySize());
	memcpy(&record[sizeof(int) + getKeySize()],(char *) &node.rightPtr,sizeof(int));
	
	memcpy(&pageData[(i + 1) * (sizeof(int) + getKeySize())],record,recordSize);
	
	totalKeys = getTotalKeysOccupied();
	setTotalKeysOccupied(totalKeys + 1);
	
	if(record != NULL){
		delete record;
	}
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	//set split = false indicating no need for split.
	split = false;
	return true;
}


//dump();
bool BranchPage::dump(const vector<int> &keyTypes){
	int i,j;
	int err = NO_ERROR;
	short totalKeys;
	bool result = false;
	int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int ptr;
        vector<string> recAttributes;
	cout <<"####################Branch Node Values###############################"<<endl;
        
        for(i = 0 ; i < getTotalKeysOccupied();i ++){
	        if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy((char *) &ptr,record,sizeof(int));
		cout << "Left Pointer  :" << ptr <<endl;
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		memcpy((char *) &ptr,&record[sizeof(int) + getKeySize()],sizeof(int));
		cout << "Right Pointer  :" << ptr <<endl;
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	        }
	        for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
	       }
	       cout << endl;
	}
        cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete record;
	if(nodeKeys != NULL)
		delete nodeKeys;
		
	return true;
}



//bool split(const vector<int> &keyType, IndexNode &node ,char* &branchleaf,const char* &key)-This method creates
// the new branch node whenever a split is necessary.
//This method fills half of the current nodes keys to the new branch.
bool BranchPage::split(const vector<int> &keyType, IndexNode &node ,char* &branchleaf,char* const &key){
	int i;
	short totalKeys;
	bool result = false;
        int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        bool split = false;
        bool inserted = false;
        IndexNode n1;
	
	BranchPage newPage = BranchPage(branchleaf);
	newPage.setPageType(getPageType());
	newPage.setMaximumNodeEntries(getMaximumNodeEntries());
	newPage.setTotalKeysOccupied(0);
	newPage.setKeySize(getKeySize());
	
	for(i = (getMaximumNodeEntries() -1) ; i >= (getMaximumNodeEntries() +1 )/2 ; i --){
		if(record != NULL){
			delete[] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		memcpy((char *) &n1.leftPtr,record ,sizeof(int));
		memcpy((char *) &n1.rightPtr,&record[sizeof(int) + getKeySize()],sizeof(int));
		n1.key = nodeKeys;
		
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
		if(result && (!inserted)){
			newPage.insert(keyType ,key ,node,split);
			inserted = true;
		}
		newPage.insert(keyType ,nodeKeys,n1,split);
		
		memcpy(&pageData[ (i * (sizeof(int) + getKeySize())) + sizeof(int)],"",(sizeof(int) + getKeySize()));
		totalKeys = getTotalKeysOccupied();
		setTotalKeysOccupied(totalKeys - 1);
	}
	if(!inserted)
		insert(keyType,key,node,split);
	if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	memcpy(record ,branchleaf,recordSize);
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	memcpy(node.key,nodeKeys,getKeySize());
	memmove(branchleaf,&branchleaf[sizeof(int) + getKeySize()],(newPage.getTotalKeysOccupied() - 1)*recordSize);
	totalKeys = newPage.getTotalKeysOccupied();
	newPage.setTotalKeysOccupied(totalKeys - 1);
	DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$Dump of new Branch$$$$$$$$$$$$$$$" << endl;
	DM_FUNC_TRACE && newPage.dump(keyType);
	if(record != NULL){
		delete record;
	}
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	
	return true;	
}


//findNextNode(const vector<int> &keyType ,char* const &key,int &pageNo)-This method returns pointer to nextNode.
//i.e., its sets the pageNo to the next node.
bool BranchPage::findNextNode(const vector<int> &keyType ,char* const &key,int &pageNo){
        DM_FUNC_TRACE && cout << "BranchPage.findNextNode() " << endl;
	int i;
	short totalKeys;
	bool result = false;
	bool result1 = false;
        int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        
        if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	memcpy(record ,pageData,recordSize);
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	
        compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
        if(!result){
	        memcpy((char *) &pageNo,record ,sizeof(int));
	        return true;
        }
        
        
        if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	memcpy(record ,&pageData[(getTotalKeysOccupied() -1) * (sizeof(int) + getKeySize())],recordSize);
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	
        compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
        if(result){
	        memcpy((char *) &pageNo,&record[sizeof(int) + getKeySize()] ,sizeof(int));
	        return true;
        }
        
        
        for(i = 0 ; i < (getTotalKeysOccupied() -1); i ++){
	        if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
		
		
		if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ (i + 1)* (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result1);
                if(result && (!result1)){
	                memcpy((char *) &pageNo,record ,sizeof(int));
		        return true;
                }
        }
        return false;
}


//Used for comparision of keys
bool BranchPage::compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes, const int &keySize1, const int &keySize2, bool &result){

	//return true if key1 is >= key2


	DM_FUNC_TRACE && cout << "BranchPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	
	vector<string> recAttributes1, recAttributes2;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	if(!pack::unpack_data(recAttributes2, keyTypes, key2, keySize2)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	for(int i = 0; i < keyTypes.size(); i++){
	
		//convert both attributes
		if( !pack::convert_to_type(recAttributes1[i], keyTypes[i], att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(recAttributes2[i], keyTypes[i], att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
		/*
		if(keyTypes[i] == VARCHARTYPE){
			toReturn = strcmp((char*)att_value1, (char*)att_value2);
			//if unequal.. stop
			if(toReturn != 0)
				goto ret;
		}else{
			if()		
		}
		*/
		switch(keyTypes[i]){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;


								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							
		 							//unequal.. stop
		 							goto ret;
		 						}
		 						
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
		 								 						
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;	 						
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
					case VARCHARTYPE:								
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 						
		 						delete (char*)att_val1;
		 						delete (char*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)			 						
			 						goto ret;
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;			
		}		
	}
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(toReturn < 0)
		result = false;
	else
		result = true;
	return err==NO_ERROR?true:false;	
}
#endif
