#include <iostream>
#include <string>
#include <vector>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
//#include "./../Common/DataType.h"
#include "./../Parser/parsedQuery.h"
#include "./../Common/Settings.h"
#include "./../Common/ErrorCodes.h"
#include "./../Common/Priorities.h"
#include "./../Common/Packing_new.h"
#include "./../Datastructures/alldatastructures.h"
#include "./../BufferManager/buffermanager.h"
#include "./../Datastructures/pagedataentries.h"


#ifndef DATAMANAGER_H
#define DATAMANAGER_H
#include "datamanagerdecl.h"
#include "datamanagerdefn.h"
#endif
