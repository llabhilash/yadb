bool LeafPage::compareGTE(const char* &key1, const char* &key2, const vector<int> &keyTypes, const int &keySize1, const int &keySize2, bool &result){

	//return true if key1 is >= key2


	DM_FUNC_TRACE && cout << "LeafPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	
	vector<string> recAttributes1, recAttributes2;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	if(!pack::unpack_data(recAttributes2, keyTypes, key2, keySize2)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	for(int i = 0; i < keyTypes.size(); i++){
	
		//convert both attributes
		if( !pack::convert_to_type(recAttributes1[i], keyTypes[i], att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(recAttributes2[i], keyTypes[i], att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
		/*
		if(keyTypes[i] == VARCHARTYPE){
			toReturn = strcmp((char*)att_value1, (char*)att_value2);
			//if unequal.. stop
			if(toReturn != 0)
				goto ret;
		}else{
			if()		
		}
		*/
		switch(keyTypes[i]){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;


								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							
		 							//unequal.. stop
		 							goto ret;
		 						}
		 						
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
		 								 						
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;	 						
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
					case VARCHARTYPE:								
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 						
		 						delete (char*)att_val1;
		 						delete (char*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)			 						
			 						goto ret;
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;			
		}		
	}
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(toReturn >= 0)
		result = true;
	else
		result = false;
	return err==NO_ERROR?true:false;	
}
