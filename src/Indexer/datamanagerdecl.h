#ifndef DATAMANAGER_DECL_H
#define DATAMANAGER_DECL_H

class DataManager{

	private:
	BufferManager bm;
	
	
	
	public:	
	DataManager();
	~DataManager();
	bool init(int);
	bool shutdown();
	bool handleShowSchemas();
	bool getFreePage(char* &, int&, DBHeader &);	//buffer, returns pageNum
	bool handleCreateTable(parsedQuery &pq);	
	bool handleUseSchema(parsedQuery &);
	bool insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader,RID &rid);
	bool handleShowTables(parsedQuery &);
	bool handleShowColumns(parsedQuery &);
	bool handleCreateSchema(parsedQuery &);
	bool handleSelect(parsedQuery &);
	bool handleInsert(parsedQuery &);
        bool handleDropSchema(parsedQuery &);
        bool handleDropTable(parsedQuery &pq);
	bool handleDescTable(parsedQuery &pq);
	bool handleDelete(parsedQuery &pq);
	bool insertIntoIndex(int &pageNo,const vector<int> &keyType ,char* const &key ,IndexNode &node ,
	              const RID &rid ,bool &split,DBHeader &dbHeader);
	bool handleIndexInsert(const int &indexerpage,const vector<int> &keyType,char* const &key,
	              const RID &rid,DBHeader &dbHeader);
        
        private:
        bool getSysTabRecForTable(string name, SysTableRecord &sysrec, DBHeader &dbHeader, RID &rid);
        bool getSysColRecsForTable(string name,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns, vector<RID> &sysColRIDs);
        bool getAllRecords(int startDirPageNo, vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs);
        bool markAsFreePage(int pageNo, DBHeader& dbHeader);
        bool deleteRecord(const int &startDP ,const RID &rid,DBHeader &dbHeader);
        bool checkDataType(int from, int to);
};

#endif
