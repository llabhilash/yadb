using namespace std;

#ifndef DATAMANAGER_DEFN_H
#define DATAMANAGER_DEFN_H

//=======================================================================
DataManager::DataManager(){}
//=======================================================================
DataManager::~DataManager(){}
//=======================================================================
bool DataManager::init(int ramSizeToUseInBytes){
		
	if(!bm.init(ramSizeToUseInBytes)){
		cout << "Cannot initialize buffer manager\n";
		return false;
	}
	
	return true;
}
//=======================================================================
bool DataManager::shutdown(){
	return bm.shutdown();
}
//=======================================================================

bool DataManager::getFreePage(char* &pageBuf, int& pageNum, DBHeader& dbheader){
	
	int err = NO_ERROR;
	char *freePageBuf = new char[PAGE_SIZE_IN_BYTES];
	FreePage *freePage = NULL;
	int nextFreePage;
	DM_FUNC_TRACE && cout<<"DM.getFreePage()\n";
	string dummy;
	
	//check if dbheader has free page pointer == -1		
	if(dbheader.getFreePagePointer() == -1){
	
		//expand heapfile
		
		if(!bm.expandHeapFile(dbheader.getLastPage())){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
				
		//set current Last Page + 1 as free page
		if(!dbheader.setFreePagePointer(dbheader.getLastPage() + 1)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		
		//increment noOfPages by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setNoOfPages(dbheader.getNoOfPages() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		//increment LastPage by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setLastPage(dbheader.getLastPage() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}	
		
	}
	
	//read free page pointer into buffer pool
	pageNum = dbheader.getFreePagePointer();
	if(!bm.read(pageNum, freePageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	freePage = new FreePage(freePageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);
	
	DM_FUNC_TRACE && cout << "Setting new free page pointer in DBHeader : " << nextFreePage << endl;
	//cin >> dummy;
	
	if(!bm.read(pageNum, pageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}	
	/*//Reset the free page int the dbheader......
	freePage = new FreePage(pageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);*/
	ret: 

	DM_FUNC_TRACE && printErrorCode(err);
	DM_FUNC_TRACE && cout << "Sending AS FreePage : " << pageNum << endl;
	return err==NO_ERROR?true:false;
	
}
//=======================================================================
bool DataManager::handleShowSchemas(){

	int err = NO_ERROR;
	DM_FUNC_TRACE && cout<<"DM.handleShowSchemas()\n";

	vector<string> schemas;

	//find all directories in FILENAME_PATH and add them to schemas
	DIR *dp;
	struct dirent *dirp;
	
	if((dp  = opendir(FILENAME_PATH)) == NULL) {
        	cout << "Error(" << errno << ") opening " << FILENAME_PATH << endl;
        	err = READ_ERROR;
        	goto ret;
    	}

    	while ((dirp = readdir(dp)) != NULL) {
    		
    		//skip .,..,.svn
    		if(string(dirp->d_name).compare(".") == 0 || string(dirp->d_name).compare("..") == 0 || string(dirp->d_name).at(0) == '.'){
    			continue;
    		}
    	
        	schemas.push_back(string(dirp->d_name));
        	cout << dirp->d_name<<"\n";
    	}
    	
   	closedir(dp);
   	
   	ret: 
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleCreateTable(parsedQuery &pq){

	int err = NO_ERROR;
	RID rid;
	
	DM_FUNC_TRACE && cout << "DM.handleCreateTable()\n";
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char *indexpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	//create sys_table record for tablename
	SysTableRecord sysTabRec, tempSysTabRec;
	sprintf(sysTabRec.tableName,"%s",pq.getTable().c_str());	//Add Table name
	sysTabRec.totalRows = 0;					//Intially there are 0 rows
	//sysTabRec.createTime;
	//sysTabRec.updateTime;
	sysTabRec.startPage = -1;//Page where the first DP for this table is
	sysTabRec.totalDP = 0;						//There are no DPs yet
	sysTabRec.totalColumns = pq.getNoOfColumns();			//Total number of columns for this table
	
	vector<string> columns;		//COLUMNS captured
	vector<int> types;			//COLUMN DATATYPES captured
	vector<bool> notNulls;		//COLUMN is NULL ?
	vector<bool> pks;				//COLUMN is PrimaryKey ?
	vector<bool> unique;			//COLUMN is Unique ?
	vector<int> maxsizes;			//COLUMN Max Size
	
	SysColumnRecord* sysColRecs = NULL;
	DBHeader* dbHeader = NULL;
	DirectoryPage* dirpage = NULL;
	LeafPage *leafpage = NULL;
	IndexPage *indexpage = NULL;
	int newpage;
	
	RID tempRID;
	
	//DUMP
	//sysTabRec.dump();
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	
	//get sys tab record for the table
	if(getSysTabRecForTable(pq.getTable(), tempSysTabRec, (*dbHeader), tempRID)){
		//Table already exists
		err = INVALIDTABLENAME;
		cout << "Table '" << pq.getTable() << "' already exists\n";
		goto ret;
	}
	
	//check if column name is duplicate
	columns = pq.getColumns();		//COLUMNS captured
	for(int i = 0; i < pq.getNoOfColumns();i++){
		for(int j=0; j < i; j++){
			//check current col name all prev col names
			if(strcmp(columns[j].c_str(), columns[i].c_str()) == 0){
				err = INVALID;
				cout << "Column names for a table have to be unique\n";
				goto ret;
			}
		}
	}
	//create new DirPage for this table
	if(!getFreePage(dirpagebuf, sysTabRec.startPage, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	//====================to be altered later========================
	/*
	if(dirpage != NULL)
		delete dirpage;
	dirpage = new DirectoryPage(dirpagebuf);
	dirpage->init();
	*/
	//===============================================================
	if(!getFreePage(indexpagebuf, newpage, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(dirpagebuf);
	indexpage->setPageType(INDEXHEADER);
	indexpage->setMaximumNodeEntries(4);
	indexpage->setKeySize(8);
	indexpage->setIndexerRoot(newpage);
	indexpage->dump();
	
	leafpage = new LeafPage(indexpagebuf);
	leafpage->setPageType(LEAFNODE);
	leafpage->setMaximumNodeEntries(4);
	leafpage->setTotalKeysOccupied(0);
	leafpage->setKeySize(8);
	leafpage->setNextLeaf(-1);
	leafpage->setPrevLeaf(-1);
	leafpage->dump(pq.getTypes());
	//write back leafpage to BM.
	if(!bm.write(newpage, indexpagebuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	//write back to BM
	if(!bm.write(sysTabRec.startPage, dirpagebuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	if(!insertRecordIntoDataPage((char *)&sysTabRec,sizeof(SysTableRecord),dbHeader->getSystemTablePointer(),(*dbHeader),rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	
	//create sys_col records for each column
	sysColRecs = new SysColumnRecord[pq.getNoOfColumns()];		//Create as many sys col records as columns
	
	columns = pq.getColumns();		//COLUMNS captured
	types = pq.getTypes();			//COLUMN DATATYPES captured
	notNulls = pq.getNotNulls();		//COLUMN is NULL ?
	pks = pq.getPKs();				//COLUMN is PrimaryKey ?
	unique = pq.getUnique();			//COLUMN is Unique ?
	maxsizes = pq.getMaxSize();			//COLUMN Max Size
	
	for(int i=0; i < pq.getNoOfColumns(); i++){
		sprintf(sysColRecs[i].columnName,"%s",columns[i].c_str());		//Add column name
		sprintf(sysColRecs[i].tableName,"%s",pq.getTable().c_str());		//Add table name
		sysColRecs[i].colPos = i;						//Column order in table
		sysColRecs[i].dataType = types[i];					//Column data type
		sysColRecs[i].isNotNull = notNulls[i];					//Column is Nullable ?
		sysColRecs[i].isPrimary = pks[i];					//Column is Primary ?		
		sysColRecs[i].isUnique = unique[i];					//Column is Unique ?
		sysColRecs[i].isDeleted = false;					//Column is Deleted ?
		sysColRecs[i].maxSize	= maxsizes[i];					//max size for the Column

		
		//check for constraint in compatibility
		//if col is primary, then set unique and not null to true
		if(pks[i]){
			sysColRecs[i].isNotNull = true;
			sysColRecs[i].isUnique = true;
		}
		
		
		
		//sysColRecs[i].dump();
               	if(!insertRecordIntoDataPage((char *)&sysColRecs[i],sizeof(SysColumnRecord),
                	dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }
	}
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(dirpagebuf != NULL)
		delete dirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(dbheadbuf != NULL)
		delete  dbheadbuf;
	if(dbHeader != NULL)
		delete dbHeader;
	if(indexpagebuf != NULL)
		delete indexpagebuf;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	delete[] sysColRecs;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleUseSchema(parsedQuery &pq){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleUseSchema()\n";
	
	if(!bm.setSchemaFile((char *)pq.getDatabase().c_str())){
		err = USE_SCHEMA_ERROR;
		goto ret;
	}
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleShowTables(parsedQuery &){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowTables()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	cout << endl;
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		cout << endl;
		//systabrec.dump();
		cout << systabrec.tableName << endl;
	}
	cout << endl;

	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

//=======================================================================
bool DataManager::handleCreateSchema(parsedQuery &pq){
	char *filename = new char[pq.getDatabase().length()];
	strcpy(filename,pq.getDatabase().c_str());
        FILE *fp;
	settings::init();
	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);
	//cout<<filewithpath<<endl;
	if((fp = fopen(filewithpath,"r")) != NULL){
		cout<<"Can't create database '"<<filename<<"'; database exists"<<endl;
		fclose(fp);
		return false;
	}
        if((fp = fopen(filewithpath,"w")) == NULL){
	        cout<<"Can't create database '"<<filename<<"';database exists"<<endl;
	        return false;
        }
        fclose(fp);
        char *data = new char[PAGE_SIZE_IN_BYTES];
        DBHeader header = DBHeader(data);
        header.setNoOfPages(7);
        header.setLastPage(6);
	header.setFreePagePointer(-1);
	header.setSystemTablePointer(SYS_TAB_START_PAGE_NUM);
	header.setSystemColumnPointer(SYS_COL_START_PAGE_NUM);	
	header.setSystemIndexPointer(SYS_INDEX_START_PAGE_NUM);			
        BufferManager Buff;
        Buff.init(100000);
        Buff.setSchemaFile(filename);
        Buff.write(DB_HEADER_PAGE_NUM,data,DB_HEADER_PRIORITY);
        delete[] data;
        data = new char[PAGE_SIZE_IN_BYTES];
        DirectoryPage dirPage = DirectoryPage(data);
        dirPage.init();
	DirectoryEntry de,de1;
	
	char *sysData = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage dbp = DataBasePage(sysData);
	SysTableRecord sysTab;
	RID rid;
	rid.pageNo = 4;
	//SYS_TABLE
	sprintf(sysTab.tableName,"%s","SYS_TABLE");
	sysTab.totalRows = 2;
	sysTab.startPage = SYS_TAB_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 7;
	dbp.init();
	dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	//SYS_COLUMN
	sprintf(sysTab.tableName,"%s","SYS_COLUMN");
	sysTab.totalRows = 8;
	sysTab.startPage = SYS_COL_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 6;
	dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	//cout<<"slotNo   "<<rid.slotNo<<endl;
	de.pageNo = 4;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_TAB_START_PAGE_NUM,data,DIR_PAGE_PRIORITY);
	Buff.write(4,sysData,DB_PAGE_PRIORITY);
	
	delete[] data;
	delete[] sysData;
	data = new char[PAGE_SIZE_IN_BYTES];
	sysData = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
        dirPage.init();
	dbp = DataBasePage(sysData);
	SysColumnRecord sysCol;
	rid.pageNo = 5;
	//SYS_TABLE->tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 0;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = VARCHARTYPE;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	dbp.init();
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalRows
	sprintf(sysCol.columnName,"%s","totalRows");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 1;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	////SYS_TABLE->createTime
	/*
        sprintf(sysCol.columnName,"%s","createTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->updateTime
	sprintf(sysCol.columnName,"%s","updateTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);*/
	//SYS_TABLE->startPage.
	sprintf(sysCol.columnName,"%s","startpage");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalDP
	sprintf(sysCol.columnName,"%s","totalDP");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->totalColumns
	sprintf(sysCol.columnName,"%s","totalColumns");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 4;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> COLUMN_NAME
	sprintf(sysCol.columnName,"%s","COLUMN_NAME");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_COLUMNNAME_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> TABLE_NAME
	sprintf(sysCol.columnName,"%s","TABLE_NAME");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> COL_POS
	sprintf(sysCol.columnName,"%s","COLUMN_POS");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> dataType
	sprintf(sysCol.columnName,"%s","DATA_TYPE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 3;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_NULLABLE
	sprintf(sysCol.columnName,"%s","IS_NULLABLE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 4;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_PRIMARY
	sprintf(sysCol.columnName,"%s","IS_PRIMARY");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 5;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> IS_UNIQUE
	sprintf(sysCol.columnName,"%s","IS_UNIQUE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 6;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> MAX_SIZE
	sprintf(sysCol.columnName,"%s","MAX_SIZE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 7;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_COLUMN -> DEFAULT_VALUE
	sprintf(sysCol.columnName,"%s","DEFAULT_VALUE");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 8;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_DATATYPE_SIZE;
        dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
        
	de.pageNo = 5;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_COL_START_PAGE_NUM,data,SYS_PRIORITY);
	Buff.write(5,sysData,DB_PAGE_PRIORITY);
	
	Buff.shutdown();
	
	
        cout<<"Schema '"<<filename<<"' created successfully"<<endl;
        delete[] filename;
        delete[] sysData;
        delete[] data;
        return true;
	
}

//=======================================================================
bool DataManager::handleShowColumns(parsedQuery &){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowColumns()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		cout << endl;
		syscolrec.dump();
	}



	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;


}

//=======================================================================
bool DataManager::getSysTabRecForTable(string tabName, SysTableRecord &sysrec, DBHeader &dbHeader, RID &rid){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysTabRecForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and check
		memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		//cout << endl;
		//systabrec.dump();
		if(tabName.compare(systabrec.tableName) == 0){
			sysrec = systabrec;
			rid = allRIDs[i];
			goto ret;
		}
	}

	//no table found
	err = INVALIDTABLENAME;
	cout << "Table '" << tabName << "' does not exist" << endl;

	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================
bool DataManager::getSysColRecsForTable(string tabName,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns, vector<RID> &sysColRIDs){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysColRecsForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	//RID tempRID;

	bool swapped = false;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec,syscolrec1;
	
	int i=0, n=0;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		//cout << endl;
		//syscolrec.dump();
		if(tabName.compare(syscolrec.tableName) == 0){
			sysColumns.push_back(syscolrec);
			sysColRIDs.push_back(allRIDs[i]);
		}
	}

	/*procedure bubbleSort( A : list of sortable items ) defined as:
  n := length( A )
  do
    swapped := false
    n := n - 1
    for each i in 0 to n  do:
      if A[ i ] > A[ i + 1 ] then
        swap( A[ i ], A[ i + 1 ] )
        swapped := true
      end if
    end for
  while swapped
end procedure

*/
        n = sysColumns.size();
	do{
		swapped = false;
		n = n-1;
		for(i = 0; i < n ; i++){
			syscolrec = sysColumns[i];
			syscolrec1 = sysColumns[i + 1];
			if(syscolrec.colPos > syscolrec1.colPos){				
				//swap			
				swap(sysColumns[i],sysColumns[i + 1]);				
				swap(sysColRIDs[i],sysColRIDs[i+1]);				
				swapped = true;
			}
		}
	}while(swapped);


	ret:
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleSelect(parsedQuery &pq){

	DM_FUNC_TRACE && cout << "DM.handleSelect()\n";
	int err = NO_ERROR;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	
	vector<int> colTypes;
	string str;
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	RID rid;
	
	int i=0, j=0;
	char* recbuf = NULL;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	//populate column types
	for(i=0; i<sysColRecs.size(); i++){
		colTypes.push_back(sysColRecs[i].dataType);
	}
	
	
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}
	
	
	for(j=0; j<allrecs.size(); j++){		
		//populate attributes
		//cout<<"Packed Record: "<<allrecs[j]<<endl;
		recAttributes.clear();
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[j].c_str(), recsizes[j])){
			err = UNPACK_ERROR;
			goto ret;
		}
		for(i=0; i<recAttributes.size(); i++){
		
			if( !pack::convert_to_type(recAttributes[i], colTypes[i], att_value)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			//output the data
			cout << sysColRecs[i].columnName << " ( " << sysColRecs[i].getTypeName(sysColRecs[i].dataType) << " ): " << recAttributes[i] << endl; 
		
			
			//Handle each attribute	
		/*
			switch( colTypes[i] ){
			
				case INTTYPE: if( !pack::convert_to_type(recAttributes[i],INTTYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"INTType: "<<colTypes[i]<<" Value: NULL"<<endl;
									}
								else{
								cout<<"INTType: "<<colTypes[i]<<" Value: "<< *((int*)att_value)<<endl;
								delete (int*)att_value;
								}
							}
							break;
				case FLOATTYPE:if(!pack::convert_to_type(recAttributes[i],FLOATTYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"FLOATType: "<<colTypes[i]<<" Value: NULL"<<endl;
								}
								else{
								cout<<"FLOATType: "<<colTypes[i]<<" Value: "<< *((float*)att_value)<<endl;
								delete (float*)att_value;
								}
							}
							break;
				case SHORTTYPE:if(!pack::convert_to_type(recAttributes[i],SHORTTYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"SHORTType: "<<colTypes[i]<<" Value: NULL"<<endl;
								}
								else{
								cout<<"SHORTType: "<<colTypes[i]<<" Value: "<< *((short*)att_value)<<endl;
								delete (short*)att_value;
								}
							}
							break;
				case LONGTYPE:	if(!pack::convert_to_type(recAttributes[i],LONGTYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"LONGType: "<<colTypes[i]<<" Value: NULL"<<endl;
								}
								else{
								cout<<"LONGType: "<<colTypes[i]<<" Value: "<< *((long*)att_value)<<endl;
								delete (long*)att_value;
								}
							}
							break;
				case DOUBLETYPE: if(!pack::convert_to_type(recAttributes[i],DOUBLETYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"DOUBLEType: "<<colTypes[i]<<" Value: NULL"<<endl;
								}
								else{
								cout<<"DOUBLEType: "<<colTypes[i]<<" Value: "<< *((double*)att_value)<<endl;
								delete (double*)att_value;
								}	
							}
							break;
				case VARCHARTYPE:if(!pack::convert_to_type(recAttributes[i],VARCHARTYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
							str = (char*)att_value;
							cout<<"VARCHARType: "<<colTypes[i]<<" Value: "<< str << endl;//(char*)att_value << endl;						
							}
							break;
				case DATETYPE: if(!pack::convert_to_type(recAttributes[i],DATETYPE,att_value)){
								err = CONVERT_ERROR;
								goto ret;
							}
							else{
								if(!recAttributes[i].compare("NULL")){
									cout<<"SHORTType: "<<colTypes[i]<<" Value: NULL"<<endl;
								}
							else{								
							cout<<"DATEType: "<<colTypes[i]<<" Value: "<< *((int*)att_value)<<endl;
							delete (int*)att_value;
							}
							}
							break;
			}
			*/
			
		}
			
	}
	
	
	ret:
	if(dbhead != NULL)
		delete dbhead;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//=======================================================================
bool DataManager::getAllRecords(int startDirPageNo, vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs){
	
	DM_FUNC_TRACE && cout << "DM.getAllRecords()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	char* recbuf = NULL;	
	int recSize;
	
	string* tempstring = NULL;
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID curRID,nextRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == -1){
				//empty DE - goto next DE
				continue;
			}
			
			//read page in DE into BufferPool
			if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			
			if(DataPage != NULL){
				delete DataPage;
			}
			DataPage = new DataBasePage(datapagebuf);
			
			
			curRID.pageNo = curDataPageNo;
			//read first sys-col RID
			if(!DataPage->firstRecord(curRID)){
				//empty DataBasePage.. goto next page
				continue;
			}
			
			nextRID.pageNo = curRID.pageNo;
			nextRID.slotNo = curRID.slotNo;
			
			//read records
			do{			
				//RECORD LEVEL
				
				curRID.pageNo = nextRID.pageNo;
				curRID.slotNo = nextRID.slotNo;
				
				if(recbuf != NULL){
					delete[] recbuf;
				}
				
				if(!DataPage->getRecord(curRID,recbuf,recSize)){
					//read error
					err = RECORD_READ_ERROR;
					goto ret;
				}			
				
				//memcopy buf into systabrec
				//memcpy((char*)&syscolrec,recbuf,recSize);
				
				//add record to allrecs
				if(tempstring != NULL)
					delete tempstring;
				tempstring = new string(recbuf,recSize);
				allrecs.push_back((*tempstring));				
				recsizes.push_back(recSize);
				allRIDs.push_back(curRID);
			
			}while(DataPage->nextRecord(curRID, nextRID));

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(recbuf != NULL){
		delete[] recbuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	if(tempstring != NULL)
		delete tempstring;	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//=======================================================================



//insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader)-This method inserts a record
//           into the database given a record ,its length ,start directory page and DBHeader instance.
//=========================================================================
bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader ,RID &recRid){

	DM_FUNC_TRACE && cout << "DM.insertRecordIntoDataPage()\n";
	
	string dummy = "";

	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
        char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	RID rid;
	int newPageNo;
      
	//Try and insert into existing DE
	//DM_FUNC_TRACE && cout << "Try and insert into existing DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(!dirpage->getFirstDirectoryEntry(curDE)){
			continue;
		}
		
		nextDE = curDE;		
		do{
			curDE = nextDE;
			//DM_FUNC_TRACE && cout << "Looking for free space in a DE in page : " << curDE.pageNo <<",next =  " << nextDE.pageNo << endl;
			//DM_FUNC_TRACE && cin >> dummy;			
			if(curDE.freeSpace >= (short)(reclen + SLOT_DIR_SIZE)){
				if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = DATABASEPAGE_READ_ERROR;
					goto ret;
				}				
				rid.pageNo = curDE.pageNo;
				if(datapage != NULL){
					delete datapage;
				}
				datapage = new DataBasePage(datapagebuf);
				
				if(DM_FUNC_TRACE){
					cout << "Before inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
				if(!datapage->insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}
				//databasepage dump
				if(DM_FUNC_TRACE){
					cout << "After inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				curDE.freeSpace = datapage->getTotalFreeSpace();
				if(DM_FUNC_TRACE){ 
					//cout << "Before updating DE to DirPage\n";
					//dirpage->dump();
				}
				
				dirpage->updateDirectoryEntry(curDE);
				
				bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
				bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
				
				if(DM_FUNC_TRACE){
					cout << "After updating DE to DirPage\n";
					dirpage->dump();
				}
				
				DM_FUNC_TRACE && cout << "Adding to existing DE, in DirPage : " << curDP << endl;
				DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
				DM_FUNC_TRACE && cout << "Record Length "<< reclen << endl;
				
				//cin >> dummy;
				recRid.pageNo = curDE.pageNo;
				recRid.slotNo =rid.slotNo;
				goto ret;
				
			}
		}while(dirpage->getNextDirectoryEntry(curDE,nextDE));
		
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//All DEs are full
	
	//Try and insert a new DE	
	DM_FUNC_TRACE && cout << "Try and insert new DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	curDP = startPage;
	
	do{
	
	
	        //cout<<"ALL are out########################################"<<endl;
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->isAnyDEEmpty()){
		
			DM_FUNC_TRACE && cout << "Inserting new DE in to DirPage : " << curDP << endl;
			//DM_FUNC_TRACE && cin >> dummy;
		        
		        if(datapagebuf != NULL){
		        	delete[] datapagebuf;
		        }
			datapagebuf = new char[PAGE_SIZE_IN_BYTES];
			
			if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			
			curDE.pageNo = newPageNo;
			//dirpage->insertDirectoryEntry(curDE);
			RID rid;
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			datapage->init();
			
			if(!datapage->insertRecord(record,reclen,rid)){
			        err = DATABASEPAGE_WRITE_ERROR;
			        goto ret;
			}
		        curDE.freeSpace = datapage->getTotalFreeSpace();
		        //dirpage->updateDirectoryEntry(curDE);
		        
		        DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
			DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "Before updating DE to DirPage\n";
				//dirpage->dump();
			}
		        
		        dirpage->insertDirectoryEntry(curDE);
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "After updating DE to DirPage\n";
				//dirpage->dump();
			}
			
			bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
			bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);

			
			
			DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
			DM_FUNC_TRACE && cout << "Record Length "<< reclen << endl;
			recRid.pageNo = curDE.pageNo;
			recRid.slotNo =rid.slotNo;
			//cin >> dummy;
			
			goto ret;
			
			
		}
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//ALL DEs in all DPs are full
	///creation of new DP
	
	//add a new DP to the end of the chain
	//Find the last DP in the chain
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	
	//dirpage has the last DP
	//curDP has the last DP page num
	//create a new DP, add link to it
	if(lastdirpagebuf != NULL){
		delete[] lastdirpagebuf;
	}
	lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	if(datapagebuf != NULL){
        	delete[] datapagebuf;
        }
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	curDE.pageNo = newPageNo;
	lastdirpage->insertDirectoryEntry(curDE);
	
	rid.pageNo = curDE.pageNo;
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	
	if(!datapage->insertRecord(record,reclen,rid)){
	        err = DATABASEPAGE_WRITE_ERROR;
	        goto ret;
	}
	
        curDE.freeSpace = datapage->getTotalFreeSpace();
        dirpage->updateDirectoryEntry(curDE);
	bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);	//write out current last page
	bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY);	//write out new data page
	bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);	//write out new last dir page
	
	
	DM_FUNC_TRACE && cout << "Adding new DirPage : " << dirpage->getNextDirectoryPage() << " , Current Last DirPage : "<< curDP << endl;
	DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
	DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
	DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
	DM_FUNC_TRACE && cout << "Record Length "<< reclen << endl;
	//cin >> dummy;
	recRid.pageNo = curDE.pageNo;
	recRid.slotNo =rid.slotNo;
	goto ret;

	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(datapagebuf != NULL)
		delete[] datapagebuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;//remove allocated memory
	if(lastdirpagebuf != NULL)
		delete[] lastdirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(lastdirpage != NULL)
		delete lastdirpage;
	if(datapage != NULL)
		delete datapage;
	return err==NO_ERROR?true:false;
}
//===========================================================================================

bool DataManager::handleInsert(parsedQuery &pq){
        int err = NO_ERROR;
        int i = 0, j = 0;
        int startPage;
        int size;
        DM_FUNC_TRACE && cout << "DM.handleInsert()\n";
        char *record = NULL;
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	RID rid;
	
	SysColumnRecord syscolrec;
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColumns;
        vector<RID> sysColRIDs;
        vector<int> types = pq.getTypes();
        vector<int> toTypes;
        vector<string> values = pq.getDataValues();
        vector<string> finalValues;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *dirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}        
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	
	
	if(!getSysColRecsForTable(pq.getTable(),sysColumns,(*dbHeader),systabrec.totalColumns, sysColRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//check for columns type.
	for(j=0, i = 0 ; j < systabrec.totalColumns ; j++ ){
	        syscolrec = sysColumns[j];
		/*if(types[i] != syscolrec.dataType && types[i] != NULLTYPE){
			cout << "Column type mismatch " << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}*/
		
		//if column is deleted, add dummy column and null data
		if(syscolrec.isDeleted){
			//add as nulltype and null data
			toTypes.push_back(NULLTYPE);			
			finalValues.push_back("NULL");
			//goto next sys coll
			continue;
		}
		
		
		//check if null is allowed and column is not deleted
		if(types[i] == NULLTYPE && syscolrec.isNotNull && !syscolrec.isDeleted){
			cout << "Column '" << syscolrec.columnName << "' cannot take NULL values" << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}
		

		
		
		//check valid conversion		
		if(types[i] == NULLTYPE){
			toTypes.push_back(NULLTYPE);
			
		}else{
			//check if recognised datatype can be converted into the type in syscol
			if(!checkDataType(types[i], syscolrec.dataType)){
				cout << "Datatype mismatch for column '" << syscolrec.columnName << "'" << endl;
				err = COLTYPE_MISMATCH;
				goto ret;				
			}
			toTypes.push_back(syscolrec.dataType);
			
		}
		finalValues.push_back(values[i]);
		//next column
		i++;
	}
	
	//i represents the count of number of non deleted columns	
	//check for no of undeleted columns. 
	if(pq.getNoOfColumns() != i){
		cout << "Column count doesn't match " << endl;
		err = TOTALCOL_MISMATCH;
		goto ret;
	}
	
	
	
	startPage = systabrec.startPage;
	
	if(!pack::pack_data(finalValues, record, size, toTypes)){
		err = PACK_ERROR;
		goto ret;
	}
	
	//================to be altered here===================================
	/*
	//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
	if(!insertRecordIntoDataPage(record,size,startPage,(*dbHeader),rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	
	DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$"<<endl;
	DM_FUNC_TRACE && cout <<"pageNo "<<rid.pageNo<<"    slotNo"<<rid.slotNo<<endl;
	DM_FUNC_TRACE && cout <<"$$$$$$$$$$$$$$$$$"<<endl;
	
	*/
	handleIndexInsert(startPage,toTypes,record,rid,(*dbHeader));
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(record != NULL){
		delete[] record;
	}
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(dirPage != NULL){
		delete dirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//==========================================================================================

bool DataManager::handleDropSchema(parsedQuery &pq){

	int err = NO_ERROR;	
        DM_FUNC_TRACE && cout << "DM.handleDropSchema()\n";
	
	if(!bm.deleteSchemaFile((char *)pq.getDatabase().c_str())){
		err = DROP_SCHEMA_ERROR;
		goto ret;
	}
	
	
        ret:
        
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//==========================================================================================

//handleDropTable()-This method drops the table if it is present else returns error message.
bool DataManager::handleDropTable(parsedQuery &pq){
	int err = NO_ERROR;
        int i = 0;
        int startPage;
        int size;
        int curDirPageNo;
        DM_FUNC_TRACE && cout << "DM.handleDropTable()\n";
        
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	RID rid;
	
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
        
        DirectoryEntry curDE,nextDE;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *DirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}        
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	startPage = systabrec.startPage;
	curDirPageNo = startPage;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			
			markAsFreePage(curDirPageNo,(*dbHeader));
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;

		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			
			markAsFreePage(curDE.pageNo,(*dbHeader));

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		//mark this DP as freePage
		markAsFreePage(curDirPageNo,(*dbHeader));
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	//delete table entry in SYSTAB
	deleteRecord(SYS_TAB_START_PAGE_NUM,rid,(*dbHeader));
	//delete columns entry in SYSCOLUMN
	for(i = 0; i < sysColRIDs.size() ; i++){
		deleteRecord(SYS_COL_START_PAGE_NUM,sysColRIDs[i],(*dbHeader));
	}
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//===========================================================================================

//markAsFreePage()-This method makes the page,pageNo as freepage and adds it to the DBHeader freepage list.
bool DataManager::markAsFreePage(int pageNo, DBHeader& dbHeader){
	DM_FUNC_TRACE && cout << "DM.markAsFreePage()";
        int err = NO_ERROR;
	int pagePtr = dbHeader.getFreePagePointer();
	char *data = new char[PAGE_SIZE_IN_BYTES];
	FreePage freePage = FreePage(data);
	freePage.setNextFreePage(pagePtr);
	
	bm.write(pageNo,data,FREE_PAGE_PRIORITY);
	dbHeader.setFreePagePointer(pageNo);
	
	ret: 
        if(data != NULL){
	        delete[] data;
        }
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}

//===========================================================================================================


//deleteRecord()-This method deletes the record in the datapage given its RID.Also it updates corresponding
//                DE entry inthe Directory Page.
bool DataManager::deleteRecord(const int &startDirPageNo ,const RID &rid,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.deleteRecord()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID tempRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == rid.pageNo){
				//page corresponding to rid was found.
				//delete the record in that page and updata its corresponding DE value..
				
				//read page in DE into BufferPool
				if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = READ_ERROR;
					goto ret;
				}
				
				if(DataPage != NULL){
					delete DataPage;
				}
				DataPage = new DataBasePage(datapagebuf);
				DataPage->deleteRecord(rid);
				//check if there are any valid records.
				if(!DataPage->firstRecord(tempRID)){
					markAsFreePage(curDataPageNo,dbHeader);
					DirPage->updateDirectoryEntry(curDE,true);
					goto ret;
				}
				//update DE in Directory Page.
				curDE.freeSpace = DataPage->getTotalFreeSpace();
				DirPage->updateDirectoryEntry(curDE,false);
				if(!bm.write(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = WRITE_ERROR;
					goto ret;
				}
				if(!bm.write(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
					err = WRITE_ERROR;
					goto ret;
				}
				goto ret;	
			}
			
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
		
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================
bool DataManager::handleDescTable(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDescTable()\n";
	int err = NO_ERROR;

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	RID rid;
	int i;	
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

		
	//print the sys cols for this table
	for(i=0; i<sysColRecs.size(); i++){
		cout << endl;
		sysColRecs[i].dump();	
	}
	cout << endl;
	
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================
bool DataManager::checkDataType(int fromDT, int toDT){

	bool err = NO_ERROR;

	//check if fromDT can be converted to toDT	
	switch(fromDT){
					case INTTYPE:
	 							if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != INTTYPE)
	 								err = INVALID;
								break;
								
					case DATETYPE:
								if(toDT != DATETYPE)
	 								err = INVALID;
								break;
								
					case FLOATTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != INTTYPE && toDT != FLOATTYPE)
	 								err = INVALID;
								break;
								
					case LONGTYPE:
								if(toDT != INTTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != LONGTYPE)
	 								err = INVALID;
								break;
								
					case DOUBLETYPE:
								if(toDT != LONGTYPE && toDT != INTTYPE && toDT != FLOATTYPE && toDT != DOUBLETYPE)
	 								err = INVALID;
								break;
								
					case VARCHARTYPE:
								if(toDT != VARCHARTYPE)
	 								err = INVALID;
								break;
								
					case SHORTTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE 
									&& toDT != INTTYPE && toDT != SHORTTYPE)
	 								err = INVALID;
								break;
								
					case BOOLTYPE:
								if(toDT != BOOLTYPE)
	 								err = INVALID;
								break;
	}
	
	
	return err==NO_ERROR?true:false;
}


//================================================================================================

//================================================================================================
bool DataManager::handleDelete(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDelete()\n";
	int err = NO_ERROR;
	

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	RID systabrid;	
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	int i;


	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), systabrid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//get all records for this table
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		deleteRecord(sysTabRec.startPage, allRIDs[i], (*dbhead));		
	}
	
	allrecs.clear();
	allRIDs.clear();
	recsizes.clear();
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}

//================================================================================================
bool DataManager::handleIndexInsert(const int &indexerpage,const vector<int> &keyType,char* const &key,const RID &rid,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.handleIndexInsert()" << endl;
        int err = NO_ERROR;
        int root;
	char * pageData = new char[PAGE_SIZE_IN_BYTES];
	char *newpageData = NULL;
	int newpageNo;
	IndexPage * indexpage = NULL;
	IndexNode node;
	BranchPage * branchpage = NULL;
	bool split = false;
	if(!bm.read(indexerpage,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(pageData);
	root = indexpage->getIndexerRoot();
	node.key = new char[(int) indexpage->getKeySize()];
	insertIntoIndex(root,keyType ,key ,node ,rid ,split,dbHeader);
	if(split){
	        DM_FUNC_TRACE && cout << "Split occured so create a new root" << endl;
	        DM_FUNC_TRACE && cout << "Key to be inserted : " << node.key << endl;
		newpageData = new char[PAGE_SIZE_IN_BYTES];
		if(!getFreePage(newpageData,newpageNo,dbHeader)){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		branchpage = new BranchPage(newpageData);
		branchpage->setPageType(BRANCHNODE);
		branchpage->setMaximumNodeEntries(indexpage->getMaximumNodeEntries());
		branchpage->setTotalKeysOccupied(0);
		branchpage->setKeySize(indexpage->getKeySize());
		branchpage->insert(keyType ,node.key ,node ,split);
		branchpage->dump(keyType);
		indexpage->setIndexerRoot(newpageNo);
		indexpage->dump();
		bm.write(indexerpage,pageData,DIR_PAGE_PRIORITY);
		bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
		
	}
	indexpage->dump();
	ret:
	if(newpageData != NULL)
		delete newpageData;
	if(branchpage != NULL)
		delete branchpage;
	if(pageData != NULL)
		delete pageData;
	if(indexpage != NULL)
		delete indexpage;
	
	
	
}
//================================================================================================

bool DataManager::insertIntoIndex(int &pageNo,const vector<int> &keyType ,char* const &key ,IndexNode &node ,const RID &rid ,bool &split,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.insertIntoIndex()" << endl;
        int err = NO_ERROR;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        BranchPage *branchpage = NULL;
        int nextPtr;
        char *newpageData = new char[PAGE_SIZE_IN_BYTES];
        int newpageNo;
        
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        
	indexpage = new IndexPage(pageData);
	if(indexpage->getPageType() == LEAFNODE){
		leafpage = new LeafPage(pageData);
		leafpage->insert(keyType,key,rid,split);
		if(split){
			DM_FUNC_TRACE && cout << "Split to be made in leaf" << endl;
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			leafpage->split(keyType,node,newpageData,key,rid,pageNo);
			leafpage->setNextLeaf(newpageNo);
			bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
			bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
			DM_FUNC_TRACE && cout << "Split done in leaf" << endl;
			split = true;
			DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Leaf$$$$$$$$$$$$$$$$$" << endl;
			leafpage->dump(keyType);
			goto ret;
		}
		leafpage->dump(keyType);
		bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
		split = false;
		goto ret;
	}
	
	branchpage = new BranchPage(pageData);
	branchpage->findNextNode(keyType ,key,nextPtr);
	insertIntoIndex(nextPtr,keyType,key,node,rid,split,dbHeader);
	if(split){
		branchpage->insert(keyType,node.key,node,split);
		if(split){
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			branchpage->split(keyType,node,newpageData,node.key);
			bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
			bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY);
			split = true;
			DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Branch$$$$$$$$$$$$$$$$$" << endl;
			branchpage->dump(keyType);
			goto ret;
		}
		branchpage->dump(keyType);
		bm.write(pageNo,pageData,DIR_PAGE_PRIORITY);
		split = false;
		goto ret;
	}
	ret:
	if(pageData != NULL)
		delete pageData;
	if(newpageData != NULL)
		delete newpageData;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(branchpage != NULL)
		delete branchpage;
		
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}
//=============================================================================================================
#endif
