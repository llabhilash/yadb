#ifndef SYSPAGE_START_POS
#define SYSPAGE_START_POS

#define DB_HEADER_PAGE_NUM 0
#define SYS_TAB_START_PAGE_NUM 1
#define SYS_COL_START_PAGE_NUM 2
#define SYS_INDEX_START_PAGE_NUM 3

//Following contains the value to indentify whether its a leafnode or a intermediate/root node.
#define LEAFNODE 2
#define BRANCHNODE 1
#define INDEXHEADER 0
#endif
