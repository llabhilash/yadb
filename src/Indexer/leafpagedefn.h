//leafpagedefn.h
//This includes function definition of all methods of class LeafPage
////////////////////////////////////////////////////////////////////////////////////

#ifndef LEAFPAGEDEFN_H
#define LEAFPAGEDEFN_H

#define PAGETYPE_START_POS (PAGE_SIZE_IN_BYTES -1 -sizeof(short) + 1)
#define FANOUT_START_POS (PAGETYPE_START_POS -sizeof(short))
#define TOTALKEYS_START_POS (FANOUT_START_POS -sizeof(short))
#define KEYSIZE_START_POS (TOTALKEYS_START_POS -sizeof(short))


using namespace std;

//LeafPage(char* &data)-Constructor for the class LeafPage.
LeafPage::LeafPage(char* &data){
	pageData = data;
}

//setPageType(const short &pageType)-This method is used to set the type of the page.i.e., whether it is a leafnode.
//or a intermidiate/root node
bool LeafPage::setPageType(const short &pageType){
	memcpy(&pageData[PAGETYPE_START_POS],(char *)&pageType,sizeof(short));
	return true;
}

//getPageType()-This method returns the type of the page i.e, whether its a leaf node or an intermidiate node.
short LeafPage::getPageType(){
	short pageType;
	memcpy((char *) &pageType,&pageData[PAGETYPE_START_POS],sizeof(short));
	return pageType;
}

//setMaximumNodeEntries(const short  &maxValue)- This method sets the maximum number of node entries i.e.,fanout.
bool LeafPage::setMaximumNodeEntries(const short  &maxValue){
	memcpy(&pageData[FANOUT_START_POS],(char *) &maxValue,sizeof(short));
	return true;
}

//getMaximumNodeEntries()-This method returns the fanout of the node.
short LeafPage::getMaximumNodeEntries(){
	short fanout;
	memcpy((char *) &fanout,&pageData[FANOUT_START_POS],sizeof(short));
	return fanout;
}


//setTotalKeysOccupied(const short &totalKeys)-This method sets the total Number of keys currently present in that node.
bool LeafPage::setTotalKeysOccupied(const short &totalKeys){
	memcpy(&pageData[TOTALKEYS_START_POS],(char *) &totalKeys,sizeof(short));
	return true;
}


//getTotalKeysOccupied()-This method returns the total number of keys currently present in that node
short LeafPage::getTotalKeysOccupied(){
	short totalKeys;
	memcpy((char *) &totalKeys,&pageData[TOTALKEYS_START_POS],sizeof(short));
	return totalKeys;
}


//setKeySize(const short &size)-This method sets the size of the indexkey.In case of composite key it should give the 
//size of the key after the key has been packed.
bool LeafPage::setKeySize(const short &size){
	memcpy(&pageData[KEYSIZE_START_POS],(char *) &size,sizeof(short));
	return true;
}


//getKeySize()-This method returns the size of the key used in indexing.
short LeafPage::getKeySize(){
	short keySize;
	memcpy((char *) &keySize ,&pageData[KEYSIZE_START_POS],sizeof(short));
	return keySize;
}


//insert(const vector<int> &keyType , const char* &key ,const RID &rid ,IndexNode &node)-This method inserts the
//tuple <rid ,key > into the leafpage in an indexed manner(sorted order).
//Here keyType is a vector containing the datatype of a key in case of composite key its consists of multiple rows of
//datatype arranged inthe order as it is to be indexed.Key represents the packed version of the key.
bool LeafPage::insert(const vector<int> &keyType ,char* const &key ,const RID &rid ,bool &split){
        int i;
        short totalKeys;
        bool result = false;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
	if(getTotalKeysOccupied() == getMaximumNodeEntries()){
	//Split the LeafNode into Two.
		split = true;
		return false;
	}
	for(i = (getTotalKeysOccupied() - 1 ) ; i >= 0 ; i --){
		if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
		if(result){
			break;
		}
		memcpy(&pageData[(i + 1) * recordSize],record,recordSize);
	}
	if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	//construct tuple<rid,key>
	memcpy(record,(char *) &rid,sizeof(RID));
	memcpy(&record[sizeof(RID)],key,getKeySize());
	
	memcpy(&pageData[(i + 1) * recordSize],record,recordSize);
	totalKeys = getTotalKeysOccupied();
	setTotalKeysOccupied(totalKeys + 1);
	
        if(record != NULL){
		delete record;
	}
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	//set split = false indicating no need for split.
	split = false;
	return true;
}



//split(IndexNode &node ,char* &newleaf)-This method creates the new leaf node whenever a split is necessary.
//This method fills half of the current nodes keys to the new leaf.
bool LeafPage::split(const vector<int> &keyType, IndexNode &node ,char* &newleaf,char* const &key ,const RID &rid){
	int i;
	short totalKeys;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        bool split = false;
        bool result = false;
        bool inserted = false;
        RID curRID;
	
	LeafPage newPage = LeafPage(newleaf);
	newPage.setPageType(getPageType());
	newPage.setMaximumNodeEntries(getMaximumNodeEntries());
	newPage.setTotalKeysOccupied(0);
	newPage.setKeySize(getKeySize());
	
	for(i = (getMaximumNodeEntries() -1) ; i >= (getMaximumNodeEntries() +1 )/2 ; i --){
		if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		memcpy((char *) &curRID,record,sizeof(RID));
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result);
		if(result && (!inserted)){
			newPage.insert(keyType ,key ,rid ,split);
			inserted = true;
		}
		newPage.insert(keyType ,nodeKeys,curRID ,split);
		
		memcpy(&pageData[i * recordSize] ,"",recordSize);
		totalKeys = getTotalKeysOccupied();
		setTotalKeysOccupied(totalKeys - 1);
	}
	if(!inserted)
		insert(keyType,key,rid,split);
	if(record != NULL){
		delete record;
	}
	record = new char[recordSize];
	DM_FUNC_TRACE && cout << "Total keys ........." << (getTotalKeysOccupied() -1) << endl;
	memcpy(record ,newleaf,recordSize);
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
	memcpy(node.key,nodeKeys,getKeySize());
	DM_FUNC_TRACE && cout << "Key passed to upper level " << node.key << ", "<< nodeKeys << endl;
	//dump to see contents of new leaf page.
	DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$Dump of new leaf$$$$$$$$$$$$$$$" << endl;
	DM_FUNC_TRACE && newPage.dump(keyType);
	if(record != NULL){
		delete record;
	}
	if(nodeKeys != NULL){
		delete nodeKeys;
	}
	
	return true;
}


//dump();
bool LeafPage::dump(const vector<int> &keyTypes){
	int i;
	int err = NO_ERROR;
	int recordSize = sizeof(RID) + getKeySize();
	int j;
        char *record = NULL;
        char *nodeKeys = NULL;
	vector<string> recAttributes;
	cout << "#######################LEAF NODE##################################" << endl;
	cout << "Page Type : " << getPageType() << "   (LEAFNODE)"<<endl;
	cout << "Maximum Node Entries (fanout) " << getMaximumNodeEntries() <<endl;
	cout << "Total Keys occupied :" << getTotalKeysOccupied() << endl;
	cout << "KeySize : " << getKeySize() << endl;
	cout <<"                     ###Leaf Node Values##                         "<<endl;
	for(i = 0 ; i < getTotalKeysOccupied();i++){
	        cout << "in pack" << endl;
		if(record != NULL){
			delete record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	       }
	       cout << "Key Start Positon = " << i * recordSize << endl;
	       cout << "Key Value/s =  ";
	       for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
	       }
	       cout << endl;
	}
	cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete record;
	if(nodeKeys != NULL)
		delete nodeKeys;
		
	return true;
}
//Used for comparision of keys
bool LeafPage::compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes, const int &keySize1, const int &keySize2, bool &result){

	//return true if key1 is >= key2


	DM_FUNC_TRACE && cout << "LeafPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	
	vector<string> recAttributes1, recAttributes2;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	if(!pack::unpack_data(recAttributes2, keyTypes, key2, keySize2)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	for(int i = 0; i < keyTypes.size(); i++){
	
		//convert both attributes
		if( !pack::convert_to_type(recAttributes1[i], keyTypes[i], att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(recAttributes2[i], keyTypes[i], att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
		/*
		if(keyTypes[i] == VARCHARTYPE){
			toReturn = strcmp((char*)att_value1, (char*)att_value2);
			//if unequal.. stop
			if(toReturn != 0)
				goto ret;
		}else{
			if()		
		}
		*/
		switch(keyTypes[i]){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;


								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							
		 							//unequal.. stop
		 							goto ret;
		 						}
		 						
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
		 								 						
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;	 						
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
					case VARCHARTYPE:								
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 						
		 						delete (char*)att_val1;
		 						delete (char*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)			 						
			 						goto ret;
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;			
		}		
	}
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(toReturn <= 0)
		result = false;
	else
		result = true;
	return err==NO_ERROR?true:false;	
}
		
		
#endif
