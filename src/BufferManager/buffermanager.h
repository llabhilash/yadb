#ifndef BM_H
#define BM_H
#include <string>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <ctime>
#include <cctype>
#include <map>
#include "./../Common/Settings.h"
#include "./../Common/ErrorCodes.h"
#include "./../Common/Priorities.h"
#include "./../Common/SysPagesStartPos.h"
#include "./../Datastructures/alldatastructures.h"
#include "frame.h"

#define BM_NO_OF_PAGES_TO_EXPAND 10

#include "buffermanagerdecl.h"
#include "buffermanagerdefn.h"
#endif
