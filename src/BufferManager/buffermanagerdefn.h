#ifndef BMDEFN_H
#define BMDEFN_H

using namespace std;

//=========================================================================
BufferManager::BufferManager(){
	
	BM_FUNC_TRACE && cout << "BM.Constructor : \n";

	noOfHits = 0;
	noOfReads = 0;
	noOfWrites = 0;
	noOfFrames = 0;
	bufferPool = NULL;
	heapfile = NULL;
	//heapfilename = NULL;
	//heapfilename = new char[FILENAME_SIZE + PATH_SIZE];
}

//=========================================================================
BufferManager::~BufferManager(){
	BM_FUNC_TRACE && cout << "BM.Destructor : \n";
	//if(heapfilename != NULL){
	//	delete[] heapfilename;
	//	heapfilename = NULL;
	//}

}
//=========================================================================
bool BufferManager::isPagePresentInBuffer(const int pageNum){
	
	if(pageToFrame[pageNum] > 0)
		return true;
	else
		return false;
}
//=========================================================================
int BufferManager::getFrameNoForPage(const int pageNum){
	//frame number is incremented by 1 while storing..  as if return value is 0.. cannot determine if frame number is 0 or page not present
	return (pageToFrame[pageNum] - 1);
}
//=========================================================================
void BufferManager::victimizeOldPageWithNewPage(const int oldPageNum, const int newPageNum, const int frameNo){
	//frame number is incremented by 1 while storing..  as if return value is 0.. cannot determine if frame number is 0 or page not present

	pageToFrame[oldPageNum] = 0;	//Old page is now out of buffer
	pageToFrame[newPageNum] = (frameNo + 1);	//New page is now in the buffer
}

//=========================================================================
void BufferManager::hexDumpPage(int pageNo, char* &pagebuf){

	unsigned char ch;
	unsigned int toprint;
	int j = 0;

	int numOfBytesPerLine = 16;

	//hex dump the page
	cout << "Hex dump of PAGE => " << pageNo << endl << endl;
	cout << "\t\tHex Dump\t\t\t\t\tCharacter Output" << endl;

	for(int i=0; i <= PAGE_SIZE_IN_BYTES; i++){
		
		if(i %  numOfBytesPerLine == 0){
			
			if(i > 0) 
				cout << " |==| ";
			//output the characters
			while(j < i){
				memcpy(&ch, &pagebuf[j], sizeof(char));
				if(!isalnum(ch))
					ch = '.';
				cout << " " << ch;
				j++;
			}
			
			cout << "\n";
		}

		if(i == PAGE_SIZE_IN_BYTES)
			break;

		memcpy(&ch, &pagebuf[i], sizeof(char));

		//print the msb
		//240 = 11110000 mask
		toprint = ch & (240);
		toprint = toprint >> 4;
		cout << hex << toprint;	

		//print the lsb
		//15 = 00001111 mask
		toprint = ch & (15);
		cout << hex << toprint;
		
		cout << " ";
	}

	
	cout << "\nNumber of bytes per line : " << dec << numOfBytesPerLine << endl;
}

//=========================================================================
void BufferManager::showWorking(){

	//Options
	//Set function trace
	//Read a page to buffer
	//Write a page to buffer
	//Show Frame contents
	//Show Statistics
	//Set buffer size
	//Commit Buffer
	//Change Replacement Policy
	

	string choice = "";
	int choiceval, frameno, pageno;
	string mode = "";
	cout << endl;
	char* pagebuf = NULL;
	
	bool bmfunctrace_old = BM_FUNC_TRACE;
	//set it to on.. will be reset while quitting
	BM_FUNC_TRACE = true;

	

	while(true){
		cout << "\n\n=====Buffer Manager=====\n\n";
		cout << "1  : Set Buffer-Manager Function trace\n";
		cout << "2  : Read a page (DISK => BufferPool)\n";
		cout << "3  : Write a page (BufferPool => Disk)\n";
		cout << "4  : Show Frame contents\n";
		cout << "5  : Show Hit Ratio\n";
		cout << "6  : Set Buffer Manager Size\n";
		cout << "7  : Commit Buffer Manager\n";
		cout << "8  : ReplacementPolicy\n\n";
		cout << "0  : Back\n\n";
	    	cout << "Choice : ";
		
		cin >> choice;
		
		if(choice.compare("quit") == 0  || choice.compare("exit") == 0|| choice.compare("0") == 0)
			choiceval = -112918329;
		else{
			choiceval = atoi(choice.c_str());
			if(choiceval < 0 || choiceval > 8){
				cout << "Invalid choice\n";
				continue;
			}
		}

		
		
		
		
		switch(choiceval){
		
							
			case 1: //Set BM_FUNC_TRACE
				
				cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				BM_FUNC_TRACE = atoi(mode.c_str());
				bmfunctrace_old = BM_FUNC_TRACE;
				//set it to true currently
				BM_FUNC_TRACE = true;
				break;
				
			case 2: //Read a page
				
				cout << "Page number to read : ";
				
				cin >> mode;
				if( atoi(mode.c_str()) < 0){
					cout << "\nInvalid Input\n";
					continue;
				}

				pageno = atoi(mode.c_str());

				if(pagebuf != NULL)
					delete[] pagebuf;
				pagebuf = new char[PAGE_SIZE_IN_BYTES];
		
				if(!read(pageno, pagebuf, MIN_PRIORITY)){
					cout << "Could not read page " << pageno << endl;
				}else{
					cout << "Show hex dump of page  ( y / n ) ? ";
					cin >> mode;
					if(mode.compare("y") == 0){
						//hex dump the page
						hexDumpPage(pageno, pagebuf);
					}
				}
				
				if(pagebuf != NULL)
					delete[] pagebuf;
				pagebuf = NULL;
				break;

			case 3: //Write a page
				cout << endl << "Contents of Frame to write to disk (0 - " << (noOfFrames-1) << ") : ";
				cin >> mode;
				
				frameno = atoi(mode.c_str());

				if(frameno < 0 || frameno > (noOfFrames-1)){
					cout << "Invalid frame number\n";					
				}else{
					
					//check if the frame is dirty
					if(!bufferPool[frameno].isDirty()){
						cout << "Frame " << frameno << " is not Dirty\n";
					}else{
						if(!writeToDisk(bufferPool[frameno].getPageNo(),frameno)){//pageNo,frameNo			
							cout << "Could not write contents of frame " << frameno << " to DISK\n";
						}else{
							cout << "Wrote contents of frame " << frameno << " to DISK\n";
						}	
					}
				}
			
				break;

			case 4: //Show frame contents
				
				for(int i=0; i<noOfFrames; i++){
					if(bufferPool[i].getPageNo() != -1){
						bufferPool[i].dump(i);
					}
				}
				cout << "\nFrames which have no page loaded into them are NOT SHOWN\n";
				break;

			case 5: //Show Hit ratio

				showHitRatio();				
				break;

			case 6: cout << "Set new size (KB): ";
				
				cin >> mode;

				if( atoi(mode.c_str()) <= 0 ){
					cout << "\nInvalid Input\n";
					continue;
				}
				if(!init(atoi(mode.c_str()))){
					cout << "Could not initialize buffer\n";
				}
				break;
			
			case 7:	if(!commitBuffer()){
					cout << "Could not commit buffer\n";
				}
				break;

			case 8:
				cout << "\n0: LRU\n1: Product of priority, hit count, time\n2: MRU\n\npolicy :";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) && (atoi(mode.c_str()) != 2) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				REPLACEMENT_POLICY = atoi(mode.c_str());
				break;


			case -112918329: 
				BM_FUNC_TRACE = bmfunctrace_old;
				return;			
				break;
		}
	}



}



//=========================================================================
void BufferManager::showHitRatio(){
	
	cout << "\nTotal number of Frames  : " << noOfFrames << endl;
	cout << "Total number of Reads   : " << noOfReads << endl;
	cout << "Total number of Writes  : " << noOfWrites << endl;
	cout << "Total number of Hits    : " << noOfHits << endl;
	cout << "Total Hit Ratio (%)     : " << (((float)(noOfHits)/(float)(noOfReads + noOfWrites)) * 100) << endl;
	switch(REPLACEMENT_POLICY){

		case 0: cout << "Replacement Policy : LRU\n";
			break;
		case 1: cout << "Replacement Policy : Product\n";
			break;
		case 2: cout << "Replacement Policy : MRU\n";
			break;
	}

}
//=========================================================================
void BufferManager::destroyBuffer(){
	
	BM_FUNC_TRACE && cout << "BM.destroyBuffer() : \n";

	if(bufferPool == NULL)
		return;	

	delete[] bufferPool;
	bufferPool = NULL;
	pageToFrame.clear();
	BM_FUNC_TRACE && cout << "Destroying Buffer Pool\n";	
}

//=========================================================================
void BufferManager::invalidateBuffer(){
	
	BM_FUNC_TRACE && cout << "BM.invalidateBuffer() : \n";

	if(bufferPool == NULL){
		return;		
	}

	//set all page numbers to -1 so that the is invalid
	for(int i=0; i<noOfFrames; i++){
		bufferPool[i].setPageNo(-1);
		//set page as not dirty
		bufferPool[i].clearDirty();
	}

	pageToFrame.clear();

	BM_FUNC_TRACE && cout << "Invalidating pages in BufferPool\n";	
}

//=========================================================================
bool BufferManager::setSchemaFile(char* filename, bool dispErr){

	//do not display db changed when called by create schema

	BM_FUNC_TRACE && cout << "BM.setSchemaFile() : \n";

	int err = NO_ERROR;
	FILE* temp;
	
	if(bufferPool == NULL){
		err = INVALID;
		goto ret;
	}	
	
	//check if buffer already open
	if(heapfile != NULL && strcmp(filename,heapfilename.c_str()) == 0){
		//requesting same file to use
		cout << "Database '" << heapfilename << "' already in use\n";
		goto ret;
	}
	
	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);

	//check if file ( filename ) exists
	temp = fopen(filewithpath,"r");
	if(temp){
		//file exists - no error
		fclose(temp);
	}else{
		cout << "\nDatabase '" << filename << "' does not exist\n\n";
		err = INVALID;
		goto ret;
	}
	
	
	//commit buffer
	if(!commitBuffer()){
		err = INVALID;
		goto ret;
	}

	//invalidate buffer.. as old pages do not matter
	invalidateBuffer();

	//close heapfile

	if(heapfile != NULL && fclose(heapfile) != 0){
		err = FILECLOSE_ERROR;
		goto ret;
	}
	heapfile = NULL;

/*	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);

	//check if file ( filename ) exists
	temp = fopen(filewithpath,"r");
	if(temp){
		//file exists - no error
		fclose(temp);
	}else{
		cout << "\nDatabase '" << filename << "' does not exist\n\n";
		err = INVALID;
		goto ret;
	}
*/
	//open and close filename in 'a' mode so that the file is created if it does not exist
	//fclose(fopen(filewithpath,"a"));
	

	//fopen in 'r+' mode
	heapfile = fopen(filewithpath,"r+b");
	if(heapfile == NULL){
		err = FILEOPEN_ERROR;
		goto ret;
	}

	//sprintf(heapfilename,"%s",filename);
	heapfilename = string(filename);
	dispErr && cout << "Database changed to " << filename << endl;	
	
	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
bool BufferManager::writeToDisk(int pageNum,int frameNum){

	BM_FUNC_TRACE && cout << "BM.writeToDisk() : \n";

	int err = NO_ERROR;

	if(frameNum >= noOfFrames || bufferPool[frameNum].getPageNo() != pageNum){
		err = INVALID;
		goto ret;
	}
	
	if(heapfile == NULL){
		err = INVALID;
		cout << "\nNo schema/database selected\n";
		goto ret;
	}
	
	if(!bufferPool[frameNum].isDirty()){
		// no need to write clean data
		goto ret;
	}

	//seek the page position
	if(fseek(heapfile, (pageNum * PAGE_SIZE_IN_BYTES), SEEK_SET) != 0){
		err = SEEK_ERROR;
		goto ret;
	}
	
	//write to disk
	if(fwrite(bufferPool[frameNum].pageData,sizeof(char),PAGE_SIZE_IN_BYTES,heapfile) != PAGE_SIZE_IN_BYTES){
		err = WRITE_ERROR;
		goto ret;
	}
	
	//set flag to not dirty
	bufferPool[frameNum].clearDirty();
	
	BM_FUNC_TRACE && cout << "Wrote Page " << pageNum << " in Frame " << frameNum << " to DISK\n";

	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
bool BufferManager::readFromDisk(int pageNum,int frameNum){

	BM_FUNC_TRACE && cout << "BM.readFromDisk() : \n";

	int err = NO_ERROR;
	
	//check for invalid conditions
	if(frameNum >= noOfFrames || heapfile == NULL){
		err = INVALID;
		if(heapfile == NULL)
			cout << "\nNo schema/database selected\n";
		goto ret;
	}
	
	//check if frame is dirty.. 	
	if(bufferPool[frameNum].isDirty()){
		// write to drive
		if(!writeToDisk(bufferPool[frameNum].getPageNo(),frameNum)){
			err = WRITE_ERROR;
			goto ret;
		}
	}

	//seek the page position
	if(fseek(heapfile, (pageNum * PAGE_SIZE_IN_BYTES), SEEK_SET) != 0){
		err = SEEK_ERROR;
		goto ret;
	}
	
	//read from disk
	if(fread(bufferPool[frameNum].pageData,sizeof(char),PAGE_SIZE_IN_BYTES,heapfile) != PAGE_SIZE_IN_BYTES){
		err = READ_ERROR;
		goto ret;
	}
	
	//set page num
	bufferPool[frameNum].setPageNo(pageNum);
	bufferPool[frameNum].setLastUsedTime();

	BM_FUNC_TRACE && cout << "Read Page " << pageNum << " to Frame " << frameNum << " from DISK\n";

	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}


//=========================================================================
bool BufferManager::commitBuffer(){

	BM_FUNC_TRACE && cout << "BM.commitBuffer() : \n";

	int err = NO_ERROR;
	for(int i=0; i<noOfFrames; i++){
		if(bufferPool[i].isDirty()){
			if(!writeToDisk(bufferPool[i].getPageNo(),i)){//pageNo,frameNo			
				err = WRITE_ERROR;
				goto ret;			
			}			
		}
	}
	
	BM_FUNC_TRACE && cout << "Committed BUFFER to disk\n";

	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//==========================================================================
bool BufferManager::init(int noOfKBytes){
	
	BM_FUNC_TRACE && cout << "BM.init() : \n";

	int err = NO_ERROR;

	int numFrames = (noOfKBytes * 1024) /(sizeof(Frame) + PAGE_SIZE_IN_BYTES);

	if(numFrames < 1){
		cout << "Buffer size must be atleast " << PAGE_SIZE_IN_BYTES << " bytes\n";
		err = INVALID;
		goto ret;
	}

	BM_FUNC_TRACE && cout << noOfKBytes << " bytes supports " << numFrames << " frames in the buffer\n" ;
	

	if(bufferPool != NULL){
		//commit the buffer
		if(!commitBuffer()){
			err = COMMIT_ERROR;
			goto ret;
		}
		//destroy the buffer		
		destroyBuffer();		
	}
	
	bufferPool = new Frame[numFrames];

	if(bufferPool == NULL){
		err = INVALID;
		goto ret;
	}

	noOfFrames = numFrames;
	
	BM_FUNC_TRACE && cout << "BufferPool initialised with " << numFrames << " frames\n";

	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
bool BufferManager::shutdown(){

	BM_FUNC_TRACE && cout << "BM.shutdown() : \n";

	int err = NO_ERROR;

	//commit buffer
	if(!commitBuffer()){
		err = COMMIT_ERROR;
		goto ret;
	}
	
	//release buffer memory
	destroyBuffer();

	//close heapfile
	if(heapfile!=NULL && fclose(heapfile) != 0){
		err = FILECLOSE_ERROR;
		goto ret;
	}	
	heapfile = NULL;	
	//sprintf(heapfilename,"");	
	heapfilename.clear();
	BM_FUNC_TRACE && cout << "Shutting down BufferManager\n";

	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}

//=========================================================================
bool BufferManager::read(int pageNum, char* buffer, int priority){

	BM_FUNC_TRACE && cout << "BM.read() : \n";

	int err=NO_ERROR, victimFrameNo;
	
	//check if buffer is null
	if(buffer == NULL || bufferPool == NULL){
		err = INVALID;
		goto ret;
	}

	//increase read count
	noOfReads++;

	

	//check if pageNum is present in bufferPool.. if yes.. copy it from there
	//for(int i=0;i<noOfFrames; i++){
	//	if(bufferPool[i].getPageNo() == pageNum){
	if(isPagePresentInBuffer(pageNum)){
		int i = getFrameNoForPage(pageNum);
		//copy it to buffer
		memcpy(buffer,bufferPool[i].pageData,PAGE_SIZE_IN_BYTES);			
		//increase hit ratio
		noOfHits++;
		BM_FUNC_TRACE && cout << "Buffer HIT : Page " << pageNum << " found in Frame " << i <<"\n";			
		bufferPool[i].setLastUsedTime();
		bufferPool[i].hit();
		bufferPool[i].setPriority(priority);
		goto ret;
	}	
	//}
	
	//PageNum not found in buffer.. find a replacement frame as a victim
	//replacement policy
	victimFrameNo = getVictimFrameNo();

	if(victimFrameNo == -1){
		err = INVALID;
		goto ret;
	}
	
	BM_FUNC_TRACE && cout << "Buffer MISS : Page " << pageNum << " not found in buffer\nReplacing Page : " << bufferPool[victimFrameNo].getPageNo() << " in  Frame : " << victimFrameNo << endl;

	//check if page is dirty.. write to disk if yes
	if(bufferPool[victimFrameNo].isDirty()){
		if(!writeToDisk(bufferPool[victimFrameNo].getPageNo(),victimFrameNo)){
			err = WRITE_ERROR;
			goto ret;
		}
	}

	
	//modify mapping from page to frame
	victimizeOldPageWithNewPage(bufferPool[victimFrameNo].getPageNo(), pageNum, victimFrameNo);

	//read pageNo from disk into victim frame
	if(!readFromDisk(pageNum,victimFrameNo)){
		err=READ_ERROR;
		goto ret;
	}

	//now copy from the frame to buffer
	memcpy(buffer,bufferPool[victimFrameNo].pageData,PAGE_SIZE_IN_BYTES);
	BM_FUNC_TRACE && cout << "Read From Buffer: Page " << pageNum << " is now in Frame : " << victimFrameNo << endl;
	bufferPool[victimFrameNo].setPageNo(pageNum);
	bufferPool[victimFrameNo].setLastUsedTime();
	bufferPool[victimFrameNo].setPriority(priority);
	bufferPool[victimFrameNo].resetHits();
	bufferPool[victimFrameNo].hit();
		
	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}

//=========================================================================
bool BufferManager::write(int pageNum, char* buffer, int priority){

	BM_FUNC_TRACE && cout << "BM.write() : \n";

	int err=NO_ERROR, victimFrameNo;

	//increase write count
	noOfWrites++;
	
	//check if buffer is null
	if(buffer == NULL || bufferPool == NULL){
		err = INVALID;
		goto ret;
	}
		
	

	//check if pageNum is present in bufferPool.. if yes.. copy it from there
	//for(int i=0;i<noOfFrames; i++){
	//	if(bufferPool[i].getPageNo() == pageNum){
	if(isPagePresentInBuffer(pageNum)){
		
		int i = getFrameNoForPage(pageNum);

		BM_FUNC_TRACE && cout << "Buffer HIT : Page " << pageNum << " found in Frame " << i <<"\n";


	
		//copy it to buffer
		memcpy(bufferPool[i].pageData,buffer,PAGE_SIZE_IN_BYTES);
		bufferPool[i].hit();
		bufferPool[i].setPriority(priority);
		bufferPool[i].setDirty();			
		bufferPool[i].setLastUsedTime();
		//increase hits
		noOfHits++;
		goto ret;
	}	
	//}
	
	//PageNum not found in buffer.. find a replacement frame as a victim
	//replacement policy
	victimFrameNo = getVictimFrameNo();
	if(victimFrameNo == -1){
		err = INVALID;
		goto ret;
	}
	
	BM_FUNC_TRACE && cout << "Buffer MISS : Page " << pageNum << " not found in buffer\nReplacing Page : " << bufferPool[victimFrameNo].getPageNo() << " in  Frame : " << victimFrameNo << endl;

	//check if page is dirty.. write to disk if yes
	if(bufferPool[victimFrameNo].isDirty()){
		if(!writeToDisk(bufferPool[victimFrameNo].getPageNo(),victimFrameNo)){
			err = WRITE_ERROR;
			goto ret;
		}
	}
	
	//modify mapping from page to frame
	victimizeOldPageWithNewPage(bufferPool[victimFrameNo].getPageNo(), pageNum, victimFrameNo);

	//read pageNo from disk into victim frame.. pageNo might not be present as it might be reading for the first time
	if(!readFromDisk(pageNum,victimFrameNo)){
		//err=READ_ERROR;
		//goto ret;
	}

	//now copy from the buffer to frame
	memcpy(bufferPool[victimFrameNo].pageData,buffer,PAGE_SIZE_IN_BYTES);	
	BM_FUNC_TRACE && cout << "Write To Buffer: Page " << pageNum << " is now in Frame : " << victimFrameNo << endl;
	bufferPool[victimFrameNo].setPageNo(pageNum);
	bufferPool[victimFrameNo].setDirty();
	bufferPool[victimFrameNo].setPriority(priority);
	bufferPool[victimFrameNo].resetHits();
	bufferPool[victimFrameNo].hit();
	bufferPool[victimFrameNo].setLastUsedTime();
		
	ret: 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}

//=========================================================================
int BufferManager::getVictimFrameNo(){

	BM_FUNC_TRACE && cout << "BM.getVictimFrameNo() : \n";	

	int victimFrameNo = -1;
	
	bool first = true;
	
	//time_t lastUsed = time (NULL) + 1000; //advance time, so that any frames time will be before that	
	//int priority = -1;
	//int hitcount = 0;
	
	time_t currentTime = time (NULL);	
	double minworth = 0, worth = 0;	
	
	//worth = lastUsedTime * priority * hitCount

	//srand(currentTime);
	//victimFrameNo = rand() % noOfFrames;
	//return victimFrameNo;



	for(int i=0; i<noOfFrames; i++){
		
		/*
		if(BM_FUNC_TRACE){
		
			cout << "Current Priority : " << priority << endl;
 			cout << "Current LastUsed : " << lastUsed << endl;
 			cout << "Current Hitcount : " << hitcount << endl;
			cout << "Current Victim : " << victimFrameNo << endl;
			
			cout << "\nCandidate Frame " << i << endl;
			bufferPool[i].dump();
		}
		*/	
		
		//calculate how much this frame is worth
		//worth = (bufferPool[i].getLastUsedTime()/currentTime) * (double)bufferPool[i].getPriority() * bufferPool[i].getHitCount();
		switch(REPLACEMENT_POLICY){
			case 0: //LRU
				worth = bufferPool[i].getLastUsedTime();
				break;
			case 1: //product of priority, hit count etc
				worth = (bufferPool[i].getLastUsedTime()/currentTime) * (double)bufferPool[i].getPriority() * bufferPool[i].getHitCount();
				break;

			case 2: //MRU
				worth = bufferPool[i].getLastUsedTime();
				break;

		}
		/*if(BM_FUNC_TRACE){
			cout << "============" << endl;
			cout << "Frame : " << i << " \nWorth : " << worth << " :: Min Worth : " << minworth << endl;
			cout << "Priority : " << bufferPool[i].getPriority() << endl;
 			cout << "LastUsed : " << bufferPool[i].getLastUsedTime() << endl;
 			cout << "Hitcount : " << bufferPool[i].getHitCount() << endl;
 			cout << "============" << endl;
		}*/
		switch(REPLACEMENT_POLICY){	
			case 0:
			case 1:
				if(worth < minworth || first){
					minworth = worth;
					victimFrameNo = i;
					first = false;
				}
				break;

			case 2:
				if(worth > minworth || first){
					minworth = worth;
					victimFrameNo = i;
					first = false;
				}
				break;
		}
		/*
		if(bufferPool[i].getLastUsedTime() <= lastUsed && bufferPool[i].getPriority() >= priority && bufferPool[i].getHitCount() <= hitcount){
		
			//Priority=1 > Priority=0 ==> 0<1<2<3..
		
			priority = bufferPool[i].getPriority();
			lastUsed = bufferPool[i].getLastUsedTime();
			victimFrameNo = i;
			hitcount = bufferPool[i].getHitCount();
		}
		*/
	}

	return victimFrameNo;
}

//=========================================================================
bool BufferManager::setPageAsFree(int pageNum, char* &dbHeaderBuf){

	int err=NO_ERROR;	
	
	BM_FUNC_TRACE && cout << "BM.markPageAsFree() : \n";	
	
	//DBHeader, Free page buffers	
	char* tempFreePageBuf = new char[PAGE_SIZE_IN_BYTES];
	
	DBHeader dbHead(dbHeaderBuf);
	FreePage tempFreePage(tempFreePageBuf);
	
	//check if pageNum == -1
	if(pageNum == -1){
		err = INVALID;
		goto ret;
	}	
	
	//add this to head of linked list
	tempFreePage.setNextFreePage(dbHead.getFreePagePointer());
	dbHead.setFreePagePointer(pageNum);
	
	//write back temp page
	if(!write(pageNum, tempFreePageBuf, FREE_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
		
	ret: 
	delete[] tempFreePageBuf; 
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
bool BufferManager::expandHeapFile(int lastPageNum){

	int err = NO_ERROR;
	
	BM_FUNC_TRACE && cout << "BM.expandHeapFile() : \n";
	
	char* tempFreePageBuf = new char[PAGE_SIZE_IN_BYTES];

	//memset the buffer to null
	memset(tempFreePageBuf, '\0', PAGE_SIZE_IN_BYTES);

	//ASSUMPTION : called only when free page pointer in DB Header is -1
	
	int nextFreePage = -1;		//last page in chain should have -1	
	FreePage tempFreePage(tempFreePageBuf);
	
	//Add pages starting at lastPageNum except the first page in chain.. 
	//write the first page in chain to bufferpool
	//for(int i = BM_NO_OF_PAGES_TO_EXPAND; i > 0; i--){		
	for(int i = 1; i <= BM_NO_OF_PAGES_TO_EXPAND; i++){		

		//create a temp free page. set next page value, write to disk
	
		//calculate free page num
		int tempFreePageNum = lastPageNum + i;
		
		nextFreePage = tempFreePageNum + 1;			

		//if this is the last page in the change.. make the next pointer as -1
		if(i == BM_NO_OF_PAGES_TO_EXPAND){
			nextFreePage = -1;			
		}		

		tempFreePage.setNextFreePage(nextFreePage);					
		
		//write to disk
		//===================================================
		if(heapfile == NULL){
			err = INVALID;
			cout << "\nNo schema/database selected\n";
			goto ret;
		}

		//seek the page position
		if(fseek(heapfile, (tempFreePageNum * PAGE_SIZE_IN_BYTES), SEEK_SET) != 0){
			err = SEEK_ERROR;
			goto ret;
		}
		
		//write to disk
		if(fwrite(tempFreePageBuf,sizeof(char),PAGE_SIZE_IN_BYTES,heapfile) != PAGE_SIZE_IN_BYTES){
			err = WRITE_ERROR;
			goto ret;
		}

		BM_FUNC_TRACE && cout << "Adding FreePage : " << tempFreePageNum <<" \n";
				
			
	}
	
	
	//read back the first page in chain to buffer pool
	if(BM_NO_OF_PAGES_TO_EXPAND > 0){		
		if(!read(lastPageNum+1, tempFreePageBuf, FREE_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
	}
	
	ret: 
	delete[] tempFreePageBuf;
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
bool BufferManager::deleteSchemaFile(char* filename){

	BM_FUNC_TRACE && cout << "BM.deleteSchemaFile() : \n";
	int err = NO_ERROR;
	
	
	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	FILE* temp;
	
	
	if(bufferPool == NULL){
		err = INVALID;
		goto ret;
	}
	
	//check if buffer already open
	if(heapfile!=NULL && strcmp(filename,heapfilename.c_str()) == 0){
		
		//commit buffer
		//if(!commitBuffer()){
		//	err = INVALID;
		//	goto ret;
		//}

		//invalidate buffer.. as old pages do not matter
		invalidateBuffer();		
		
		//close heapfile
		if(heapfile != NULL && fclose(heapfile) != 0){
			err = FILECLOSE_ERROR;
			goto ret;
		}
		heapfile = NULL;
		
		//sprintf(heapfilename,"");	
		heapfilename.clear();
	}
	
	
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);
	//check if file ( filename ) exists
	temp = fopen(filewithpath,"r");
	if(temp){
		//file exists - no error
		fclose(temp);
	}else{
		cout << "\nDatabase '" << filename << "' does not exist\n\n";
		err = INVALID;
		goto ret;
	}
	
	
	//delete file
	if(remove(filewithpath) != 0){
		err = SCHEMAFILE_DELETE_ERROR;
		goto ret;
	}
	cout << "\nDatabase '" << filename << "' dropped\n\n";
	
	
	ret:
	BM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=========================================================================
//=========================================================================
/*
bool getFreePage(int* freePageNum, char* &dbHeaderBuf){

	

	//call expand if FreePagePointer == -1
		//free pointer = lastPageNum		
		//increment totNoOfPages, lastPageNum in dbHeader by BM_NO_OF_PAGES_TO_EXPAND no of pages
		

	//give free page pointer in dbheader as freepagenum

	return true;
}
*/

#endif
