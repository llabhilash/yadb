#ifndef BMDECL_H
#define BMDECL_H

class BufferManager{
	
	public:
	BufferManager();
	~BufferManager();
	
	bool commitBuffer();		
	bool read(int, char*, int);				//pageNo, buffer, priority
	bool write(int, char*, int);				//pageNo, buffer, priority
//	bool getFreePage(int*);					//===================UNDEFINED============================
	bool setSchemaFile(char*, bool dispErr = true);		//filename
	bool init(int);						//size of ram to use as buffer
	bool shutdown();
	bool setPageAsFree(int, char* &);			//pageNo
	bool expandHeapFile(int);				//last page num
	bool deleteSchemaFile(char*);				//schemaname
	void showHitRatio();
	void showWorking();

	private:
	int noOfHits;
	int noOfReads;
	int noOfWrites;
	int noOfFrames;
	Frame* bufferPool;
	FILE* heapfile;
	//char* heapfilename;
	string heapfilename;
	map<int, int> pageToFrame;

	void invalidateBuffer();
	void destroyBuffer();	
	bool writeToDisk(int,int);		//pageNo,frameNo
	bool readFromDisk(int,int);		//pageNo,frameNo
	int getVictimFrameNo();
	bool isPagePresentInBuffer(const int pageNum);
	int getFrameNoForPage(const int pageNum);
	void victimizeOldPageWithNewPage(const int oldPageNum, const int newPageNum, const int frameNo);
	void hexDumpPage(int pageNo, char* &pagebuf);
};
#endif
