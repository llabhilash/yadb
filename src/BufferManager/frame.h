#include "./../Common/Settings.h"
#include "./../Common/Priorities.h"
#include "ctime"
#include "iostream"


#ifndef FRAME_H
#define FRAME_H

class Frame{

    private:

    int dirty; //Tells if the page has been modified
    int priority; //Priority of this page
    time_t lastUsedTime; //Last time when this frame was used to read/write
    int pageNo; //Page number used to identify the page
    int pageHitCount;  //number of times, this pageNo has been hit

    public:
    char* pageData; //actual content of the page       

    Frame(){
        
        dirty = 0;
        pageHitCount = 0;
        pageNo = -1;
        priority = MIN_PRIORITY; //least priority
	lastUsedTime = time (NULL);
	pageData = NULL;
	pageData = new char[PAGE_SIZE_IN_BYTES];
        //verbose && cout << "Creating a new page\n";

    }
	
    void dump(int frameNo){
		using namespace std;
		cout << "\nFrame " << frameNo << " Details : " << endl;
		cout << "\tPageNo : " << getPageNo();
		cout << ", Dirty : ";
		if(isDirty()) 
			cout << "YES";
		else 
			cout << "NO";
		cout << ", HitCount : " << getHitCount();		
		cout << ", Priority : " << getPriority() << " (MAX PRIORITY : " << MAX_PRIORITY << ")";
		cout << ", LastUsed : " << getLastUsedTime() << endl;
    }
  

    ~Frame(){            
		delete[] pageData;
		pageData = NULL;		
    }

    void setDirty(){
		dirty = 1;
    }

    void clearDirty(){
		dirty = 0;
    }

    bool isDirty(){
		if(dirty == 1)
			return true;
		else
			return false;
    }

    void setPriority(int pri){
		priority = pri;
    }
    
    int getPriority(){
		return (int)priority;
    }

    time_t getLastUsedTime(){
		return lastUsedTime;
    }

    void setLastUsedTime(){
		lastUsedTime = time (NULL);
		//cout << "Set last used time = " << lastUsedTime << endl;
		//int num = 99999999999;
		//while(num)
		//	num--;
		//sleep(1);
    }

    int getPageNo(){
		return pageNo;
    }
 
    void setPageNo(int pageNum){
		pageNo = pageNum;
    }
    
    void hit(){
    	pageHitCount++;
    }
    
    int getHitCount(){
    	return pageHitCount;
    }
    
    void resetHits(){
    	pageHitCount = 0;
    }

};
#endif
