#include <sstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <stack>

#ifndef EVALUATE_EXPRESSION_H
#define EVALUATE_EXPRESSION_H

#include "./../Common/Settings.h"
#include "./../Common/DataType.h"
#include "./../Common/ErrorCodes.h"
//#include "./../Common/Packing_new.h"
//#include "./../Datastructures/alldatastructures.h"
#include "./../Datastructures/pagedataentries.h"
#include "./../Parser/infix2postfix.h"
#include "./evaluate_expressiondefn.h"

#endif
