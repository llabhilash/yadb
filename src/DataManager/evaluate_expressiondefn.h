#ifndef EXPR_EVAL
#define EXPR_EVAL

//check Where condition
int evaluate_expression(vector<string> condition_array, vector<string> recAttributes,vector<SysColumnRecord> SysColRecs, string &retResult){
	
	//cout<<"Entered eval_expr fn"<<endl;

	int err = NO_ERROR;
	int* getAttrIndex;
	string item;
	double dval1,dval2;
	string compRes;
	string result("0");
	bool exists;
	string oper, opnd1,opnd2;
	stack<int, vector<string> > postfixStack;
	int i,j;
	
	if( condition_array.size() == 0 ){
		retResult = "1";
		goto ret;
	}
	retResult = "0";
	// Evaluate the condition
	for(i=0;i<condition_array.size();i++){
		
			//cout<<"Entered loop for condition_array, item: "<<i+1<<endl;
			stringstream out("");
			item = condition_array[i];
			dval1 = dval2 = 0;
			result = "0";
			err = NO_ERROR;
			oper.clear();
			opnd1.clear();
			opnd2.clear();
			CONDITION_ARRAY_TRACE && cout<<"Current Item: "<<item<<endl;
			//Item is b/w single Qoutes
			if( item[0] == '\'' && item[item.size()-1] == '\''){
				CONDITION_ARRAY_TRACE && cout<<"Item b/w S Quotes "<<endl;
				postfixStack.push(item);
			}
			//Item is b/w Double Qoutes
			else if(item[0] == '"' && item[item.size() - 1] == '"'){
				CONDITION_ARRAY_TRACE && cout<<"Item b/w D Quotes "<<endl;
				postfixStack.push(item);
			}
			//Item is Operator
			else if(isOperator(item)){
				CONDITION_ARRAY_TRACE && cout<<"Item is operator "<<endl;
				if(postfixStack.size() < 1){
					CONDITION_ARRAY_TRACE &&  cout<<"Error stack is empty"<<endl;
					err = CONDITION_ERROR;
					goto ret;
				}
				oper = item;
				opnd2 = (string)postfixStack.top();
				postfixStack.pop();
				if(oper.compare("!") != 0){
					opnd1 = (string)postfixStack.top();
				}
				if( isMathOperator(oper)){
				
					if(opnd1[0] == '\'' && opnd1[opnd1.size() - 1] == '\''){
						err = NOT_NUMBER_ERROR;
						goto ret;
					}
					else if(!isNumber(opnd1,dval1)){
						exists = false;
						if( !(opnd1[0] == '"' && opnd1[opnd1.size() - 1] == '"' ) ){
							cout << " Error, can perform mathematical operations on strings and numbers only "<<endl;
							err = NOT_NUMBER_ERROR;
							goto ret;
						}
						/*for(j=0;j<SysColRecs.size();j++){
							if(!opnd1.compare(SysColRecs[j].columnName) && !SysColRecs[j].isDeleted)
							{
								if(!isNumber(recAttributes[j],dval1)){
									err = NOT_NUMBER_ERROR;
									goto ret;
								}
								if(exists){
									err = COLUMN_REDEFINITION;
									goto ret;
								}
								exists = true;
							}
						}*/
						/*if(!exists){
							err = COLUMN_NOT_DEFINED;
							goto ret;
						}*/
					}				
					/*if(!isNumber(opnd2,dval2) && (opnd2[0] == '\'' && opnd2[opnd2.size() - 1] == '\'') ){
						err = NOT_NUMBER_ERROR;
						goto ret;
					}*/
					else if(!isNumber(opnd2,dval2)){
						exists = false;
						if( !(opnd2[0] == '"' && opnd2[opnd2.size() - 1] == '"')){
							cout << " Error, can perform mathematical operations on strings and numbers only "<<endl;
							err = NOT_NUMBER_ERROR;
							goto ret;						
						}
						/*for(j=0;j<SysColRecs.size();j++){
							if(!opnd2.compare(SysColRecs[j].columnName) && !SysColRecs[j].isDeleted)
							{
								if(!isNumber(recAttributes[j],dval2)){
									err = NOT_NUMBER_ERROR;
									goto ret;
								}
								if(exists){
									err = COLUMN_REDEFINITION;
									goto ret;
								}
								exists = true;
							}
						}*/
						/*if(!exists){
							err = COLUMN_NOT_DEFINED;
							goto ret;
						}*/
					}
					
				if(!evalMathExpr(opnd1,opnd2,oper,result)){
					cout<<"Error, given expression involves use of mathematical operator on illegal operands, only integer and strings allowed"<<endl;
					err = CONDITION_ERROR;
					goto ret;
				}
				
				out << result;
				postfixStack.pop();
				postfixStack.push(out.str());
			}//end of Math Operator Block
			
			//is comparision operator
			if(isComparisionOperator(oper)){
				CONDITION_ARRAY_TRACE && cout<<"Operator is Comparision "<<endl;
				compRes = "0";
				compRes = evalComparExpr(opnd1,opnd2,oper);
				out << compRes;
				postfixStack.pop();
				postfixStack.push(out.str());
				CONDITION_ARRAY_TRACE && cout<<"Result: "<<out.str()<<endl;
				//cout<<"Exiting comparision "<<endl;
			}
			
			//logical Operator
			if(!oper.compare("AND") || !oper.compare("OR")){
				CONDITION_ARRAY_TRACE && cout<<"Operator is AND or OR "<<endl;
				compRes = "0";
				compRes = evalLogicalOperator(opnd1,opnd2,oper);
				out << compRes;
				postfixStack.pop();
				postfixStack.push(out.str());
				CONDITION_ARRAY_TRACE && cout<<"Result: "<<out.str()<<endl;
			}
			
			//NOT operator
			if(!oper.compare("!")){
				CONDITION_ARRAY_TRACE && cout<<"Operator is NOT "<<endl;
				CONDITION_ARRAY_TRACE && cout<<"Operand: "<<opnd2<<endl;
				compRes = "0";
				compRes = evalNotOperator(opnd2,oper);
				out << compRes;
				
				// 'cause ! is a unary operator, need to push the operand tht has been poped out at the beganning of loop
				//postfixStack.push(opnd2);
				
				postfixStack.push(out.str());
				CONDITION_ARRAY_TRACE && cout<<"Result: "<<out.str()<<endl;
			}
			
			//LIKE operator
			if(!oper.compare("LIKE") || !oper.compare("NOTLIKE")){
				 CONDITION_ARRAY_TRACE && cout<<"Operator is LIKE or NOTLIKE "<<endl;
				//cout<<"opnd1: "<<opnd1<<endl;
				//cout<<"opnd2: "<<opnd2<<endl;
				compRes = "0";
				/*if( (opnd2[0] != '"' && opnd2[opnd2.size()-1] != '"') || (opnd2[0] != '\'' && opnd2[opnd2.size()-1] != '\'')){
					err = CONDITION_ERROR;
					goto ret;
				}*/
				/*
				if( (opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"') || (opnd1[0] == '\'' && opnd1[opnd1.size()-1] == '\''))				{
					err = CONDITION_ERROR;
					goto ret;
				}*/
				compRes = evalLikeOperator(opnd1,opnd2,oper);
				out << compRes;
				postfixStack.pop();
				postfixStack.push(out.str());
				//cout<<"Result: "<<out.str()<<endl;
			}
			
		}//end of is Operator Block
		
		//item is column name or number or result of previous evaluation
		else{
			//cout<<"Item possibly col name or number "<<item<<endl;
			if(isNumber(item,dval1)){
				CONDITION_ARRAY_TRACE && cout<<"Adding Item: "<<item<<endl;
				postfixStack.push(item);
			}
			else{
				//cout<<"Checking for "<<item<<endl;
				exists = false;
				for(j=0;j<SysColRecs.size();j++){
		
					if(!item.compare(SysColRecs[j].columnName) && !SysColRecs[j].isDeleted )
					{
						if(j >= recAttributes.size()){

							if(SysColRecs[j].dataType != VARCHARTYPE)
								item = SysColRecs[j].defaultvalue;
							else{
								item = "\"";
								item.append(SysColRecs[j].defaultvalue);
								item.append("\"");
							}
						}
						else{	
							if(SysColRecs[j].dataType != VARCHARTYPE)
								item = recAttributes[j];
							else{
								item = "\"";
								item.append(recAttributes[j]);
								item.append("\"");
							}
						}	
						if(exists){
							err = COLUMN_REDEFINITION;
							goto ret;
						}
						exists = true;
					}
				}
				if(!exists){
					cout<<"Column: "<<item<<" not defined"<<endl;
					err = COLUMN_NOT_DEFINED;
					goto ret;
				}
				CONDITION_ARRAY_TRACE && cout<<"Adding Item: "<<item<<endl;
				postfixStack.push(item);
			}
		}
	}//End of for loop of all items in where condition
	
	if( postfixStack.empty()  )
		err = CONDITION_ERROR;
	else if( (postfixStack.top()).compare("0")  != 0){
		retResult = postfixStack.top();
		if(retResult[0] == '"' && retResult[retResult.size() -1] == '"'){
			retResult.erase(0,1);
			retResult.erase(retResult.size() - 1, 1);
		}
	}
	ret:	return err;
}

bool checkValidColumnName(vector<string> condition_array, vector<SysColumnRecord> SysColRecs){
	int i=0, j=0;
	string item;
	bool exists = true;
	double dval1;
	
	for(i=0;i<condition_array.size();i++){
		item = condition_array[i];
		if( item[0] == '\'' && item[item.size()-1] == '\''){
				continue;
			}
			//Item is b/w Double Qoutes
			else if(item[0] == '"' && item[item.size() - 1] == '"'){
				continue;
			}
			//Item is Operator
			else if(isOperator(item)){
				continue;
			}
			else if(isNumber(item,dval1)){
				continue;
			}
			else{
				//cout<<"Checking for "<<item<<endl;
				exists = false;
				for(j=0;j<SysColRecs.size();j++){
					if(!item.compare(SysColRecs[j].columnName) && !SysColRecs[j].isDeleted ){
						exists = true;
						break;
					}	
				}
				if(!exists){
					cout<<"Column: "<<item<<" not defined"<<endl;
					break;
				}
			}
	}
	return exists;
}
#endif
