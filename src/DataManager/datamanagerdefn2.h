//================================================================================================
bool DataManager::checkDataType(int fromDT, int toDT){

	bool err = NO_ERROR;

	//check if fromDT can be converted to toDT	
	switch(fromDT){
					case INTTYPE:
	 							if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != INTTYPE)
	 								err = INVALID;
								break;
								
					case DATETYPE:
								if(toDT != DATETYPE)
	 								err = INVALID;
								break;
								
					case FLOATTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != INTTYPE && toDT != FLOATTYPE)
	 								err = INVALID;
								break;
								
					case LONGTYPE:
								if(toDT != INTTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE && toDT != LONGTYPE)
	 								err = INVALID;
								break;
								
					case DOUBLETYPE:
								if(toDT != LONGTYPE && toDT != INTTYPE && toDT != FLOATTYPE && toDT != DOUBLETYPE)
	 								err = INVALID;
								break;
								
					case VARCHARTYPE:
								if(toDT != VARCHARTYPE)
	 								err = INVALID;
								break;
								
					case SHORTTYPE:
								if(toDT != LONGTYPE && toDT != DOUBLETYPE && toDT != FLOATTYPE 
									&& toDT != INTTYPE && toDT != SHORTTYPE)
	 								err = INVALID;
								break;
								
					case BOOLTYPE:
								if(toDT != BOOLTYPE)
	 								err = INVALID;
								break;
	}
	
	
	return err==NO_ERROR?true:false;
}

 
//=============================================================================================================

template <class X>
string DataManager::to_string(const X& x){
	stringstream s;
	s << x;
	return s.str();
}

//===============================================================================

bool DataManager::convertRecordToSysColRec(SysColumnRecord &syscolrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysColRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //column name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col pos
	colTypes.push_back(INTTYPE); //data type
	colTypes.push_back(BOOLTYPE); //is not null
	colTypes.push_back(BOOLTYPE); //is unique
	colTypes.push_back(BOOLTYPE); //is primary
	colTypes.push_back(BOOLTYPE); //is deleted
	colTypes.push_back(INTTYPE); //max size
	colTypes.push_back(VARCHARTYPE); //default value
	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//syscolrec.columnname
	memcpy(&syscolrec.columnName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	//table name
	memcpy(&syscolrec.tableName, recAttributes[1].c_str(), (recAttributes[1].size() + 1));
	
	//col pos
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.colPos = *(short*)att_value;
	delete (short*)att_value;
	
	//data type
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.dataType = *(int*)att_value;
	delete (int*)att_value;
	
	//is not null
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isNotNull = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is primary
	if( !pack::convert_to_type(recAttributes[5], colTypes[5], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isPrimary = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is Unique
	if( !pack::convert_to_type(recAttributes[6], colTypes[6], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isUnique = *(bool*)att_value;
	delete (bool*)att_value;
	
	//is deleted
	if( !pack::convert_to_type(recAttributes[7], colTypes[7], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.isDeleted = *(bool*)att_value;
	delete (bool*)att_value;
	
	//max size
	if( !pack::convert_to_type(recAttributes[8], colTypes[8], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	syscolrec.maxSize = *(int*)att_value;
	delete (int*)att_value;
	
	
	//default value
	memcpy(&syscolrec.defaultvalue, recAttributes[9].c_str(), (recAttributes[9].size() + 1));
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysColRecToRecord(SysColumnRecord syscolrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysColRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //column name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col pos
	colTypes.push_back(INTTYPE); //data type
	colTypes.push_back(BOOLTYPE); //is not null
	colTypes.push_back(BOOLTYPE); //is primary
	colTypes.push_back(BOOLTYPE); //is unique
	colTypes.push_back(BOOLTYPE); //is deleted
	colTypes.push_back(INTTYPE); //max size
	colTypes.push_back(VARCHARTYPE); //default value
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	//add column name	
	recAttributes.push_back((to_string(syscolrec.columnName)));
	
	//add table name
	recAttributes.push_back((to_string(syscolrec.tableName)));
	
	//add col pos
	recAttributes.push_back(to_string(syscolrec.colPos));

	//add data type
	recAttributes.push_back(to_string(syscolrec.dataType));
	
	//add is not null
	recAttributes.push_back(to_string(syscolrec.isNotNull));	
	
	//add is primary
	recAttributes.push_back(to_string(syscolrec.isPrimary));	
	
	//add is unique
	recAttributes.push_back(to_string(syscolrec.isUnique));	
	
	//add is deleted
	recAttributes.push_back(to_string(syscolrec.isDeleted));
	
	//add max size
	recAttributes.push_back(to_string(syscolrec.maxSize));	
	
	//add default value
	recAttributes.push_back(to_string(syscolrec.defaultvalue));
	
	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertRecordToSysTabRec(SysTableRecord &systabrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysTabRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(INTTYPE); //total rows
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(INTTYPE); //total DP
	colTypes.push_back(INTTYPE); //total columns

	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//systabrec.tablename
	memcpy(&systabrec.tableName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	
	//total rows
	if( !pack::convert_to_type(recAttributes[1], colTypes[1], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalRows = *(int*)att_value;
	delete (int*)att_value;
	
	
	//start page
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.startPage = *(int*)att_value;
	delete (int*)att_value;
	
	//total DP
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalDP = *(int*)att_value;
	delete (int*)att_value;
	
	//no of cols
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	systabrec.totalColumns = *(int*)att_value;
	delete (int*)att_value;
		
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysTabRecToRecord(SysTableRecord systabrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysTabRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(INTTYPE); //total rows
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(INTTYPE); //total DP
	colTypes.push_back(INTTYPE); //total columns
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	//add table name	
	recAttributes.push_back((to_string(systabrec.tableName)));
	
	//add total rows
	recAttributes.push_back((to_string(systabrec.totalRows)));
	
	//add start page
	recAttributes.push_back((to_string(systabrec.startPage)));
	
	//add total DP
	recAttributes.push_back((to_string(systabrec.totalDP)));
	
	//add total columns
	recAttributes.push_back((to_string(systabrec.totalColumns)));
	
	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertSysIndexRecToRecord(SysIndexRecord sysindexrec, char* &rec, int &recSize){

	DM_FUNC_TRACE && cout << "DM.convertSysIndexRecToRecord()\n";
	int err = NO_ERROR;
	
	vector<int> colTypes;
	
	colTypes.push_back(VARCHARTYPE); //index name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col orig pos
	colTypes.push_back(SHORTTYPE); //col index pos
	colTypes.push_back(INTTYPE); //start page
	colTypes.push_back(SHORTTYPE); //no of index cols
	
	vector<string> recAttributes;
	string* attrib = NULL;
	
	
	//add index name	
	recAttributes.push_back((to_string(sysindexrec.indexName)));
	
	//add table name	
	recAttributes.push_back((to_string(sysindexrec.tableName)));
	
	//add col orig pos
	recAttributes.push_back((to_string(sysindexrec.colOrigPos)));
	
	//add col index pos
	recAttributes.push_back((to_string(sysindexrec.colIndexPos)));
	
	//add start page
	recAttributes.push_back((to_string(sysindexrec.startPage)));

	//add no of index cols
	recAttributes.push_back((to_string(sysindexrec.totalIndexCols)));
	

	//now pack the attributes
	if(!pack::pack_data(recAttributes, rec, recSize, colTypes)){
		err = PACK_ERROR;
		goto ret;
	}	
	
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::convertRecordToSysIndexRec(SysIndexRecord &sysindexrec, char* rec, int recSize){

	DM_FUNC_TRACE && cout << "DM.convertRecordToSysIndexRec()\n";
	int err = NO_ERROR;

	//convert the record to the syscolrec
	
	//columns types of SysTabRec
	vector<int> colTypes;
	vector<string> recAttributes;
	void* att_value = NULL;
	
	colTypes.push_back(VARCHARTYPE); //index name
	colTypes.push_back(VARCHARTYPE); //table name
	colTypes.push_back(SHORTTYPE); //col orig pos
	colTypes.push_back(SHORTTYPE); //col index pos
	colTypes.push_back(INTTYPE); //col index pos
	colTypes.push_back(SHORTTYPE); //total index cols
	
	
	//use col types to extract attributes
	if(!pack::unpack_data(recAttributes, colTypes, rec, recSize)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	//now copy every attribute to syscolrec
	
	//sysindexrec.tablename
	memcpy(&sysindexrec.indexName, recAttributes[0].c_str(), (recAttributes[0].size() + 1));
	
	//systabrec.tablename
	memcpy(&sysindexrec.tableName, recAttributes[1].c_str(), (recAttributes[1].size() + 1));
	
	
	//col orig pos
	if( !pack::convert_to_type(recAttributes[2], colTypes[2], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.colOrigPos = *(short*)att_value;
	delete (short*)att_value;
	
	//col index pos
	if( !pack::convert_to_type(recAttributes[3], colTypes[3], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.colIndexPos = *(short*)att_value;
	delete (short*)att_value;
	
	//col start page
	if( !pack::convert_to_type(recAttributes[4], colTypes[4], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.startPage = *(int*)att_value;
	delete (int*)att_value;
	
	//total index cols
	if( !pack::convert_to_type(recAttributes[5], colTypes[5], att_value)){
		err = CONVERT_ERROR;
		goto ret;
	}
	sysindexrec.totalIndexCols = *(short*)att_value;
	delete (short*)att_value;
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=============================================================================================================
bool DataManager::checkRecordForConstraints(SysTableRecord &sysTabRec, vector<SysColumnRecord> &sysColumns, vector<SysIndexRecord> &sysIndexRecs, vector<string> &finalValues){
	
	DM_FUNC_TRACE && cout << "DM.checkRecordForConstraints()\n";
	int err = NO_ERROR;

	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> recAttributes;

	vector<int> colTypes;

	vector<SysIndexRecord> primarySysIndexRecs;
	string tempstring;

	vector<string> key;
	vector<int> keyTypes;
	char* record = NULL;
	int size = 0;
	RID rid;


	bool primaryConstraintViolation = false;
	bool primaryConstraintPresent = false;
	bool uniqueConstraintPresent = false;
	int colsDeleted = 0,j=0;


	for(int i=0; i<sysColumns.size(); i++){
		colTypes.push_back(sysColumns[i].dataType);
		if(sysColumns[i].isPrimary)
			primaryConstraintPresent = true;		
		if(sysColumns[i].isUnique)
			uniqueConstraintPresent = true;		
	}

	//check in primary key index for primary key constraint
	//make use of table scan if unique is present else use only indexer
	

	if(primaryConstraintPresent && !uniqueConstraintPresent){
		//use indexer

		//get the primary key index recs
		//name of the primary key index
		tempstring = "primary_key_index";
		for(int j=0; j<sysIndexRecs.size(); j++){
			//find all index recs matching indexNames[i]
			if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
				primarySysIndexRecs.push_back(sysIndexRecs[j]);
			}
			
		}

		if(primarySysIndexRecs.size() > 0 ){

			key = vector<string> (primarySysIndexRecs.size(),""); // dummy initial values for both key and key types
			keyTypes = vector<int> (primarySysIndexRecs.size(),-1);
	
			DM_FUNC_TRACE && cout << "Searching if the following primary key exists in the primary key index\n"; 
			for(int j=0; j<primarySysIndexRecs.size(); j++){
			
				//write the attribute at colOrigPos to the key in the colIndexPos
		
				key[primarySysIndexRecs[j].colIndexPos] = finalValues[primarySysIndexRecs[j].colOrigPos];
				keyTypes[primarySysIndexRecs[j].colIndexPos] = sysColumns[primarySysIndexRecs[j].colOrigPos].dataType;
				
				DM_FUNC_TRACE && cout << key[primarySysIndexRecs[j].colIndexPos] << " ( " << sysColumns[primarySysIndexRecs[j].colOrigPos].columnName << " :: " << sysColumns[0].getTypeName(keyTypes[primarySysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
			}

			//pack the key
			if(record != NULL)
				delete[] record;
			if(!pack::pack_data(key, record, size, keyTypes)){
				err = PACK_ERROR;
				goto ret;
			}

			//now the primary key is built.. search it using the index
			//indexSearch(const int &indexerpage,const vector<int> &keyType,char* const &key,RID &rid,bool &found);	
			if(!indexSearch(primarySysIndexRecs[0].startPage, keyTypes, record, rid, primaryConstraintViolation)){
				err = INVALID;
				goto ret;
			}

			//check if it is present or not....
			if(primaryConstraintViolation){
				err = PRIMARY_KEY_ERROR;
				cout << "PRIMARY KEY constraint violated" << endl;
				goto ret;
			}	

			//goto ret on success or failure
			goto ret;
		}else{
			DM_FUNC_TRACE && cout << "'primary key index' has been DROPPED. Resorting to table scan\n"; 
		}
	}

	

	
	//if unique constraint is present only then do table scan and search 
	//fetch all records.. check each record if any of the value are not unique
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}
	
	for(int i=0; i<allrecs.size(); i++){

		recAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}

		primaryConstraintViolation = true;

		colsDeleted = 0;

		//for the current record..  check if there are any unqiue constraint conflict
		for(j=0; j<recAttributes.size(); j++){

			if(sysColumns[j].isDeleted){
				colsDeleted++;
				continue;
			}

			//if atleast one of the primary key fields differ, then primary key constraint holds
			//if(primaryConstraintPresent && sysColumns[j].isPrimary && (finalValues[j].compare(recAttributes[j]) != 0)){
			if(primaryConstraintPresent && sysColumns[j].isPrimary){
				
				//cout << "Comparing the two values for equality : Final-" << finalValues[j] << " , rec-" << recAttributes[j] << endl;

				if(finalValues[j].compare(recAttributes[j]) != 0){
					primaryConstraintViolation = false;
				}
				//if(primaryConstraintViolation)
				//cout << "Primary Error : " << finalValues[j] << endl ;
			}


			//if the column value has to be unique and same as that of a record present in the DB and is not a NULL value.. then unique constraint is violated.
			if(sysColumns[j].isUnique && (finalValues[j].compare(recAttributes[j]) == 0) && (recAttributes[j].compare("NULL") != 0)){
				err=NOT_UNIQUE_ERROR;
				cout << "Value for column '" << to_string(sysColumns[j].columnName)<< "' violates UNIQUE constraint" << endl;
				goto ret;
			}	
		}
		
		//if all cols are deleted.. make primary constraint violation as false
		if((colsDeleted == (j-1)) && recAttributes.size()!=1)
			primaryConstraintViolation=false;

		if(primaryConstraintPresent && primaryConstraintViolation){
			err = PRIMARY_KEY_ERROR;
			cout << "PRIMARY KEY constraint violated" << endl;
			goto ret;
		}
	}



	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::getRecordsUsingIndex(string indexName, vector<int> indexPos, vector<int> operatorCodes, vector<string> values, vector<SysColumnRecord> &sysColumns, DBHeader &dbHeader, vector<SysIndexRecord> sysIndexRecs, vector<string> &records, vector<int> &recsizes, vector<RID> &recRIDs){

	DM_FUNC_TRACE && cout << "DM.getRecordsUsingIndex()\n";
	int err = NO_ERROR;

	vector<SysIndexRecord> currentSysIndexRecs;
	vector<RID> allrids;
	vector<int> indexKeyTypes;

	map<int, vector<RID> > mapPageToRIDs;
	map<int, vector<RID> >::iterator iter;
	vector<RID> tempRIDList;

	vector<string> tempRecords;
	vector<int> tempRecSizes;
	DataBasePage* DataPage = NULL;

	vector<string> newKey;
	vector<string> key;
	vector<string> rightmostKey;
	vector<string> leftmostKey;
	

	int size;
	char* packedkey = NULL;
	char* datapagebuf = NULL;

	//pad the 'extra' fields to fill up as required by the indexer..
	//eg: if the query is on col A and the index is on col A, col B ..  put in some dummy value for col B

	

	//get sys index recs for this index
	for(int j=0; j<sysIndexRecs.size(); j++){
		//find all index recs matching indexName ( the index which has to be used to fetch the records)
		if(indexName.compare(to_string(sysIndexRecs[j].indexName)) == 0){
			currentSysIndexRecs.push_back(sysIndexRecs[j]);
		}
	}	

	// dummy initial values for both key types
	indexKeyTypes = vector<int> (currentSysIndexRecs.size(),-1);
	for(int i=0; i<currentSysIndexRecs.size(); i++){
		indexKeyTypes[currentSysIndexRecs[i].colIndexPos] = sysColumns[currentSysIndexRecs[i].colOrigPos].dataType;
		//cout << "indexkeytypes["<< currentSysIndexRecs[i].colIndexPos << "] ==> data type : " <<sysColumns[currentSysIndexRecs[i].colOrigPos].dataType <<endl;
	}




	//find out the rightmost key and the leftmost key and use the one with more columns
	//and assign the key with most keys to 'key'
	//
	//
	//
	//
	//

	for(int k=0; k<indexPos.size(); k++){
		//cout << "indexpos = " << indexPos[k] << ", op = " << operatorCodes[k] << ", value = " << values[k] << endl;
	}

	if(!constructRightmostKey(indexKeyTypes, indexPos, operatorCodes, values, rightmostKey)){
		err = INVALID;
		goto ret;
	}

	if(!constructLeftmostKey(indexKeyTypes, indexPos, operatorCodes, values, leftmostKey)){
		err = INVALID;
		goto ret;
	}

	
	key = rightmostKey;

	//use the key which has maximum key size
	if(leftmostKey.size() > rightmostKey.size()){
	
		key = leftmostKey;
	}

	newKey = key;

	//cout << "newkey = " << newKey.size() << ", key = " << key.size() << endl;


	for(int i=key.size(); i<currentSysIndexRecs.size(); i++){
		newKey.push_back("0");	//padded values for the extra columns
		//cout << "Padding\n";
	}	

	for(int i=0; i<newKey.size(); i++){
		//cout << "key ["<<i<<"] ("<< newKey[i] <<") ==> types("<< indexKeyTypes[i]<< ")\n";
	}

	//pack the new key
	if(!pack::pack_data(newKey, packedkey, size, indexKeyTypes)){
		err = PACK_ERROR;
		goto ret;
	}

	//get all the RIDs using the index given
	if(!indexGetRID(currentSysIndexRecs[0].startPage, indexKeyTypes, packedkey, allrids, key.size(), indexPos, operatorCodes, values)){
		err = INVALID;
		goto ret;
	}	

	recRIDs = allrids;	

	//get unique page numbers from the RIDs
	//maintain a map for page number to vector of RIDS for that page
	for(int i=0; i<allrids.size(); i++){
		//check if the page number is present in the map..
		iter = mapPageToRIDs.find(allrids[i].pageNo);
		tempRIDList.clear();
		if( iter != mapPageToRIDs.end() ){
			//a vector of RIDs already exists
			tempRIDList = iter->second;
		}

		//add the current RID to the temp RID list
		bool isPresent = false;
		for(int j=0; j<tempRIDList.size(); j++){
			//add to list only if it does not exist
			if(allrids[i].slotNo == tempRIDList[j].slotNo){
				isPresent = true;			
				break;
			}
		}
		if(!isPresent)
			tempRIDList.push_back(allrids[i]);

		//add the tempRIDList to this page
		mapPageToRIDs[allrids[i].pageNo] = tempRIDList;

		tempRIDList.clear();
	}

	//read a page.. collect all records corresponding to those RIDs
	for(map<int, vector<RID> >::const_iterator it = mapPageToRIDs.begin(); it != mapPageToRIDs.end(); ++it){
		
		//read the page
		//read page in DE into BufferPool
		if(datapagebuf != NULL){
			delete[] datapagebuf;
		}
		datapagebuf = new char[PAGE_SIZE_IN_BYTES];

		if(!bm.read( it->first, datapagebuf, DB_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		
		if(DataPage != NULL){
			delete DataPage;
		}
		
		DataPage = new DataBasePage(datapagebuf);
			
		//get all records for this page with the given set of RIDs
		tempRecords.clear();
		tempRecSizes.clear();
		tempRIDList.clear();
		tempRIDList = it->second;
		if(!DataPage->getRecordsForRIDs( tempRecords, tempRecSizes, tempRIDList)){
			err = INVALID;
			goto ret;
		}
		
		//add the records to the final list
		for(int i=0; i<tempRecords.size(); i++){
			records.push_back(tempRecords[i]);
			recsizes.push_back(tempRecSizes[i]);
		}
		tempRecords.clear();
		tempRecSizes.clear();
		tempRIDList.clear();
	}

	ret:
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	if(packedkey != NULL){
		delete[] packedkey;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=============================================================================================================
bool DataManager::constructRightmostKey(vector<int> indexTypes, vector<int> colPos, vector<int> operatorCodes, vector<string> values, vector<string> &newKey){

	DM_FUNC_TRACE && cout << "DM.constructRightmostKey()\n";
	int err = NO_ERROR;
	

	//construct the key to be sent to the index
	//find out the greatest of values if all the operators on column is >
	//find out the smallest of values if all the operators on column is <
	//store operator for the first column.. store for other columns which has the same operator as the first column
	//eg: if 1st column has only > (<) operators.. then they generated key will have the maximum (minimum) value supplied by the user
	//the following columns should have only > or = (< or =) operators for it.. 
	//
	//   index
	//  colpos     operatorCodes    values
	//  	0           =		  10
	//  	0           <             20
	//  	0           <             14    (choose..  store operator as <) 
	//  	1           =             30    (choose.. as it can be used for tree traversal)
	//  	2           >             50    (stop as this columns has no operator < ..)
	

	for(int i = 0; i < colPos.size(); ){

		//find out till where does this column extend..
		int j = i;
		while(j < colPos.size() && colPos[j] == colPos[i]){
			j++;			
		}

		//this column extends from i ... to j-1
		bool found = false;
		string curMaxVal = "";

		//check 
		for(int k=i; k<j; k++){
			
			//cout << "indexpos = " << colPos[k] << ", op = " << operatorCodes[k] << ", value = " << values[k] << endl;
		
			if((operatorCodes[k] == GREATER) || (operatorCodes[k] == GREATERTHANEQUAL) || (operatorCodes[k] == EQUALS)){
				
				if(!found)	//first time
					curMaxVal = values[k];
				else{
						
					//convert both the values[k] and curVal to its datatype.. 
					//call function with the two values, datatype and the operator as greater than equal...
					//and store the maximum back
					bool result = false;
					if(!LeafPage::compareFunction( values[k], curMaxVal, indexTypes[colPos[i]], GREATERTHANEQUAL, result)){
						err = INVALID;
						goto ret;
					}
								

					//if true.. store the new value as maximum
					if(result){
						curMaxVal = values[k];
						
					}


				}

				found = true;

			}
		}

		//if not found.. stop
		if(!found){
			break;
		}else{
				
			//store the cur max val into the new key index pos
			//newKey[colPos[i]] = curMaxVal;		
			newKey.push_back(curMaxVal);
			DM_FUNC_TRACE && cout << "max right ..   key[" << colPos[i] << "] ==> " << curMaxVal << endl;
			//increment i by j
			i += j;
		}
				
	}

	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=============================================================================================================
bool DataManager::constructLeftmostKey(vector<int> indexTypes, vector<int> colPos, vector<int> operatorCodes, vector<string> values, vector<string> &newKey){
	
	DM_FUNC_TRACE && cout << "DM.constructLeftmostKey()\n";
	int err = NO_ERROR;

	//construct the key to be sent to the index
	//find out the greatest of values if all the operators on column is >
	//find out the smallest of values if all the operators on column is <
	//store operator for the first column.. store for other columns which has the same operator as the first column
	//eg: if 1st column has only > (<) operators.. then they generated key will have the maximum (minimum) value supplied by the user
	//the following columns should have only > or = (< or =) operators for it.. 
	//
	//   index
	//  colpos     operatorCodes    values
	//  	0           =		  10
	//  	0           <             20
	//  	0           <             14    (choose..  store operator as <) 
	//  	1           =             30    (choose.. as it can be used for tree traversal)
	//  	2           >             50    (stop as this columns has no operator < ..)

	 

	for(int i = 0; i < colPos.size(); ){

		//find out till where does this column extend..
		int j = i;
		while(j < colPos.size() && colPos[j] == colPos[i]){
			j++;			
		}

		//this column extends from i ... to j-1
		bool found = false;
		string curMinVal = "";

		//check 
		for(int k=i; k<j; k++){
		
			//cout << "indexpos = " << colPos[k] << ", op = " << operatorCodes[k] << ", value = " << values[k] << endl;
			
			if((operatorCodes[k] == LESSER) || (operatorCodes[k] == LESSTHANEQUAL) || (operatorCodes[k] == EQUALS)){
					
				if(!found)	//first time
					curMinVal = values[k];
				else{
				
					//convert both the values[k] and curVal to its datatype.. 
					//call function with the two values, datatype and the operator as less than equal...
					//and store the minimum back
					bool result = false;
					if(!LeafPage::compareFunction( values[k], curMinVal, indexTypes[colPos[i]], LESSTHANEQUAL, result)){
						err = INVALID;
						goto ret;

					}

					//if true.. store the new value as maximum
					if(result){
						curMinVal = values[k];
					}
				}

				found = true;
			}
		}

		//if not found.. stop
		if(!found){
			break;
		}else{
			
			//store the cur min val into the new key index pos
			//newKey[colPos[i]] = curMinVal;		
			newKey.push_back(curMinVal);
			DM_FUNC_TRACE && cout << "min left ..   key[" << colPos[i] << "] ==> " << curMinVal << endl;
			//increment i by j
			i += j;
		}
				
	}

	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
