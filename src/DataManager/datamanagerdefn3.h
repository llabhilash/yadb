//=======================================================================
bool DataManager::handleCreateIndex(parsedQuery &pq){
	
	int err = NO_ERROR;
	INDEX_FUNC_TRACE && cout << "DM.handleCreateIndex()\n";
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* indexbuf = new char[PAGE_SIZE_IN_BYTES];
	char* leafbuf = new char[PAGE_SIZE_IN_BYTES];
	char* rec= NULL;
	int recSize;
	int keySize = 0;
	int pos = 0;
	
	DBHeader* dbHeader = NULL;
	SysTableRecord sysTabRec;
	RID systabRID, dummyRID;
	
	vector<SysColumnRecord> sysColumns;
	vector<RID> sysColRIDs;
	vector<int> colTypes;
	char* record = NULL;
	int size;

	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> recAttributes;

	IndexPage* indexpage = NULL;
	LeafPage* leafpage = NULL;
	
	
	vector<string> indexCols = pq.getColumns();
	vector<int> indexKeyTypes;
	vector<string> key;
	int count = 0, indexpageno, leafpageno;
	SysIndexRecord tempsysindexrec;
	vector<SysIndexRecord> sysIndexRecs;
	vector<int> sysColOrigPos;
	vector<RID> sysIndexRIDs;
	
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbHeader), systabRID)){
		//Table already exists
		err = INVALIDTABLENAME;
		cout << "Table '" << pq.getTable() << "' does not exist\n";
		goto ret;
	}	
		
	//get sys col records for the table
	if(!getSysColRecsForTable(pq.getTable(), sysColumns, (*dbHeader), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

	for(int i=0; i<sysColumns.size(); i++){
		colTypes.push_back(sysColumns[i].dataType);	
	}
	
	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	
	//check if index has duplicate columns
	for(int i=0; i<indexCols.size(); i++){
		for(int j=i+1; j<indexCols.size(); j++){
			if(indexCols[i].compare(indexCols[j]) == 0){
				err = INVALID;
				cout << "Column list must be unique\n";
				goto ret;
			}
		}	
	}
	
	
	
	//check column name validity
	for(int i = 0; i < indexCols.size(); i++){		
			
		for(int j=0; j<sysColumns.size(); j++){
			
			if(sysColumns[j].isDeleted)
			continue;
		
			if(indexCols[i].compare(to_string(sysColumns[j].columnName)) == 0){
				count++;
				indexKeyTypes.push_back(sysColumns[j].dataType);
				//store orignal position
				sysColOrigPos.push_back(sysColumns[j].colPos);
				//add to sysColumns[j].maxSize to keySize
				keySize = keySize + sysColumns[j].maxSize;
				break;				
			}
		}
	}
	
	//if count != indexCols.size().. it means not all columns in column list is valid
	if(count != indexCols.size()){
		err = INVALID;
		cout << "INVALID Column(s) in column list\n";
		goto ret;
	}
	
	//check if index name exists
	for(int i=0; i<sysIndexRecs.size(); i++){
		if(pq.getIndexName().compare(to_string(sysIndexRecs[i].indexName)) == 0){
			//duplicate index
			err = 	INVALID;
			cout << "Index '" << pq.getIndexName() << "' already exists on table '" << pq.getTable() << "'\n";
			goto ret;
		}
	}
	
	//compute actual keySize.
	keySize = keySize + ((indexCols.size() + 1)*sizeof(short));
	//create dummy pages for new index	
	if(!getFreePage(indexbuf, indexpageno, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	//create dummy pages for new leaf ( ROOT )	
	if(!getFreePage(leafbuf, leafpageno, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}

	//compute the fan out factor
	// extra int added for safety... :D
	size = (PAGE_SIZE_IN_BYTES - (3 * sizeof(int) ) - (4 * sizeof(short)))/(keySize + sizeof(RID));
	//size = 4;
	indexpage = new IndexPage(indexbuf);
	indexpage->setPageType(INDEXHEADER);
	indexpage->setMaximumNodeEntries(size);
	indexpage->setKeySize(keySize);
	indexpage->setIndexerRoot(leafpageno);
	INDEX_FUNC_TRACE && indexpage->dump();
	
	leafpage = new LeafPage(leafbuf);
	leafpage->setPageType(LEAFNODE);
	leafpage->setMaximumNodeEntries(size);
	leafpage->setTotalKeysOccupied(0);
	leafpage->setKeySize(keySize);
	leafpage->setNextLeaf(-1);
	leafpage->setPrevLeaf(-1);
	INDEX_FUNC_TRACE && leafpage->dump(indexKeyTypes,pos);
	
	//write back leafpage to BM.
	if(!bm.write(leafpageno, leafbuf, INDEX_ROOT_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	//write back index to BM
	if(!bm.write(indexpageno, indexbuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	
	//create sysindexrec and insert
	sprintf(tempsysindexrec.indexName,"%s",pq.getIndexName().c_str());
	sprintf(tempsysindexrec.tableName,"%s",pq.getTable().c_str());
	tempsysindexrec.startPage = indexpageno;
	tempsysindexrec.totalIndexCols = indexCols.size();
	for(int i=0; i<indexCols.size(); i++){
		tempsysindexrec.colOrigPos = sysColOrigPos[i];
		tempsysindexrec.colIndexPos = i;
		
		//convert index-rec to rec
		if(!convertSysIndexRecToRecord(tempsysindexrec, rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}	
		
             	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemIndexPointer(), (*dbHeader), dummyRID)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }

		delete[] rec;
				
	}

	
	//get all records and insert them into this index
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}

	//insert every record to index
	for(int i=0; i<allrecs.size(); i++){

		recAttributes.clear();

		//unpack the record
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}
		

		//construct key
		key.clear();
		key = vector<string> (indexCols.size(),""); // dummy initial values for both key and key types
		

		//INDEX_FUNC_TRACE && cout << "Inserting Key : ( ";
		//cout << "Inserting Key : ( ";
		for(int j=0; j<indexCols.size(); j++){
		
			//write the attribute at colOrigPos to the key in the colIndexPos
			if(recAttributes.size() > sysColOrigPos[j])
				//the value for the column exists in the record itself
				key[j] = recAttributes[sysColOrigPos[j]];
			else
				//syscolumn value is not present in the record, so use the default value
				key[j] = sysColumns[sysColOrigPos[j]].defaultvalue;
			//INDEX_FUNC_TRACE && cout << key[j] << " , ";
			//cout << key[j] << " , ";
		}
		//INDEX_FUNC_TRACE && cout << " )\n";
		//cout << " )\n";
		
		//now the key for index is built..  pack it and insert
		
		//pack the key
		if(record != NULL)
			delete[] record;
		if(!pack::pack_data(key, record, size, indexKeyTypes)){
			err = PACK_ERROR;
			goto ret;
		}
		
		//insert the key
		if(!handleIndexInsert(indexpageno, indexKeyTypes, record, allRIDs[i], (*dbHeader))){
			err = RECORD_INSERT_ERROR;
			goto ret;
		}
	}

	
	//writeback header
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(record != NULL)
		delete [] record;
	if(dbHeader != NULL)
		delete dbHeader;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;	
	if(indexbuf != NULL)
		delete[] indexbuf;
	if(leafbuf != NULL)
		delete[] leafbuf;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	//if(rec != NULL)
	//	delete[] rec;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=============================================================================================================
bool DataManager::handleDropIndex(parsedQuery &pq, bool DBHeaderIsPassed, DBHeader* &dbHeader){
	
	INDEX_FUNC_TRACE && cout<<"DM.handleDropIndex()\n";
	int err = NO_ERROR;

	
	char* dbheadbuf = NULL;
	//DBHeader* dbHeader = NULL;
	if(!DBHeaderIsPassed){
		dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
		dbHeader = NULL;
	}

	char* indexpagebuf = new char[PAGE_SIZE_IN_BYTES];
	IndexPage* indexPage = NULL;

	vector<int> indexAllPageNums;

	vector<SysIndexRecord> sysIndexRecs;
	vector<SysIndexRecord> tempSysIndexRecs;
	vector<RID> sysIndexRIDs;
	vector<RID> tempSysIndexRIDs;
	bool invalidIndex = true;
	bool duplicate = false;
	
	if(!DBHeaderIsPassed){
		if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		//Initialize DBHeader.
		dbHeader = new DBHeader(dbheadbuf);
	}

	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), tempSysIndexRecs, (*dbHeader), tempSysIndexRIDs)){
		err = INVALID;
		goto ret;
	}

	//check if such an index exists
	for(int i=0; i<tempSysIndexRecs.size(); i++){
		if(pq.getIndexName().compare(to_string(tempSysIndexRecs[i].indexName)) == 0){
			//index exists.. collect only 'this' index records
			invalidIndex = false;
			sysIndexRecs.push_back(tempSysIndexRecs[i]);
			sysIndexRIDs.push_back(tempSysIndexRIDs[i]);
		}
	}

	if(invalidIndex){
		//index does not exist
		err = INVALID;
		cout << "Index '" << pq.getIndexName() << "' on table '" << pq.getTable() << "' does not exist\n";
		goto ret;

	}

	//index exists
	if(!bm.read(sysIndexRecs[0].startPage, indexpagebuf, INDEX_ROOT_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexPage = new IndexPage(indexpagebuf);

	//collect all index page nums
	if(!getIndexPageNumbersRecursively(indexPage->getIndexerRoot(), indexAllPageNums)){
		err = INVALID;
		goto ret;
	}

	//mark each of the pages in indexAllPageNums as free pages
	for(int i=0; i<indexAllPageNums.size(); i++){
		
		duplicate = false;
		for(int j=0; j<i; j++){
			if(indexAllPageNums[i] == indexAllPageNums[j])
			{
				duplicate = true;
				break;			
			}
		}

		if(!duplicate && !markAsFreePage(indexAllPageNums[i],(*dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
	}

	//mark index page as free page
	if(!markAsFreePage(sysIndexRecs[0].startPage,(*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	
	//delete sys index records
	for(int i=0; i<sysIndexRecs.size(); i++){		
		if(!deleteRecord(SYS_INDEX_START_PAGE_NUM, sysIndexRIDs[i], (*dbHeader))){
			err = DATAMANAGER_DELETE_RECORD_ERROR;
			goto ret;
		}
	}

	//write back db header page
	if(!DBHeaderIsPassed && !bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}

	ret:
	if(!DBHeaderIsPassed && dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(!DBHeaderIsPassed && dbHeader != NULL)
		delete dbHeader;
	if(indexPage != NULL)
		delete indexPage;
	if(indexpagebuf != NULL)
		delete[] indexpagebuf;

	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================

bool DataManager::handleIndexInsert(const int &indexerpage,const vector<int> &keyType,char* const &packedkey,const RID &rid,DBHeader &dbHeader){
        INDEX_FUNC_TRACE && cout << "DM.handleIndexInsert()" << endl;
        int err = NO_ERROR;
        int root;
        int pos;
	char * pageData = new char[PAGE_SIZE_IN_BYTES];
	char *newpageData = NULL;
	int newpageNo;
	IndexPage * indexpage = NULL;
	IndexNode node;
	node.key = NULL;
	BranchPage * branchpage = NULL;
	bool split = false;
	short keysize;

	char* key = NULL;

	if(!bm.read(indexerpage,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(pageData);
	root = indexpage->getIndexerRoot();

	//get length of current packed record - length of the record is stored in the last pointer
	memcpy((char*)&keysize, &packedkey[(keyType.size() * sizeof(short))], sizeof(short));
	key = new char[indexpage->getKeySize()];
	memcpy(key, packedkey, keysize);
	//pad any extra bytes
	if(keysize  < indexpage->getKeySize()){
		for(int i=keysize; i<indexpage->getKeySize(); i++){
			//pad null
			key[i] = '-';
		}	
	}



	node.key = new char[(int)indexpage->getKeySize()];
	INDEX_FUNC_TRACE && cout <<"<INDEX_INSERT>Insert is passed to Page : "<< root << endl;
	if(!insertIntoIndex(root,keyType ,key ,node ,rid ,split,dbHeader)){
		err = INVALID;
		goto ret;
	}
	if(split){
	        INDEX_FUNC_TRACE && cout << "Split occured so create a new root" << endl;
	        //INDEX_FUNC_TRACE && cout << "Key to be inserted : " << node.key << endl;
		newpageData = new char[PAGE_SIZE_IN_BYTES];
		if(!getFreePage(newpageData,newpageNo,dbHeader)){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		branchpage = new BranchPage(newpageData);
		branchpage->setPageType(BRANCHNODE);
		branchpage->setMaximumNodeEntries(indexpage->getMaximumNodeEntries());
		branchpage->setTotalKeysOccupied(0);
		branchpage->setKeySize(indexpage->getKeySize());
		branchpage->insert(keyType ,node.key ,node ,split,pos);
		INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
		indexpage->setIndexerRoot(newpageNo);
		//INDEX_FUNC_TRACE && indexpage->dump();
		if(!bm.write(indexerpage,pageData,DIR_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}
		if(!bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}
		
	}

	INDEX_FUNC_TRACE && indexpage->dump();

	ret:
	if(node.key != NULL)
		delete [] node.key;
	if(newpageData != NULL)
		delete [] newpageData;
	if(branchpage != NULL)
		delete branchpage;
	if(pageData != NULL)
		delete [] pageData;
	if(indexpage != NULL)
		delete indexpage;
	if(key != NULL)
		delete[] key;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}
//================================================================================================

bool DataManager::insertIntoIndex(int &pageNo,const vector<int> &keyType ,char* const &key ,IndexNode &node ,const RID &rid ,bool &split,DBHeader &dbHeader){
        INDEX_FUNC_TRACE && cout << "DM.insertIntoIndex()" << endl;
        int err = NO_ERROR;
        int pos = 0;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        LeafPage *nextleaf = NULL;
        BranchPage *branchpage = NULL;
        int nextPtr;
        char *newpageData = new char[PAGE_SIZE_IN_BYTES];
        char *nextpageData = new char[PAGE_SIZE_IN_BYTES];
        int newpageNo;
        int nextpageNo;
        
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        
	indexpage = new IndexPage(pageData);
	if(indexpage->getPageType() == LEAFNODE){
		leafpage = new LeafPage(pageData);
		INDEX_FUNC_TRACE && cout << "<INDEX_INSERT>Trying to insert key in Leaf Page: " << pageNo << endl;
		leafpage->insert(keyType,key,rid,split,pos);
		if(split){
			INDEX_FUNC_TRACE && cout << "Split to be made in leaf" << endl;
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			leafpage->split(keyType,node,newpageData,key,rid,pageNo);
			nextpageNo = leafpage->getNextLeaf();
			if(nextpageNo != -1){
				if(!bm.read(nextpageNo,nextpageData,DIR_PAGE_PRIORITY)){
					err = READ_ERROR;
					goto ret;
				}
				nextleaf = new LeafPage(nextpageData);
				nextleaf->setPrevLeaf(newpageNo);
				if(!bm.write(nextpageNo,nextpageData,DIR_PAGE_PRIORITY)){
					err = WRITE_ERROR;
					goto ret;
				}
				INDEX_FUNC_TRACE && cout << "Dump of Next Leaf" << endl;
				INDEX_FUNC_TRACE && nextleaf->dump(keyType,pos);
			}
			leafpage->setNextLeaf(newpageNo);
			if(!bm.write(pageNo,pageData,DIR_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			if(!bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			//INDEX_FUNC_TRACE && cout << "Split done in leaf" << endl;
			split = true;
			INDEX_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Leaf$$$$$$$$$$$$$$$$$" << endl;
			INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
			goto ret;
		}
		INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
		if(!bm.write(pageNo,pageData,DIR_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}
		split = false;
		goto ret;
	}
	
	branchpage = new BranchPage(pageData);
	branchpage->findNextNode(keyType ,key,nextPtr,-1);
	INDEX_FUNC_TRACE && cout <<"<INDEX_INSERT>Key insertion passed to ChildNode :" << nextPtr <<" from Branch Page: " <<pageNo << endl;
	if(!insertIntoIndex(nextPtr,keyType,key,node,rid,split,dbHeader)){
		err = INVALID;
		goto ret;
	}
	if(split){
		branchpage->insert(keyType,node.key,node,split,pos);
		if(split){
			if(!getFreePage(newpageData,newpageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			branchpage->split(keyType,node,newpageData,node.key);
			node.leftPtr = pageNo;
			node.rightPtr = newpageNo;
			if(!bm.write(pageNo,pageData,DIR_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			if(!bm.write(newpageNo,newpageData,DIR_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			split = true;
			INDEX_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$$$Dump of Old Branch$$$$$$$$$$$$$$$$$" << endl;
			INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
			goto ret;
		}
		INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
		if(!bm.write(pageNo,pageData,DIR_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}
		split = false;
		goto ret;
	}
	ret:
	if(pageData != NULL)
		delete [] pageData;
	if(newpageData != NULL)
		delete [] newpageData;
	if(nextpageData != NULL)
		delete [] nextpageData;	
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(nextleaf != NULL)
		delete nextleaf;
	if(branchpage != NULL)
		delete branchpage;
		
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}


//=============================================================================================================
bool DataManager::getIndexPageNumbersRecursively(int curPageNum, vector<int> &pageNums){

	INDEX_FUNC_TRACE && cout<<"DM.getIndexPageNumbersRecursively()\n";
	int err = NO_ERROR;
	
	BranchPage* branch = NULL;
	char* pagebuf = new char[PAGE_SIZE_IN_BYTES];

	vector<int> childPageNums;

	//read the current page..
	if(!bm.read(curPageNum, pagebuf, INDEX_INTERMEDIATE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//assume its a branch by default
	branch = new BranchPage(pagebuf);
	
	//check page type
	if(branch->getPageType() == LEAFNODE){
		//add this page to page num
		pageNums.push_back(curPageNum);
	}else {

		if(branch->getPageType() == BRANCHNODE){

			if(!branch->getAllChildPageNumbers(childPageNums)){
				err = INVALID;
				goto ret;
			}

			//call this function for each of the child page nums
			for(int i=0; i<childPageNums.size(); i++){
				if(!getIndexPageNumbersRecursively(childPageNums[i], pageNums)){
					err = INVALID;
					goto ret;
				}
				pageNums.push_back(childPageNums[i]);
			}

			
		}else{
			err = INVALID;
			goto ret;
		}

	}

	//if its a leaf delete it
	//if its a branch.. get other nodes
	

	ret:
	childPageNums.clear();
	if(branch != NULL)
		delete branch;
	if(pagebuf != NULL)
		delete[] pagebuf;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
} 

//=============================================================================================================

//Recursive function to delete an entry in index and to set the rid of the deleted item.
bool DataManager::deleteFromIndex(BranchPage &parentPage,int &childPage,int &oldChild,const vector<int> &keyType,char* const &key,bool &merge,RID &rid , DBHeader &dbHeader){
	INDEX_FUNC_TRACE && cout << "DM.deleteFromIndex()" << endl;
	int err = NO_ERROR;
	int pos = 0;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        char *siblingPageData = NULL;
        char *newIndexKey = NULL;
        char *indexKey = NULL;
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        LeafPage *siblingleafpage = NULL;
        BranchPage *branchpage = NULL;
        BranchPage *siblingbranchpage = NULL;
        int nextPtr = 0;
        int siblingPtr = 0;
        
        if(!bm.read(childPage,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        indexpage = new IndexPage(pageData);
        if(indexpage->getPageType() == BRANCHNODE){
	        branchpage = new BranchPage(pageData);
	        branchpage->findNextNode(keyType ,key,nextPtr,-1);
	        INDEX_FUNC_TRACE && cout <<"<INDEX_DELETE>Index delete is passed to Child Node : "<< nextPtr <<" from Branch Page :" << childPage << endl;
	        if(!deleteFromIndex(*branchpage , nextPtr ,oldChild , keyType ,key ,merge ,rid , dbHeader)){
	        	err = INDEX_KEY_NOT_FOUND;
			goto ret;
	        }
	        if(!merge){
		        bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
		        goto ret;
	        }
	        branchpage->deleteNode(keyType ,oldChild , merge);
	        if(!merge){
		        oldChild = -1;
		        bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
		        goto ret;
	        }
	        if(parentPage.getPrevSibling(childPage,siblingPtr)){
		        if(siblingPageData != NULL)
				delete [] siblingPageData;
		        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
		        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			if(indexKey != NULL)
				delete [] indexKey;
			parentPage.getKeyBetweenNodes(siblingPtr,childPage,indexKey);
			if(siblingbranchpage != NULL)
				delete siblingbranchpage;
			siblingbranchpage = new BranchPage(siblingPageData);
			if(newIndexKey != NULL)
				delete [] newIndexKey;
			if(branchpage->adjustFromLeftSibling(*siblingbranchpage,keyType,newIndexKey,indexKey)){
				parentPage.replaceKeyBetweenNodes(siblingPtr ,childPage,newIndexKey);
				oldChild = -1;
				merge = false;
				bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
				bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
				goto ret;
                        }
	        }
	        if(parentPage.getNextSibling(childPage,siblingPtr)){
		        if(siblingPageData != NULL)
				delete [] siblingPageData;
		        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
		        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			if(indexKey != NULL)
				delete [] indexKey;
			parentPage.getKeyBetweenNodes(childPage,siblingPtr,indexKey);
			if(siblingbranchpage != NULL)
				delete siblingbranchpage;
			siblingbranchpage = new BranchPage(siblingPageData);
			if(newIndexKey != NULL)
				delete [] newIndexKey;
			if(branchpage->adjustFromRightSibling(*siblingbranchpage,keyType,newIndexKey,indexKey)){
				parentPage.replaceKeyBetweenNodes(childPage,siblingPtr,newIndexKey);
				oldChild = -1;
				merge = false;
				bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
				bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
				goto ret;
			}
	        }
	        //Merge.........
	        if(parentPage.getPrevSibling(childPage,siblingPtr)){
		        if(siblingPageData != NULL)
				delete [] siblingPageData;
		        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
		        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			if(indexKey != NULL)
				delete [] indexKey;
			parentPage.getKeyBetweenNodes(siblingPtr,childPage,indexKey);
			if(siblingbranchpage != NULL)
				delete siblingbranchpage;
			siblingbranchpage = new BranchPage(siblingPageData);
			INDEX_FUNC_TRACE && cout << "Before Merging Branch Page" << endl;
			INDEX_FUNC_TRACE && siblingbranchpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
			siblingbranchpage->merge(*branchpage, keyType,indexKey);
			if(!markAsFreePage(childPage,(dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			oldChild = childPage;
			INDEX_FUNC_TRACE && cout << "After Merging" << endl;
			INDEX_FUNC_TRACE && siblingbranchpage->dump(keyType,pos);
			merge = true;
			bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
			goto ret;
	        }
	        if(parentPage.getNextSibling(childPage ,siblingPtr)){
		        if(siblingPageData != NULL)
				delete [] siblingPageData;
		        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
		        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			if(indexKey != NULL)
				delete [] indexKey;
			parentPage.getKeyBetweenNodes(childPage,siblingPtr,indexKey);
			if(siblingbranchpage != NULL)
				delete siblingbranchpage;
			siblingbranchpage = new BranchPage(siblingPageData);
			INDEX_FUNC_TRACE && cout << "Before Merging Branch Page" << endl;
			INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && siblingbranchpage->dump(keyType,pos);
			branchpage->merge(*siblingbranchpage, keyType,indexKey);
			if(!markAsFreePage(siblingPtr,(dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			oldChild = siblingPtr;
			INDEX_FUNC_TRACE && cout << "After Merging" << endl;
			INDEX_FUNC_TRACE && branchpage->dump(keyType,pos);
			merge = true;
			bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
			goto ret;
	        }
        }
        
        leafpage = new LeafPage(pageData);
        INDEX_FUNC_TRACE && cout << "<INDEX_DELETE>Trying to delete index key in LeafPage: " << childPage << endl;
        if(!leafpage->deleteKey(keyType,key,merge,rid)){
	        err = INDEX_KEY_NOT_FOUND;
		goto ret;
        }
        if(!merge){
	        oldChild = -1;
	        INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
	        bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
	        goto ret;
        }
        if(parentPage.getPrevSibling(childPage,siblingPtr)){
        	INDEX_FUNC_TRACE && cout << "Trying to adjust from Left leaf sibling " << endl;
	        if(siblingPageData != NULL)
			delete [] siblingPageData;
	        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
	        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(indexKey != NULL)
			delete [] indexKey;
		parentPage.getKeyBetweenNodes(siblingPtr,childPage,indexKey);
		if(siblingleafpage != NULL)
			delete siblingleafpage;
		siblingleafpage = new LeafPage(siblingPageData);
		if(newIndexKey != NULL)
			delete [] newIndexKey;
		INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
		INDEX_FUNC_TRACE && siblingleafpage->dump(keyType,pos);
		if(leafpage->adjustFromLeftSibling(*siblingleafpage,keyType,newIndexKey)){
			INDEX_FUNC_TRACE && cout << "Keys adjusted successfully from Left leaf sibling " << endl;
			INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && siblingleafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && parentPage.dump(keyType,pos);
			parentPage.replaceKeyBetweenNodes(siblingPtr ,childPage,newIndexKey);
			INDEX_FUNC_TRACE && parentPage.dump(keyType,pos);
			oldChild = -1;
			merge = false;
			bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
			bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
			goto ret;
                }
        }
        if(parentPage.getNextSibling(childPage,siblingPtr)){
        	INDEX_FUNC_TRACE && cout << "Trying to adjust from Right leaf sibling " << endl;
	        if(siblingPageData != NULL)
			delete [] siblingPageData;
	        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
	        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(indexKey != NULL)
			delete [] indexKey;
		parentPage.getKeyBetweenNodes(childPage,siblingPtr,indexKey);
		if(siblingleafpage != NULL)
			delete siblingleafpage;
		siblingleafpage = new LeafPage(siblingPageData);
		if(newIndexKey != NULL)
			delete [] newIndexKey;
		INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
		INDEX_FUNC_TRACE && siblingleafpage->dump(keyType,pos);
		if(leafpage->adjustFromRightSibling(*siblingleafpage,keyType,newIndexKey)){
			INDEX_FUNC_TRACE && cout << "Keys adjusted successfully from Right leaf sibling " << endl;
			INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && siblingleafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
			INDEX_FUNC_TRACE && parentPage.dump(keyType,pos);
			parentPage.replaceKeyBetweenNodes(childPage,siblingPtr,newIndexKey);
			INDEX_FUNC_TRACE && parentPage.dump(keyType,pos);
			oldChild = -1;
			merge = false;
			bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
			bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
			goto ret;
		}
        }
          //Merge.........
        if(parentPage.getPrevSibling(childPage,siblingPtr)){
        	INDEX_FUNC_TRACE && cout << "Merge from the left leaf sibling";
	        if(siblingPageData != NULL)
			delete [] siblingPageData;
	        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
	        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(indexKey != NULL)
			delete [] indexKey;
		parentPage.getKeyBetweenNodes(siblingPtr,childPage,indexKey);
		if(siblingleafpage != NULL)
			delete siblingleafpage;
		siblingleafpage = new LeafPage(siblingPageData);
		siblingleafpage->merge(*leafpage, keyType);
		if(!markAsFreePage(childPage,(dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		oldChild = childPage;
		INDEX_FUNC_TRACE && cout << "After Merging " << endl;
		INDEX_FUNC_TRACE && cout << siblingleafpage->dump(keyType,pos);
		merge = true;
		bm.write(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY);
		goto ret;
        }
        if(parentPage.getNextSibling(childPage ,siblingPtr)){
        	INDEX_FUNC_TRACE && cout << "Merge from the right leaf sibling";
	        if(siblingPageData != NULL)
			delete [] siblingPageData;
	        siblingPageData = new char[PAGE_SIZE_IN_BYTES];
	        if(!bm.read(siblingPtr,siblingPageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(indexKey != NULL)
			delete [] indexKey;
		parentPage.getKeyBetweenNodes(childPage,siblingPtr,indexKey);
		if(siblingleafpage != NULL)
			delete siblingleafpage;
		siblingleafpage = new LeafPage(siblingPageData);
		leafpage->merge(*siblingleafpage, keyType);
		if(!markAsFreePage(siblingPtr,(dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		INDEX_FUNC_TRACE && cout << "After Merging " << endl;
		INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
		oldChild = siblingPtr;
		merge = true;
		bm.write(childPage,pageData,DIR_PAGE_PRIORITY);
		goto ret;
        }
        
        
        
        ret:
        
        if(pageData != NULL)
		delete [] pageData;
	if(siblingPageData != NULL)
		delete [] siblingPageData;
	if(newIndexKey != NULL)
		delete [] newIndexKey;
	if(indexKey != NULL)
		delete [] indexKey;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(siblingleafpage != NULL)
		delete siblingleafpage;
	if(branchpage != NULL)
		delete branchpage;
	if(siblingbranchpage != NULL)
		delete siblingbranchpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================================


bool DataManager::handleIndexDelete(const int &indexerpage,const vector<int> &keyType,char* const &packedkey,RID &rid,DBHeader &dbHeader){
	INDEX_FUNC_TRACE && cout << "DM.handleIndexDelete()" << endl;
        int err = NO_ERROR;
        int root;
        int childPtr;
        int oldChild = -1;
	char * indexPageData = new char[PAGE_SIZE_IN_BYTES];
	char *rootPageData = new char[PAGE_SIZE_IN_BYTES];
	IndexPage * indexpage = NULL;
	BranchPage * branchpage = NULL;
	LeafPage *leafpage = NULL;
	bool merge = false;
	short keysize;
	char *key = NULL;
	int pos = 0;
	if(!bm.read(indexerpage,indexPageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(indexPageData);
	root = indexpage->getIndexerRoot();
	//get length of current packed record - length of the record is stored in the last pointer
	memcpy((char*)&keysize, &packedkey[(keyType.size() * sizeof(short))], sizeof(short));
	key = new char[indexpage->getKeySize()];
	memcpy(key, packedkey, keysize);
	//pad any extra bytes
	if(keysize  < indexpage->getKeySize()){
		for(int i=keysize; i<indexpage->getKeySize(); i++){
			//pad null
			key[i] = '-';
		}	
	}
	if(!bm.read(root,rootPageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	branchpage = new BranchPage(rootPageData);
	if(branchpage->getPageType() == BRANCHNODE){
		branchpage->findNextNode(keyType ,key,childPtr,-1);
		INDEX_FUNC_TRACE && cout <<"<INDEX_DELETE>Index delete is passed to Child Node : "<< childPtr <<" from Branch Page :" << root << endl;
		if(!deleteFromIndex(*branchpage , childPtr ,oldChild , keyType ,key ,merge ,rid , dbHeader)){
			err = INDEX_KEY_NOT_FOUND;
			goto ret;
        	}
		if(!merge){
			bm.write(root,rootPageData,DIR_PAGE_PRIORITY);
		        goto ret;
		}
		if(branchpage->getTotalKeysOccupied() == 1){
			if(branchpage->getNextSibling(oldChild,childPtr)){
				indexpage->setIndexerRoot(childPtr);
				INDEX_FUNC_TRACE && indexpage->dump();
				if(!markAsFreePage(root,(dbHeader))){
					err = FREEPAGE_READ_ERROR;
					goto ret;
				}
				bm.write(indexerpage,indexPageData,DIR_PAGE_PRIORITY);
				goto ret;
			}
			if(branchpage->getPrevSibling(oldChild,childPtr)){
				indexpage->setIndexerRoot(childPtr);
				INDEX_FUNC_TRACE && indexpage->dump();
				if(!markAsFreePage(root,(dbHeader))){
					err = FREEPAGE_READ_ERROR;
					goto ret;
				}
				bm.write(indexerpage,indexPageData,DIR_PAGE_PRIORITY);
				goto ret;
			}
		}
		branchpage->deleteNode(keyType ,oldChild , merge);
		bm.write(root,rootPageData,DIR_PAGE_PRIORITY);
		goto ret;
	}
	
	
	leafpage = new LeafPage(rootPageData);
	INDEX_FUNC_TRACE && cout << "<INDEX_DELETE>Trying to delete key from LeafPage : "<< root <<endl;
	if(!leafpage->deleteKey(keyType,key,merge,rid)){
		err = INDEX_KEY_NOT_FOUND;
		goto ret;
        }
	INDEX_FUNC_TRACE && leafpage->dump(keyType,pos);
	bm.write(root,rootPageData,DIR_PAGE_PRIORITY);
	goto ret;
	
	
	ret:
	if(indexPageData != NULL)
		delete [] indexPageData;
	if(key != NULL){
		delete [] key;
	}
	if(rootPageData != NULL)
		delete [] rootPageData;
	if(indexpage != NULL)
		delete indexpage;
	if(branchpage != NULL)
		delete branchpage;
	if(leafpage != NULL)
		delete leafpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//================================================================================================================
bool DataManager::indexSearch(const int &indexerpage,const vector<int> &keyType,char* const &packedkey,RID &rid,bool &found){
	INDEX_FUNC_TRACE && cout << "DM.indexSearch()" << endl;
        int err = NO_ERROR;
        int root;
	char * indexPageData = new char[PAGE_SIZE_IN_BYTES];
	IndexPage * indexpage = NULL;
	char *key = NULL;
	short keysize ;
	if(!bm.read(indexerpage,indexPageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(indexPageData);
	root = indexpage->getIndexerRoot();
	//get length of current packed record - length of the record is stored in the last pointer
	memcpy((char*)&keysize, &packedkey[(keyType.size() * sizeof(short))], sizeof(short));
	key = new char[indexpage->getKeySize()];
	memcpy(key, packedkey, keysize);
	//pad any extra bytes
	if(keysize  < indexpage->getKeySize()){
		for(int i=keysize; i<indexpage->getKeySize(); i++){
			//pad null
			key[i] = '-';
		}	
	}
	if(!searchFromIndex(root,keyType,key,rid,found)){
		//err = INDEX_KEY_NOT_FOUND;
		goto ret;
	}
	if(INDEX_FUNC_TRACE and found){
		rid.dump();
	}
	
	ret:
	if(indexPageData != NULL)
		delete [] indexPageData;
	if(key != NULL )
		delete [] key;
	if(indexpage != NULL)
		delete indexpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}



//================================================================================================================
//recursive function to search a key in index.
bool DataManager::searchFromIndex(int &pageNo,const vector<int> &keyType,char* const &key,RID &rid,bool &found){
	INDEX_FUNC_TRACE && cout << "DM.searchFromIndex()" << endl;
	int err = NO_ERROR;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        char *newIndexKey = NULL;
        char *indexKey = NULL;
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        BranchPage *branchpage = NULL;
        int nextPtr = 0;
        int pos = 0;
        
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        indexpage = new IndexPage(pageData);
        if(indexpage->getPageType() == BRANCHNODE){
	        branchpage = new BranchPage(pageData);
	        //INDEX_FUNC_TRACE && branchpage->dump(keyType);
	        branchpage->findNextNode(keyType ,key,nextPtr,-1);
	        INDEX_FUNC_TRACE && cout << "<INDEX_SEARCH>Index Search Passed to Child Node Page:" << nextPtr << " from Branch Page:" << pageNo << endl;
	        if(!searchFromIndex(nextPtr ,keyType ,key ,rid,found)){
		        //err = INDEX_KEY_NOT_FOUND;
		        goto ret;
	        }
	        goto ret;
	        
        }
        
        leafpage = new LeafPage(pageData);
        //INDEX_FUNC_TRACE && leafpage->dump(keyType);
        INDEX_FUNC_TRACE && cout << "<INDEX_SEARCH>Searching for key in Leaf Page:" << pageNo << endl;
        if(!leafpage->findKey(keyType,key,pos,rid)){
        	found = false;
	        //err = INDEX_KEY_NOT_FOUND;
		goto ret;
        }else{
        	found = true;
        }
      
       
        ret:
        
        if(pageData != NULL)
		delete [] pageData;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(branchpage != NULL)
		delete branchpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================================
bool DataManager::indexGetRID(const int &indexerpage,const vector<int> &keyType,char* const &packedkey,vector<RID> &allrids,int totalKeys,vector<int> columnPos,vector<int> operatorCode,vector<string> values){
	INDEX_FUNC_TRACE && cout << "DM.indexGetRID()" << endl;
        int err = NO_ERROR;
        int root;
	char * indexPageData = new char[PAGE_SIZE_IN_BYTES];
	IndexPage * indexpage = NULL;
	char *key = NULL;
	short keysize ;
	if(!bm.read(indexerpage,indexPageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(indexPageData);
	root = indexpage->getIndexerRoot();
	//get length of current packed record - length of the record is stored in the last pointer
	memcpy((char*)&keysize, &packedkey[(keyType.size() * sizeof(short))], sizeof(short));
	key = new char[indexpage->getKeySize()];
	memcpy(key, packedkey, keysize);
	//pad any extra bytes
	if(keysize  < indexpage->getKeySize()){
		for(int i=keysize; i<indexpage->getKeySize(); i++){
			//pad null
			key[i] = '-';
		}	
	}
	if(!getRIDofRecordsFromIndex(root,keyType,key,allrids,totalKeys,columnPos,operatorCode,values)){
		//err = INDEX_KEY_NOT_FOUND;
		goto ret;
	}
	
	ret:
	if(indexPageData != NULL)
		delete [] indexPageData;
	if(key != NULL )
		delete [] key;
	if(indexpage != NULL)
		delete indexpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//================================================================================================================
//
bool DataManager::getRIDofRecordsFromIndex(int &pageNo,const vector<int> &keyType,char* const &key,vector<RID> &rid,int totalKeys,vector<int> columnPos,vector<int> operatorCode,vector<string> values){
	INDEX_FUNC_TRACE && cout << "DM.getRIDofRecordsFromIndex()" << endl;
	int err = NO_ERROR;
	int nextPage;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        char *newIndexKey = NULL;
        char *indexKey = NULL;
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        BranchPage *branchpage = NULL;
        int nextPtr = 0;
        int pos = 0;
        bool firsttime ;
        int i;
        RID rids;
        int prevPage;
        bool isResultsStarted = false;
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        indexpage = new IndexPage(pageData);
        if(indexpage->getPageType() == BRANCHNODE){
	        branchpage = new BranchPage(pageData);
	        branchpage->findNextNode(keyType ,key,nextPtr,totalKeys);
	        if(!getRIDofRecordsFromIndex(nextPtr ,keyType ,key ,rid,totalKeys,columnPos,operatorCode,values)){
		        //err = INDEX_KEY_NOT_FOUND;
		        goto ret;
	        }
	        goto ret;
	        
        }

        leafpage = new LeafPage(pageData);
        firsttime = true;
        prevPage = leafpage->getPrevLeaf();
        //cout << "##################################" << endl;
        //cout << keyType.size() << endl;
        //cout << totalKeys << endl;
        //cout << "##################################" << endl;
        //leafpage->dump(keyType,5);
        while(leafpage->getRIDs(keyType,columnPos,operatorCode,values,rid,totalKeys,firsttime,isResultsStarted)){
        	//cout << "inside while loop"<< endl;
        	firsttime = false;
        	nextPage = leafpage->getNextLeaf();
        	if(nextPage == -1){
        		//cout << "nextleaf is negative" << endl;
        		break;
        	}
        	if(pageData != NULL)
			delete [] pageData;
		pageData = new char[PAGE_SIZE_IN_BYTES];
		if(!bm.read(nextPage,pageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(leafpage != NULL)
			delete leafpage;
		leafpage = new LeafPage(pageData);
		//leafpage->dump(keyType,5);
        	//cout << "inside while loop"<< endl;
	}
	//cout << "###################################"<< endl;
	//cout << rid.size()<<endl;
	/*for(i = 0 ; i < rid.size() ; i ++ ){
		rids = rid[i];
		rids.dump();
	}*/
	//cout << "##################################" << endl;
        if(prevPage == -1){
        	//cout << "##################################" << endl;
        	goto ret;
        }
        if(pageData != NULL)
		delete [] pageData;
	pageData = new char[PAGE_SIZE_IN_BYTES];
	if(!bm.read(prevPage,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	if(leafpage != NULL)
		delete leafpage;
	leafpage = new LeafPage(pageData);
	firsttime = true;
	isResultsStarted = false;
	//leafpage->dump(keyType,5);
	while(leafpage->getRIDs(keyType,columnPos,operatorCode,values,rid,totalKeys,firsttime,isResultsStarted)){
        	//cout << "inside while loop"<< endl;
        	firsttime = false;
        	prevPage = leafpage->getPrevLeaf();
        	if(prevPage == -1){
        		//cout << "nextleaf is negative" << endl;
        		break;
        	}
        	if(pageData != NULL)
			delete [] pageData;
		pageData = new char[PAGE_SIZE_IN_BYTES];
		if(!bm.read(prevPage,pageData,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}
		if(leafpage != NULL)
			delete leafpage;
		leafpage = new LeafPage(pageData);
		//leafpage->dump(keyType,5);
        	//cout << "inside while loop"<< endl;
	}
	
	
	
        ret:
        
        if(pageData != NULL)
		delete [] pageData;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(branchpage != NULL)
		delete branchpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//================================================================================================================
bool DataManager::indexTraverse(const int &indexerpage,const vector<int> &keyType){
	INDEX_FUNC_TRACE && cout << "DM.indexTraverse()" << endl;
        int err = NO_ERROR;
        int root;
	char * indexPageData = new char[PAGE_SIZE_IN_BYTES];
	IndexPage * indexpage = NULL;
	
	if(!bm.read(indexerpage,indexPageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	indexpage = new IndexPage(indexPageData);
	root = indexpage->getIndexerRoot();
	
	traverseInIndex(root,keyType);	
	ret:
	if(indexPageData != NULL)
		delete [] indexPageData;
	
	if(indexpage != NULL)
		delete indexpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//===========================================================================================
//recursive function to search a key in index.
bool DataManager::traverseInIndex(int &pageNo,const vector<int> &keyType){
	INDEX_FUNC_TRACE && cout << "DM.traverseInIndex()" << endl;
	int err = NO_ERROR;
        char *pageData = new char[PAGE_SIZE_IN_BYTES];
        char *newIndexKey = NULL;
        char *indexKey = NULL;
        IndexPage *indexpage = NULL;
        LeafPage *leafpage = NULL;
        BranchPage *branchpage = NULL;
        int num;
        
        if(!bm.read(pageNo,pageData,DIR_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        indexpage = new IndexPage(pageData);
        if(indexpage->getPageType() == BRANCHNODE){
	        branchpage = new BranchPage(pageData);
	        branchpage->traverseDump(keyType); 
	        cout << "Enter the page Number to traverse:";
	        cin >> num;
	        traverseInIndex(num,keyType);
	        goto ret;
        }
        
        leafpage = new LeafPage(pageData);
        leafpage->traverseDump(keyType);
        goto ret;
      
       
        ret:
        
        if(pageData != NULL)
		delete [] pageData;
	if(indexpage != NULL)
		delete indexpage;
	if(leafpage != NULL)
		delete leafpage;
	if(branchpage != NULL)
		delete branchpage;
	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}


//=======================================================================
bool DataManager::handleTraverseIndex(parsedQuery &pq){

	INDEX_FUNC_TRACE && cout << "DM.handleTraverseIndex" << endl;
	int err = NO_ERROR;


	char *record = NULL;
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	RID rid;
	DBHeader* dbHeader = NULL;
	
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColumns;
        vector<RID> sysColRIDs;
        vector<int> types = pq.getTypes();
        vector<int> toTypes;
        vector<string> values = pq.getDataValues();
        vector<string> finalValues;
        vector<string> key;
        vector<int> keyTypes;
	int size = 0;
	bool found = false;
	string tempstring;

        vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
        vector<RID> sysIndexRIDs;
	


	
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	//get systab recs for that table
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}

	//get syscol rec
	if(!getSysColRecsForTable(pq.getTable(),sysColumns,(*dbHeader),systabrec.totalColumns, sysColRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	for(int i=0; i<sysColumns.size(); i++){
		toTypes.push_back(sysColumns[i].dataType);
		//cout << "totypes[" << i << "] = " << toTypes[i] << endl;
	}

	//get sys index recs for that table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}


	tempSysIndexRecs.clear();
	tempstring.clear();
	tempstring = pq.getIndexName();
	key.clear();
	keyTypes.clear();
	
	
	INDEX_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << endl;
	
	for(int j=0; j<sysIndexRecs.size(); j++){
		//find all index recs matching indexNames[i]
		if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
			tempSysIndexRecs.push_back(sysIndexRecs[j]);
		}
		
	}
	
	//now we have index recs for one index name
	//key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
	keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
	//finalValues = vector<string> (tempSysIndexRecs.size(), "");
	
	for(int j=0; j<tempSysIndexRecs.size(); j++){

		//write the attribute at colOrigPos to the key in the colIndexPos
	
		//key[tempSysIndexRecs[j].colIndexPos] = finalValues[tempSysIndexRecs[j].colIndexPos];
		keyTypes[tempSysIndexRecs[j].colIndexPos] = toTypes[tempSysIndexRecs[j].colOrigPos];
	
		//cout << "totypes : " << toTypes[tempSysIndexRecs[j].colOrigPos] << ", index pos : " << tempSysIndexRecs[j].colIndexPos << ", Col orig pos : " <<  tempSysIndexRecs[j].colOrigPos<< endl;

		//INDEX_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColumns[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColumns[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
		
	}

	
	//search the key
	indexTraverse(tempSysIndexRecs[0].startPage, keyTypes);
	
	ret:
	if(record != NULL){
		delete[] record;
	}
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}

	if(dbHeader != NULL){
		delete dbHeader;
	}

	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

//=======================================================================
bool DataManager::handleSearchIndex(parsedQuery &pq){

	INDEX_FUNC_TRACE && cout << "DM.searchFromIndex()" << endl;
	int err = NO_ERROR;


	char *record = NULL;
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	RID rid;
	DBHeader* dbHeader = NULL;
	
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColumns;
        vector<RID> sysColRIDs;
        vector<int> types = pq.getTypes();
        vector<int> toTypes;
        vector<string> values = pq.getDataValues();
        vector<string> finalValues;
        vector<string> key;
        vector<int> keyTypes;
	int size = 0;
	bool found = false;
	string tempstring;

        vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
        vector<RID> sysIndexRIDs;
	


	
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	//get systab recs for that table
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}

	//get syscol rec
	if(!getSysColRecsForTable(pq.getTable(),sysColumns,(*dbHeader),systabrec.totalColumns, sysColRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	for(int i=0; i<sysColumns.size(); i++){
		toTypes.push_back(sysColumns[i].dataType);
		//cout << "totypes[" << i << "] = " << toTypes[i] << endl;
	}

	//get sys index recs for that table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}


	tempSysIndexRecs.clear();
	tempstring.clear();
	tempstring = pq.getIndexName();
	key.clear();
	keyTypes.clear();
	
	
	INDEX_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << endl;
	
	for(int j=0; j<sysIndexRecs.size(); j++){
		//find all index recs matching indexNames[i]
		if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
			tempSysIndexRecs.push_back(sysIndexRecs[j]);
		}
		
	}
	
	//now we have index recs for one index name
	key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
	keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
	finalValues = vector<string> (tempSysIndexRecs.size(), "");
	
	if(finalValues.size() != values.size()){
		cout << "Values do match the number of columns in the index\n";
		err = INVALID;
		goto ret;
	}

	for(int i=0; i<finalValues.size(); i++){
		finalValues[i] = values[i];	//populate input values
	}

	
	for(int j=0; j<tempSysIndexRecs.size(); j++){


		//check if recognised datatype can be converted into the type in syscol
		if(!checkDataType(types[j],  toTypes[tempSysIndexRecs[j].colOrigPos])){
			cout << "Datatype mismatch for column '" << sysColumns[tempSysIndexRecs[j].colOrigPos].columnName << "'" << endl;
			err = COLTYPE_MISMATCH;
			goto ret;	
		}


		//write the attribute at colOrigPos to the key in the colIndexPos
	
		key[tempSysIndexRecs[j].colIndexPos] = finalValues[tempSysIndexRecs[j].colIndexPos];
		keyTypes[tempSysIndexRecs[j].colIndexPos] = toTypes[tempSysIndexRecs[j].colOrigPos];
	
		//cout << "totypes : " << toTypes[tempSysIndexRecs[j].colOrigPos] << ", index pos : " << tempSysIndexRecs[j].colIndexPos << ", Col orig pos : " <<  tempSysIndexRecs[j].colOrigPos<< endl;

		INDEX_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColumns[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColumns[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
		
	}
		
	//now the key for one index is built..  pack it and insert
	//cout << "Press a key\n";
	//hcin  >> tempstring;
	
	
	//pack the key
	if(record != NULL)
		delete[] record;
	if(!pack::pack_data(key, record, size, keyTypes)){
		err = PACK_ERROR;
		goto ret;
	}
	
	//search the key
	if(!indexSearch(tempSysIndexRecs[0].startPage, keyTypes, record, rid, found)){
		err = INVALID;
		goto ret;
	}

	if(found)
		cout << "Key found in index :" << pq.getIndexName() << endl ;
	else
		cout << "Key not found in index :" << pq.getIndexName() << endl ;
	
	ret:
	if(record != NULL){
		delete[] record;
	}
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}

	if(dbHeader != NULL){
		delete dbHeader;
	}

	INDEX_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================
//=======================================================================
