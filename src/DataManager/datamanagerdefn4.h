//=======================================================================
bool DataManager::handleCreateTable(parsedQuery &pq){

	int err = NO_ERROR;
	RID rid;
	
	DM_FUNC_TRACE && cout << "DM.handleCreateTable()\n";
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	//create sys_table record for tablename
	SysTableRecord sysTabRec, tempSysTabRec;
	sprintf(sysTabRec.tableName,"%s",pq.getTable().c_str());	//Add Table name
	sysTabRec.totalRows = 0;					//Intially there are 0 rows
	//sysTabRec.createTime;
	//sysTabRec.updateTime;
	sysTabRec.startPage = -1;//Page where the first DP for this table is
	sysTabRec.totalDP = 1;						//There are no DPs yet
	sysTabRec.totalColumns = pq.getNoOfColumns();			//Total number of columns for this table
	
	vector<string> columns;		//COLUMNS captured
	vector<int> types;			//COLUMN DATATYPES captured
	vector<bool> notNulls;		//COLUMN is NULL ?
	vector<bool> pks;				//COLUMN is PrimaryKey ?
	vector<bool> unique;			//COLUMN is Unique ?
	vector<int> maxsizes;			//COLUMN Max Size
	vector<defaultValue> defaultValues = (vector<defaultValue>)pq.getDefaultValues();
	SysColumnRecord* sysColRecs = NULL;
	DBHeader* dbHeader = NULL;
	DirectoryPage* dirpage = NULL;

	vector<string> primaryCols;
	parsedQuery tempPQ;

	char* rec = NULL;
	int recSize;
	
	RID tempRID;
	
	//DUMP
	//sysTabRec.dump();
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
	if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	
	//get sys tab record for the table
	if(getSysTabRecForTable(pq.getTable(), tempSysTabRec, (*dbHeader), tempRID, false)){
		//Table already exists
		err = INVALIDTABLENAME;
		cout << "Table '" << pq.getTable() << "' already exists\n";
		goto ret;
	}
	
	//check if column name is duplicate
	columns = pq.getColumns();		//COLUMNS captured
	for(int i = 0; i < pq.getNoOfColumns();i++){
		for(int j=i+1; j < pq.getNoOfColumns(); j++){
			//check current col name all prev col names
			if(columns[i].compare(columns[j]) == 0){
				err = INVALID;
				cout << "Column names for a table have to be unique\n";
				goto ret;
			}
		}
	}
	
	//create new DirPage for this table
	if(!getFreePage(dirpagebuf, sysTabRec.startPage, (*dbHeader))){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(dirpage != NULL)
		delete dirpage;
	dirpage = new DirectoryPage(dirpagebuf);
	dirpage->init();
	
	//write back to BM
	if(!bm.write(sysTabRec.startPage, dirpagebuf, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	//convert systabrec
	if(!convertSysTabRecToRecord(sysTabRec, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	
	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemTablePointer(), (*dbHeader), rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	delete[] rec;
	
	//create sys_col records for each column
	sysColRecs = new SysColumnRecord[pq.getNoOfColumns()];		//Create as many sys col records as columns
	
	columns = pq.getColumns();		//COLUMNS captured
	types = pq.getTypes();			//COLUMN DATATYPES captured
	notNulls = pq.getNotNulls();		//COLUMN is NULL ?
	pks = pq.getPKs();				//COLUMN is PrimaryKey ?
	unique = pq.getUnique();			//COLUMN is Unique ?
	maxsizes = pq.getMaxSize();			//COLUMN Max Size
	
	for(int i=0; i < pq.getNoOfColumns(); i++){
		sprintf(sysColRecs[i].columnName,"%s",columns[i].c_str());		//Add column name
		sprintf(sysColRecs[i].tableName,"%s",pq.getTable().c_str());		//Add table name
		sysColRecs[i].colPos = i;						//Column order in table
		sysColRecs[i].dataType = types[i];					//Column data type
		sysColRecs[i].isNotNull = notNulls[i];					//Column is Nullable ?
		sysColRecs[i].isPrimary = pks[i];					//Column is Primary ?		
		sysColRecs[i].isUnique = unique[i];					//Column is Unique ?
		sysColRecs[i].isDeleted = false;					//Column is Deleted ?
		sysColRecs[i].maxSize	= maxsizes[i];					//max size for the Column
		if(defaultValues[i].isDefault)
			strcpy(sysColRecs[i].defaultvalue,(defaultValues[i].value).c_str());
		else
			strcpy(sysColRecs[i].defaultvalue,"NULL");						//Default Value is NULL if no user defined value
		
		//check for constraint in compatibility
		//if col is primary, then set unique and not null to true
		if(pks[i]){
			sysColRecs[i].isNotNull = true;
			sysColRecs[i].isUnique = false;

			//collect primary column name to create index on it
			primaryCols.push_back(to_string(sysColRecs[i].columnName));
		}
		
		//sysColRecs[i].dump();
		
		if(!convertSysColRecToRecord(sysColRecs[i], rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		
             	if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }

		delete[] rec;
		
/*             	if(!insertRecordIntoDataPage((char *)&sysColRecs[i],sizeof(SysColumnRecord),
                	dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
			err = RECORD_INSERT_ERROR;
			goto ret;
	        }
*/	     
	}
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}

	
	//call create index if the primary key is mentioned
	if(primaryCols.size() != 0){
		//create an index on this table'
			
		tempPQ.setTable(pq.getTable());		//index on the same table
		tempPQ.setColumns(primaryCols);		//on the primary key columns
		tempPQ.setIndexName("primary_key_index");

		DM_FUNC_TRACE && cout << "Creating index on primary key" << endl;

		//call create index
		if(!handleCreateIndex(tempPQ)){
			err = INVALID;
			goto ret;
		}

	}


	ret:
	delete[] sysColRecs;
	if(dirpage != NULL)
		delete dirpage;
	if(dbHeader!=NULL)
		delete dbHeader;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//==========================================================================================

//handleDropTable()-This method drops the table if it is present else returns error message.
bool DataManager::handleDropTable(parsedQuery &pq){
	int err = NO_ERROR;
        int i = 0;
        int startPage;
        int size;
        int curDirPageNo;
        DM_FUNC_TRACE && cout << "DM.handleDropTable()\n";
        
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	RID rid;
	
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	vector<SysIndexRecord> sysIndexRecs;
	vector<RID> sysIndexRIDs;
	vector<string> indexOnTable;
	bool newindexname = false;
        string tempindexname;

	parsedQuery tempPQ;

        DirectoryEntry curDE,nextDE;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *DirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}        
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		err = INVALID;
		goto ret;
	}

	startPage = systabrec.startPage;
	curDirPageNo = startPage;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			
			if(!markAsFreePage(curDirPageNo,(*dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;

		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			
			if(!markAsFreePage(curDE.pageNo,(*dbHeader))){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		//mark this DP as freePage
		if(!markAsFreePage(curDirPageNo,(*dbHeader))){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);

	//drop all indexes on this table	
	//get all sys index records for this table.. call handle drop table for each of the indexes
	//get unique list of index names
	for(int i=0; i<sysIndexRecs.size(); i++){
		newindexname=true;
		tempindexname = to_string(sysIndexRecs[i].indexName);
		for(int j=0; j<i; j++){
			if(tempindexname.compare(to_string(sysIndexRecs[j].indexName)) == 0){
				newindexname = false;
				break;
			}	
		}
		if(newindexname)
			indexOnTable.push_back(tempindexname);
	}	

	//for each of the indexes, call handle drop index with the constructed parsed query
	tempPQ.setTable(pq.getTable());
	for(int i=0; i< indexOnTable.size(); i++){
		tempPQ.setIndexName(indexOnTable[i]);
		if(!handleDropIndex(tempPQ, true, dbHeader)){
			err = DROP_INDEX_ERROR;
			goto ret;
		}			
	}




	//delete table entry in SYSTAB
	if(!deleteRecord(SYS_TAB_START_PAGE_NUM,rid,(*dbHeader))){
		err = DATAMANAGER_DELETE_RECORD_ERROR;
		goto ret;
	}
	//delete columns entry in SYSCOLUMN
	for(i = 0; i < sysColRIDs.size() ; i++){
		if(!deleteRecord(SYS_COL_START_PAGE_NUM,sysColRIDs[i],(*dbHeader))){
			err = DATAMANAGER_DELETE_RECORD_ERROR;
			goto ret;
		}
	}
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
} 
//=============================================================================================================

bool DataManager::handleAlterTable(parsedQuery &pq){

	DM_FUNC_TRACE && cout<<"DM.handleAlterTable()\n";
	int err = NO_ERROR;
	
	//column Names, Column Types, Add or Delete indices, Unique, Not Nullable, PKs
	vector<string> columns(pq.getColumns());
	vector<int> types(pq.getTypes()); //Columns to add
	vector<string> deleteCols(pq.getDeleteCols()); //Columns to Delete
	vector<bool> notNulls(pq.getNotNulls());			//COLUMN is NULL ?
	vector<bool> pks(pq.getPKs());				//COLUMN is PrimaryKey ?
	vector<bool> unique(pq.getUnique());			//COLUMN is Unique ?
	vector<defaultValue> defaultValues(pq.getDefaultValues()); //Contains default values
	vector<int> maxsizes(pq.getMaxSize());
	int i,j;
	parsedQuery tempPQ;
	/*
	cout<<"Entered handleAlterTable"<<endl;
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"Columns to add"<<endl;
	for(i=0;i<columns.size();i++)
		cout<<"\t"<<columns[i]<<" Type: "<<types[i]<<endl;
	cout<<"Columns to delete"<<endl;
	for(i=0;i<deleteCols.size();i++)
		cout<<"\t"<<deleteCols[i]<<endl;	
	cout<<"Default Vaules specified"<<endl;
	for(i=0;i<defaultValues.size();i++){
		if(defaultValues[i].isDefault)
			cout<<"Default value:"<<defaultValues[i].value<<endl;
		else
			cout<<"Default value: NULL"<<endl;
	}
	
	cout<<"No of Columns: "<<pq.noOfColumns<<endl;
	*/

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
       	DBHeader* dbHeader = NULL;
	int updatedNoOfColumns;

	RID rid, sysTabRID;
	SysTableRecord systabrec,sysTabRec;
	vector<SysColumnRecord> syscolrecs_prev;
	vector<SysColumnRecord> syscolrecs;
      	SysColumnRecord* sysColRecs = NULL;

      	SysTableRecord tmpsystabrec;
	vector<SysColumnRecord> sysColumns;
	vector<RID> sysColRIDs;

	vector<int> toTypes;
	char* rec= NULL;
	int recSize;
	bool exists;


	vector<SysIndexRecord> sysIndexRecs;
	vector<int> sysColOrigPos;
	vector<RID> sysIndexRIDs;



	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
        
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}

	//Get SysTableRecord for "table-name" from Sys Table Page
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}
	
	//Needed to update totalColumns of systab entry corresponding to table
	sysTabRID = rid;
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), syscolrecs_prev, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	

	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}



	//If columns.size() != 0 , Add columns to SysCols
	if(columns.size() != 0){
	
		//check for adding duplicate columns
		for(i=0; i<syscolrecs_prev.size(); i++){
			for(j=0;j<columns.size();j++){
				if(!(columns[j]).compare(syscolrecs_prev[i].columnName) && !syscolrecs_prev[i].isDeleted){
					err = COLUMN_EXISTS;
					goto ret;
				}
				if(!defaultValues[j].isDefault && notNulls[j]){
					err = COLUMN_NULLABLE;
					goto ret;
				}	
			}	
		}
		sysColRecs = new SysColumnRecord[columns.size()];					//Create as many sys col records as columns
		
		for(i=0; i < pq.getNoOfColumns(); i++){
			sprintf(sysColRecs[i].columnName,"%s",columns[i].c_str());			//Add column name
			sprintf(sysColRecs[i].tableName,"%s",pq.getTable().c_str());			//Add table name
			sysColRecs[i].colPos = i + systabrec.totalColumns;				//Column order in table
			sysColRecs[i].dataType = types[i];						//Column data type
			sysColRecs[i].isNotNull = notNulls[i];						//Column is Nullable ?
			sysColRecs[i].isPrimary = pks[i];						//Column is Primary ?		
			sysColRecs[i].isUnique = unique[i];						//Column is Unique ?
			sysColRecs[i].isDeleted = false;						//Column is Deleted ?
			sysColRecs[i].maxSize	= maxsizes[i];						//max size for the Column
			if(defaultValues[i].isDefault)
				strcpy(sysColRecs[i].defaultvalue,(defaultValues[i].value).c_str());
			else
				strcpy(sysColRecs[i].defaultvalue,"NULL");				//Default Value is NULL if no user defined value
			
			//check for constraint in compatibility
			//if col is primary, then set unique and not null to true
			if(pks[i]){
				sysColRecs[i].isNotNull = true;
				sysColRecs[i].isUnique = true;
			}

			if(!convertSysColRecToRecord(sysColRecs[i], rec, recSize)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			
			if(!insertRecordIntoDataPage(rec, recSize, dbHeader->getSystemColumnPointer(),(*dbHeader),rid)){
				err = RECORD_INSERT_ERROR;
				goto ret;
		       }
		       DM_FUNC_TRACE && cout<<"Column "<<columns[i]<<" added"<<endl;
			
		}//Columns have been added to SysCol Page
		
		//Now create a updated systab record for the given tablename
		updatedNoOfColumns = systabrec.totalColumns + pq.getNoOfColumns();
		DM_FUNC_TRACE && cout<<"Updated no. of Columns:"<<updatedNoOfColumns;
		strcpy(sysTabRec.tableName,systabrec.tableName);
		sysTabRec.startPage = systabrec.startPage;
		sysTabRec.totalRows = systabrec.totalRows;
		sysTabRec.totalDP = systabrec.totalDP;
		sysTabRec.totalColumns = updatedNoOfColumns;
		
		if(rec != NULL)
			delete[] rec;
		
		//convert systabrec
		if(!convertSysTabRecToRecord(sysTabRec, rec, recSize)){
			err = CONVERT_ERROR;
			goto ret;
		}
		//convertRecordToSysTabRec(tmpsystabrec, rec, recSize);
		
		//cout<<"No of columns after converting to sys col record:"<<tmpsystabrec.totalColumns<<endl;
		
		if(!updateRecordFromDataPage(rec, recSize,sysTabRID,systabrec.startPage,(*dbHeader))){
			err = UPDATE_RECORD_FROM_DATAPAGE_ERROR;
			goto ret;
		}
		
	}//Columns are added
	

	//Delete Columns
	if(deleteCols.size() != 0){
		
		sysColRIDs.clear();
		syscolrecs.clear();

		//check if any of the columns given are the same
		for(i=0; i<deleteCols.size(); i++){
			for(j=0; j<i; j++){
				if(deleteCols[i].compare(deleteCols[j]) == 0){
					//duplicate column in list
					cout << "Duplicate column '" << deleteCols[i] << "' in column list\n" ;
					err = INVALID;
					goto ret;
				}			
			}
		}
		
		//Get SysTableRecord for "table-name" from Sys Table Page
		if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
		        err = INVALIDTABLENAME;
		        goto ret;
		}
		
		if(!getSysColRecsForTable(pq.getTable(), syscolrecs, (*dbHeader), systabrec.totalColumns, sysColRIDs)){
			//Column Read Error
			err = INVALID;
			goto ret;
		}	
		
		//Check if the given columns exists
		for(i=0;i<deleteCols.size();i++){
			exists = false;
			for(j=0; j<syscolrecs.size();j++)
				if(!deleteCols[i].compare(syscolrecs[j].columnName) && !syscolrecs[j].isDeleted){
					exists = true;
					break;
				}
			if(!exists){
				cout << "Column '" << deleteCols[i] << "' does not exist\n";
				err = COLUMN_NOT_DEFINED;
				goto ret;
			}
			
			//Modify syscolrecs[j] and Update corresponding sysColRecord
			syscolrecs[j].isDeleted = true;

			//collect sys col orig pos to delete indexes on this col
			sysColOrigPos.push_back(syscolrecs[j].colPos);

			if(rec != NULL)
				delete[] rec;
				
			if(!convertSysColRecToRecord(syscolrecs[j], rec, recSize)){
				err = CONVERT_ERROR;
				goto ret;
			}
			
			if(!updateRecordFromDataPage(rec, recSize,sysColRIDs[j], dbHeader->getSystemColumnPointer(),(*dbHeader))){
				err = UPDATE_RECORD_FROM_DATAPAGE_ERROR;
				goto ret;
			}

		}
		
		tempPQ.setTable(pq.getTable());
		for(i=0; i<sysColOrigPos.size(); i++){
			//delete all indexes on this column
			for(j=0; j<sysIndexRecs.size(); j++){
				if(sysIndexRecs[j].colOrigPos == sysColOrigPos[i])
				{
					//create parsed query => drop index <index_name> on <table>					
					tempPQ.setIndexName(to_string(sysIndexRecs[j].indexName));
					if(!handleDropIndex(tempPQ, true, dbHeader)){
						cout << "Error dropping index '" << to_string(sysIndexRecs[j].indexName) << "' on table '" << pq.getTable() << "'\n";
						err = DROP_INDEX_ERROR;
						goto ret;
					}			

				}				
			}
		}

	}

	//write back dbheadbuf
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:	
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbHeader != NULL)
		delete dbHeader;
	if(rec != NULL)
		delete[] rec;
	if(sysColRecs != NULL )
		delete[] sysColRecs; 
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//===============================================================================

bool DataManager::handleUpdate(parsedQuery &pq){

	DM_FUNC_TRACE && cout<<"DM.handleUpdate()\n";
	int err = NO_ERROR;
	
	//column Names, Column Types, Add or Delete indices, Unique, Not Nullable, PKs
	vector<string>	condition_array = (vector<string>)pq.getConditionArray();
	vector<string>	colNames = (vector<string>)pq.getColumns();	
	vector<string>	dataValues(pq.getDataValues());
	vector<int>		updateExprIndex(pq.getUpdateExprIndex());
	vector< vector<string> > conditionArrays(pq.getConditionArrays());
	vector< string > updateCondArray;
	
	int i=0,j=0, noOfRecMods=0, k=0;
	
	//cout<<"Entered handleUpdate"<<endl;
	//cout<<"Table: "<<pq.getTable()<<endl;strtmp.insert(4,"/",2);
							//strtmp.insert(8,"/",2);
	//cout<<"Columns "<<endl;
	//for(i=0;i<colNames.size();i++)
	//	cout<<"\t"<<colNames[i]<<" : "<< dataValues[i]<<endl;
		
	//cout<<"No of Columns: "<<pq.noOfColumns<<endl;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	
	vector<int> colTypes;
	string str;

	vector<string> key;
        vector<int> keyTypes;
	int size = 0;
	char* record = NULL;
	bool found = false;
	string tempstring;
	vector<string> indexNames;

        vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
        vector<RID> sysIndexRIDs;
	RID oldrecRID, newrecRID;

	vector<string> keyValues;
	string indexName;
	vector<int> opr;
	vector<int> indexPos;
	
	vector<bool> displayCol; // contains the col indexs of the columns to display
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> recAttributes;
	vector<string> updateAttributes;
	bool condition;
	string result("0");
	RID rid,sysTabRecRID;
	bool exists;
	int updatedSize;
	char* updatedRec = NULL;	
	vector<int> updateIndex;
	vector<int> updateBySysCol;
	bool foundColName;
	int typeFound;
	char* retValue = NULL;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}

	exists = true;
	//check if system tables are being modified
	if(sysTabRec.startPage==DB_HEADER_PAGE_NUM
	||	sysTabRec.startPage==SYS_TAB_START_PAGE_NUM 
	||	sysTabRec.startPage==SYS_COL_START_PAGE_NUM
	||	sysTabRec.startPage==SYS_INDEX_START_PAGE_NUM
	){
		cout << pq.getTable() << " is a System Table\nModification of System Tables PROHIBITED\n";
		err = INVALID;
		goto ret;
	}

	sysTabRecRID = rid;
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	//coltypes are in colTypes

	//get sys index recs for that table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbhead), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	//find out list of unique index names
	for(int i=0; i<sysIndexRecs.size(); i++){
		found = false;
		for(int j=0; j<indexNames.size(); j++){
			if(indexNames[j].compare(to_string(sysIndexRecs[i].indexName)) == 0){
				found = true;
				break;
			}
		}
		
		if(found) continue;
		
		//add index name to unique list
		indexNames.push_back(to_string(sysIndexRecs[i].indexName));
	}

	if(pq.conditionExists){
		if(!checkValidColumnName(condition_array, sysColRecs)){
			err = INVALID;
			goto ret;	
		}	
	}	
	
	//checking if the columns to set exists in table
	exists = false;
	for(j=0;j<colNames.size();j++){
		exists = false;
		for(i=0;i<sysColRecs.size();i++){
			if(!colNames[j].compare(sysColRecs[i].columnName) && !sysColRecs[i].isDeleted){
				exists = true;
			}
		}
		if(!exists){
			cout<<"Column to set:"<<colNames[j]<<" does not exist in table"<<endl;
			err = INVALID;
			goto ret;
		}
	}	

	//check if indexes can be present
     	if(!getSuitableIndex( sysColRecs, sysIndexRecs, condition_array, keyValues, opr, indexPos, indexName)){
		err = INVALID;
		goto ret;
	}

	//cout<<"IndexName: "<<indexName<<endl;
	
	//if indexName == "NULL" ===> no index can be used
	if(indexName.compare("NULL") != 0){
		//make use of the index
		cout << endl << "Fetching records using the index : " << indexName << endl;

		if(!getRecordsUsingIndex(indexName, indexPos,opr, keyValues, sysColRecs, (*dbhead), sysIndexRecs, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}


	}else{
		cout << endl << "Fetching all records" << endl;
		//cout<<"Getting All Records by table scan"<<endl;	
		if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}

	}

/*
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}

*/	
	
	for(j=0;j<sysColRecs.size();j++){
		updateAttributes.push_back("");
		updateIndex.push_back(-2);		
		colTypes.push_back(sysColRecs[j].dataType);
		updateBySysCol.push_back(-1);
	}
	
	for(i=0;i<pq.getNoOfColumns();i++){
		exists = false;
		for(j=0;j<sysColRecs.size();j++){
			if(!colNames[i].compare(sysColRecs[j].columnName)){
				if( updateExprIndex[i] != -1){
					//This column is equated to an expr, whose postfix representation is stored in an vector, whose index
					//is stored in updateExprIndex, and this index is now stored in updateIndex
					updateIndex[j] = updateExprIndex[i];
				}
				else{
					//cout<<"To update column: "<< colNames[i] <<" with value: "<<dataValues[i]<<endl;
					//The column is equated to a column name or literal value
					foundColName = false;
					//find column name
					for(k=0;k<sysColRecs.size();k++)
						if(!dataValues[i].compare(sysColRecs[k].columnName)){
							foundColName = true;
							updateBySysCol[j] = k;
							updateIndex[j] = -3;
							break;
						}
					//is a literal value	
					if(!foundColName){
						updateAttributes[j] = dataValues[i];
						updateIndex[j] = -1;
					}
				}
				exists = true;
			}
		}
		if(!exists){
			cout<<"Column not defined for given table"<<endl;
			err = COLUMN_NOT_DEFINED;
			goto ret;
		}
	}
	
	for(i=0;i<allrecs.size();i++){
		recAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}
		
		err = evaluate_expression(pq.getConditionArray(),recAttributes,sysColRecs,result);
		if(err != NO_ERROR )
		{
			goto ret;
		}
		
		if( result.compare("0") ){
			
			noOfRecMods++;
			//Update Record i
			rid = allRIDs[i];
			DM_FUNC_TRACE && cout<<"Updated Record with values: ";
			for(j=0;j<sysColRecs.size();j++){
				updateCondArray.clear();
				//if( sysColRecs)
				if(j >= recAttributes.size() && updateIndex[j] != -1){
					updateAttributes[j] = sysColRecs[j].defaultvalue;
					//cout<<"SysCol Index: "<<j<<" Updated value: "<<updateAttributes[j]<<endl;
					continue;
				}
				//Copy contents column 'j' of the old record verbatum to the new record
				else if(updateIndex[j] == -2)
					updateAttributes[j] = recAttributes[j];	
				//Copy contents of other Column to the column 'j' of new record ( in cases of v2 = v3 )				
				else if(updateIndex[j] == -3){
					//cout<<"Need to update value"<<endl;
						updateAttributes[j] = recAttributes[updateBySysCol[j]];
				}
				//Evaluate Expression given to find value of column 'j' of new record
				else if(updateIndex[j] != -1){
					updateCondArray = conditionArrays[updateIndex[j]];
					err = evaluate_expression(updateCondArray,recAttributes,sysColRecs,result);
					if( err != NO_ERROR ){
						cout<<"Error in expression given for column: "<<sysColRecs[j].columnName<<endl;
						goto ret;
					}
					//check if type of string - result matches with sysColRecs[j].dataType
					//typeFound = getTypeFromValue(result);
					retValue = NULL;
					if( roundToType(sysColRecs[j].dataType,result.c_str(),retValue) == 0){
						cout<<"Cannot convert computed value : "<< result <<" to type: "<<sysColRecs[j].dataType<<endl;
						err = COLTYPE_MISMATCH;
						goto ret;
					}
					if(retValue != NULL){
						//cout<<"Converted value: "<<retValue <<endl;
						result = retValue;
						free(retValue);
					}
					updateAttributes[j] = result;
				}
				DM_FUNC_TRACE && cout<<updateAttributes[j]<<"\t";				
			}
			DM_FUNC_TRACE && cout<<endl;
	
	
			//delete the old key from all indexes
			for(int k=0; k<indexNames.size(); k++){
			
				tempSysIndexRecs.clear();
				tempstring.clear();
				tempstring = indexNames[k];
				key.clear();
				keyTypes.clear();
		
		
				INDEX_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << ", to delete from index"<< endl;
		
				for(int j=0; j<sysIndexRecs.size(); j++){
					//find all index recs matching indexNames[i]
					if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
						tempSysIndexRecs.push_back(sysIndexRecs[j]);
					}
			
				}
		
				//now we have index recs for one index name
				key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
				keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
		
		
				for(int j=0; j<tempSysIndexRecs.size(); j++){
		
					//write the old attribute at colOrigPos to the key in the colIndexPos
			
					key[tempSysIndexRecs[j].colIndexPos] = recAttributes[tempSysIndexRecs[j].colOrigPos];
					keyTypes[tempSysIndexRecs[j].colIndexPos] = colTypes[tempSysIndexRecs[j].colOrigPos];
			
					INDEX_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColRecs[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColRecs[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
				}
		
				//now the key for one index is built..  pack it and insert
		
				//pack the key
				if(record != NULL)
					delete[] record;
				if(!pack::pack_data(key, record, size, keyTypes)){
					err = PACK_ERROR;
					goto ret;
				}
				
				//use the old rec RID
				oldrecRID = allRIDs[i];

				//delete the key
				if(!handleIndexDelete(tempSysIndexRecs[0].startPage, keyTypes, record, oldrecRID, (*dbhead))){
					err = RECORD_INSERT_ERROR;
					goto ret;
				}

			}


			if(updatedRec != NULL)
				delete[] updatedRec;
			if(!pack::pack_data(updateAttributes,updatedRec,updatedSize,colTypes)){
				//cout<< "Here"<<endl;
				cout<<"Input value does not match column data type "<<endl;
				err = CONVERT_ERROR;
				goto ret;
			}
			
			//updateRecordFromDataPage((char*)allrecs[i].c_str(),allrecs[i].size(),rid,sysTabRec.startPage,(*dbhead));
			if(!updateRecordFromDataPage(updatedRec, updatedSize, rid, sysTabRec.startPage, (*dbhead))){
				err = UPDATE_RECORD_FROM_DATAPAGE_ERROR;
				goto ret;
			}


			//insert the new updated key in all indexes
			for(int k=0; k<indexNames.size(); k++){
			
				tempSysIndexRecs.clear();
				tempstring.clear();
				tempstring = indexNames[k];
				key.clear();
				keyTypes.clear();
		
		
				INDEX_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << ", to insert into index"<< endl;
		
				for(int j=0; j<sysIndexRecs.size(); j++){
					//find all index recs matching indexNames[i]
					if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
						tempSysIndexRecs.push_back(sysIndexRecs[j]);
					}
			
				}
		
				//now we have index recs for one index name
				key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
				keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
		
		
				for(int j=0; j<tempSysIndexRecs.size(); j++){
		
					//write the old attribute at colOrigPos to the key in the colIndexPos
			
					key[tempSysIndexRecs[j].colIndexPos] = updateAttributes[tempSysIndexRecs[j].colOrigPos];
					keyTypes[tempSysIndexRecs[j].colIndexPos] = colTypes[tempSysIndexRecs[j].colOrigPos];
			
					INDEX_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColRecs[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColRecs[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
				}
		
				//now the key for one index is built..  pack it and insert
		
				//pack the key
				if(record != NULL)
					delete[] record;
				if(!pack::pack_data(key, record, size, keyTypes)){
					err = PACK_ERROR;
					goto ret;
				}
				
				//use the old rec RID
				newrecRID = rid;

				//insert the updated key
				if(!handleIndexInsert(tempSysIndexRecs[0].startPage, keyTypes, record, newrecRID, (*dbhead))){
					err = RECORD_INSERT_ERROR;
					goto ret;
				}

			}

		}
	}		

	//write back header
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	cout << "No. of records updated : " << noOfRecMods << endl;

	ret:
	if(record != NULL)
		delete[] record;
	if(updatedRec != NULL)
		delete[] updatedRec;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}
//===============================================================================

bool DataManager::handleAddRecords(parsedQuery &pq){

	DM_FUNC_TRACE && cout << "DM.handleAddRecods()\n";
	int err = NO_ERROR;
	
	int i =0,j=0;
	
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"No of Records to Add: "<<pq.getNoOfRecords()<<endl;
	cout<<"Record Type: ";
	if(pq.getRecordType() == DEFAULT)
		cout<<"DEFAULT"<<endl;
	else if(pq.getRecordType() == RANGE){
		cout<<"RANGE"<<endl;
		cout<<"Start of Range: "<<pq.getStartRange()<<endl;
	}	
	else
		cout<<"RANDOM"<<endl;

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	SysTableRecord sysTabRec;
	RID rid;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	char* record;
	int recSize = 0;
	vector<string> recAttributes;
	vector<char*> records; //contains list of records to add
	vector<int> recordSizes; 
	vector<int> types;
	vector<RID> recordRIDs;
	int randno;
	char* randstr;
	string randString,tempstr;
	int rangeno;
	char* rangestr;
	string rangeString;
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	char* tmpcstr;
	vector<string*> recordStrings;
	string* attributes;
	int k = 0;
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	record = NULL;
	/*for(i=0;i<pq.getNoOfRecords();i++)
		records.push_back(record);
	*/	
	types.clear();
	for(i=0;i<sysColRecs.size();i++)
		types.push_back(sysColRecs[i].dataType);
		
	if(pq.getRecordType() == DEFAULT){
		//Enter default values records to table
		DM_FUNC_TRACE && cout <<"Entering Default values"<<endl;
		records.clear();
		recAttributes.clear();
		for(i=0;i<sysColRecs.size();i++){
			if(!sysColRecs[i].isDeleted && !strcmp(sysColRecs[i].defaultvalue,"NULL") && sysColRecs[i].isNotNull ){
				err = COLUMN_NOT_NULLABLE;
				goto ret;
			}
			if(sysColRecs[i].isDeleted)
				recAttributes.push_back("NULL");
			else
				recAttributes.push_back(sysColRecs[i].defaultvalue);	
		}
		
		for(i=0;i<pq.getNoOfRecords();i++){
			attributes = (string*)new string[recAttributes.size()];
			for(k=0;k<recAttributes.size();k++)
				attributes[k] = recAttributes[k];
			recordStrings.push_back(attributes);
		}
		
		DM_FUNC_TRACE && cout <<"No of records: "<<recordStrings.size()<<endl;			
	}
	else if(pq.getRecordType() == RANDOM){
		//insert pq.getNoOfRecords() records of type RANDOM
		//initialize the seed
		DM_FUNC_TRACE && cout <<"Entering Random values"<<endl;		
		srand( time(NULL) );
		for(i=0;i<pq.getNoOfRecords();i++){
			recAttributes.clear();
			randno = rand() % 10000 + 1; //Random no b/w: 1 to 10000
			sprintf(randstr,"%d",randno);
			randString = randstr;
			attributes = (string*)new string[sysColRecs.size()];
			
			for(j=0;j<sysColRecs.size();j++){
			
				if(sysColRecs[i].isDeleted){
					recAttributes.push_back("NULL");
					attributes[j] = "NULL";
				}	
				else if(sysColRecs[i].dataType == INTTYPE || sysColRecs[i].dataType == SHORTTYPE || sysColRecs[i].dataType == LONGTYPE || sysColRecs[i].dataType == FLOATTYPE || sysColRecs[i].dataType == DOUBLETYPE){
					recAttributes.push_back(randString);
					attributes[j] = randString;
				}	
				else if(sysColRecs[i].dataType == BOOLTYPE){
					if(randno % 2 ==1){
						recAttributes.push_back("1");
						attributes[j] = "1";
						}
					else{
						recAttributes.push_back("0");
						attributes[j] = "0";
						}
				}
				else if(sysColRecs[i].dataType == VARCHARTYPE){
					tempstr = "word" + randString;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
				else if(sysColRecs[i].dataType == DATETYPE){
					sprintf(randstr,"%04d%02d%02d",(randno % 9999 + 1),(randno % 12 + 1),(randno % 31 + 1));
					tempstr = randstr;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
				else if(sysColRecs[i].dataType == TEXTTYPE){
					tempstr = "word" + randString;
					recAttributes.push_back(tempstr);
					attributes[j] = tempstr;
				}
			}
			
			recordStrings.push_back(attributes);
		/*	tmpcstr = (char*)new char[recSize];
			strcpy(tmpcstr,record);
			records.push_back(tmpcstr);
			recordSizes.push_back(recSize);
		*/
		}
	}
	else if(pq.getRecordType() == RANGE){
		//insert pq.getNoOfRecords() records into table of type RANGE
		//if range value is specified, insert values starting from that range till range+pq.getNoOfRecords()
		//else find max value in the unique column, or else if varchar is present find max num whr value = "wordnum".
		DM_FUNC_TRACE && cout <<"Entering Range values"<<endl;		
		if(pq.getStartRange() != -1){
			//Range value specified
			rangeno = pq.getStartRange();
			for(i=0;i<pq.getNoOfRecords();i++){
				recAttributes.clear();
				rangeno = rangeno + i;
				sprintf(rangestr,"%d",rangeno);
				rangeString = rangestr;
				attributes = (string*)new string[sysColRecs.size()];
				
				for(j=0;j<sysColRecs.size();j++){
				
					if(sysColRecs[i].isDeleted){
						recAttributes.push_back("NULL");
						attributes[j] = "NULL";
					}	
					else if(sysColRecs[i].dataType == INTTYPE || sysColRecs[i].dataType == SHORTTYPE || sysColRecs[i].dataType == LONGTYPE || sysColRecs[i].dataType == FLOATTYPE || sysColRecs[i].dataType == DOUBLETYPE){
						recAttributes.push_back(rangeString);
						attributes[j] = rangeString;
					}	
					else if(sysColRecs[i].dataType == BOOLTYPE){
						if(rangeno % 2 ==1){
							recAttributes.push_back("1");
							attributes[j] = "1";
						}	
						else{
							recAttributes.push_back("0");
							attributes[j] = "0";
						}	
					}
					else if(sysColRecs[i].dataType == VARCHARTYPE){
						tempstr = "word" + rangeString;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
					else if(sysColRecs[i].dataType == DATETYPE){
						sprintf(rangestr,"%04d%02d%02d",(rangeno % 9999 + 1),(rangeno % 12 + 1),(rangeno % 31 + 1));
						tempstr = rangestr;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
					else if(sysColRecs[i].dataType == TEXTTYPE){
						tempstr = "word" + rangeString;
						recAttributes.push_back(tempstr);
						attributes[j] = tempstr;
					}
				}
				recordStrings.push_back(attributes);
			/*	recSize = 0;
				if(!pack::pack_data(recAttributes,record,recSize,types)){
						err = PACK_ERROR;
						goto ret;
				}
				tmpcstr = (char*)new char[recSize];
				strcpy(tmpcstr,record);
				records.push_back(tmpcstr);
				recordSizes.push_back(recSize);
			*/	
			}
		}
		else {
			//Range value not specified, find the range from the records in table
			getAllRecords(sysTabRec.startPage,allrecs,recsizes,allRIDs);
			
			//try to find max values for each unique col, and insert records frm that values on wards for each col
			
		}
	}
	DM_FUNC_TRACE && cout <<"No of records:"<<records.size()<<endl;		
	//insert pq.getNoOfRecords() into table with
	if(!insertRecordsIntoTable(sysTabRec.startPage,(*dbhead),recordRIDs,types,recordStrings)){
		err = INSERT_RECORDS_TO_TABLE;
		goto ret;
	}
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);	
	return err==NO_ERROR?true:false;
}

//=======================================================================

bool DataManager::handleSelect(parsedQuery &pq){

	DM_FUNC_TRACE && cout << "DM.handleSelect()\n";
	int err = NO_ERROR;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	vector<SysIndexRecord> sysIndexRecs;
        vector<RID> sysIndexRIDs;
	vector<int> colTypes;
	string str;
	vector<string> keyValues;
	string indexName;
	vector<int> opr;
	vector<int> indexPos;

	vector<string> condition_array = (vector<string>)pq.getConditionArray();
	vector<string> colNames = (vector<string>)pq.getColumns();
	vector<bool> displayCol; // contains the col indexs of the columns to display
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	vector<string> orderCols = (vector<string>)pq.getOrderByCols();
	vector<bool> descOrdering = (vector<bool>)pq.getDescOrder();
	vector<int> orderBy;
	
	RID rid;
	
	
	string displayHead, displayRecord;
	
	vector<string*> finalRecords;
	string* strptr = NULL;
	int* getAttrIndex;
	int i=0, j=0;
	int colsToAdd;
	char* recbuf = NULL;
	vector<string> recAttributes;
	vector<string> finalRecAttributes;
	vector<int> displayIndex; //Required for the order in which the records must be diplayed
	vector<int> maxSizes; //Max Sizes of Data values of each of the columns
	int var_col = 0;
	void* att_value = NULL;
	bool RecPassedCond = false;
	bool condition = false;
	string result("0");
	bool condition_mismatch = false;
	bool columnexists;
	bool displayAll;
	bool swapped = false;
	bool sortAgain = true;
	int tmp, noOrderColFound = 0;
	string tmpstr;
	double dval1,dval2;
	//double* result = NULL;
	
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	//get sys index recs
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbhead), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	
	for(i=0;i<orderCols.size();i++)
		orderBy.push_back(-1);
	
	for(i=0; i<sysColRecs.size(); i++){
		displayCol.push_back(false);
		colTypes.push_back(sysColRecs[i].dataType);
		maxSizes.push_back(5);
		if(strlen(sysColRecs[i].columnName)>maxSizes[i])
			maxSizes[i] = strlen(sysColRecs[i].columnName);
		if(sysColRecs[i].maxSize < 20){	
			if(sysColRecs[i].maxSize > maxSizes[i] && sysColRecs[i].dataType == VARCHARTYPE)
				maxSizes[i] = sysColRecs[i].maxSize * 70/100;
			else if(sysColRecs[i].maxSize > maxSizes[i])
				maxSizes[i] = sysColRecs[i].maxSize;
		}
		else
			if(maxSizes[i] < 25)
				maxSizes[i] = 25 * 70/100;		
		displayHead = "";
		displayHead += "(" + sysColRecs[i].getTypeName(sysColRecs[i].dataType) + ")";
		if(displayHead.size() >maxSizes[i])
				maxSizes[i] = displayHead.size();
		for(j=0;j<orderCols.size();j++){
			if(strcmp(sysColRecs[i].columnName,orderCols[j].c_str()) == 0){
				orderBy[j] = i;
				noOrderColFound++;
			}
		}
	}
	
	if(noOrderColFound != orderCols.size()){
		cout<<" Error: Column not defined"<<endl;
		err = COLUMN_NOT_DEFINED;
		goto ret;
	}
	
	displayAll = false;
	//populate column types
	for(i=0; i<colNames.size(); i++){
		//cout<<"entered1"<<endl;
		if(displayAll)
		break;
		
		if(!colNames[i].compare("*")){
			//cout<<"entered2"<<endl;
			displayAll = true;
		}

		columnexists = false;
		for(j=0;j<sysColRecs.size();j++){

			if( (displayAll || !colNames[i].compare(sysColRecs[j].columnName)) && !sysColRecs[j].isDeleted){
				//cout<<"entered3"<<endl;
				columnexists = true;
				displayCol[j] = true;
			}
		}
		if(!columnexists){
			err = COLUMN_NOT_DEFINED;
			goto ret;
		}
	}

	if(pq.conditionExists){
		if(!checkValidColumnName(condition_array, sysColRecs)){
			err = INVALID;
			goto ret;	
		}	
	}

	//check if indexes can be present
     	if(!getSuitableIndex( sysColRecs, sysIndexRecs, condition_array, keyValues, opr, indexPos, indexName)){
		err = INVALID;
		goto ret;
	}

	//cout<<"IndexName: "<<indexName<<endl;
	
	//if indexName == "NULL" ===> no index can be used
	if(indexName.compare("NULL") != 0){
		//make use of the index
		cout << "Fetching records using the index : " << indexName << endl;
		
		if(!getRecordsUsingIndex(indexName, indexPos,opr, keyValues, sysColRecs, (*dbhead), sysIndexRecs, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}


	}else{
		cout << "Fetching all records" << endl;
		//cout<<"Getting All Records by table scan"<<endl;	
		if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}

	}

	cout << "\nFetched " << allrecs.size() << " records\n";;

	//Where Condition not entered, Just Display the Values
	//if(!pq.conditionExists){
	
	//Display only those columns whch are specified
	//cout<<"Setting finalRecords to diplay"<<endl;
	for(j=0; j<sysColRecs.size(); j++){
		if(displayCol[j]){
			cout<<setw(maxSizes[j]+1)<<sysColRecs[j].columnName;
			cout<<setw(0);
		}
	}
	cout << endl;

	for(j=0; j<sysColRecs.size(); j++){
		displayHead = "";
		if(displayCol[j]){
			displayHead += "(" + sysColRecs[j].getTypeName(sysColRecs[j].dataType) + ")";
			cout<<setw(maxSizes[j]+1)<<displayHead;
			cout<<setw(0);
		}
	}
	cout << endl;
	
	//cout<<"Unpacking all Records and filling finalRecords"<<endl;	
	for(j=0; j<allrecs.size(); j++){
		
		recAttributes.clear();
		finalRecAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[j].c_str(), recsizes[j])){
			err = UNPACK_ERROR;
			goto ret;
		}
		//columns to add
		colsToAdd = sysColRecs.size() - recAttributes.size();
		strptr = (string*)new string[sysColRecs.size()];

		for(i=0;i<recAttributes.size();i++){
			
			if(recAttributes[i].size()+2 > maxSizes[i])
				maxSizes[i] = recAttributes[i].size()+2;
			if(colTypes[i] == DATETYPE ){
				recAttributes[i].insert(4,"/",2);
				recAttributes[i].insert(8,"/",2);
			}
			strptr[i] = recAttributes[i];
			finalRecAttributes.push_back(recAttributes[i]);
		}

		displayRecord.clear();
		displayRecord = "| ";
		
		if(colsToAdd > 0){
		
			//cout<<"Adding values to finalRecords for the deleted columns and column with default values"<<endl;
			//Add values to final records for  the deleted columns and column with default values
			for(i=recAttributes.size();i<sysColRecs.size();i++){
			
				if(sysColRecs[i].isDeleted){
					strptr[i] = "NULL";
					finalRecAttributes.push_back("NULL");
					continue;
				}
				tmpstr = sysColRecs[i].defaultvalue;
				if(sysColRecs[i].dataType == DATETYPE){
					tmpstr.insert(4,"/",2);
					tmpstr.insert(8,"/",2);
				}
				strptr[i] = tmpstr;
				if(strlen(sysColRecs[i].defaultvalue)+2 > maxSizes[i])
					maxSizes[i] = tmpstr.length()+2;
				finalRecAttributes.push_back(tmpstr);
			}
		}
		finalRecords.push_back(strptr);
		//cout<<"Evaluating condition for record: "<<j+1<<endl;
		err = evaluate_expression(pq.getConditionArray(),finalRecAttributes,sysColRecs,result);
		if(err != NO_ERROR )
		{
			goto ret;
		}
		if( result.compare("0") ){
			displayIndex.push_back(j);
			strptr = finalRecords[j];
			if(!pq.orderBy){
				for(i=0;i<sysColRecs.size();i++){	
					if(displayCol[i]){
						cout<<"|";
						cout<<setw(maxSizes[i] )<<strptr[i];
					}
				}
				cout<<endl;
			}
		}
	}

	//Sort using bubble sort where after each pass, the largest element is pushed to its correct pos
	//After pass 1, the n-1 element is sorted, after pass 2, n-2 element is sorted and so on.
	//n element array requires max n-1 passes.
	if( pq.orderBy ){
	
		QuickSort* qs = new QuickSort(finalRecords,displayIndex,orderBy,descOrdering,colTypes);
		
		qs->quicksort(0,displayIndex.size()-1);
		displayIndex = qs->getDisplayIndex();
		for( j = 0;j<displayIndex.size();j++){
			strptr = finalRecords[displayIndex[j]];
			for(i=0;i<sysColRecs.size();i++){
			
				if(displayCol[i]){
				cout<<"|";
				cout<<setw(maxSizes[i] )<<strptr[i];
				}
			}
			cout<<endl;
		}
		/*
		for( j=displayIndex.size()-1;j>0;j--){
			recAttributes.clear();
			if(pq.orderBy && sortAgain){
				swapped = false;
				for(i=0;i<j;i++){
					//cout<<"comparing recs:("<< i <<" " << i+1 << ")\t";
					if(QuickSort::compareRecords(finalRecords[displayIndex[i]],finalRecords[displayIndex[i+1]],orderBy,descOrdering,colTypes) < 0){
						swapped = true;
						tmp = displayIndex[i];
						displayIndex[i] = displayIndex[i+1];
						displayIndex[i+1] = tmp;
					}
				}
				if(!swapped)
					sortAgain = false;
			}
			strptr = finalRecords[displayIndex[j]];
			for(i=0;i<sysColRecs.size();i++){
			
				if(displayCol[i]){
				cout<<"|";
				cout<<setw(maxSizes[i] )<<strptr[i];
				}
			}
			cout<<endl;
		}
		//Displaying the first record, which is sorted after n-1 passes
		if(displayIndex.size() > 0){
			recAttributes.clear();
				strptr = finalRecords[displayIndex[0]];
				for(i=0;i<sysColRecs.size();i++){
				
					if(displayCol[i]){
					cout<<"|";
					cout<<setw(maxSizes[i] )<<strptr[i];
					}
				}
			cout<<endl;
		}
	*/	
	}

	cout << "\nNumber of Rows in Result Set : " << displayIndex.size() << endl;
	//}
	
	for(i=0;i<finalRecords.size();i++){
		strptr = finalRecords[i];
		delete[] strptr;
	}
	
	finalRecords.clear();
	
	ret:
	if(dbhead != NULL)
		delete dbhead;
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}



//===========================================================================================




bool DataManager::handleInsert(parsedQuery &pq){
        int err = NO_ERROR;
        int i = 0, j = 0, noCols = 0;
        int startPage;
        int size;
        DM_FUNC_TRACE && cout << "DM.handleInsert()\n";
        char *record = NULL;
	char buf_dbhead[PAGE_SIZE_IN_BYTES];
	char* dbheadbuf = &buf_dbhead[0];
	//char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	char buf_dirpage[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = &buf_dirpage[0];
	//char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	RID rid;
	
	SysColumnRecord syscolrec;
        SysTableRecord systabrec;
        vector<SysColumnRecord> sysColumns;
        vector<RID> sysColRIDs;
        vector<int> types = pq.getTypes();
        vector<int> toTypes;
        vector<string> values = pq.getDataValues();
        vector<string> finalValues;
        vector<string> key;
        vector<int> keyTypes;
        
        vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
        
        vector<RID> sysIndexRIDs;
        vector<string> indexNames;
        bool flag = false;
	bool constraintPresent = false;
        string tempstring;
        
        DBHeader* dbHeader = NULL;
        DirectoryPage *dirPage = NULL;
      
      
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
                
        //Initialize DBHeader.
	dbHeader = new DBHeader(dbheadbuf);
        if(dbHeader->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getSysTabRecForTable(pq.getTable(), systabrec , (*dbHeader), rid)){
	        err = INVALIDTABLENAME;
	        goto ret;
	}


	//check if system tables are being modified
	if(systabrec.startPage==DB_HEADER_PAGE_NUM
	||	systabrec.startPage==SYS_TAB_START_PAGE_NUM 
	||	systabrec.startPage==SYS_COL_START_PAGE_NUM
	||	systabrec.startPage==SYS_INDEX_START_PAGE_NUM
	){
		cout << pq.getTable() << " is a System Table\nModification of System Tables PROHIBITED\n";
		err = INVALID;
		goto ret;
	}

	if(!getSysColRecsForTable(pq.getTable(),sysColumns,(*dbHeader),systabrec.totalColumns, sysColRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbHeader), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}
	
	noCols = 0;
	//check for columns type.
	for(j=0, i = 0 ; j < systabrec.totalColumns ; j++ ){
	        syscolrec = sysColumns[j];
		/*if(types[i] != syscolrec.dataType && types[i] != NULLTYPE){
			cout << "Column type mismatch " << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}*/
		
		/*if( j >= pq.getNoOfColumns() ){
			//Then there are some columns for which values have not been specified
			toTypes.push_back(syscolrec.dataType);
			finalValues.push_back(syscolrec.defaultvalue);
			continue;
		}*/
		
		//if column is deleted, add dummy column and null data
		if(syscolrec.isDeleted){
			//add as nulltype and null data
			toTypes.push_back(NULLTYPE);
			finalValues.push_back("NULL");
			//goto next sys coll
			continue;
		}
	
		//check if unique OR Primary constraint is present
		if(syscolrec.isUnique || syscolrec.isPrimary){
			constraintPresent = true;
		}

		//check if null is not allowed and column is not deleted
		if(types[i] == NULLTYPE && syscolrec.isNotNull && !syscolrec.isDeleted){
			cout << "Column '" << syscolrec.columnName << "' cannot take NULL values" << endl;
			err = COLTYPE_MISMATCH;
			goto ret;
		}
		
		//user specified null value
		if(!values[i].compare("uNULL")){
			toTypes.push_back(NULLTYPE);
			finalValues.push_back("NULL");
			i++;
			noCols++;
			continue;
		}
		
		//check valid conversion		
		if(types[i] == NULLTYPE){
			//check if default value exists
			if(!strcmp(syscolrec.defaultvalue,"NULL"))
				toTypes.push_back(NULLTYPE);
			else
				toTypes.push_back(syscolrec.dataType);
			values[i] = (char*)syscolrec.defaultvalue;			
		}else{
			//check if recognised datatype can be converted into the type in syscol
			if(!checkDataType(types[i], syscolrec.dataType)){
				cout << "Datatype mismatch for column '" << syscolrec.columnName << "'" << endl;
				err = COLTYPE_MISMATCH;
				goto ret;	
			}
			toTypes.push_back(syscolrec.dataType);
			
		}
		
		//Check if the DataType is Varchar, if yes then check size
		if(types[i] == VARCHARTYPE){
			if(values[i].size() > syscolrec.maxSize){
				cout<< "Input value for column '"<<syscolrec.columnName<<"' exceeds max size"<<endl;
				err = SIZE_MISMATCH;
				goto ret;	
			}	
		}
		
		noCols++;
		finalValues.push_back(values[i]);
		//next column
		i++;
	}
	
	if( finalValues.size() != j){
		cout << "Column count doesn't match " << endl;
		err = TOTALCOL_MISMATCH;
		goto ret;
	}
	
	//i represents the count of number of non deleted columns	
	//check for no of undeleted columns. 
	if(pq.getNoOfColumns() > noCols){
		cout << "Column count doesn't match " << endl;
		err = TOTALCOL_MISMATCH;
		goto ret;
	}

	//check if the record has unique constraints.. if yes check with the database
	if(constraintPresent && !checkRecordForConstraints(systabrec, sysColumns, sysIndexRecs, finalValues)){
		err = INVALID;
		goto ret;
	}
	
	startPage = systabrec.startPage;
	
	if(!pack::pack_data(finalValues, record, size, toTypes)){
		err = PACK_ERROR;
		goto ret;
	}
	
	//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
	if(!insertRecordIntoDataPage(record,size,startPage,(*dbHeader),rid)){
		err = RECORD_INSERT_ERROR;
		goto ret;
	}
	
	DM_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$$$$$"<<endl;
	DM_FUNC_TRACE && cout <<"pageNo "<<rid.pageNo<<"    slotNo"<<rid.slotNo<<endl;
	DM_FUNC_TRACE && cout <<"$$$$$$$$$$$$$$$$$"<<endl;
	
	//Add record to all Indexes on this table
	
	//find out list of unique index names
	for(int i=0; i<sysIndexRecs.size(); i++){
		flag = false;
		for(int j=0; j<indexNames.size(); j++){
			if(indexNames[j].compare(to_string(sysIndexRecs[i].indexName)) == 0){
				flag = true;
				break;
			}
		}
		
		if(flag) continue;
		
		//add index name to unique list
		indexNames.push_back(to_string(sysIndexRecs[i].indexName));
	}
	
	//now we have unique list of index names
	//for each of this index... insert the key
	
	for(int i=0; i<indexNames.size(); i++){	
	
		tempSysIndexRecs.clear();
		tempstring.clear();
		tempstring = indexNames[i];
		key.clear();
		keyTypes.clear();
		
		
		DM_FUNC_TRACE && cout << "Building Key for Index : " << tempstring << endl;
		
		for(int j=0; j<sysIndexRecs.size(); j++){
			//find all index recs matching indexNames[i]
			if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
				tempSysIndexRecs.push_back(sysIndexRecs[j]);
			}
			
		}
		
		//now we have index recs for one index name
		key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
		keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
		
		
		for(int j=0; j<tempSysIndexRecs.size(); j++){
		
			//write the attribute at colOrigPos to the key in the colIndexPos
		
			key[tempSysIndexRecs[j].colIndexPos] = finalValues[tempSysIndexRecs[j].colOrigPos];
			keyTypes[tempSysIndexRecs[j].colIndexPos] = toTypes[tempSysIndexRecs[j].colOrigPos];
			
			DM_FUNC_TRACE && cout << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColumns[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColumns[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
		}
		
		//now the key for one index is built..  pack it and insert
		//cout << "Press a key\n";
		//hcin  >> tempstring;
		
		
		//pack the key
		if(record != NULL)
			delete[] record;
		if(!pack::pack_data(key, record, size, keyTypes)){
			err = PACK_ERROR;
			goto ret;
		}
		
		//insert the key
		if(!handleIndexInsert(tempSysIndexRecs[0].startPage,keyTypes,record,rid,(*dbHeader))){
			err = RECORD_INSERT_ERROR;
			goto ret;
		}
		
	}
	
	
	
	
	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	ret:
	if(record != NULL){
		delete[] record;
	}
	
	if(dbheadbuf != NULL){
		//delete[] dbheadbuf;
		dbheadbuf = NULL;
	}
	if(dirpagebuf != NULL){
		//delete[] dirpagebuf;
		dirpagebuf = NULL;
	}
	if(dirPage != NULL){
		delete dirPage;
	}
	if(dbHeader != NULL){
		delete dbHeader;
	}
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================

bool DataManager::handleDelete(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDelete()\n";
	int err = NO_ERROR;
	

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	RID systabrid;	
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;

	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	vector<int> colTypes;

	vector<SysIndexRecord> sysIndexRecs, tempSysIndexRecs;
	vector<RID> sysIndexRIDs;
	vector<string> key;
        vector<int> keyTypes;
        vector<string> indexNames;
        bool flag = false;
        string tempstring;

	vector<string> keyValues;
	string indexName;
	vector<int> opr;
	vector<int> indexPos;
	vector<string>	condition_array = (vector<string>)pq.getConditionArray();

        char *record = NULL;
	int size = 0;
	vector<string> recAttributes;
	bool condition = false;
	string result("0");
	int i=0,noOfRecMods=0;


	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), systabrid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}

	
	//check if system tables are being modified
	if(sysTabRec.startPage==DB_HEADER_PAGE_NUM
	||	sysTabRec.startPage==SYS_TAB_START_PAGE_NUM 
	||	sysTabRec.startPage==SYS_COL_START_PAGE_NUM
	||	sysTabRec.startPage==SYS_INDEX_START_PAGE_NUM
	){
		cout << pq.getTable() << " is a System Table\nModification of System Tables PROHIBITED\n";
		err = INVALID;
		goto ret;
	}


	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	for(int j=0;j<sysColRecs.size();j++){
		colTypes.push_back(sysColRecs[j].dataType);
	}

	//get sys index records for the table
	if(!getSysIndexRecsForTable(pq.getTable(), sysIndexRecs, (*dbhead), sysIndexRIDs)){
		err = INVALIDTABLENAME;
	        goto ret;
	}

	//find out list of unique index names
	for(int i=0; i<sysIndexRecs.size(); i++){
		flag = false;
		for(int j=0; j<indexNames.size(); j++){
			if(indexNames[j].compare(to_string(sysIndexRecs[i].indexName)) == 0){
				flag = true;
				break;
			}
		}
		
		if(flag) continue;
		
		//add index name to unique list
		indexNames.push_back(to_string(sysIndexRecs[i].indexName));
	}

	if(pq.conditionExists){
		if(!checkValidColumnName(condition_array, sysColRecs)){
			err = INVALID;
			goto ret;	
		}	
	}



	//check if indexes can be present
     	if(!getSuitableIndex( sysColRecs, sysIndexRecs, condition_array, keyValues, opr, indexPos, indexName)){
		err = INVALID;
		goto ret;
	}

	//cout<<"IndexName: "<<indexName<<endl;
	
	//if indexName == "NULL" ===> no index can be used
	if(indexName.compare("NULL") != 0){
		//make use of the index
		cout << "Fetching records using the index : " << indexName << endl;

		if(!getRecordsUsingIndex(indexName, indexPos,opr, keyValues, sysColRecs, (*dbhead), sysIndexRecs, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}


	}else{
		cout << "Fetching all records" << endl;
		//cout<<"Getting All Records by table scan"<<endl;	
		if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
			err = INVALID;
			goto ret;
		}

	}


/*
	//get all records for this table
	if(!getAllRecords(sysTabRec.startPage, allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
*/

	for(i=0; i<allrecs.size(); i++){

		recAttributes.clear();
		
		if(!pack::unpack_data(recAttributes, colTypes, (char*)allrecs[i].c_str(), recsizes[i])){
			err = UNPACK_ERROR;
			goto ret;
		}
		
		err = evaluate_expression(pq.getConditionArray(),recAttributes,sysColRecs,result);
		if(err != NO_ERROR )
		{
			goto ret;
		}
		
		if( result.compare("0") ){
			
			noOfRecMods++;

			if(!deleteRecord(sysTabRec.startPage, allRIDs[i], (*dbhead))){
				err = DATAMANAGER_DELETE_RECORD_ERROR;
				goto ret;
			}

			//Delete from index
			
			//now we have unique list of index names
			//for each of this index... delete the key			

			for(int k=0; k<indexNames.size(); k++){	
	
				tempSysIndexRecs.clear();
				tempstring.clear();
				tempstring = indexNames[k];
				key.clear();
				keyTypes.clear();
		
		
				DM_FUNC_TRACE && cout << "Deleting Key from Index : " << tempstring << endl;
		
				for(int j=0; j<sysIndexRecs.size(); j++){
				//find all index recs matching indexNames[i]
					if(tempstring.compare(to_string(sysIndexRecs[j].indexName)) == 0){
						tempSysIndexRecs.push_back(sysIndexRecs[j]);
					}
				}
		
				//now we have index recs for one index name
				key = vector<string> (tempSysIndexRecs.size(),""); // dummy initial values for both key and key types
				keyTypes = vector<int> (tempSysIndexRecs.size(),-1);
		
		
				for(int j=0; j<tempSysIndexRecs.size(); j++){
		
				//write the attribute at colOrigPos to the key in the colIndexPos
		
					key[tempSysIndexRecs[j].colIndexPos] = recAttributes[tempSysIndexRecs[j].colOrigPos];
					keyTypes[tempSysIndexRecs[j].colIndexPos] = colTypes[tempSysIndexRecs[j].colOrigPos];
			
					DM_FUNC_TRACE && cout << "Deleting key : " << key[tempSysIndexRecs[j].colIndexPos] << " ( " << sysColRecs[tempSysIndexRecs[j].colOrigPos].columnName << " :: " << sysColRecs[0].getTypeName(keyTypes[tempSysIndexRecs[j].colIndexPos]) << " ) " << endl;
			
				}
		
				//now the key for one index is built..  pack it and send for delete
		
				//pack the key
				if(record != NULL)
					delete[] record;
				if(!pack::pack_data(key, record, size, keyTypes)){
					err = PACK_ERROR;
					goto ret;
				}
		
				//delete the key
				if(!handleIndexDelete(tempSysIndexRecs[0].startPage,keyTypes,record,allRIDs[i],(*dbhead))){
					err = DATAMANAGER_DELETE_RECORD_ERROR;
					goto ret;
				}
		
			}//deleting from all indexes


		}
	}

	cout << "No of records deleted : " << noOfRecMods << endl;
	
	allrecs.clear();
	allRIDs.clear();
	recsizes.clear();

	if(!bm.write(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}

	ret:	
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}

//===============================================================================================================

