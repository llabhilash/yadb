#ifndef QUICKSORT
#define QUICKSORT

#include <iostream>
#include <vector>

class QuickSort{

	private: 
		vector<string*> finalRecords;
		vector<int> displayIndex;
		vector<int> orderBy;
		vector<bool> descOrdering;
		vector<int> types;
	
	//Swap two integers
	void swap( int a, int b );

	//Find the index of the Median of the elements
	//of array that occur at every "shift" positions.
	int findMedianIndex( int left, int right, int shift);
	 
	 //Computes the median of each group of 5 elements and stores
	//it as the first element of the group. Recursively does this
	//till there is only one group and hence only one Median
	string* findMedianOfMedians( int left, int right);
	
	//Partition the array into two halves and return the
	//index about which the array is partitioned
	int partition(int left, int right);
	
	public: 
	
	//construtor
	QuickSort(vector<string*> finalRecs, vector<int>  & displayIn, vector<int> ordering, vector<bool> descOrder, vector<int> type){
		finalRecords = finalRecs;
		displayIndex = displayIn;
		orderBy = ordering;
		descOrdering = descOrder;
		types = type;
	}
	
	//Quicksort the array
	void quicksort( int left, int right );
	int compareRecords(string* record1,string* record2);
	vector<int> getDisplayIndex(){
		return displayIndex;
	}	
 };
 
 //Swap two integers
void QuickSort::swap(int a, int b){
	    int temp;
	    temp = displayIndex[a];
	    displayIndex[a] = displayIndex[b];
	    displayIndex[b] = temp;
}

//Find the index of the Median of the elements
//of array that occur at every "shift" positions.
int QuickSort::findMedianIndex(int left, int right, int shift){
    int i, groups = (right - left)/shift + 1, k = left + groups/2*shift;
    for(i = left; i <= k; i+= shift){
    
        int minIndex = i, j;
        string* minValue = finalRecords[displayIndex[minIndex]];
        for(j = i; j <= right; j+=shift)
            if(QuickSort::compareRecords(finalRecords[displayIndex[j]],minValue) < 0)
            {
                minIndex = j;
                minValue = finalRecords[displayIndex[minIndex]];
            }
        swap(i, minIndex);
    }
 
    return k;
}

 //Computes the median of each group of 5 elements and stores
//it as the first element of the group. Recursively does this
//till there is only one group and hence only one Median
string* QuickSort::findMedianOfMedians(int left, int right){

	if(left == right)
		return finalRecords[displayIndex[left]];
 
	int i, shift = 1;
	while(shift <= (right - left)){
    		for(i = left; i <= right; i+=shift*5){    
			int endIndex = (i + shift*5 - 1 < right) ? i + shift*5 - 1 : right;
			int medianIndex = findMedianIndex(i, endIndex, shift);
			swap(i, medianIndex);
		}
		shift *= 5;
	}
 
	return finalRecords[displayIndex[left]];
}

int QuickSort::partition(int left, int right){

	//Makes the leftmost element a good pivot,
	//specifically the median of medians
	findMedianOfMedians(left, right);
	int pivotIndex = left,  index = left, i;
	string* pivotValue = finalRecords[displayIndex[pivotIndex]];

	swap(pivotIndex, right);
	for(i = left; i < right; i++){
	       if(compareRecords(finalRecords[displayIndex[i]],pivotValue) < 0){
		       swap(i, index);
		       index += 1;
		}
	}
	swap(right, index);
	
	return index;
}

//Partition the array into two halves and return the
//index about which the array is partitioned
void QuickSort::quicksort( int left, int right ){

	if(left >= right)
	       return;
	int index = partition(left, right);
	quicksort(left, index - 1);
	quicksort(index + 1, right);
}

int QuickSort::compareRecords(string* record1,string* record2){
	bool done = false;
	int i=0;
	int val = 0;
	double no1, no2;
	long l1,l2;
	size_t pos;
	string str1,str2;
	for(i=0;i<orderBy.size();i++){
		no1 = no2 = 0;
		l1 = l2 = 0;
		switch(types[orderBy[i]]){
		
		case DATETYPE:		if( (val = record1[orderBy[i]].compare(record2[orderBy[i]])) != 0){
								if(descOrdering[i])
									val *= -1;
									done = true;
							}	
							break;
		case VARCHARTYPE:	
							if( isNumber(record1[orderBy[i]],no1) && isNumber(record2[orderBy[i]],no2) ){
								if(no1 != no2){
									val = (no1 > no2)?1:-1;
									if(descOrdering[i])
										val *= -1;
									done = true;
								}
							 }
							else if( (val = record1[orderBy[i]].compare(record2[orderBy[i]])) != 0){
								if(descOrdering[i])
									val*= -1;
									done = true;
							}
							break;
		default:				no1 = strtod(record1[orderBy[i]].c_str(),NULL);
							no2 = strtod(record2[orderBy[i]].c_str(),NULL);
							if(no1 != 0 && no2 != 0){
								if(no1 != no2){
									val = (no1 > no2)?1:-1;
									if(descOrdering[i])
										val *= -1;
									done = true;
								}
							}	
							else if( (val = record1[orderBy[i]].compare(record2[orderBy[i]])) != 0){
								if(descOrdering[i])
									val *= -1;
									done = true;
							}
							break;
		}
		
		if(done)
			break;
	}
	
	/*for(i=0;i<types.size();i++){
		cout<<record1[i]<<"   ";
	}
	if(val == 0)
		cout<<" = ";
	else if(val < 0)
		cout<<" < ";
	else if(val > 0)
		cout<<" > ";
	for(i=0;i<types.size();i++)
		cout<<record2[i]<<"   ";
	cout<<endl;*/
	return val;
}

#endif
