#ifndef DATAMANAGER_DECL_H
#define DATAMANAGER_DECL_H

class DataManager{

	private:
	BufferManager bm;
	
	
	
	public:
	DataManager();
	~DataManager();
	bool init(int);
	bool shutdown();
	void showBMHitRatio();
	void showBMWorking();
	bool commitBM();
	bool handleShowSchemas();
	bool getFreePage(char* &, int&, DBHeader &);	//buffer, returns pageNum
	bool handleCreateTable(parsedQuery &pq);	
	bool handleUseSchema(parsedQuery &);
	bool insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader,RID &rid);
	bool insertRecordsIntoTable(int startPage,DBHeader & dbHeader, vector<RID> &rid, vector<int> types,vector<string*> recordStrings);	
	bool handleShowTables(parsedQuery &);
	bool handleShowColumns(parsedQuery &);
	bool handleCreateSchema(parsedQuery &);
	bool handleCreateIndex(parsedQuery &pq);
	bool handleSearchIndex(parsedQuery &pq);
	bool handleTraverseIndex(parsedQuery &pq);
	bool handleSelect(parsedQuery &);
	bool handleInsert(parsedQuery &);
       	bool handleDropSchema(parsedQuery &);
       	bool handleDropTable(parsedQuery &pq);
	bool handleDropIndex(parsedQuery &pq, bool, DBHeader* &);
	bool handleDescTable(parsedQuery &pq);
	bool handleDelete(parsedQuery &pq);
	bool insertIntoIndex(int &pageNo,const vector<int> &keyType ,char* const &key ,IndexNode &node ,
	              const RID &rid ,bool &split,DBHeader &dbHeader);
	bool handleIndexInsert(const int &indexerpage,const vector<int> &keyType,char* const &key,
	              const RID &rid,DBHeader &dbHeader);
	bool getRIDofRecordsFromIndex(int &pageNo,const vector<int> &keyType,char* const &key,
		vector<RID> &allrids,int totalKeys,vector<int> columnPos,vector<int> operatorCode,vector<string> values);
	bool indexGetRID(const int &indexerpage,const vector<int> &keyType,char* const &packedkey,
		vector<RID> &allrids,int totalKeys,vector<int> columnPos,vector<int> operatorCode,vector<string> values);
	bool handleAlterTable(parsedQuery &);
	bool handleUpdate(parsedQuery &);              
	bool handleAddRecords(parsedQuery &);
	bool handleAddRecordes(parsedQuery &);	
	bool handleIndexDelete(const int &indexerpage,const vector<int> &keyType,char* const &key,
	                                         RID &rid,DBHeader &dbHeader);
	bool deleteFromIndex(BranchPage &parentPage,int &childPage,int &oldChild,const vector<int> &keyType,
                                                 char* const &key,bool &merge,RID &rid , DBHeader &dbHeader);
        bool indexSearch(const int &indexerpage,const vector<int> &keyType,char* const &key,RID &rid,bool &found);
        bool searchFromIndex(int &pageNo,const vector<int> &keyType,char* const &key,RID &rid,bool &found);
        bool traverseInIndex(int &pageNo,const vector<int> &keyType);
        bool indexTraverse(const int &indexerpage,const vector<int> &keyType);
     	bool getSuitableIndex(vector<SysColumnRecord> sysColRecs, vector<SysIndexRecord> sysIndexRecs, vector<string> condition_array,vector<string> &keyValues,vector<int> &opr,vector<int> &indexPos, string &indexName);
                                                 
        private:
        bool getSysTabRecForTable(string name, SysTableRecord &sysrec, DBHeader &dbHeader, RID &rid, bool dispErr = true);
        bool getSysColRecsForTable(string name,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns, vector<RID> &sysColRIDs);
        bool getSysIndexRecsForTable(string name,vector<SysIndexRecord> &sysIndexRecs,DBHeader &dbHeader, vector<RID> &sysIndexRIDs);
        bool getAllRecords(int startDirPageNo, vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs);
	bool getRecordsUsingIndex(string indexName, vector<int> indexPos, vector<int> operatorCodes, vector<string> values, vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader, vector<SysIndexRecord> sysIndexRecs, vector<string> &records, vector<int> &recsizes, vector<RID> &allRIDs);
        bool markAsFreePage(int pageNo, DBHeader& dbHeader);
        bool deleteRecord(const int &startDP ,const RID &rid,DBHeader &dbHeader);
        bool updateRecordFromDataPage(char* rec, int reclen,RID &rid, int startPage, DBHeader & dbHeader);
	bool getIndexPageNumbersRecursively(int curPageNum, vector<int> &pageNums);
        bool checkDataType(int from, int to);
        template <class X>
	string to_string(const X& x);
	bool convertRecordToSysColRec(SysColumnRecord &syscolrec, char* rec, int recSize);
	bool convertSysColRecToRecord(SysColumnRecord syscolrec, char* &rec, int &recSize);
	bool convertRecordToSysTabRec(SysTableRecord &systabrec, char* rec, int recSize);
	bool convertSysTabRecToRecord(SysTableRecord systabrec, char* &rec, int &recSize);
	bool convertRecordToSysIndexRec(SysIndexRecord &sysindexrec, char* rec, int recSize);
	bool convertSysIndexRecToRecord(SysIndexRecord sysindexrec, char* &rec, int &recSize);
	bool checkRecordForConstraints(SysTableRecord &sysTabRec, vector<SysColumnRecord> &sysColumns, vector<SysIndexRecord> &sysIndexRecs, vector<string> &finalValues);
	bool constructLeftmostKey(vector<int> indexTypes, vector<int> colPos, vector<int> operatorCodes, vector<string> value, vector<string> &newKeys);
	bool constructRightmostKey(vector<int> indexTypes, vector<int> colPos, vector<int> operatorCodes, vector<string> value, vector<string> &newKeys);	
};
	
#endif
