//=======================================================================
DataManager::DataManager(){}
//=======================================================================
DataManager::~DataManager(){}
//=======================================================================
bool DataManager::init(int ramSizeToUseInBytes){
		
	if(!bm.init(ramSizeToUseInBytes)){
		cout << "Cannot initialize buffer manager\n";
		return false;
	}
	
	return true;
}
//=======================================================================
bool DataManager::shutdown(){
	return bm.shutdown();
}
//=======================================================================
void DataManager::showBMHitRatio(){
	bm.showHitRatio();
}
//=======================================================================
void DataManager::showBMWorking(){
	bm.showWorking();
}


//=======================================================================
bool DataManager::commitBM(){
	return bm.commitBuffer();
}

//=======================================================================

bool DataManager::getFreePage(char* &pageBuf, int& pageNum, DBHeader& dbheader){
	
	int err = NO_ERROR;
	char *freePageBuf = new char[PAGE_SIZE_IN_BYTES];
	FreePage *freePage = NULL;
	int nextFreePage;
	DM_FUNC_TRACE && cout<<"DM.getFreePage()\n";
	string dummy;
	
	//check if dbheader has free page pointer == -1		
	if(dbheader.getFreePagePointer() == -1){
	
		//expand heapfile
		
		if(!bm.expandHeapFile(dbheader.getLastPage())){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
				
		//set current Last Page + 1 as free page
		if(!dbheader.setFreePagePointer(dbheader.getLastPage() + 1)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		
		//increment noOfPages by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setNoOfPages(dbheader.getNoOfPages() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}
		
		//increment LastPage by BM_NO_OF_PAGES_TO_EXPAND
		if(!dbheader.setLastPage(dbheader.getLastPage() + BM_NO_OF_PAGES_TO_EXPAND)){
			err = HEAP_EXPAND_ERROR;
			goto ret;
		}	
		
	}
	
	//read free page pointer into buffer pool
	pageNum = dbheader.getFreePagePointer();
	if(!bm.read(pageNum, freePageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	freePage = new FreePage(freePageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);
	
	DM_FUNC_TRACE && cout << "Setting new free page pointer in DBHeader : " << nextFreePage << endl;
	//cin >> dummy;
	
	if(!bm.read(pageNum, pageBuf, FREE_PAGE_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}	
	/*//Reset the free page int the dbheader......
	freePage = new FreePage(pageBuf);
	nextFreePage = freePage->getNextFreePage();
	dbheader.setFreePagePointer(nextFreePage);*/
	ret: 
	if(freePage!=NULL)
		delete freePage;
	if(freePageBuf != NULL)
		delete[] freePageBuf;
	DM_FUNC_TRACE && printErrorCode(err);
	DM_FUNC_TRACE && cout << "Sending AS FreePage : " << pageNum << endl;
	return err==NO_ERROR?true:false;
	
}
//=======================================================================
bool DataManager::handleShowSchemas(){

	int err = NO_ERROR;
	DM_FUNC_TRACE && cout<<"DM.handleShowSchemas()\n";

	vector<string> schemas;

	//find all directories in FILENAME_PATH and add them to schemas
	DIR *dp;
	struct dirent *dirp;
	
	if((dp  = opendir(FILENAME_PATH)) == NULL) {
        	cout << "Error(" << errno << ") opening " << FILENAME_PATH << endl;
        	err = READ_ERROR;
        	goto ret;
    	}

    	while ((dirp = readdir(dp)) != NULL) {
    		
    		//skip .,..,.svn
    		if(string(dirp->d_name).compare(".") == 0 || string(dirp->d_name).compare("..") == 0 || string(dirp->d_name).at(0) == '.'){
    			continue;
    		}
    	
        	schemas.push_back(string(dirp->d_name));
        	cout << dirp->d_name<<"\n";
    	}
    	
   	closedir(dp);
   	
   	ret: 
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
} 
//======================================================================

bool DataManager::handleUseSchema(parsedQuery &pq){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleUseSchema()\n";
	
	if(!bm.setSchemaFile((char *)pq.getDatabase().c_str())){
		err = USE_SCHEMA_ERROR;
		goto ret;
	}
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleShowTables(parsedQuery &){
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowTables()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	cout << endl;
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysTabRec(systabrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}

		//systabrec.dump();
		cout << endl << systabrec.tableName << endl;
	}
	cout << endl;

	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

bool DataManager::handleShowColumns(parsedQuery &){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleShowColumns()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysColRec(syscolrec, (char*)allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		cout << endl;
		syscolrec.dump();
	}

	ret:	
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;

}
//================================================================================================
bool DataManager::handleDescTable(parsedQuery &pq){

        DM_FUNC_TRACE && cout << "DM.handleDescTable()\n";
	int err = NO_ERROR;

	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord sysTabRec;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	RID rid;
	int i;	
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}

		
	//print the sys cols for this table
	for(i=0; i<sysColRecs.size(); i++){
		if(sysColRecs[i].isDeleted)
			continue;
		cout << endl;
		sysColRecs[i].dump();	
	}
	cout << endl;
	
	
	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL)
		delete dbhead;	
	sysColRecs.clear();
	sysColRIDs.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::handleCreateSchema(parsedQuery &pq){

	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.handleCreateSchema()\n";

	char* filename = new char[pq.getDatabase().length() + 1];
	char* rec= NULL;
	char* null = new char[5];
	sprintf(null, "NULL");
	int recSize;
	strcpy(filename,pq.getDatabase().c_str());
        FILE *fp;
	settings::init();
	//add path to schema file
	char filewithpath[FILENAME_SIZE + PATH_SIZE];
	sprintf(filewithpath,"%s%s",FILENAME_PATH,filename);
	//cout<<filewithpath<<endl;
	if((fp = fopen(filewithpath,"r")) != NULL){
		cout<<"Can't create database '"<<filename<<"'; database exists"<<endl;
		fclose(fp);
		return false;
	}
        if((fp = fopen(filewithpath,"w")) == NULL){
	        cout<<"Can't create database '"<<filename<<"';database exists"<<endl;
	        return false;
        }
        fclose(fp);
        char *data = new char[PAGE_SIZE_IN_BYTES];
        DBHeader header = DBHeader(data);
        header.setNoOfPages(6);
        header.setLastPage(SYS_INDEX_START_PAGE_NUM + 2);
	header.setFreePagePointer(-1);
	header.setSystemTablePointer(SYS_TAB_START_PAGE_NUM);
	header.setSystemColumnPointer(SYS_COL_START_PAGE_NUM);	
	header.setSystemIndexPointer(SYS_INDEX_START_PAGE_NUM);			
        BufferManager Buff;
        Buff.init(100000);
        Buff.setSchemaFile(filename, false);
        Buff.write(DB_HEADER_PAGE_NUM,data,DB_HEADER_PRIORITY);
        delete[] data;
        data = new char[PAGE_SIZE_IN_BYTES];
        DirectoryPage dirPage = DirectoryPage(data);
        dirPage.init();
	DirectoryEntry de,de1;
	
	char *sysData = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage dbp = DataBasePage(sysData);
	SysTableRecord sysTab;
	RID rid;
	rid.pageNo = 4;
	//SYS_TABLE
	sprintf(sysTab.tableName,"%s","SYS_TABLE");
	sysTab.totalRows = 3;
	sysTab.startPage = SYS_TAB_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 5;
	dbp.init();
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	//SYS_COLUMN
	sprintf(sysTab.tableName,"%s","SYS_COLUMN");
	sysTab.totalRows = 21;
	sysTab.startPage = SYS_COL_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 10;	
	
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	
	//SYS_INDEX
	sprintf(sysTab.tableName,"%s","SYS_INDEX");
	sysTab.totalRows = 0;
	sysTab.startPage = SYS_INDEX_START_PAGE_NUM;
	sysTab.totalDP = 1;
	sysTab.totalColumns = 6;	
	
	if(!convertSysTabRecToRecord(sysTab, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	//dbp.insertRecord((char *)&sysTab,sizeof(SysTableRecord),rid);
	
	
	de.pageNo = SYS_INDEX_START_PAGE_NUM + 1;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_TAB_START_PAGE_NUM,data,DIR_PAGE_PRIORITY);
	Buff.write(de.pageNo,sysData,DB_PAGE_PRIORITY);
	
	
	//write dummy sys_index dirpage
	delete[] data;
	data = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
	dirPage.init();
	//write back to BM
	if(!Buff.write(SYS_INDEX_START_PAGE_NUM, data, DIR_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	
	delete[] data;
	delete[] sysData;
	data = new char[PAGE_SIZE_IN_BYTES];
	sysData = new char[PAGE_SIZE_IN_BYTES];
	dirPage = DirectoryPage(data);
        dirPage.init();
	dbp = DataBasePage(sysData);
	SysColumnRecord sysCol;
	rid.pageNo = SYS_INDEX_START_PAGE_NUM + 2;
	//SYS_TABLE->tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 0;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = VARCHARTYPE;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	dbp.init();
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_TABLE->totalRows
	sprintf(sysCol.columnName,"%s","totalRows");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 1;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	////SYS_TABLE->createTime
	/*
        sprintf(sysCol.columnName,"%s","createTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	//SYS_TABLE->updateTime
	sprintf(sysCol.columnName,"%s","updateTime");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.dataType = DATETYPE;
	sysCol.maxSize = DATE_SIZE;
	dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);*/
	//SYS_TABLE->startPage.
	
	sprintf(sysCol.columnName,"%s","startpage");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 2;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_TABLE->totalDP
	sprintf(sysCol.columnName,"%s","totalDP");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 3;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_TABLE->totalColumns
	sprintf(sysCol.columnName,"%s","totalColumns");
	sprintf(sysCol.tableName,"%s","SYS_TABLE");
	sysCol.colPos = 4;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.dataType = INTTYPE;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> COLUMN_NAME
	sprintf(sysCol.columnName,"%s","columnName");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_COLUMNNAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
	if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> TABLE_NAME
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> COL_POS
	sprintf(sysCol.columnName,"%s","colPos");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> dataType
	sprintf(sysCol.columnName,"%s","dataType");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 3;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_NULLABLE
	sprintf(sysCol.columnName,"%s","isNotNull");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 4;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_PRIMARY
	sprintf(sysCol.columnName,"%s","isPrimary");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 5;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> IS_UNIQUE
	sprintf(sysCol.columnName,"%s","isUnique");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 6;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_COLUMN -> IS_DELETED
	sprintf(sysCol.columnName,"%s","isDeleted");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 7;
	sysCol.dataType = BOOLTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = BOOL_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	//SYS_COLUMN -> MAX_SIZE
	sprintf(sysCol.columnName,"%s","maxSize");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 8;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	//SYS_COLUMN -> DEFAULT_VALUE
	sprintf(sysCol.columnName,"%s","defaultValue");
	sprintf(sysCol.tableName,"%s","SYS_COLUMN");
	sysCol.colPos = 9;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_DATATYPE_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	
	
	//SYS_INDEX -> indexName
	sprintf(sysCol.columnName,"%s","indexName");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 0;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> tableName
	sprintf(sysCol.columnName,"%s","tableName");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 1;
	sysCol.dataType = VARCHARTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = true;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = MAX_TABLENAME_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> colOrigPos
	sprintf(sysCol.columnName,"%s","colOrigPos");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 2;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> colIndexPos
	sprintf(sysCol.columnName,"%s","colIndexPos");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 3;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> startPage
	sprintf(sysCol.columnName,"%s","startPage");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 4;
	sysCol.dataType = INTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = true;
	sysCol.isDeleted = false;
	sysCol.maxSize = INT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	
	//SYS_INDEX -> totalIndexCols
	sprintf(sysCol.columnName,"%s","totalIndexCols");
	sprintf(sysCol.tableName,"%s","SYS_INDEX");
	sysCol.colPos = 5;
	sysCol.dataType = SHORTTYPE;
	sysCol.isNotNull = true;
	sysCol.isPrimary = false;
	sysCol.isUnique = false;
	sysCol.isDeleted = false;
	sysCol.maxSize = SHORT_SIZE;
	strcpy(sysCol.defaultvalue,null);	
        if(!convertSysColRecToRecord(sysCol, rec, recSize)){
		err = CONVERT_ERROR;
		goto ret;
	}
	//dbp.insertRecord((char *)&sysCol,sizeof(SysColumnRecord),rid);
	dbp.insertRecord(rec, recSize, rid);
	delete[] rec;
	
	de.pageNo = SYS_INDEX_START_PAGE_NUM + 2;
	de.freeSpace = dbp.getTotalFreeSpace();
	dirPage.insertDirectoryEntry(de);
	Buff.write(SYS_COL_START_PAGE_NUM,data,SYS_PRIORITY);
	Buff.write(de.pageNo,sysData,DB_PAGE_PRIORITY);
	
	Buff.shutdown();
	
	
        cout<<"Schema '"<<filename<<"' created successfully"<<endl;
        
        ret:
        delete[] filename;
        delete[] sysData;
        delete[] data;
	if(null != NULL){
		delete[] null;
	}

        
        DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
}

//==========================================================================================

bool DataManager::handleDropSchema(parsedQuery &pq){

	int err = NO_ERROR;	
        DM_FUNC_TRACE && cout << "DM.handleDropSchema()\n";
	
	if(!bm.deleteSchemaFile((char *)pq.getDatabase().c_str())){
		err = DROP_SCHEMA_ERROR;
		goto ret;
	}
	
	
        ret:
        
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
bool DataManager::getSysTabRecForTable(string tabName, SysTableRecord &sysrec, DBHeader &dbHeader, RID &rid, bool dispErr){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysTabRecForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysTableRecord systabrec;
	
	int i;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemTablePointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and check
		//memcpy((char*)&systabrec,allrecs[i].c_str(),recsizes[i]);
		if(!convertRecordToSysTabRec(systabrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}

		if(tabName.compare(systabrec.tableName) == 0){
			sysrec = systabrec;
			rid = allRIDs[i];
			goto ret;
		}
	}

	//no table found
	err = INVALIDTABLENAME;
	dispErr && cout << "Table '" << tabName << "' does not exist" << endl;

	ret:
	if(dbheadbuf != NULL)
		delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

bool DataManager::getSysColRecsForTable(string tabName,vector<SysColumnRecord> &sysColumns,DBHeader &dbHeader,int noOfColumns, vector<RID> &sysColRIDs){


	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysColRecsForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;
	
	//RID tempRID;

	bool swapped = false;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	
	SysColumnRecord syscolrec,syscolrec1, tempSysCol;
	//SysColumnRecord* tempSysCol = NULL; 
	
	int i=0, n=0;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	if(dbhead->getSystemTablePointer() != SYS_TAB_START_PAGE_NUM){
		err = INVALID;
		goto ret;
	}
	
	if(!getAllRecords(dbhead->getSystemColumnPointer(), allrecs, recsizes, allRIDs)){
		err = READ_ERROR;
		goto ret;
	}
	
	
	for(i=0; i<allrecs.size(); i++){
		//read the data into systabrec and print
		//memcpy((char*)&syscolrec,allrecs[i].c_str(),recsizes[i]);
		
		//tempSysCol = new SysColumnRecord;
		//if(!convertRecordToSysColRec((*tempSysCol), (char*)allrecs[i].c_str(), recsizes[i])){
		if(!convertRecordToSysColRec(tempSysCol, (char*)allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		//cout << endl;
		//syscolrec.dump();
		//if(tabName.compare((*tempSysCol).tableName) == 0){
		//	sysColumns.push_back((*tempSysCol));
		if(tabName.compare(tempSysCol.tableName) == 0){
			sysColumns.push_back(tempSysCol);
			sysColRIDs.push_back(allRIDs[i]);
		}
	}

	/*procedure bubbleSort( A : list of sortable items ) defined as:
  n := length( A )
  do
    swapped := false
    n := n - 1
    for each i in 0 to n  do:
      if A[ i ] > A[ i + 1 ] then
        swap( A[ i ], A[ i + 1 ] )
        swapped := true
      end if
    end for
  while swapped
end procedure

*/
        n = sysColumns.size();
	do{
		swapped = false;
		n = n-1;
		for(i = 0; i < n ; i++){
			syscolrec = sysColumns[i];
			syscolrec1 = sysColumns[i + 1];
			if(syscolrec.colPos > syscolrec1.colPos){				
				//swap			
				swap(sysColumns[i],sysColumns[i + 1]);				
				swap(sysColRIDs[i],sysColRIDs[i+1]);				
				swapped = true;
			}
		}
	}while(swapped);


	ret:
	delete[] dbheadbuf;
	if(dbhead != NULL){
		delete dbhead;
	}
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}
//=======================================================================

bool DataManager::getSysIndexRecsForTable(string tabName,vector<SysIndexRecord> &sysIndexRecs,DBHeader &dbHeader, vector<RID> &sysIndexRIDs){
	
	int err = NO_ERROR;
	DM_FUNC_TRACE && cout << "DM.getSysIndexRecsForTable()\n";
	
	vector<string> allrecs;
	vector<int> recsizes;
	vector<RID> allRIDs;

	SysIndexRecord sysindexrec;
	
	int i=0;

	if(!getAllRecords(dbHeader.getSystemIndexPointer(), allrecs, recsizes, allRIDs)){
		err = INVALID;
		goto ret;
	}
	
	for(i = 0; i<allrecs.size(); i++){
	
		//convert each rec to sysindex rec
		//check if table name is same.. then add to sysindexrecs
		if(!convertRecordToSysIndexRec(sysindexrec, (char*) allrecs[i].c_str(), recsizes[i])){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if(tabName.compare(to_string(sysindexrec.tableName)) == 0){
			//add sysindex rec
			sysIndexRecs.push_back(sysindexrec);
			sysIndexRIDs.push_back(allRIDs[i]);
		}
	
	}
	
	ret:
	allRIDs.clear();
	allrecs.clear();
	recsizes.clear();
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//===========================================================================================

//markAsFreePage()-This method makes the page,pageNo as freepage and adds it to the DBHeader freepage list.
bool DataManager::markAsFreePage(int pageNo, DBHeader& dbHeader){
	DM_FUNC_TRACE && cout << "DM.markAsFreePage()\n";
        int err = NO_ERROR;
	int pagePtr = dbHeader.getFreePagePointer();
	char *data = new char[PAGE_SIZE_IN_BYTES];
	FreePage freePage = FreePage(data);
	freePage.setNextFreePage(pagePtr);
	
	if(!bm.write(pageNo,data,FREE_PAGE_PRIORITY)){
		err = WRITE_ERROR;
		goto ret;
	}
	dbHeader.setFreePagePointer(pageNo);
	
	ret: 
        if(data != NULL){
	        delete[] data;
        }
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
	
}

