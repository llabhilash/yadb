//insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader)-This method inserts a record
//           into the database given a record ,its length ,start directory page and DBHeader instance.
//===========================================================================================

bool DataManager::insertRecordIntoDataPage(char *record,int reclen,int startPage,DBHeader & dbHeader ,RID &recRid){

	DM_FUNC_TRACE && cout << "DM.insertRecordIntoDataPage()\n";
	
	string dummy = "";

	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
	char buf_dirpage[PAGE_SIZE_IN_BYTES];
	char* dirpagebuf = &buf_dirpage[0];
        //char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char buf_datapage[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = &buf_datapage[0];
	//char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char buf_lastdirpage[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = &buf_lastdirpage[0];
	//char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	RID rid;
	int newPageNo;
      
      /*
	For all DP{
		check if reclen + SLOTSIZE >= MAXFREESPACE
			insert rec
			update maxfreespace
		
		check if new DE insertion is possible
			if not get next DP
		
		insert new DE
			insert rec
			update maxfreespace
	}
	
	refresh all DP maxfree space and last modified DE
	
	insert new DP, DE and DataPage
	insert rec
	update maxfreespace
      */
      
	//Try and insert into existing DE
	//DM_FUNC_TRACE && cout << "Try and insert into existing DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	do{
	
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		//check if insert is possible in current DP at the last modified DE..
		if(dirpage->getLatestFreeSpace() >= reclen + SLOT_DIR_SIZE){
			//read the DE and insert. and exit.
			dirpage->getLastModifiedDE(curDE);
			if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = DATABASEPAGE_READ_ERROR;
				goto ret;
			}
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			
			if(DM_FUNC_TRACE){
				//cout << "Before inserting record into DataBasePage" << endl;
				//datapage->dump();
				//cin >> dummy;
			}
			//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
			if(!datapage->insertRecord(record,reclen,rid)){
			        err = DATABASEPAGE_WRITE_ERROR;
			        goto ret;
			}
			//databasepage dump
			if(DM_FUNC_TRACE){
				cout << "After inserting record into DataBasePage" << endl;
				datapage->dump();
				//cin >> dummy;
			}
			curDE.freeSpace = datapage->getTotalFreeSpace();
			if(DM_FUNC_TRACE){ 
				//cout << "Before updating DE to DirPage\n";
				//dirpage->dump();
			}
			
			dirpage->updateDirectoryEntry(curDE);
			dirpage->setLatestFreeSpace(curDE.freeSpace);
			dirpage->setLastModifiedDE(curDE);
			
			if(!bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			if(!bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = WRITE_ERROR;
				goto ret;
			}
			
			if(DM_FUNC_TRACE){
				cout << "After updating DE to DirPage\n";
				dirpage->dump();
			}
			
			DM_FUNC_TRACE && cout << "Adding to existing DE, in DirPage : " << curDP << endl;
			DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
			
			//cin >> dummy;
			recRid.pageNo = curDE.pageNo;
			recRid.slotNo =rid.slotNo;
			goto ret;

		}
		
		//cannot insert into last modified DE.
		//check if this can DP can accomodate new DE.
		if(!dirpage->isAnyDEEmpty()){
			continue;
		}
		//create new DE.  and insert.......update latestfreespace and modified DE.
		if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
			err = FREEPAGE_READ_ERROR;
			goto ret;
		}
		
		curDE.pageNo = newPageNo;
		//dirpage->insertDirectoryEntry(curDE);
		RID rid;
		rid.pageNo = curDE.pageNo;
		if(datapage != NULL){
			delete datapage;
		}
		datapage = new DataBasePage(datapagebuf);
		datapage->init();
		
		if(!datapage->insertRecord(record,reclen,rid)){
		        err = DATABASEPAGE_WRITE_ERROR;
		        goto ret;
		}
	        curDE.freeSpace = datapage->getTotalFreeSpace();
	        
	        DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
		DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
	        
	        dirpage->insertDirectoryEntry(curDE);
	        dirpage->setLatestFreeSpace(curDE.freeSpace);
	        dirpage->setLastModifiedDE(curDE);
	        if(DM_FUNC_TRACE){
	        	//cout << "After updating DE to DirPage\n";
			//dirpage->dump();
		}
		
		if(!bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}
		if(!bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
			err = WRITE_ERROR;
			goto ret;
		}

		
		
		DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
		recRid.pageNo = curDE.pageNo;
		recRid.slotNo =rid.slotNo;
		//cin >> dummy;
		
		goto ret;
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//Cannot insert in any existing DEs in DPs..  
	//Refresh Maxfree space and latest modified DE in all DPs
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		dirpage->refreshMaxFreeSpace();
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	//insert new DP to end of chain
	
	//create a new DP and DE also refresh latestFreeSpace and last modified DE in other DPs
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	//get free page for new database page
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	
	if(!datapage->insertRecord(record,reclen,rid)){
	        err = DATABASEPAGE_WRITE_ERROR;
	        goto ret;
	}
	
	curDE.pageNo = newPageNo;
	rid.pageNo = curDE.pageNo;	
        curDE.freeSpace = datapage->getTotalFreeSpace();
        lastdirpage->insertDirectoryEntry(curDE);
        lastdirpage->setLatestFreeSpace(curDE.freeSpace);
        lastdirpage->setLastModifiedDE(curDE);
        
	if(!bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){	
	//write out current last page
		err = WRITE_ERROR;
		goto ret;
	}
	if(!bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY)){
	//write out new data page
		err = WRITE_ERROR;
		goto ret;
	}
	if(!bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){	
	//write out new last dir page
		err = WRITE_ERROR;
		goto ret;
	}
	
	
	DM_FUNC_TRACE && cout << "Adding new DirPage : " << dirpage->getNextDirectoryPage() << " , Current Last DirPage : "<< curDP << endl;
	DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
	DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
	DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
	//cin >> dummy;
	recRid.pageNo = curDE.pageNo;
	recRid.slotNo =rid.slotNo;
	
		
	ret:
	
	DM_FUNC_TRACE && printErrorCode(err);
	if(datapagebuf != NULL)
		//delete[] datapagebuf;
		datapagebuf = NULL;
	if(dirpagebuf != NULL)
		dirpagebuf = NULL;
		//delete[] dirpagebuf;//remove allocated memory
	if(lastdirpagebuf != NULL)
		lastdirpagebuf = NULL;
		//delete[] lastdirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(lastdirpage != NULL)
		delete lastdirpage;
	if(datapage != NULL)
		delete datapage;
	return err==NO_ERROR?true:false;
}


//===========================================================================================

bool DataManager::insertRecordsIntoTable(int startPage,DBHeader &  dbHeader,vector<RID> & recordRIDs, vector<int> types, vector<string*> recordStrings){


	DM_FUNC_TRACE && cout << "DM.insertRecordIntoDataPage()\n";
	DM_FUNC_TRACE && cout<<"No of records to insert:"<<recordStrings.size()<<endl;
	string dummy = "";
	
	int curDP = startPage;
	int err = NO_ERROR;
	DirectoryEntry curDE,nextDE;
        char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage *dirpage = NULL;
	DirectoryPage *lastdirpage = NULL;
	DataBasePage *datapage = NULL;
	char* record;
	int reclen;
	RID rid;
	int noOfRecs = recordStrings.size();
	int newPageNo ,i=0, j=0;
	int curRecord = 0;
	vector<string> attributes;
	string* recordStr;
	char* recordbuf;
	int bufsize;
	vector<char*> charRecords;
	vector<int> charRecSizes;
	
	for(i=0;i<recordStrings.size();i++){
		recordStr = (string*)recordStrings[i];
		for(j=0;j<types.size();j++)
			attributes.push_back(recordStr[j]);
		if(!pack::pack_data(attributes,recordbuf,bufsize,types)){
			err = CONVERT_ERROR;
			goto ret;
		}
		charRecords.push_back(recordbuf);
		charRecSizes.push_back(bufsize);	
	}
	
	/*for(i=0;i<records.size();i++){
		recAttributes.clear();
		if(!pack::unpack_data(recAttributes,types,records[i],recordSizes[i])){
			err = PACK_ERROR;
			goto ret;
		}
		cout<<"Unpacked "<<i+1<<"'records values are:";
		for(j=0;j<recAttributes.size();j++)
		cout<<"\t"<<recAttributes[j];
		cout<<endl;
	}*/
	
	record = (char*)charRecords[curRecord];
	reclen = charRecSizes[curRecord];
	
	/*for(i=0;i<recordSizes.size();i++)
		recordslen += recordSizes[i];
	*/
	//Try and insert into existing DE
	//DM_FUNC_TRACE && cout << "Try and insert into existing DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(!dirpage->getFirstDirectoryEntry(curDE)){
			continue;
		}
		
		nextDE = curDE;
		do{
			curDE = nextDE;
			//DM_FUNC_TRACE && cout << "Looking for free space in a DE in page : " << curDE.pageNo <<",next =  " << nextDE.pageNo << endl;
			//DM_FUNC_TRACE && cin >> dummy;			
			
			if(curDE.freeSpace >= (short)(reclen + SLOT_DIR_SIZE)){
			
				if(!bm.read(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = DATABASEPAGE_READ_ERROR;
					goto ret;
				}
				rid.pageNo = curDE.pageNo;
				if(datapage != NULL){
					delete datapage;
				}
				datapage = new DataBasePage(datapagebuf);
				
				if(DM_FUNC_TRACE){
					cout << "Before inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				//ERROR HERE WHEN INSERTING WITH NEW PACKING.CPP
				
				do{
				
					if(!datapage->insertRecord(record,reclen,rid)){
					        err = DATABASEPAGE_WRITE_ERROR;
					        goto ret;
					}
					curRecord++;
					rid.pageNo = curDE.pageNo;
					recordRIDs.push_back(rid);
					curDE.freeSpace = datapage->getTotalFreeSpace();						
					if(curRecord == noOfRecs)
						break;
					record = (char*)charRecords[curRecord];
					reclen = charRecSizes[curRecord];
					//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
										
				}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
				
				cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;	
				//databasepage dump
				if(DM_FUNC_TRACE){
					cout << "After inserting record into DataBasePage" << endl;
					datapage->dump();
					//cin >> dummy;
				}
				curDE.freeSpace = datapage->getTotalFreeSpace();
				if(DM_FUNC_TRACE){ 
					//cout << "Before updating DE to DirPage\n";
					//dirpage->dump();
				}
				
				dirpage->updateDirectoryEntry(curDE);
				
				bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
				bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);
				
				if(DM_FUNC_TRACE){
					cout << "After updating DE to DirPage\n";
					dirpage->dump();
				}
				
				DM_FUNC_TRACE && cout << "Adding to existing DE, in DirPage : " << curDP << endl;
				DM_FUNC_TRACE && cout << "Inserted record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
				
				//cin >> dummy;
				if(curRecord == noOfRecs)
					goto ret;
				
			}
		}while(dirpage->getNextDirectoryEntry(curDE,nextDE));
		
		
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//All DEs are full
	
	//Try and insert a new DE	
	DM_FUNC_TRACE && cout << "Try and insert new DE\n";
	//DM_FUNC_TRACE && cin >> dummy;
	
	curDP = startPage;
	
	insertNewDE:
	do{
	
	        //cout<<"ALL are out########################################"<<endl;
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		//Error in this scope when loop is reentered, to enter new DE into existing DP
		while(dirpage->isAnyDEEmpty()){
		
			//cin >> dummy;
			DM_FUNC_TRACE && cout << "Inserting new DE in to DirPage : " << curDP << endl;
			//DM_FUNC_TRACE && cin >> dummy;
		        
		        if(datapagebuf != NULL){
		        	delete[] datapagebuf;
		        }
			datapagebuf = new char[PAGE_SIZE_IN_BYTES];
			
			//Error here whn loop is reentered, free page returned is same as the previously returned page no.
			if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
				err = FREEPAGE_READ_ERROR;
				goto ret;
			}
			
			curDE.pageNo = newPageNo;
			//dirpage->insertDirectoryEntry(curDE);
			RID rid;
			rid.pageNo = curDE.pageNo;
			if(datapage != NULL){
				delete datapage;
			}
			datapage = new DataBasePage(datapagebuf);
			datapage->init();
			
			cout<<"trying to insert records to page:"<<newPageNo<<endl;
			do{
			
				if(!datapage->insertRecord(record,reclen,rid)){
				        err = DATABASEPAGE_WRITE_ERROR;
				        goto ret;
				}
				curRecord++;
				rid.pageNo = curDE.pageNo;
				recordRIDs.push_back(rid);
				curDE.freeSpace = datapage->getTotalFreeSpace();
				//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;				
				if(curRecord == noOfRecs)
					break;
				record = (char*)charRecords[curRecord];
				reclen = charRecSizes[curRecord];
				//DM_FUNC_TRACE && cout<<"FreeSpace of page:"<<curDE.pageNo<<" :"<<curDE.freeSpace<<" size of nxt record in page: "<<reclen+SLOT_DIR_SIZE<<endl;
			}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
			
			cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;
		        //dirpage->updateDirectoryEntry(curDE);
		        
		        DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
			DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "Before updating DE to DirPage\n";
				//dirpage->dump();
			}
		        
		        dirpage->insertDirectoryEntry(curDE);
		        
		        if(DM_FUNC_TRACE){
		        	//cout << "After updating DE to DirPage\n";
				//dirpage->dump();
			}
			
			bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);
			bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);

			//cin >> dummy;
			if(curRecord == noOfRecs)
					goto ret;			
			
		}
	}while((curDP = dirpage->getNextDirectoryPage()) != -1 );
	
	//ALL DEs in all DPs are full
	///creation of new DP
	
	//add a new DP to the end of the chain
	//Find the last DP in the chain
	curDP = startPage;
	do{
		if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = DIRPAGE_READ_ERROR;
			goto ret;
		}
		
		if(dirpage != NULL){
			delete dirpage;
		}
		dirpage = new DirectoryPage(dirpagebuf);
		
		if(dirpage->getNextDirectoryPage() != -1 )
			curDP = dirpage->getNextDirectoryPage();
		
	}while(dirpage->getNextDirectoryPage() != -1 );
	
	//dirpage has the last DP
	//curDP has the last DP page num
	//create a new DP, add link to it
		
	if(lastdirpagebuf != NULL){
		delete[] lastdirpagebuf;
	}
	lastdirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(lastdirpagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	if(lastdirpage != NULL){
		delete lastdirpage;
	}
	lastdirpage = new DirectoryPage(lastdirpagebuf);
	lastdirpage->init();
	//set lastdirpage as next page for the current last page => dirpage
	if(!dirpage->setNextDirectoryPage(newPageNo)){
		err = INVALID;
		goto ret;
	}
	//insert new DE into lastdirpage and a new databasepage into the DE with the record
	
	if(datapagebuf != NULL){
        	delete[] datapagebuf;
        }
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!getFreePage(datapagebuf, newPageNo,dbHeader)){
		err = FREEPAGE_READ_ERROR;
		goto ret;
	}
	
	curDE.pageNo = newPageNo;
	lastdirpage->insertDirectoryEntry(curDE);
	
	rid.pageNo = curDE.pageNo;
	if(datapage != NULL){
		delete datapage;
	}
	datapage = new DataBasePage(datapagebuf);
	datapage->init();
	DM_FUNC_TRACE && cout<<"trying to insert records to page:"<<newPageNo<<endl;
	do{
	
		if(!datapage->insertRecord(record,reclen,rid)){
		        err = DATABASEPAGE_WRITE_ERROR;
		        goto ret;
		}
		curRecord++;
		rid.pageNo = curDE.pageNo;
		recordRIDs.push_back(rid);
		curDE.freeSpace = datapage->getTotalFreeSpace();						
		if(curRecord == noOfRecs)
			break;
		record = (char*)charRecords[curRecord];
		reclen = charRecSizes[curRecord];
		//DM_FUNC_TRACE && cout << "\t Inserted "<<curRecord-1<<" record into page : " << curDE.pageNo << " , Slot : " << rid.slotNo << endl ;
		
	}while( (curRecord < noOfRecs) && (curDE.freeSpace >= (short)(reclen+SLOT_DIR_SIZE)));
	
	DM_FUNC_TRACE && cout<<"No of Records inserted:"<<curRecord<<" into page:"<<curDE.pageNo<<endl;
       dirpage->updateDirectoryEntry(curDE);
	bm.write(curDP,dirpagebuf,DIR_PAGE_PRIORITY);	//write out current last page
	bm.write(dirpage->getNextDirectoryPage(),lastdirpagebuf,DIR_PAGE_PRIORITY);	//write out new data page
	bm.write(curDE.pageNo,datapagebuf,DB_PAGE_PRIORITY);	//write out new last dir page
	
	DM_FUNC_TRACE && cout << "Adding new DirPage : " << dirpage->getNextDirectoryPage() << " , Current Last DirPage : "<< curDP << endl;
	DM_FUNC_TRACE && cout << "Adding to new DE, in DirPage : " << curDP << endl;
	DM_FUNC_TRACE && cout << "Adding New DataPage : " << curDE.pageNo << endl;

	if(curRecord != noOfRecs){
		curDP = dirpage->getNextDirectoryPage();
		goto insertNewDE;
	}	
	//cin >> dummy;
	//curDP now holds the Newly inserted last directory page num
	//and dirpage holds the directory page object pointing to this page.
/*	curDP = dirpage->getNextDirectoryPage();
	
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;
		
	dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	
	if(!bm.read(curDP,dirpagebuf,DIR_PAGE_PRIORITY)){
		err = DIRPAGE_READ_ERROR;
		goto ret;
	}
	
	if(dirpage != NULL){
		delete dirpage;
	}
	dirpage = new DirectoryPage(dirpagebuf);
*/	
	goto ret;
		
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	if(datapagebuf != NULL)
		delete[] datapagebuf;
	if(dirpagebuf != NULL)
		delete[] dirpagebuf;//remove allocated memory
	if(lastdirpagebuf != NULL)
		delete[] lastdirpagebuf;
	if(dirpage != NULL)
		delete dirpage;
	if(lastdirpage != NULL)
		delete lastdirpage;
	if(datapage != NULL)
		delete datapage;
	return err==NO_ERROR?true:false;
}

//===========================================================================================
//=======================================================================
bool DataManager::getAllRecords(int startDirPageNo, vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs){
	
	DM_FUNC_TRACE && cout << "DM.getAllRecords()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	char* recbuf = NULL;	
	int recSize;
	
	string* tempstring = NULL;
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID curRID,nextRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			//DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == -1){
				//empty DE - goto next DE
				continue;
			}
			
			//read page in DE into BufferPool
			if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
				err = READ_ERROR;
				goto ret;
			}
			
			if(DataPage != NULL){
				delete DataPage;
			}
			DataPage = new DataBasePage(datapagebuf);
			
			
			curRID.pageNo = curDataPageNo;
			//read first sys-col RID
			if(!DataPage->firstRecord(curRID)){
				//empty DataBasePage.. goto next page
				continue;
			}
			
			nextRID.pageNo = curRID.pageNo;
			nextRID.slotNo = curRID.slotNo;
			
			//read records
			do{			
				//RECORD LEVEL
				
				curRID.pageNo = nextRID.pageNo;
				curRID.slotNo = nextRID.slotNo;
				
				if(recbuf != NULL){
					delete[] recbuf;
				}
				
				if(!DataPage->getRecord(curRID,recbuf,recSize)){
					//read error
					err = RECORD_READ_ERROR;
					goto ret;
				}			
				
				//memcopy buf into systabrec
				//memcpy((char*)&syscolrec,recbuf,recSize);
				
				//add record to allrecs
				if(tempstring != NULL)
					delete tempstring;
				tempstring = new string(recbuf,recSize);
				allrecs.push_back((*tempstring));				
				recsizes.push_back(recSize);
				allRIDs.push_back(curRID);
			
			}while(DataPage->nextRecord(curRID, nextRID));

		
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(recbuf != NULL){
		delete[] recbuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
	if(tempstring != NULL)
		delete tempstring;	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//=======================================================================
//===========================================================================================================


//deleteRecord()-This method deletes the record in the datapage given its RID.Also it updates corresponding
//                DE entry inthe Directory Page.
bool DataManager::deleteRecord(const int &startDirPageNo ,const RID &rid,DBHeader &dbHeader){
        DM_FUNC_TRACE && cout << "DM.deleteRecord()\n";
	int err = NO_ERROR;
	
	DirectoryEntry curDE,nextDE;
	char* dirpagebuf = new char[PAGE_SIZE_IN_BYTES];
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DirectoryPage* DirPage = NULL;
	DataBasePage* DataPage = NULL;	
	
	int curDirPageNo;
	int curDataPageNo;
	
	RID tempRID;
	
	curDirPageNo = startDirPageNo;
	
	do{
		
		//DIRECTORY PAGE LEVEL
	
		if(!bm.read(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
			err = READ_ERROR;
			goto ret;
		}		
		
		if(DirPage != NULL){
			delete DirPage;
		}
		DirPage = new DirectoryPage(dirpagebuf);
		
		//read first DE
		if(!DirPage->getFirstDirectoryEntry(curDE)){
			//empty DP - goto next DP
			continue;
		}
		
		nextDE.pageNo = curDE.pageNo;
		nextDE.freeSpace = curDE.freeSpace;
		
		do{
		
			curDE.pageNo = nextDE.pageNo;
			curDE.freeSpace = nextDE.freeSpace;
			
			DM_FUNC_TRACE && cout << "Trying to read from DE, DataPage : " << curDE.pageNo << endl;
			//DirectoryEntry-DataBasePage LEVEL
		
			//read the page in the DE
			curDataPageNo = curDE.pageNo;
			if(curDataPageNo == rid.pageNo){
				//page corresponding to rid was found.
				//delete the record in that page and updata its corresponding DE value..
				
				//read page in DE into BufferPool
				if(!bm.read(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
					err = READ_ERROR;
					goto ret;
				}
				
				if(DataPage != NULL){
					delete DataPage;
				}
				DataPage = new DataBasePage(datapagebuf);
				DataPage->deleteRecord(rid);
				//check if there are any valid records.
				if(!DataPage->firstRecord(tempRID)){
					if(!markAsFreePage(curDataPageNo,dbHeader)){
						err = FREEPAGE_READ_ERROR;
						goto ret;
					}
					DirPage->updateDirectoryEntry(curDE,true);
					//goto ret;
				}else{
					//update DE in Directory Page.
					curDE.freeSpace = DataPage->getTotalFreeSpace();
					DirPage->updateDirectoryEntry(curDE,false);
					if(!bm.write(curDataPageNo,datapagebuf,DB_PAGE_PRIORITY)){
						err = WRITE_ERROR;
						goto ret;
					}
				}
				if(!bm.write(curDirPageNo,dirpagebuf,DIR_PAGE_PRIORITY)){
					err = WRITE_ERROR;
					goto ret;
				}
				goto ret;	
			}
			
		}while( DirPage->getNextDirectoryEntry(curDE, nextDE) );
		
	}while((curDirPageNo = DirPage->getNextDirectoryPage()) != -1);
	
	ret:
	if(dirpagebuf != NULL){
		delete[] dirpagebuf;
	}
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(DirPage != NULL){
		delete DirPage;
	}
	if(DataPage != NULL){
		delete DataPage;
	}
		
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//================================================================================================


bool DataManager::updateRecordFromDataPage(char* rec, int reclen, RID &rid, int startPage, DBHeader & dbHeader){
	int err = NO_ERROR;
        DM_FUNC_TRACE && cout << "DM.updateRecordFromDataPage()" << endl;
	char* datapagebuf = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage* DataPage = NULL;
	
	//cout<<"Trying to read pageno: "<<rid.pageNo<<endl;
	if(!bm.read(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_READ_ERROR;
		goto ret;
	}	

	DataPage = new DataBasePage(datapagebuf);
	
	if(DM_FUNC_TRACE){
		//cout<<"Before updating pageno: "<<rid.pageNo<<endl;
		//DataPage->dump();
		//cin >> dummy;
	}
	
	if(!DataPage->updateRecord(rec,reclen,rid))
	{
		//update not possible, then delete old record and insert new record
			DM_FUNC_TRACE && cout<<"Update not possible, possibly because size(old_rec) != size(update_rec)"<<endl;
			//cout<<"Update not possible, possibly because size(old_rec) != size(update_rec)"<<endl;
			//delete old record
			if(!deleteRecord(startPage,rid,dbHeader))
			{
				//DM's delete Record Error
				err = DATAMANAGER_DELETE_RECORD_ERROR;
				goto ret;
			}
			
			//old record deleted, now insert new record
			DM_FUNC_TRACE && cout<<"New Rec Size : " << reclen <<endl;
			if(!insertRecordIntoDataPage(rec,reclen,startPage,dbHeader,rid))
			{
				//DM's insert record into DataPage error
				err = DATAMANAGER_INSERT_RECORD_INTO_DATAPAGE_ERROR;
				goto ret;
			}

			//success.. 
			goto ret;
	}//updated record

	//record updated in the same page
	if(!bm.write(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_WRITE_ERROR;
		goto ret;
	}

	/*
	if(datapagebuf != NULL)
		delete[] datapagebuf;
		
	datapagebuf = new char[PAGE_SIZE_IN_BYTES];

	if(!bm.read(rid.pageNo,datapagebuf,DB_PAGE_PRIORITY)){
		err = DATABASEPAGE_READ_ERROR;
		goto ret;
	}	

	DataPage = new DataBasePage(datapagebuf);
	*/
	/*
	if(DM_FUNC_TRACE){
		cout<<"After updating pageno: "<<rid.pageNo<<endl;
		DataPage->dump();
		//cin >> dummy;
	}
	*/				
	ret:
	if(datapagebuf != NULL){
		delete[] datapagebuf;
	}
	if(DataPage != NULL)
		delete DataPage;
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;
}

//======================================================================= 


bool DataManager::handleAddRecordes(parsedQuery &pq){
	
	DM_FUNC_TRACE && cout << "DM.handleAddRecods()\n";
	int err = NO_ERROR;
	
	int i =0,j=0;
	
	cout<<"Table: "<<pq.getTable()<<endl;
	cout<<"No of Records to Add: "<<pq.getNoOfRecords()<<endl;
	cout<<"Record Type: ";
	if(pq.getRecordType() == DEFAULT)
		cout<<"DEFAULT"<<endl;
	else if(pq.getRecordType() == RANGE){
		cout<<"RANGE"<<endl;
		cout<<"Start of Range: "<<pq.getStartRange()<<endl;
	}	
	else
		cout<<"RANDOM"<<endl;

	parsedQuery* pq_template = new parsedQuery();
	pq_template->setAction(INSERT);
	pq_template->insert = true;
	pq_template->setTable(pq.getTable());
	//pq_template->setTypes(columnTypes);
	//pq_template->setDataValues(datavalues);
	//pq_template->setMaxSize(maxsizes);
	//pq_template->noOfColumns = noOfColumns;
	
	char* dbheadbuf = new char[PAGE_SIZE_IN_BYTES];
	DBHeader* dbhead = NULL;
	SysTableRecord sysTabRec;
	RID rid;
	vector<SysColumnRecord> sysColRecs;
	vector<RID> sysColRIDs;
	char* record;
	int recSize = 0;
	vector<int> types;
	vector<int> sizes;
	
	//read the dbheader
	if(!bm.read(DB_HEADER_PAGE_NUM,dbheadbuf,DB_HEADER_PRIORITY)){
		err = READ_ERROR;
		goto ret;
	}
	
	//initialize dbheader
	dbhead = new DBHeader(dbheadbuf);
	
	//get sys tab record for the table
	if(!getSysTabRecForTable(pq.getTable(), sysTabRec, (*dbhead), rid)){
		//No Such Table
		err = INVALID;
		goto ret;
	}
	
	//cout<<"Getting SysColsRecords"<<endl;
	//get sys cols
	if(!getSysColRecsForTable(pq.getTable(), sysColRecs, (*dbhead), sysTabRec.totalColumns, sysColRIDs)){
		//Column Read Error
		err = INVALID;
		goto ret;
	}
	
	types.clear();
	for(i=0;i<sysColRecs.size();i++){
		types.push_back(sysColRecs[i].dataType);
		sizes.push_back(getTypeSize(sysColRecs[i].dataType));
	}
		
	pq_template->setTypes(types);
	pq_template->setMaxSize(sizes);
	
	ret:
	if(dbheadbuf != NULL){
		delete[] dbheadbuf;
	}
	
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;	
	
}

//======================================================================= 

bool DataManager::getSuitableIndex(vector<SysColumnRecord> sysColRecs,vector<SysIndexRecord> sysIndexRecs, vector<string> condition_array, vector<string> &keyValues, vector<int> &opr, vector<int> &indexPos, string &indexName ){

	indexName = "NULL";
	DM_FUNC_TRACE && cout << "DM.getSuitableIndex()\n";
	int err = NO_ERROR;

	char* record;
	int recSize = 0;
	int i=0,j=0,k=0,tmp = 0;
	string colname;
	vector<int> indexColsPos;
	vector<int> types;
	vector<int> sizes;
	typedef map<string, vector<int> > IndexMap; // Index Name => Sys Col Positon <vector>
	IndexMap indexMap;
	IndexMap::iterator iter = indexMap.begin();
	vector<int> indexVal;
	string tempName;
	int maxSize = 1;
	int maxIndexSize = 1;
	bool columnNotFound = false;
	double dval1;
	//cout<<"Condition array size: "<<condition_array.size()<<endl;
	
	for(i=0;i<condition_array.size();i++){
		columnNotFound = true;
		colname = condition_array[i];
		if(colname.compare("OR") == 0){
			goto ret;
		}
		if(isOperator(colname)){
			continue;
		}
		if( colname[0] == '\'' && colname[colname.size()-1] == '\''){
			continue;
		}
		if( colname[0] == '"' && colname[colname.size()-1] == '"'){
			continue;
		}
		if( isNumber(colname, dval1) ){
			continue;
		}
		if(condition_array[i+2].compare("=") != 0 && condition_array[i+2].compare("<") != 0 && condition_array[i+2].compare(">") != 0 && condition_array[i+2].compare(">=") != 0 && condition_array[i+2].compare("<=") != 0 ){
					//PARSER_FUNC_TRACE && cout<<condition_array[i+2]<<" ==> Operator not \"=\" "<<endl;
					i+=2;
					continue;
		}
		for(k=0;k<sysColRecs.size();k++)
			if(colname.compare(sysColRecs[k].columnName) == 0 && !sysColRecs[k].isDeleted){
				columnNotFound = false;
		}
		for(j=0;j<sysIndexRecs.size();j++){
			if(colname.compare((sysColRecs[sysIndexRecs[j].colOrigPos].columnName)) == 0){
				//DM_FUNC_TRACE && cout<<"Found Name in sys_index: " <<colname<<endl;
				iter = indexMap.find(sysIndexRecs[j].indexName);
				if(iter != indexMap.end() ){
					//DM_FUNC_TRACE && cout<<"index already inserted into indexMap, name "<<sysIndexRecs[j].indexName<<endl;
					PARSER_FUNC_TRACE && cout<<"Adding to pos: "<<sysIndexRecs[j].colIndexPos<<" value: "<<sysIndexRecs[j].colOrigPos <<"in Index: "<<sysIndexRecs[j].indexName<<endl;
					vector<int> ColPosIndex = (vector<int>)iter->second;
					ColPosIndex[sysIndexRecs[j].colIndexPos] = sysIndexRecs[j].colOrigPos;
					iter->second = (vector<int>)ColPosIndex;
				}
				else{
					//DM_FUNC_TRACE && cout<<"index does not exist, create new index entry and add col pos"<<endl;
					vector<int> ColPosIndex;
					ColPosIndex.clear();
					for(k=0;k<sysIndexRecs[j].totalIndexCols;k++)
						ColPosIndex.push_back(-1);
					ColPosIndex[sysIndexRecs[j].colIndexPos] = sysIndexRecs[j].colOrigPos;
					indexMap.insert(make_pair(sysIndexRecs[j].indexName,ColPosIndex));
					PARSER_FUNC_TRACE && cout<<"created new index name: "<<sysIndexRecs[j].indexName<<endl;
					PARSER_FUNC_TRACE && cout<<"Adding to pos: "<<sysIndexRecs[j].colIndexPos<<" value: "<< sysIndexRecs[j].colOrigPos<<endl;
				}

				for(k=0;k<sysColRecs.size();k++)
					if(condition_array[i+1].compare(sysColRecs[k].columnName) == 0){
						columnNotFound = false;
						PARSER_FUNC_TRACE && cout<<condition_array[i+1]<<" ==> operand is column name"<<endl;
						goto ret;
					}
			}
			if( columnNotFound ){
				cout<<"Column "<<colname<<" does not exist."<<endl;
				err = INVALID;
				goto ret;
			}
		}
	}
	
	//Display the Index Map	
	iter = indexMap.begin();
	//cout<<"Map Size: "<<indexMap.size()<<endl;
	for(iter; iter!= indexMap.end(); iter++){
		vector<int> ColPosIndex = (vector<int>)iter->second;
		//if(ColPosIndex[0] == -1)
		//	break;
		if(ColPosIndex[0] != -1){
			PARSER_FUNC_TRACE && cout<<"Index name: "<<iter->first<<endl;
			PARSER_FUNC_TRACE && cout<<"Values: "<<ColPosIndex[0]<<"\t";
			//find the no of continuious index values	
			for(i=1,j=1;i<ColPosIndex.size();i++){
				PARSER_FUNC_TRACE && cout<<ColPosIndex[i]<<endl;
				if(ColPosIndex[i] == -1)
					break;
				j++;	
			}
			if( j >= maxSize){
				maxSize = j;
				indexName = iter->first;
				indexVal = iter->second;
				maxIndexSize = ColPosIndex.size();
			}
			PARSER_FUNC_TRACE && cout<<endl;
		 }
	}
	
	//Find among the current Max found index cols from user query, the min sized Index
	iter = indexMap.begin();
	for(iter; iter!= indexMap.end(); iter++){
		vector<int> ColPosIndex = (vector<int>)iter->second;
		if(ColPosIndex[0] == -1)
			break;
		//cout<<"Index name: "<<iter->first<<endl;
		//cout<<"Values: "<<ColPosIndex[0]<<"\t";
		//find the no of continuious index values	
		for(i=1,j=1;i<ColPosIndex.size();i++){
			//cout<<ColPosIndex[i]<<endl;
			if(ColPosIndex[i] == -1)
				break;
			j++;	
		}
		if( j == maxSize && ColPosIndex.size() < maxIndexSize ){
			indexName = iter->first;
			indexVal = iter->second;
			maxIndexSize = ColPosIndex.size();
		}
		//cout<<endl;
	}
	
	
	PARSER_FUNC_TRACE && cout<<"\nMax Index Name: "<<indexName<<endl;
	//cout<<endl;
	keyValues.clear();k=0;
	//keyValues = vector<string> (maxSize,"-1");
	for(i=0;i<indexVal.size();i++){
		if(indexVal[i] == -1)
			break;
		tempName = sysColRecs[indexVal[i]].columnName;
		for(j=0;j<condition_array.size();j++){
	//		DM_FUNC_TRACE && cout<<"Getting col : "<<tempName<<endl;
			if(tempName.compare(condition_array[j]) == 0){
	//			DM_FUNC_TRACE && cout<<"Found match: "<<tempName<<endl;
				if(j+1<condition_array.size()){
					//cout<<"condition array index: "<<j+2<<endl;
					//keyValues[i] = condition_array[j+1];
	//				DM_FUNC_TRACE && cout<<"key value added: "<<condition_array[j+1]<<endl;
					string tmp = condition_array[j+1];
					if(tmp[0] == '"' && tmp[tmp.size()-1] == '"'){
						tmp.erase(0,1);
						tmp.erase(tmp.size()-1,1);
					}
					else if(tmp[0] == '\'' && tmp[tmp.size()-1] == '\''){
						tmp.erase(0,1);
						tmp.erase(tmp.size()-1,1);
					}
					keyValues.push_back(tmp);
					if(condition_array[j+2].compare("=")==0){
	//					DM_FUNC_TRACE && cout<<"Opr: + addded"<<endl;
						opr.push_back(EQUALS);
					}
					else if(condition_array[j+2].compare(">")==0){
	//					DM_FUNC_TRACE && cout<<"Opr: > addded"<<endl;
						opr.push_back(GREATER);
					}
					else if(condition_array[j+2].compare("<")==0){
	//					DM_FUNC_TRACE && cout<<"Opr: < addded"<<endl;
						opr.push_back(LESSER);
					}
					else if(condition_array[j+2].compare(">=")==0){
	//					DM_FUNC_TRACE && cout<<"Opr: >= addded"<<endl;
						opr.push_back(GREATERTHANEQUAL);
					}
					else if(condition_array[j+2].compare("<=")==0){
	//					DM_FUNC_TRACE && cout<<"Opr: <= addded"<<endl;
						opr.push_back(LESSTHANEQUAL);
					}
	//				DM_FUNC_TRACE && cout<<"Index Position added: "<<i<<endl;
					indexPos.push_back(i);
					j+=1;
					//break;
				}
			}
		}
	}
	
	//DM_FUNC_TRACE && cout<<"Sorting on index positions, indexPos: "<< indexPos.size()<<endl;
	
	//sort indexPos and give same positions to vectors - opr, keyValues
	if(indexPos.size()!=0){
		for(i=0;i<indexPos.size()-1;i++){
			for(j=i;j<indexPos.size()-1;j++)
				if(indexPos[j]>indexPos[j+1]){
					//swapping indexPos[j] and indexPos[j+1]
					tmp = indexPos[j];
					indexPos[j] = indexPos[j+1];
					indexPos[j+1] = tmp;
					
					//swapping opr[j] and opr[j+1]
					tmp = opr[j];
					opr[j] = opr[j+1];
					opr[j+1] = tmp;
					
					//swapping keyValues[j] and keyValues[j+1]
					tempName = keyValues[j];
					keyValues[j] = keyValues[j+1];
					keyValues[j+1] = tempName;
				}
		}
	}
	//DM_FUNC_TRACE && cout<<"Sorting complete"<<endl;
	//DM_FUNC_TRACE && cout<<"Sending Index  positions , Operators  and  Values "<<endl;
	//DM_FUNC_TRACE && for(i=0;i<indexPos.size();i++){
	for(i=0;i<indexPos.size();i++){
		PARSER_FUNC_TRACE && cout<<"IndexPos: "<<indexPos[i]<<" Operator: "<<opr[i]<<" Value: "<<keyValues[i]<<endl;
	}
	
	ret:
	DM_FUNC_TRACE && printErrorCode(err);
	return err==NO_ERROR?true:false;			
}

//======================================================================= 


