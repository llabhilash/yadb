#ifndef TYPESTUDY
#define TYPESTUDY
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "math.h"
#include "errno.h"
#include "limits.h"
#include "float.h"
#include "malloc.h"
#include	"../Common/DataType.h"
#define True 1
#define False 0
#define Decimal 2

typedef union tagMyUnion
{
   short TShort_ ; int TInt_; long TLong_ ; double TFloat_ ; double TDouble_ ;
} MyUnion ;

typedef enum tagMyEnum
{
   TShort, TInt, TLong, TFloat, TDouble, TNaN
} MyEnum ;

extern int *flag;

void whatIsTheValue(const char * string_, MyEnum * enum_, MyUnion * union_)
{
   char * endptr ;
   long lValue ;
   double dValue ;
   int flag = 0;
   
   *enum_ = TNaN ;

   /* integer value */
   flag = 0;
   lValue = strtol(string_, &endptr, 10) ;

   if(*endptr == 0) /* It is an integer value ! */
   {
      if((lValue >= SHRT_MIN) && (lValue <= SHRT_MAX)) /* is it a short ? */
      {
         *enum_ = TShort ;
         union_->TShort_ = (short) lValue ;
      }
      else if((lValue >= INT_MIN) && (lValue <= INT_MAX))
      {
      	*enum_ = TInt;
      	union_->TInt_ = (int) lValue;
      }	
      else if((lValue >= LONG_MIN) && (lValue <= LONG_MAX)) /* is it a long ? */
      {
         *enum_ = TLong ;
         union_->TLong_ = (long) lValue ;
      }

      return ;
   }

   /* real value */
   dValue = strtod(string_, &endptr) ;
		
   if(errno == ERANGE){
   	//printf("Exceeds capacity of float\n");
     *enum_ = TDouble ;
     union_->TDouble_ = (double) dValue ;
   } 
   else if(*endptr == 0) /* It is an real value ! */
   {
   	//printf("FLT_MAX = %f\n",FLT_MAX);
   	//printf("DBL_MAX = %f\n",DBL_MAX);
   	if((dValue >= -FLT_MAX) && (dValue <= FLT_MAX))
   	 {
   	 	*enum_ = TFloat ;
         union_->TFloat_ = (double) dValue ;
         if(union_->TFloat_ == HUGE_VAL || union_->TFloat_ == -HUGE_VAL || union_->TFloat_ == 0.0)
         flag = 1;
      } 
      /*else if((dValue >= -DBL_MAX) && (dValue <= DBL_MAX))
      {
         *enum_ = TDouble ;
         union_->TDouble_ = (double) dValue ;
      }
      */
      if(flag){
         *enum_ = TDouble ;
         union_->TDouble_ = (double) dValue ;      
      }

      return ;
   }
   errno = 0;	
   return ;
}

int studyValue(const char * string_)
{
   MyEnum enum_ ;
   MyUnion union_ ;

   whatIsTheValue(string_, &enum_, &union_) ;

   switch(enum_)
   {
      case TShort   : return SHORTTYPE;
      case TInt     : return INTTYPE;
      case TLong    : return LONGTYPE;
      case TFloat	: return FLOATTYPE;
      case TDouble  : return DOUBLETYPE;
   }
   return 0;
}

int checkIsNumberAndDecimal( char* cstr ){
	int i=0;
	int size = strlen(cstr);
	int success = False;
	int decimal = False;
	while( i < size ){
		if( isdigit(cstr[i])  ) i++;
		else if( cstr[i] == '.' && decimal == False) { i++; decimal = True; }
		else break;
	}
	if( i != size )	success = False;
	else if ( i == size && decimal == False) success = True;
	else if ( i == size && decimal == True ) success = Decimal;
	
	return success;
}

int getTypeFromValue(const char* cstr){
	int i=0,j=0;
	double dval;
	int type;
	int numberType;
	
	if( (numberType = checkIsNumberAndDecimal((char*)cstr)) == Decimal ){
			//can be a float or double, as it has a decimal point
			type = studyValue( (char*)cstr );
			switch( type ) {
				case SHORTTYPE : 	type = FLOATTYPE;
									break;
				case INTTYPE		:	type = FLOATTYPE;	
									break;
				case LONGTYPE	:	type = FLOATTYPE;								
									break;										
			}
		}
	else if( numberType == True){
			//can be an int, short, or long
			type = studyValue( (char*)cstr );
			switch( type ){
				case FLOATTYPE	:	type = INTTYPE;
									break;
			}
		}
	else{
		//can be a string ie VARCHARTYPE, as it is not a valid number
		type = VARCHARTYPE;	
	}
	return type;
}

int roundToType( int toType, const char* value, char* retValue){
	int i=0;
	int fromType = getTypeFromValue( value );
	int ival = 0, sval = 0;
	float fval = 0;
	double dval = 0;
	int success = True;
	//printf("From type: %d to type: %d\n",fromType,toType);
	
	switch(fromType){
						case SHORTTYPE	:	switch( toType ){
												// short can be stored in all the below types 
												case INTTYPE		:	
												case LONGTYPE	:
												case FLOATTYPE	:
												case DOUBLETYPE:
												case VARCHARTYPE	:
																	break;
												/*case VARCHARTYPE	:	retValue = (char*)malloc(sizeof(char)*strlen(value));
																	strcpy(retValue,value);
																	printf("Return Value: %s\n",retValue);*/
												default			:	printf("The type of the computed value does not match the column type\n");
																	success = False;
																	break;
											}
											break;
						case INTTYPE		:	switch(toType){
												case SHORTTYPE	:	sval = atoi( value );
																	sval = sval % SHRT_MAX;
																	retValue = (char*)malloc(sizeof(char)*8);
																	sprintf(retValue,"%d",sval);
																	break;
												case DATETYPE	:					
												case LONGTYPE	:
												case FLOATTYPE	:
												case DOUBLETYPE:
												case VARCHARTYPE	:
																	break;
												default			:	printf("The type of the computed value does not match the column type\n");
																	success = False;
																	break;
											}
											break;
						case LONGTYPE	:	switch(toType){
												case SHORTTYPE	:	sval = atoi( value );
																	sval = sval % SHRT_MAX;
																	retValue = (char*)malloc(sizeof(char)*8);
																	sprintf(retValue,"%d",sval);
																	break;
												case DATETYPE	:
												case INTTYPE		:
												case FLOATTYPE	:
												case DOUBLETYPE:
												case VARCHARTYPE	:
																	break;
												default			:	printf("The type of the computed value does not match the column type\n");
																	success = False;
																	break;
											}									
											break;
						case FLOATTYPE	:	switch(toType){
												case SHORTTYPE	:	fval = (float)atof( value );
																	sval = (int)floor(fval);
																	sval = sval % SHRT_MAX;
																	retValue = (char*)malloc(sizeof(char)*10);
																	sprintf(retValue,"%d",sval);
																	break;
												case DATETYPE	:					
												case INTTYPE		:	
												case LONGTYPE	:	fval = (float)atof( value );
																	ival = (int)floor(fval);
																	ival = ival % INT_MAX;
																	retValue = (char*)malloc(sizeof(char)*10);
																	sprintf(retValue,"%d",ival);
																	break;
												case DOUBLETYPE:
												case VARCHARTYPE	:	break;
												default			:	printf("The type of the computed value does not match the column type\n");
																	success = False;
																	break;
											}
											break;
						case DOUBLETYPE:	switch(toType){
												case SHORTTYPE	:	dval = (double)strtod( value, NULL );
																	sval = (int)floor(dval);
																	sval = sval % SHRT_MAX;
																	retValue = (char*)malloc(sizeof(char)*12);
																	sprintf(retValue,"%d",sval);
																	break;
												case DATETYPE	:
												case INTTYPE		:
												case LONGTYPE	:	dval = (double)strtod( value, NULL );
																	ival = (int)floor(dval);
																	ival = ival % INT_MAX;
																	retValue = (char*)malloc(sizeof(char)*12);
																	sprintf(retValue,"%d",ival);
																	break;
												case FLOATTYPE	:	dval = (float)strtod( value, NULL );
																	fval = (float)dval;
																	retValue = (char*)malloc(sizeof(char)*12);
																	sprintf(retValue,"%f",fval);
																	break;
												case VARCHARTYPE	:	break;					
												default			:	printf("The type of the computed value does not match the column type\n");
																	success = False;
																	break;
											}
											break;						
						default			:	break;
	}
	return success;					
}

#endif
