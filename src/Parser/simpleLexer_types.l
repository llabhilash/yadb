%{
	
	#undef YY_INPUT
	#define YY_INPUT(b,r,ms) (r = my_yyinput(b,ms))


       #include "simpleSQL.hh"
       #include "string.h"
	#include "stdio.h"
       #include "stdlib.h"
	#include "malloc.h"
	#include "../Common/DataType.h"
	#include "TypeStudy.h"
     void yyerror(char *);
	int ch;
     
// extern char myinput[];
 extern char *myinputptr; /* current poistion in myinput */
 extern int *myinputlim;  /* end of data */
 extern int *mypos;
 
 int i=0,j=0;
 char* cstr = NULL;
 char prev;
 char true_val[] = "1";
 char false_val[] = "0";
 //extern int yy_flex_debug;
 //yy_flex_debug = 0;
 
 int my_yyinput(char *buf, int max_size)
 {
 
 	int n = (max_size>*myinputlim-*mypos)?*myinputlim-*mypos:max_size;
 	
 	if(n > 0) {
 		memcpy(buf , myinputptr, n);
 		*mypos += n;
 		myinputptr += n;
 	}	
 	
 	return n;
 }


%}
%s INSQUOTES INDQUOTES

COMPARISION "="|"<="|">="|"<"|">"|"!="

%%

<INSQUOTES>[a-zA-Z0-9]([a-zA-Z0-9_\-\+,\.\"@;\*\?\(\)%]*[ \t]*(\\')*)*	{ 
												   //printf("Matched Inquotes Word %s\n",yytext);
												   i=0;
												   while(yytext[i] != '\0') i++;
												   cstr = (char*)malloc(i*sizeof(char));
												   prev = yytext[0];
												   i=0;j=0;
												   while(yytext[i] != '\0'){
												   	if(yytext[i] == '\'' && prev == '\\'){
												   		cstr[j-1] = '\'';
												   		i++;
												   		continue;
												   	}
												   	prev = yytext[i];
												   	cstr[j] = yytext[i];
												   	i++;	j++;
												   }
												   cstr[j] = '\0';
												   //printf("After removing sequence: %s\n",cstr);
												   yylval.str = strdup(cstr);
												   free (cstr);
												   return WORD; 
												   }
<INDQUOTES>[a-zA-Z0-9]([a-zA-Z0-9_\-\+,\.\*'@;%\?\(\)]*[ \t]*(\\\")*)*	{ 
													   i=0;
													   while(yytext[i] != '\0') i++;
													   cstr = (char*)malloc(i*sizeof(char));
													   prev = yytext[0];
													   i=0;j=0;
													   while(yytext[i] != '\0'){
													   	if(yytext[i] == '"' && prev == '\\'){
													   		cstr[j-1] = '"';
													   		i++;
													   		continue;
													   	}
													   	prev = yytext[i];
													   	cstr[j] = yytext[i];
													   	i++;	j++;
													   }
													   cstr[j] = '\0';
													   //printf("After removing sequence: %s\n",cstr);
													   yylval.str = strdup(cstr);
													   free (cstr);
													   return WORD; 
													   }
<INSQUOTES>"'"				{ yylval.str = strdup(yytext);BEGIN(INITIAL); return SQUOTE; }
<INDQUOTES>"\""				{ yylval.str = strdup(yytext);BEGIN(INITIAL); return DQUOTE; }

[ \t]+       ;       /* skip whitespace */

create	|
CREATE	{ return CREATEX; }
drop	|
DROP	{ return DROPX; }
USE	|
use	{ return USEX; }
SHOW	|
show	{ return SHOWX; }
INSERT	|
insert	{ return INSERTX; }
SELECT	|
select	{ return SELECTX; }
schemas	|
SCHEMAS	|
databases	|
DATABASES	{ return DATABASESX; }
schema	|
SCHEMA	|
database	|
DATABASE 	{ return DATABASEX; }
TABLES	|
tables	{ return TABLES; }
COLUMNS	|
columns	{ return COLUMNSX; }
INDICES	|
indices	{ return INDICESX; }
TABLE	|
table	{ return TABLE; }
INTO	|
into	{ return INTO; }
VALUES	|
values	{ return VALUES; }
FROM	|
from	{ return FROM; }
WHERE	|
where	{ return WHERE; }
OF	|
of	{ return OF; }
LIKE	|
like	{ yylval.str = strdup("LIKE");return LIKE; }
{COMPARISION}	{ yylval.str = strdup(yytext);return COMPARISION; }
AND	|
and	{ yylval.str = strdup("AND");return AND; }
OR	|
or	{ yylval.str = strdup("OR");return OR; }
NOT	|
not	{ yylval.str = strdup("NOT");return NOT; }
PRIMARY	|
primary	{ yylval.str = strdup(yytext);return PRIMARY; }
KEY	|
key	{ yylval.str = strdup(yytext);return KEY; }
BETWEEN	|
between	{ yylval.str = strdup(yytext);return BETWEEN; }
UNIQUE	|
unique	{ yylval.str = strdup(yytext);return UNIQUE; }
ORDER	|
order	{ yylval.str = strdup(yytext);return ORDER; }
BY	|
by	{ yylval.str = strdup(yytext);return BY; }
ASC	|
asc	{ yylval.str = strdup(yytext);return ASC; }
DESC	|
desc	{ yylval.str = strdup(yytext);return DESC; }
null	|
NULL	{ yylval.str = strdup(yytext);return NULLX; }
delete	|
DELETE	{ yylval.str = strdup(yytext);return DELETEX; }
update	|
"UPDATE"	{ return UPDATEX; }
set	|
"SET"		{ return SET; }
[%]			{ yylval.str = strdup(yytext);return PERC; }
"'"			{ yylval.str = strdup(yytext);BEGIN(INSQUOTES); return SQUOTE; }
"\""			{ yylval.str = strdup(yytext);BEGIN(INDQUOTES); return DQUOTE; }
","			{ yylval.str = strdup(yytext);return COMMA; }
INT |
int	{ yylval.number = INTTYPE; return DATATYPEX; }
float |
FLOAT { yylval.number = FLOATTYPE; return DATATYPEX; } 	
varchar |
VARCHAR { yylval.number = VARCHARTYPE; return DATATYPEX; }
date |
DATE { yylval.number = DATETYPE; return DATATYPEX; }
long |
LONG { yylval.number = LONGTYPE; return DATATYPEX; }
double |
DOUBLE { yylval.number = DOUBLETYPE; return DATATYPEX; }
short |
SHORT { yylval.number = SHORTTYPE; return DATATYPEX; }
bool	|
BOOL	{ yylval.number = BOOLTYPE; return DATATYPEX; }
text |
TEXT { yylval.number = TEXTTYPE; return DATATYPEX; } 
INDEX |
index	{ return INDEX; }
ON |
on	{ return ON; }
alter	|
ALTER	{ return ALTERX; }
ADD	|
add		{ return ADD; }
column	|
COLUMN	{ return COLUMN; }
DEFAULT |
default	{ return DEFAULTX; }
record	|
RECORD	{ return RECORD; }
records	|
RECORDS	{ return RECORDS; }
to	|
TO	{ return TO; }
range	|
RANGE	{ return RANGEX; }
random	|
RANDOM	{ return RANDOMX; }
SOURCE	|
source	{ return SOURCE; }
search	|
SEARCH	{ return SEARCH; }
TRAVERSE	|
traverse	{ return TRAVERSE; }
BM	|
bm	{ return BM; }
"true"	|
"TRUE"	{ yylval.str = true_val; return BOOLX; }
"false"	|
"FALSE"	{ yylval.str = false_val; return BOOLX; }

[a-zA-Z\._][a-zA-Z0-9_\-\+\*@\./]*		{ yylval.str = strdup(yytext); //printf("Matched Word %s\n",yytext); 
						  return WORD; }


[+-]?[0-9]+      		{
						yylval.str = strdup(yytext); 
			               	 switch(studyValue(yytext)){
				               	 case INTTYPE: return INTEGERX;break;
				               	 case SHORTTYPE: return SHORTX;break;
				               	 case LONGTYPE: return LONGX;break;
			    			}
		    			}
[+-]?[0-9]*\.[0-9]+	{
				 		yylval.str = strdup(yytext); 
				 		switch(studyValue(yytext)){
				              	case FLOATTYPE: return FLOATX;
			               		case DOUBLETYPE: return DOUBLEX;
		    				}
					}
							    			
[0-9]{4,4}("/"|"-")[0-1][0-9]("/"|"-")[0-3][0-9] {
				               	 yylval.str = strdup(yytext);
               	 				 return DATEX;
							  }			 
.	|
\n    {return yytext[0];}

<<EOF>>			{
					YY_FLUSH_BUFFER;
					yyterminate();
				}
%%
int yywrap(void) {
    BEGIN(INITIAL);
    *mypos = *myinputlim;
    return 1;
}

