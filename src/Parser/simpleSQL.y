%{
	
    #include <iostream>
    #include <string>
    #include <cctype>
    #include <vector>
    #include <stdlib.h>
    #include <stdio.h>
    #include "parsedQuery.h"
    #include "../Common/DataType.h"
    #include "../Common/Settings.h"
    #include "../Common/ActionCodes.h"
    


    #define YYERROR_VERBOSE 1
    #define YYDEBUG 1
	

    //	#ifdef VERBOSE
    //	#define YYERROR_VERBOSE 1
    //	#define YYDEBUG 1
    //	#endif
    

	
	char* myinputptr = NULL;
	int val;
	int* myinputlim = NULL;
	int initial = 0;
	int* mypos = NULL;
	
	
	using namespace std;
	extern "C"
	{
        int yyparse(void);
        int yylex(void);  
        void yyerror(char *);
	}
	parsedQuery* pq = NULL;
	vector<string> datavalues;//same
	vector< defaultValue > defaultValues;
	vector<string> columnNames;
	vector<int> columnTypes;//changed
	vector<bool>  	notNull;
	vector<bool>	pks;
	vector<bool>	uniqueArr;
//	vector<string>	opt_unique_array;
//	vector<string>	opt_pk_array;
	vector<string> order_by;
	vector<bool> desc_ordering;
	vector<string> condArray;
	vector<int> maxsizes;	
	vector<string> addIndex;
	vector<string> delIndex;
	vector<string> deleteCols;
	vector<int>	updateExprIndex;
	int getTypeSize(int dataType);
	void reverseEscSeqSQuotes(char* text, char* &cstr, int &len);
	void reverseEscSeqDQuotes(char* text, char* &cstr, int &len);
	string reverseEscSeqDQuotes(char* text);
	
	defaultValue defVal;
	string errorstr;
	bool error_state;
	bool unexpectedEnd;
	bool found;
	bool user_def = false;
	int colnum;
	int noOfColumns = 0;
%}

 %union 
{
        int number;
        short sno;
        long lno;
        char *str;
        float fval;
        double dval;
}

%token BOOLX SHORTX INTEGERX LONGX FLOATX DOUBLEX DATEX VARCHARX DATATYPEX ORDER BY
%token CREATEX USEX SHOWX SELECTX INSERTX DATABASEX DATABASESX TABLES TABLE FROM VALUES OF INTO WHERE LIKE COMPARISION SCHEMA WORD COMMA SQUOTE DQUOTE PERC NOT BETWEEN DROPX DELETEX INDEX TO
%token AND OR UNIQUE NULLX PRIMARY KEY ASC DESC ON ALTERX COLUMN ADD UPDATEX SET DEFAULTX RECORD RECORDS RANDOMX RANGEX SOURCE SEARCH TRAVERSE BM COLUMNSX INDICESX

%type<str> scalar_exp matchphrase not_predicate like_predicate  compare_predicate between_predicate search_condition

%destructor {free ($$); } scalar_exp matchphrase not_predicate like_predicate  compare_predicate between_predicate search_condition

%left OR LIKE NOT_LIKE
%left AND
%left NOT
%left COMPARISION
%left '+' '-'
%left '*' '/'
%left '(' ')'
%%


sql:	  creation_database error ';' { yyclearin; YYACCEPT; yyerrok; }
	|show_database error ';' { yyclearin; YYACCEPT; yyerrok; }
	|show_tables error ';' { yyclearin; YYACCEPT; yyerrok; }
	|show_columns error ';' { yyclearin; YYACCEPT; yyerrok; }
	|show_indices error ';' { yyclearin; YYACCEPT; yyerrok; }
	|creation_table error ';' { yyclearin; YYACCEPT; yyerrok; }
	|usage error ';' { yyclearin; YYACCEPT; yyerrok; }
	|insertion error ';' { yyclearin; YYABORT; yyerrok; }
	|selection error ';' { yyclearin; YYACCEPT; yyerrok; }
	|drop_table error ';' { yyclearin; YYACCEPT; yyerrok; }
	|drop_database error ';' { yyclearin; YYACCEPT; yyerrok; }
	|desc_table error ';' { yyclearin; YYACCEPT; yyerrok; }
	|deletion error ';' { yyclearin; YYACCEPT; yyerrok; }
	|create_index error ';' { yyclearin; YYACCEPT; yyerrok; }
	|alter_table error ';' { yyclearin; YYACCEPT; yyerrok; }
	|update_table error ';' { yyclearin; YYACCEPT; yyerrok; }
	|add_records error ';' { yyclearin; YYACCEPT; yyerrok; }
	|source_file error ';' { yyclearin; YYACCEPT; yyerrok; }
	|drop_index error ';' { yyclearin; YYACCEPT; yyerrok; }
	|search_index error ';' { yyclearin; YYACCEPT; yyerrok; }
	|traverse_index error ';' { yyclearin; YYACCEPT; yyerrok; }
	|buffer_manager error ';' { yyclearin; YYACCEPT; yyerrok; }	
	|traverse_index ';'
	|drop_database ';'
	|creation_database ';'
	|show_database ';'
	|show_tables ';'
	|show_columns ';'
	|show_indices ';'
	|desc_table ';'
	|creation_table ';'
	|usage ';'
	|insertion ';'
	|selection ';'
	|deletion ';'
	|create_index ';'
	|alter_table ';'
	|update_table ';'
	|drop_table ';'
	|add_records ';'
	|drop_index ';'        
	|search_index ';'        
	|source_file ';'
	|buffer_manager ';'
	|';'
                ;
drop_table:	
			DROPX TABLE WORD {
							  PARSER_FUNC_TRACE && cout<<"Caught Drop Table"<<endl;	
							  pq->setAction(DROP_TABLE);
							  pq->setTable($<str>3);
							  pq->drop_table = true;
						}
			;

desc_table:	
			DESC WORD {
							  PARSER_FUNC_TRACE && cout<<"Caught Desc Table"<<endl;	
							  //pq->setAction(DESC_TABLE);
							  //pq->setTable($<str>2);
							  //pq->desc_table = true;
							  
							  pq->noOfColumns = 0;
							  pq->select = true;
							  //PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
							  columnNames.clear();
							  columnNames.push_back("*");
							  noOfColumns = 1;
							  pq->setColumns(columnNames);
							  pq->noOfColumns = noOfColumns;
							  pq->setAction(SELECT);
							  pq->setTable("SYS_COLUMN");
							  pq->setColumns(columnNames);
							  //PARSER_FUNC_TRACE && cout<<"Condition Array:"<<$<str>2<<endl;
							  string temps = $<str>2;
							  string temp = "tableName = \""+temps+"\"";							   
							  pq->setConditionArray((char*)temp.c_str());
						}
			;

drop_database:
			DROPX DATABASEX WORD {
							  PARSER_FUNC_TRACE && cout<<"Caught Drop Database"<<endl;	
							  pq->setAction(DROP_DATABASE);
							  pq->setDatabase($<str>3);
							  pq->drop_database = true;
						}							  
             ;
creation_database: 
		CREATEX DATABASEX WORD	{ 
							  PARSER_FUNC_TRACE && cout<<"Caught Create Database"<<endl;
							  pq->setAction(CREATE_DATABASE);
							  pq->setDatabase($<str>3);
							  pq->createDatabase = true;
        						}
        	;				
show_database: 
		SHOWX DATABASESX		{
							  PARSER_FUNC_TRACE && cout<<"Caught Show Database"<<endl;
							  pq->setAction(SHOW_DATABASE);							  
							  pq->showDatabase = true;
        						}
		;
show_tables: 
		SHOWX TABLES			{ 
							  //PARSER_FUNC_TRACE && cout<<"Caught Show Tables"<<endl;
							  //pq->setAction(SHOW_TABLES);							  
							  //pq->showTables = true;

							  pq->noOfColumns = 0;
							  pq->select = true;
							  PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
							  pq->setAction(SELECT);
							  pq->setTable("SYS_TABLE");
							  columnNames.clear();
							  columnNames.push_back("*");
							  noOfColumns = 1;
							  pq->setColumns(columnNames);
							  pq->noOfColumns = noOfColumns;
							  //pq->setOrderByCols(order_by);
							  //pq->setDescOrder(desc_ordering);
        						}


		;

show_columns: 
		SHOWX COLUMNSX			{ 
							  //PARSER_FUNC_TRACE && cout<<"Caught Show Tables"<<endl;
							  //pq->setAction(SHOW_TABLES);							  
							  //pq->showTables = true;

							  pq->noOfColumns = 0;
							  pq->select = true;
							  PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
							  pq->setAction(SELECT);
							  pq->setTable("SYS_COLUMN");
							  columnNames.clear();
							  columnNames.push_back("*");
							  noOfColumns = 1;
							  pq->setColumns(columnNames);
							  pq->noOfColumns = noOfColumns;
							  //pq->setOrderByCols(order_by);
							  //pq->setDescOrder(desc_ordering);
        						}


		;

show_indices: 
		SHOWX INDICESX			{ 
							  //PARSER_FUNC_TRACE && cout<<"Caught Show Tables"<<endl;
							  //pq->setAction(SHOW_TABLES);							  
							  //pq->showTables = true;

							  pq->noOfColumns = 0;
							  pq->select = true;
							  PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
							  pq->setAction(SELECT);
							  pq->setTable("SYS_INDEX");
							  columnNames.clear();
							  columnNames.push_back("*");
							  noOfColumns = 1;
							  pq->setColumns(columnNames);
							  pq->noOfColumns = noOfColumns;
							  //pq->setOrderByCols(order_by);
							  //pq->setDescOrder(desc_ordering);
        						}


		;


create_index:	
		CREATEX INDEX WORD ON WORD '(' 	col_list ')'	{
												PARSER_FUNC_TRACE && cout<<"Caught Create Index"<<endl;
												pq->setAction(CREATE_INDEX);
												pq->setTable($<str>5);
												pq->setIndexName($<str>3);
												pq->setColumns(columnNames);
												}
		;
drop_index:
		DROPX INDEX WORD ON WORD					{
												PARSER_FUNC_TRACE && cout<<"Caught Drop Index"<<endl;
												pq->setAction(DROP_INDEX);
												pq->setTable($<str>5);
												pq->setIndexName($<str>3);
											}
		;
search_index:
		SEARCH INDEX WORD ON WORD VALUES '(' insertData	')'	{
												PARSER_FUNC_TRACE && cout<<"Caught serach index"<<endl;
												pq->setAction(SEARCH_INDEX);
												pq->setIndexName($<str>3);
												pq->setTable($<str>5);
												pq->setTypes(columnTypes);
							  					pq->setDataValues(datavalues);
							  					pq->setMaxSize(maxsizes);
							  				}
		;
traverse_index: TRAVERSE INDEX WORD ON WORD	{
												PARSER_FUNC_TRACE && cout<<"Caught traverse index for "<< $<str>3<<" on table"<<$<str>5<<endl;
												pq->setAction(TRAVERSE_INDEX);
												pq->setIndexName($<str>3);
												pq->setTable($<str>5);
											}	
		;		
source_file:
		SOURCE WORD								{
												PARSER_FUNC_TRACE && cout<<"Caught Source filename"<<endl;
												pq->setAction(SOURCE_FILE);
												pq->sourceFile = true;
												pq->setSourceFile($<str>2);
											}
		;
col_list:
		col_list COMMA WORD		{
									columnNames.push_back($<str>3);
								}	
		| WORD					{
									columnNames.push_back($<str>1);
								}
		;														
creation_table:
		CREATEX TABLE WORD '(' base_table_element_commalist ')'	{ 
										PARSER_FUNC_TRACE && cout<<"Caught Create Table"<<endl;
										  pq->createTable = true;
										  pq->setAction(CREATE_TABLE);
										  pq->setTable($<str>3);
										  pq->setColumns(columnNames);
										  pq->setTypes(columnTypes);
										  pq->setMaxSize(maxsizes);
										  pq->setNotNulls(notNull);
										  pq->setUnique(uniqueArr);
										  pq->setDefaultValues(defaultValues);
										  pq->setPKs(pks);
										  pq->setNoOfColumns(noOfColumns);
									}
        	;
base_table_element_commalist: base_table_element
				| base_table_element COMMA base_table_element_commalist						
		;									 
base_table_element: column_def
				| table_constraints	
		;
column_def: WORD DATATYPEX user_def_size column_def_opt_list	{	
										//PARSER_FUNC_TRACE && cout<<"Caught column definition"<<endl;
										columnNames.push_back($<str>1);
										columnTypes.push_back($<number>2);
										if($<number>2 != VARCHARTYPE )
											maxsizes.push_back(getTypeSize($<number>2));
										else if(!user_def){
											maxsizes.push_back(getTypeSize(VARCHARTYPE));
										}	
										user_def = false;
									 	}
		;
user_def_size: '(' SHORTX ')'				{
										user_def = true;
										PARSER_FUNC_TRACE && cout<<"Caught user def size"<<endl;
										maxsizes.push_back(atoi($<str>2));
									}						
			|  						{
										user_def = false;
										//maxsizes.push_back(getTypeSize(VARCHARTYPE));
									}
		;							
column_def_opt_list: 
			NOT NULLX default_value	{ notNull.push_back(true); pks.push_back(false); uniqueArr.push_back(false); noOfColumns++;}
			| UNIQUE	default_value	{ uniqueArr.push_back(true); pks.push_back(false); notNull.push_back(false); noOfColumns++;}
			| NOT NULLX UNIQUE default_value{ notNull.push_back(true); pks.push_back(false); uniqueArr.push_back(true); noOfColumns++;}
			| UNIQUE NOT NULLX default_value{ notNull.push_back(true); pks.push_back(false); uniqueArr.push_back(true); noOfColumns++;}
			| PRIMARY KEY default_value	{ pks.push_back(true); uniqueArr.push_back(true); notNull.push_back(true); noOfColumns++;}
			| default_value	{ pks.push_back(false); uniqueArr.push_back(false); notNull.push_back(false); 								noOfColumns++; } 
		;
default_value: DEFAULTX '(' SQUOTE WORD SQUOTE ')' {  
												//strcat(c,"");
												//strcat(c,$<str>4);
												//strcat(c,"");
												
												defVal.isDefault = true;
												defVal.value = $<str>4;
												defaultValues.push_back(defVal);
											   }
			| DEFAULTX '(' DQUOTE WORD DQUOTE ')'		{	//char c[] = "";
												//strcat(c,"");
												//strcat(c,$<str>4);
												//strcat(c,"");
												
												defVal.isDefault = true;
												defVal.value = $<str>4;
												defaultValues.push_back(defVal);
											 }
			| DEFAULTX '(' SHORTX ')'				{	defVal.isDefault = true;
												defVal.value = $<str>3;
												defaultValues.push_back(defVal);
											}
			| DEFAULTX '(' INTEGERX ')'				{
												defVal.isDefault = true;
												defVal.value = $<str>3;
												defaultValues.push_back(defVal);
											 }
			| DEFAULTX '(' LONGX ')'			 	{
												defVal.isDefault = true;
												defVal.value = $<str>3;
												defaultValues.push_back(defVal);
											 }
			| DEFAULTX '(' FLOATX ')'			 	{
												defVal.isDefault = true;
												defVal.value = $<str>3;
												defaultValues.push_back(defVal);
											 }
			| DEFAULTX '(' DOUBLEX ')'				 {
												defVal.isDefault = true;
												defVal.value = $<str>3;
												defaultValues.push_back(defVal);
											 }
			| DEFAULTX '(' DATEX ')'			 	{
												defVal.isDefault = true;
												defVal.value = $<str>3;
												(defVal.value).erase(4,1);
												(defVal.value).erase(8,1);
												defaultValues.push_back(defVal);
											 }
			|								{					 
												defVal.isDefault = false;
												defVal.value = "";
												defaultValues.push_back(defVal);
											}				
		;
table_constraints: 		UNIQUE '(' column_commalist ')'				
					| PRIMARY KEY '(' column_commalist_pks ')'
		;
column_commalist:	
									WORD {
										//cout<<"Inside column_commalist for unique col defs"<<endl;
										found = false; colnum = 0;
										for(int i=0; i<columnNames.size();i++)
											if(!((columnNames[i]).compare($<str>1))){
												found = true;
												colnum = i;
											}	
										if(found){
										//cout<<"Found col"<<colnum<<": "<<$<str>1<<" Was Unique: "<< uniqueArr[colnum]<<endl;
										uniqueArr[colnum] = true;
										}	
										else{
											cout<<"Error Column: "<<$<str>1<<" not declared"<<endl;
											pq->parseError = true;
											}
								          }

					| column_commalist COMMA WORD {
											//cout<<"Inside column_commalist for unique col defs"<<endl;
											found = false; colnum = 0;
											for(int i=0; i<columnNames.size();i++)
												if(!((columnNames[i]).compare($<str>3))){
													found = true;
													colnum = i;
												}	
											if(found){
												cout<<"Found col"<<colnum<<": "<<$<str>3<<" Was Unique: "<< uniqueArr[colnum]<<endl;
												uniqueArr[colnum] = true;
											}	
											else{
												cout<<"Error Column: "<<$<str>3<<" not declared"<<endl;
												pq->parseError = true;
												}
									          }
			;	
column_commalist_pks:	 
									WORD {
		 									//cout<<"Inside column_commalist for primary key col defs"<<endl;
											found = false; colnum = 0;
											for(int i=0; i<columnNames.size();i++){
												if(!((columnNames[i]).compare($<str>1))){
													found = true;
													colnum = i;
												}
											}	
											if(found)
												pks[colnum] = true;
											else{
												cout<<"Error Column: "<<$<str>1<<" not declared"<<endl;
												pq->parseError = true;
												}
											}
					
					| column_commalist_pks COMMA WORD	{
           									//cout<<"Inside column_commalist for primary key col defs"<<endl;
											found = false; colnum = 0;
											for(int i=0; i<columnNames.size();i++)
												if(!((columnNames[i]).compare($<str>3))){
													found = true;
													colnum = i;
												}
											if(found)
												pks[colnum] = true;
											else
												cout<<"Error Column: "<<$<str>3<<" not declared"<<endl;
											}							
		;
usage:
		USEX WORD				{ 
							  //check = 1;
							  PARSER_FUNC_TRACE && cout<<"Caught Use"<<endl;	
							  pq->setAction(USE);
							  pq->setDatabase($<str>2);
							  pq->use = true;
							}
		;					
alter_table:	
		ALTERX TABLE WORD alter_specs			{
												pq->noOfColumns = 0;
												PARSER_FUNC_TRACE && cout<<"Caught Alter Table"<<endl;
												pq->setAction(ALTER_TABLE);
												pq->setTable($<str>3);
												pq->setColumns(columnNames);
												pq->setTypes(columnTypes);
												pq->setDeleteCols(deleteCols);
												pq->setNotNulls(notNull);
												pq->setUnique(uniqueArr);
												pq->setDefaultValues(defaultValues);
												pq->setPKs(pks);
												pq->setMaxSize(maxsizes);
												pq->setNoOfColumns(noOfColumns);
												}
		;
alter_specs:
		ADD COLUMN column_def COMMA alter_specs 		{
														cout<<"In comma, Adding "<<$<str>3<<" to addIndex\n";
													}
		| DROPX COLUMN WORD COMMA alter_specs		{
														noOfColumns++;
														deleteCols.push_back($<str>3);
														//cout<<"In comma, Adding "<<$<str>3<<" to delIndex\n";
													}
		| ADD COLUMN column_def						{
														//cout<<"Adding "<<$<str>3<<" to addIndex\n";
													}
		| DROPX COLUMN WORD 						{
														noOfColumns++;
														deleteCols.push_back($<str>3);
														//cout<<"Adding "<<$<str>3<<" to delIndex\n";
													}
		;
update_table:	
		UPDATEX WORD SET col_equals_list 				{
														pq->noOfColumns = 0;
														pq->alterTable = true;
														PARSER_FUNC_TRACE && cout<<"Caught Update"<<endl;
														pq->setAction(UPDATE_TABLE);
														pq->setTable($<str>2);
														pq->setColumns(columnNames);
														pq->setDataValues(datavalues);
														pq->noOfColumns = noOfColumns;
														pq->setUpdateExprIndex(updateExprIndex);
													}
		| UPDATEX WORD SET col_equals_list WHERE search_condition {
														pq->noOfColumns = 0;
														pq->alterTable = true;
														PARSER_FUNC_TRACE && cout<<"Caught Update"<<endl;
														pq->setAction(UPDATE_TABLE);
														pq->setTable($<str>2);
														pq->setColumns(columnNames);
														pq->setDataValues(datavalues);
														pq->noOfColumns = noOfColumns;
														pq->setConditionArray($<str>6);
														pq->setUpdateExprIndex(updateExprIndex);
													}
		;
col_equals_list:
		WORD COMPARISION update_predicate COMMA col_equals_list {
														noOfColumns++;
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														updateExprIndex.push_back(pq->getNoConditionArrays());
														pq->setConditionArrays($<str>3);
														}
		| WORD COMPARISION WORD COMMA col_equals_list		{
														noOfColumns++;
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														updateExprIndex.push_back(-1);
														}
		| WORD COMPARISION SHORTX COMMA col_equals_list	{
														noOfColumns++;
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														updateExprIndex.push_back(-1);
														}
		| WORD COMPARISION INTEGERX COMMA col_equals_list	{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
														}
		| WORD COMPARISION LONGX COMMA col_equals_list	{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
														}
		| WORD COMPARISION FLOATX COMMA col_equals_list	{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DOUBLEX COMMA col_equals_list		{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DATEX COMMA col_equals_list			{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION SQUOTE WORD SQUOTE COMMA col_equals_list  {
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>4);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DQUOTE WORD DQUOTE COMMA col_equals_list  {
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>4);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION update_predicate 			{
														noOfColumns++;
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														updateExprIndex.push_back(pq->getNoConditionArrays());
														pq->setConditionArrays($<str>3);
													}	
		| WORD COMPARISION WORD 					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
													
		| WORD COMPARISION SHORTX					{
														noOfColumns++;
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION INTEGERX					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION LONGX					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION FLOATX					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DOUBLEX					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DATEX					{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>3);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}				
		| WORD COMPARISION SQUOTE WORD SQUOTE	{
														columnNames.push_back($<str>1);					
														datavalues.push_back($<str>4);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		| WORD COMPARISION DQUOTE WORD DQUOTE	{
														columnNames.push_back($<str>1);
														datavalues.push_back($<str>4);
														noOfColumns++;
														updateExprIndex.push_back(-1);
													}
		;
update_predicate: 
				upd_predicate		{
								$<str>$ = strdup($<str>1);
							}
		/*	   | update_predicate OR upd_predicate {
			   								char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
 			   								strcat(c,$<str>2);
 			   								strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
											//free($<str>1);
										}
 			 | update_predicate AND upd_predicate{
											char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
 			   								strcat(c,$<str>2);
 			   								strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
											//free($<str>1);
										 }								 
		*/								 
		;
upd_predicate: 
		/*	 upd_compare_predicate 		{ $<str>$ = strdup($<str>1); 
							}
			 | upd_between_predicate	{ $<str>$ = strdup($<str>1);
		 					}
			 | upd_not_predicate		{ $<str>$ = strdup($<str>1);
		 					}
		*/ 					
 			 | upd_scalar_exp	{	//cout<<"entered here1"<<endl;
 			 					$<str>$ = strdup($<str>1);
 		 			}
		;	
	/* upd_compare_predicate: upd_scalar_exp COMPARISION upd_scalar_exp { 
											char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
											strcat(c,$<str>2);
											strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
										   }					 
		;
upd_between_predicate: upd_scalar_exp NOT BETWEEN upd_scalar_exp AND upd_scalar_exp {
														char c1[] = "";
														strcat(c1,$<str>1);
														char *c = strdup(c1);
														strcat(c1," < ");
														strcat(c1,$<str>4);
														strcat(c1," AND ");
														strcat(c1,c);
														strcat(c1," > ");
														strcat(c1,$<str>6);
														$<str>$ = strdup(c1);
													   }	
				| upd_scalar_exp BETWEEN upd_scalar_exp AND upd_scalar_exp  {
														char c1[] = "";
														strcat(c1,$<str>1);
														char *c = strdup(c1);
														strcat(c1," >= ");
														strcat(c1,$<str>3);
														strcat(c1," AND ");
														strcat(c1,c);
														strcat(c1," <= ");
														strcat(c1,$<str>5);
														$<str>$ = strdup(c1);
													    }	
		;		
upd_not_predicate:  	NOT update_predicate {
								char c[] = " ! ";
								strcat(c,$<str>2);
 								strcat(c," ");
  								$<str>$ = strdup(c);

  							}
		;
	*/		
upd_scalar_exp:  		  value '+' upd_expr	    {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," + "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  |value '-' upd_expr    {			//cout<<"entered here2"<<endl;
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," - "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  |value '*' upd_expr  {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," * "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  |value '/' upd_expr    {
					  							//cout<<"entered here1 " <<endl;
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," / "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}
		  			  | '(' update_predicate ')'			{
		  		  								char c[] = " ( ";
		  		  								strcat(c,$<str>2);
		  		  								strcat(c," ) ");
		  		  								$<str>$  = strdup(c);
		  		  								}				
 		;
 upd_expr:	
 					value						{
 													//cout<<"entered here1 token: "<< $<str>1 <<endl;
 													$<str>$ = $<str>1;
 												}
 					| upd_expr '+' value			{
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," + "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  | upd_expr '-' value     			{
		  										char c[] = "";
			  										strcat(c,$<str>1);
		  										strcat(c," - "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  | upd_expr '*' value 			 {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," * "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
					  | upd_expr '/' value 			{
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," / "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}
 		;											
 value:
 		  			  WORD					  	{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  						  		}
					  | SQUOTE WORD SQUOTE			{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>2<<" ";
		  		  								char* tmp;
		  		  								int len;
		  		  								reverseEscSeqSQuotes($<str>2,tmp,len);
		  		  								char c[] = "'";
		  		  								strcat(c,tmp);
		  		  								strcat(c,"'");
		  		  								$<str>$ = strdup(c);
		  		  								if(len != 0)
		  		  									delete[] tmp;
		  		  						  		}
		  			  | DQUOTE WORD DQUOTE			{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>2<<" ";
		  		  								char *tmp;
		  		  								int len;
		  		  								string str = reverseEscSeqDQuotes($<str>2);
		  		  								char c[] = "\"";
		  		  								strcat(c,str.c_str());
		  		  								strcat(c,"\"");
		  		  								$<str>$ = strdup(c);
		  		  								//if(len != 0)
		  		  								//	delete[] tmp;
		  		  						  		}
		  			  | SHORTX						{
		  		  								 PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		  			  | INTEGERX						{
		  		  								 PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		  			  | BOOLX						{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" "<<endl;
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		;  		  								
add_records: ADD RECORD TO TABLE WORD 				{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>5);
														pq->setNoOfRecords(1);
														pq->setRecordType(DEFAULT);
														pq->setStartRange(-1);
													}
			| ADD SHORTX RECORDS TO TABLE WORD		{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(DEFAULT);
														pq->setStartRange(-1);
													}
			| ADD INTEGERX RECORDS TO TABLE WORD		{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(DEFAULT);
														pq->setStartRange(-1);
													}										
			| ADD RECORD TO TABLE WORD 	RANGEX		{								
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>5);
														pq->setNoOfRecords(1);
														pq->setRecordType(RANGE);
														pq->setStartRange(-1);
													}
			| ADD SHORTX RECORDS TO TABLE WORD RANGEX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(-1);
													}
			| ADD INTEGERX RECORDS TO TABLE WORD RANGEX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(-1);
													}													
			| ADD RECORD TO TABLE WORD 	RANGEX	SHORTX	{								
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>5);
														pq->setNoOfRecords(1);
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>7));
													}											
			| ADD RECORD TO TABLE WORD 	RANGEX	INTEGERX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>5);
														pq->setNoOfRecords(1);
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>7));
													}														
			| ADD SHORTX RECORDS TO TABLE WORD RANGEX	SHORTX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>8));
													}
			| ADD SHORTX RECORDS TO TABLE WORD RANGEX	INTEGERX	{								
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>8));
													}		
			| ADD INTEGERX RECORDS TO TABLE WORD RANGEX SHORTX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>8));
													}
			| ADD INTEGERX RECORDS TO TABLE WORD RANGEX INTEGERX	{								
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANGE);
														pq->setStartRange(atoi($<str>8));
													}
			| ADD RECORD TO TABLE WORD RANDOMX 	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>5);
														pq->setNoOfRecords(1);
														pq->setRecordType(RANDOM);
														pq->setStartRange(-1);
													}
			| ADD SHORTX RECORDS TO TABLE WORD RANDOMX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANDOM);
														pq->setStartRange(-1);
													}
			| ADD INTEGERX RECORDS TO TABLE WORD RANDOMX	{
														pq->setAction(ADD_RECORDS);
														pq->setTable($<str>6);
														pq->setNoOfRecords(atoi($<str>2));
														pq->setRecordType(RANDOM);
														pq->setStartRange(-1);
													}		
		;						
insertion: 
		INSERTX INTO WORD VALUES '(' insertData ')' { 
										   pq->noOfColumns = 0;
										   pq->insert = true;
										   PARSER_FUNC_TRACE  && cout<<"Caught Insert"<<endl;	
										   pq->setAction(INSERT);
							  			   pq->insert = true;
							  			   pq->setTable($<str>3);
							  			   pq->setTypes(columnTypes);
							  			   pq->setDataValues(datavalues);
							  			   pq->setMaxSize(maxsizes);
							  			   pq->noOfColumns = noOfColumns;
							  			  }
		| error {	yyclearin; YYABORT; yyerrok; }
		;					  			  
insertData:
		INTEGERX				{
							  PARSER_FUNC_TRACE && cout<<"Adding integer"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(INTTYPE);
							  maxsizes.push_back(getTypeSize(INTTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
							 }
		| LONGX				{
							  PARSER_FUNC_TRACE && cout<<"Adding long"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(LONGTYPE);
							  maxsizes.push_back(getTypeSize(LONGTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
							 }
		| SHORTX				{
							  PARSER_FUNC_TRACE && cout<<"Adding Short"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(SHORTTYPE);
							  maxsizes.push_back(getTypeSize(SHORTTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
							 }
		| BOOLX				{
							  PARSER_FUNC_TRACE && cout<<"Adding Bool"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(BOOLTYPE);
							  maxsizes.push_back(getTypeSize(BOOLTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
							 }	 						 							 
		| FLOATX				{
							  PARSER_FUNC_TRACE && cout<<"Adding Float"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(FLOATTYPE);
							  maxsizes.push_back(getTypeSize(FLOATTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							}
		| DATEX				{
 							  PARSER_FUNC_TRACE && cout<<"Adding Date"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(DATETYPE);
							  maxsizes.push_back(getTypeSize(DATETYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							}
		| DOUBLEX 			{
							  PARSER_FUNC_TRACE && cout<<"Adding Double"<<endl;
							  datavalues.push_back($<str>1);
							  columnTypes.push_back(DOUBLETYPE);
							  maxsizes.push_back(getTypeSize(DOUBLETYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							 } 					  	  

		| SQUOTE WORD SQUOTE		{ 
							  PARSER_FUNC_TRACE && cout<<"Adding String in Quotes"<<endl;
							  datavalues.push_back($<str>2); 
							  columnTypes.push_back(VARCHARTYPE);
							  maxsizes.push_back(getTypeSize(VARCHARTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							}
		| DQUOTE WORD DQUOTE		{ 
							  PARSER_FUNC_TRACE && cout<<"Adding String in Quotes"<<endl;
							  datavalues.push_back($<str>2); 
							  columnTypes.push_back(VARCHARTYPE);
							  maxsizes.push_back(getTypeSize(VARCHARTYPE));
							  noOfColumns++;
							  PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
					}
		| NULLX				{
							PARSER_FUNC_TRACE && cout<<"Adding Null"<<endl;
							datavalues.push_back("uNULL"); 
							columnTypes.push_back(NULLTYPE);
							maxsizes.push_back(getTypeSize(NULLTYPE));
							noOfColumns++;
							}
		| insertData COMMA SQUOTE WORD SQUOTE { 
							 		 PARSER_FUNC_TRACE && cout<<"Adding String in quotes"<<endl;
							 		 datavalues.push_back($<str>4); 
							 		 columnTypes.push_back(VARCHARTYPE);
							 		 maxsizes.push_back(getTypeSize(VARCHARTYPE));
							 		 noOfColumns++;
							 		 PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
									}
		| insertData COMMA DQUOTE WORD DQUOTE { 
							 		 PARSER_FUNC_TRACE && cout<<"Adding String in quotes"<<endl;
							 		 datavalues.push_back($<str>4); 
							 		 columnTypes.push_back(VARCHARTYPE);
							 		 maxsizes.push_back(getTypeSize(VARCHARTYPE));
							 		 noOfColumns++;
							 		 PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
									}									
		| insertData COMMA SHORTX		{
									  PARSER_FUNC_TRACE && cout<<"Adding Short"<<endl;
									  datavalues.push_back($<str>3);
									  columnTypes.push_back(SHORTTYPE);
									  maxsizes.push_back(getTypeSize(SHORTTYPE));
									  noOfColumns++;
									  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
									 }
		| insertData COMMA INTEGERX	 	{
						  			PARSER_FUNC_TRACE && cout<<"Adding integer"<<endl;
						  			datavalues.push_back($<str>3); 
						 			columnTypes.push_back(INTTYPE);
						 			maxsizes.push_back(getTypeSize(INTTYPE));
								  	noOfColumns++;
								  	PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
								 	}
		| insertData COMMA LONGX			{
									  PARSER_FUNC_TRACE && cout<<"Adding long"<<endl;
									  datavalues.push_back($<str>3);
									  columnTypes.push_back(LONGTYPE);
									  maxsizes.push_back(getTypeSize(LONGTYPE));
									  noOfColumns++;
									  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
									 }
		| insertData COMMA BOOLX				{
									  PARSER_FUNC_TRACE && cout<<"Adding Bool"<<endl;
									  datavalues.push_back($<str>3);
									  columnTypes.push_back(BOOLTYPE);
									  maxsizes.push_back(getTypeSize(BOOLTYPE));
									  noOfColumns++;
									  PARSER_FUNC_TRACE && cout<<"Current No. of columns " << noOfColumns << endl;
									 }	 									 
		| insertData COMMA FLOATX	 	{
							  	PARSER_FUNC_TRACE && cout<<"Adding float"<<endl;
							  	datavalues.push_back($<str>3); 
     						 		columnTypes.push_back(FLOATTYPE);
     						 		maxsizes.push_back(getTypeSize(FLOATTYPE));  		
						  		noOfColumns++;
						  		PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							 }
		| insertData COMMA DOUBLEX	 	{
							  	PARSER_FUNC_TRACE && cout<<"Adding double"<<endl;
							  	datavalues.push_back($<str>3); 
     						 		columnTypes.push_back(DOUBLETYPE);
     						 		maxsizes.push_back(getTypeSize(DOUBLETYPE));  		
							  	noOfColumns++;
							  	PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							 	}
		| insertData COMMA DATEX	 	{
							  	PARSER_FUNC_TRACE && cout<<"Adding date"<<endl;
							  	datavalues.push_back($<str>3); 
     						 		columnTypes.push_back(DATETYPE);
     						 		maxsizes.push_back(getTypeSize(DATETYPE));  		
							  	noOfColumns++;
							  	PARSER_FUNC_TRACE && cout<<"Current No. of columns "<<noOfColumns<<endl;
							 }
		| insertData COMMA NULLX	{
							PARSER_FUNC_TRACE && cout<<"Adding Null"<<endl;
							datavalues.push_back("uNULL"); 
							columnTypes.push_back(NULLTYPE);
							maxsizes.push_back(getTypeSize(NULLTYPE));
							noOfColumns++;
							}							 
		| insertData COMMA			 {
								PARSER_FUNC_TRACE && cout<<"Adding Null"<<endl;
								datavalues.push_back("NULL"); 
								columnTypes.push_back(NULLTYPE);
								maxsizes.push_back(getTypeSize(NULLTYPE));
								noOfColumns++;
							 }
		| 					{
								PARSER_FUNC_TRACE && cout<<"Adding Null"<<endl;
								datavalues.push_back("NULL"); 
								columnTypes.push_back(NULLTYPE);
								maxsizes.push_back(getTypeSize(NULLTYPE));
								noOfColumns++;
							 }					 						 						 	
		;								  
					  
selection: SELECTX columnlist FROM WORD order_by_clause{ 
								 pq->noOfColumns = 0;
								 pq->select = true;
								 PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
								 pq->setAction(SELECT);
								 pq->setTable($<str>4);
								 pq->setColumns(columnNames);
								 pq->noOfColumns = noOfColumns;
								 pq->setOrderByCols(order_by);
								 pq->setDescOrder(desc_ordering);
   							    }
		| SELECTX columnlist FROM WORD WHERE search_condition order_by_clause{
									 pq->noOfColumns = 0;
									 pq->select = true;
									 PARSER_FUNC_TRACE && cout<<"Caught Select"<<endl;
									 pq->setAction(SELECT);
									 pq->setTable($<str>4);
									 pq->setColumns(columnNames);
									 PARSER_FUNC_TRACE && cout<<"Condition Array:"<<$<str>6<<endl;
									 pq->setConditionArray($<str>6);
									 pq->noOfColumns = noOfColumns;
									 pq->setOrderByCols(order_by);
									 pq->setDescOrder(desc_ordering);
									}
		;
order_by_clause:	  
				ORDER BY order_col_list	{
			  						pq->orderBy = true;
			  					}
			  	| 				{
			  						pq->orderBy = false;
			  					}
			  ;
order_col_list: order_col_list COMMA order_word {
											
								}	
			| order_word
			;						   
order_word:	WORD				{
							order_by.push_back($<str>1);
							desc_ordering.push_back(false);							
						}
			| WORD DESC		{
							order_by.push_back($<str>1);	
							desc_ordering.push_back(true);	
						}
			| WORD ASC		{
							order_by.push_back($<str>1);	
							desc_ordering.push_back(false);
						}
			;
columnlist: '*'					{	columnNames.push_back("*");
								noOfColumns = 1;
							}	
		  | columnnames
		;  		
		
columnnames: WORD			{	columnNames.push_back($<str>1);
								noOfColumns++;
							}
		   | columnnames COMMA WORD { columnNames.push_back($<str>3);
		   						noOfColumns++;
		   					   }	
		;
search_condition: predicate		{
								$<str>$ = strdup($<str>1);
							}
			   | search_condition OR predicate {
			   								char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
 			   								strcat(c,$<str>2);
 			   								strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
											//free($<str>1);
										}
 			 | search_condition AND predicate{
											char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
 			   								strcat(c,$<str>2);
 			   								strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
											//free($<str>1);
										 }
			   						 
			   /*| noted_search_condition		 {
			   								$<str>$ = strdup($<str>1);
			   								//free($<str>1);								 											}*/
			   								
			   /*| NOT search_condition		{
			   								char c[] = " ! ";
			   								strcat(c,$<str>2);
			   								strcat(c,")");
			   								$<str>$ = strdup(c);
			   								//free($<str>2);
			   							}*/	
			   /*| '(' search_condition ')'		{
			   								char c[] = " ( ";
 			   								strcat(c,$<str>2); 
											strcat(c," ) ");
											$<str>$ = strdup(c);
											//free($<str>1);
										}*/
			 // | scalar_exp	{ $<str>$ = strdup($<str>1); }							
		;		
 /*noted_search_condition: NOT search_condition	{
									char c[] = " = ";
									strcat($<str>1,c);
									strcat($<str>1,$<str>2);
									$<str>$ = strdup($<str>1);
									//free($<str>1);
									}		
		;*/
predicate: compare_predicate 		{ $<str>$ = strdup($<str>1); 
							}
		 | between_predicate	{ $<str>$ = strdup($<str>1);
		 					}
		 | like_predicate		{ $<str>$ = strdup($<str>1);
		 					}
		 | not_like_predicate	{ $<str>$ = strdup($<str>1);
		 					}
		 | not_predicate		{ $<str>$ = strdup($<str>1);
		 					}
		/* | bracketed_predicate	{ $<str>$ = strdup($<str>1);
		 					}*/
 		 |  scalar_exp	{ $<str>$ = strdup($<str>1);
 		 			}
		// | bracket_predicate	{ $<str>$ = strdup($<str>1);}
		;
		
compare_predicate: scalar_exp COMPARISION scalar_exp { 
											char c[] = "";
											strcat(c,$<str>1);
											strcat(c," ");
											strcat(c,$<str>2);
											strcat(c," ");
											strcat(c,$<str>3);
											$<str>$ = strdup(c);
										   }
			    //|  scalar_exp	{ $<str>$ = strdup($<str>1);}							 
		;

between_predicate: scalar_exp NOT BETWEEN scalar_exp AND scalar_exp {
														char c1[] = "";
														strcat(c1,$<str>1);
														strcat(c1," < ");
														strcat(c1,$<str>4);
														strcat(c1," AND ");
														strcat(c1,$<str>1);
														strcat(c1," > ");
														strcat(c1,$<str>6);
														$<str>$ = strdup(c1);
													   }	
				| scalar_exp BETWEEN scalar_exp AND scalar_exp  {
														char c1[] = "";
														strcat(c1,$<str>1);
														strcat(c1," > ");
														strcat(c1,$<str>3);
														strcat(c1," AND ");
														strcat(c1,$<str>1);
														strcat(c1," < ");
														strcat(c1,$<str>5);
														$<str>$ = strdup(c1);
													    }	
		;		
like_predicate: 

			 scalar_exp LIKE matchphrase 	{
												  char c[] = " LIKE ";
												  char c1[] = "";
												  strcat(c1,$<str>1);
												  strcat(c1,c);
												  strcat(c1,$<str>3);
												  $<str>$ = strdup(c1);
												  }
		;										  
not_like_predicate:	     		  scalar_exp NOT LIKE matchphrase	{
												  char c[] = " NOTLIKE ";
												  char c1[] = "";
												  strcat(c1,$<str>1);
												  strcat(c1,c);
												  strcat(c1,$<str>4); 
												  $<str>$ = strdup(c1);

											}
		;
not_predicate:  	NOT search_condition {
								char c[] = " ! ";
								strcat(c,$<str>2);
 								strcat(c," ");
  								$<str>$ = strdup(c);

  							}
		;
/*bracketed_predicate: '(' search_condition ')'		{
			   								char c[] = " ( ";
 			   								strcat(c,$<str>2); 
											strcat(c," ) ");
											$<str>$ = strdup(c);
											}
		;*/
/*bracket_predicate: '(' scalar_exp ')' {
											char c[] = " ( ";
 			   								strcat(c,$<str>2); 
											strcat(c," ) ");
											$<str>$ = strdup(c);
								}
		;*/						
matchphrase: 	
			SQUOTE WORD SQUOTE	{
							char* tmp;
							int len;
							reverseEscSeqSQuotes($<str>2,tmp,len);
							strcat($<str>1,tmp);
							strcat($<str>1,$<str>3);
							$<str>$ = strdup($<str>1);
							if(len != 0)
							delete[] tmp;
							}
		   | DQUOTE WORD DQUOTE	{
		   					char* tmp = NULL;
		   					int len;
							string str = reverseEscSeqDQuotes($<str>2);
		  		  			char c[] = "\"";
							strcat(c,str.c_str());
							strcat(c,"\"");
							$<str>$ = strdup(c);
							//if(tmp != NULL)
 							//	delete[] tmp;
		   					cout<<"Here data: "<<$<str>$<<endl;
							}
		   | WORD 			{
		   					char c[] = "";
		   					strcat(c,$<str>1);
		   					$<str>$ = strdup(c); 
		   					}
		   | SQUOTE PERC WORD SQUOTE {
		   						  char c[] = "";
		   						  char* tmp;
		   						  int len;
		   						  reverseEscSeqSQuotes($<str>3,tmp,len);
		   						  strcat(c,$<str>1);
		   						  strcat(c,$<str>2);
		   						  strcat(c,tmp);
		   						  strcat(c,$<str>4);
		   						  $<str>$ = strdup(c);
		   						  if(len != 0)
		   						  delete[] tmp;
		   						}
		   | SQUOTE WORD PERC SQUOTE {			
		   						  char c[] = "";
		   						  char* tmp;
		   						  int len;
		   						  reverseEscSeqSQuotes($<str>2,tmp,len);
		   						  strcat(c,$<str>1);
		   						  strcat(c,tmp);
		   						  strcat(c,$<str>3);
		   						  strcat(c,$<str>4);
		   						  $<str>$ = strdup(c);
		   						  if(len != 0)
		   						  delete[] tmp;
		   						}						
		   | SQUOTE PERC WORD PERC SQUOTE		{ 		   								
		   								char c[] = "";
		   								char* tmp;
		   								int len;
		   								reverseEscSeqSQuotes($<str>3,tmp,len);
		   								strcat(c,$<str>1);
		   								strcat(c,$<str>2);
		   								strcat(c,tmp);
		   					  			strcat(c,$<str>4);
		   					  			strcat(c,$<str>5);
		   					  			$<str>$ = strdup(c);
		   					  			if(len != 0)
		   					  			delete[] tmp;
		   								}
		;
scalar_exp:  		  scalar_exp '+' scalar_exp	    {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," + "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
				  |scalar_exp '-' scalar_exp    {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," - "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
				  |scalar_exp '*' scalar_exp   {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," * "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		}	
				  |scalar_exp '/' scalar_exp    {
		  										char c[] = "";
		  										strcat(c,$<str>1);
		  										strcat(c," / "); 
												strcat(c,$<str>3);
												$<str>$ = strdup(c);
		  								  		} 		
		  		  | WORD					  		{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  						  		}
				  | SQUOTE WORD SQUOTE			{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>2<<" ";
		  		  								char* tmp;
		  		  								int len;
		  		  								reverseEscSeqSQuotes($<str>2,tmp,len);
		  		  								char c[] = "'";
		  		  								strcat(c,tmp);
		  		  								strcat(c,"'");
		  		  								$<str>$ = strdup(c);
		  		  								if(len != 0)
		  		  								delete[] tmp;
		  		  						  		}
		  		  | DQUOTE WORD DQUOTE			{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>2<<" ";
		  		  								char *tmp;
		  		  								int len;
		  		  								string str = reverseEscSeqDQuotes($<str>2);
		  		  								char c[] = "\"";
		  		  								strcat(c,str.c_str());
		  		  								strcat(c,"\"");
		  		  								$<str>$ = strdup(c);
		  		  								//if(len != 0)
		  		  								//	delete[] tmp;
		  		  						  		}
		  		  | SHORTX						{
		  		  								 PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		  		  | INTEGERX						{
		  		  								 PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" ";
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		  		  | BOOLX						{
		  		  								PARSER_FUNC_TRACE && cout<<" "<<$<str>1<<" "<<endl;
		  		  								char c[] = "";
		  		  								strcat(c,$<str>1);
		  		  								$<str>$ = strdup(c);
		  		  								}
		  		  | '(' search_condition ')'			{
		  		  								char c[] = " ( ";
		  		  								strcat(c,$<str>2);
		  		  								strcat(c," ) ");
		  		  								$<str>$  = strdup(c);
		  		  								}
 		;
deletion: 			DELETEX FROM WORD			{
									PARSER_FUNC_TRACE && cout<<"Caught DELETE Table"<<endl;	
									pq->setAction(DELETE);
									pq->setTable($<str>3);
									pq->delete_table = true;
									}
				| DELETEX FROM WORD WHERE search_condition {
									PARSER_FUNC_TRACE && cout<<"Caught DELETE Table"<<endl;	
									pq->setAction(DELETE);
									pq->setTable($<str>3);
									pq->setConditionArray($<str>5);
									pq->delete_table = true;
									}
 		;
buffer_manager:	BM		{
									PARSER_FUNC_TRACE && cout<<"Caught buffer manager"<<endl;	
									pq->setAction(BUFFER_MANAGER);
									}
		;						 		
%%

//Reverses the process of replacing \' with '. 
void reverseEscSeqSQuotes(char* text, char* &cstr,int &len){
	 //cout<<"Before Reversing Esc Seq S Quote:"<<text<<endl;
	   int i=0, j=0;
	   len = 0;
	   while(text[i] != '\0'){
	   	if(text[i] == '\'' )
	   		len++;
	   	i++;
	   	len++;
	   }
	   len++;
	   if( len != 0 ){
		   cstr = (char*)new char[len];
		   i=0;j=0;
		   while(text[i] != '\0'){
		   	if(text[i] == '\''){
		   		cstr[j] = '\\';
		   		j++;
		   	}
		   	cstr[j] = text[i];
		   	i++;	j++;
		   }
		   cstr[j] = '\0';
		   //cout<<"After Reversing Esc Seq S Quote:"<<cstr<<endl;
	   }
}

//Reverses the process of replacing \" with ". 
void reverseEscSeqDQuotes(char* text,char * &cstr,int &len){
	int i=0, j=0;
	size_t pos = string::npos;
	string str(text);	   

	len = 0;
	//cout<<"Before Reversing Esc Seq D Quote:"<<text<<endl;
	/*
	while(text[i] != '\0'){
		   	if(text[i] == '"' ) 
		   		len++;
		   	i++;
		   	len++;
	}
	len++;
	cstr = (char*)new char[len];
	i=0;j=0;
	while(text[i] != '\0'){
	   	if(text[i] == '"'){
	   		cstr[j] = '\\';
	   		j++;
	   	}
	   	cstr[j] = text[i];
		i++;	j++;
	}
	cstr[j] = '\0';*/
	pos = str.find('"',0);
	while(pos != string::npos){
		str.replace(pos,1,"\\\"");
		pos = str.find('"', pos+1);
	}
	//cout<<"After Reversing Esc Seq D Quote:"<<cstr<<endl;
}

string reverseEscSeqDQuotes(char* text){
	size_t pos = string::npos;
	string str(text);	   

	pos = str.find('"',0);
	while(pos != string::npos){
		str.replace(pos,1,"\\\"");
		pos = str.find('"', pos+1);
	}
	return str;
}

void yyerror(char *s) {

	if(!unexpectedEnd)
		printf("Error '%s'\n", s);
	pq->errorstr = strdup(s);
	*mypos = *myinputlim;
	errorstr = strdup(s);
	error_state = true;
	pq->parseError = true;
}

void clearResult(){
	
	//When commented out, works well,otherwise the getAction() returns some random number each time the entire parser is rerun.
	//Mostly because clearResult must be called only after the entire parsedQuery result has been processed and freed only bfore nxt 		//iteration
	/*=========================*/
	if(pq != NULL){
		delete pq; 
		pq = NULL;
	}
	/*=========================*/
	//free(datavalues);//same
	
	
	vector<string>().swap(datavalues);
	vector<string>().swap(columnNames);
	vector<int>().swap(columnTypes);
	vector<bool>().swap(notNull);
	vector<bool>().swap(pks);
	vector<bool>().swap(uniqueArr);
	//vector<string>().swap(opt_unique_array);
	//vector<string>().swap(opt_pk_array);
	vector<string>().swap(order_by);
	vector<int>().swap(maxsizes);
	vector<bool>().swap(desc_ordering);
	vector<string>().swap(condArray);
	vector<string>().swap(deleteCols);
	defaultValues.clear();
	updateExprIndex.clear();
	/*
	datavalues.clear();
	columnNames.clear();
	notNull.clear();
	pks.clear();
	uniqueArr.clear();
	order_by.clear();
	desc_ordering.clear();
	maxsizes.clear();
	condArray.clear();
	*/
	
	error_state = false;
	errorstr = "";
	found = false;
	user_def = false;
	colnum = 0;
	noOfColumns = 0;
	/*=========================*/
	/* problem area , how to free the below memory ? */
	/*=========================*/	
	//free(myinputptr);
	//delete myinputlim;
	//delete mypos;
	/*=========================*/
	
	if(myinputlim != NULL){
		delete myinputlim;
		myinputlim = NULL;
	}
	if(myinputptr != NULL){
		//delete[] myinputptr;
		myinputptr = NULL;
	}
		
	if(mypos != NULL){
		delete mypos;
		mypos = NULL;
	}	
	
}     

parsedQuery* getResult(string input) {
	
	int tmpsize = input.size()+1;
	//char* tmpstr = (char*)input.c_str();
	char reinput[tmpsize];
	//int len = input.copy(reinput,tmpsize,(size_t)0);
	strncpy(reinput,input.c_str(),input.size());
	reinput[tmpsize-1] = '\0';
	//printf("ReInput: %s, size: %d\n",reinput,tmpsize);
	clearResult();
	pq = (parsedQuery*) new parsedQuery();
	//myinputptr = (char *)input.c_str();
	//int size  = input.size();
	myinputptr = reinput;
	myinputlim = (int *)new int(tmpsize-1);
	mypos = (int *)new int(0);
		
	//PARSER_FUNC_TRACE && cout<<"Parser input: " <<myinputptr<<endl;
	
	error_state = false;
       yyparse();
	//cout<<"Size: " << input.size()<<endl;
	
	/*
	required to remove a bug- when input does'nt match any rule then, the parser stops abruptly, and at nxt iteration starts off
	at same state, expecting the rule which was stopped (due to synatx error )to complete.
	========================================================================*/
	
	if(error_state == true && errorstr.compare("") != 0 ){
		//cout<<"Error String returned: "<<pq->errorstr<<endl;
		//while(strcmp(errorstr.c_str(),"syntax error, unexpected ';'")!=0){
		  while(error_state == true){
			//cout<<"caught unexpected ';'"<<endl;
			unexpectedEnd = true;
			getResult(";");
		}
	}
	
	//=======================================================================
	unexpectedEnd = false;
    	return pq;     
}
