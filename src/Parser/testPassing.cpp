#include <iostream>
#include <string>
#include "simpleSQL.cc"
#include "parsedQuery.h"
#include "../Common/DataType.h"
#include "../Common/ActionCodes.h"
#include "../Common/Packing_new.h"
#include "../DataManager/evaluate_expression.h"

using namespace std;

int main(){
	string s;// = "1 + 2";
	char input[255];
	bool conti = true;
	do{
	cout<<"<yadb>:";
	cin.getline(input,255);
	if(!strcmp(input,"quit"))
		break;
	s = input;
	int i;
	//cout<<s<<endl;
	parsedQuery *result = getResult(s); 
	cout << "Action: "<<result->getAction()<<endl;
	
	if(result->getAction() == CREATE_DATABASE){
		cout<<"Create Database: "<<result->getDatabase()<<endl;
	}
	if(result->getAction() == USE){
		cout<<"Using Database: "<<result->getDatabase()<<endl;
	}
	if(result->getAction() == CREATE_TABLE){
		cout<<"Creating Table: "<<result->getTable()<<endl;
		cout<< "Column names: "<<endl;
		vector<string> columns = result->getColumns();
		vector<int> types = result->getTypes();
		vector<bool> notNulls = result->getNotNulls();
		vector<bool> pks = result->getPKs();
		vector<bool> unique = result->getUnique();						
		vector<int> maxsizes = result->getMaxSize();
		//cout<<"Number of Max Sizes: "<<maxsizes.size()<<endl;
		for(i=0;i<result->noOfColumns;i++){
				switch( types[i] ){
			
				case INTTYPE: 
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: INT Size: "<< maxsizes[i]<<endl;	
							break;
				case FLOATTYPE:
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: FLOAT Size: "<< maxsizes[i]<<endl;
							break;
				case LONGTYPE:	
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: LONG Size: "<< maxsizes[i]<<endl;
							break;
				case DOUBLETYPE: 
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: DOUBLE Size: "<< maxsizes[i]<<endl;
							break;
				case VARCHARTYPE: 
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: VARCHAR Size: "<< maxsizes[i]<<endl;
							break;
				case DATETYPE: 
							cout<<"Column "<<i<<": "<<columns[i]<<" , Type: DATE Size: "<< maxsizes[i]<<endl;
							break;
			}		
		}
		//cout<<"Columns tht are not null:"<<endl;
		for(i=0;i<notNulls.size();i++)
			if(notNulls[i])
			cout<<"Not Null column"<< i<<endl;
			else
			cout<<"Nullable column"<< i<<endl;
						
		//cout<<"Columns tht are Primary Keys:"<<endl;
		for(i=0;i<pks.size();i++)
			if(pks[i])
			cout<<"Primary key column:"<< i<<endl;
			else
			cout<<"Not Primary key column:"<< i<<endl;			
		//cout<<"Columns tht are Unique:"<<endl;
		for(i=0;i<unique.size();i++)
			if(unique[i])
			cout<<"unique column"<< i<<endl;
			else
			cout<<"not unique column"<< i<<endl;			
						
	}
	if(result->getAction() == INSERT){
		int *ival = NULL; float *fval = NULL; long *lval = NULL; double *dval = NULL; int *date = NULL; char* str = NULL;short* sval = NULL;
		string strtmp;
		int ivalue; float fvalue; long lvalue; double dvalue; int datevalue; short svalue;
		cout << "Inserting to Table: "<<result->getTable()<<endl;
		vector<int> types = result->getTypes();
		vector<string> values = result->getDataValues();
		void* value;
		int flag = 0;
		cout<<"No of values: "<<values.size()<<endl;
		
		for(int i=0;i<values.size();i++){
			cout<<"Datavalues "<<i<<": "<<values[i]<<endl;
		}
		cout<<"noOfColumns: "<<result->noOfColumns<<endl;

		for(int i=0;i<result->noOfColumns;i++){
	
			switch( types[i] ){
			
				case INTTYPE: if(pack::convert_to_type(values[i],INTTYPE,value))
							cout<<"INTType: "<<types[i]<<" Value: "<< *((int*)value)<<endl;
							break;
				case FLOATTYPE:if(pack::convert_to_type(values[i],FLOATTYPE,value))
							cout<<"FLOATType: "<<types[i]<<" Value: "<< *((float*)value)<<endl;
							break;
				case SHORTTYPE:if(pack::convert_to_type(values[i],SHORTTYPE,value))
							cout<<"SHORTType: "<<types[i]<<" Value: "<< *((short*)value)<<endl;
							break;
				case LONGTYPE:	if(pack::convert_to_type(values[i],LONGTYPE,value))
							cout<<"LONGType: "<<types[i]<<" Value: "<< *((long*)value)<<endl;
							break;
				case DOUBLETYPE: if(pack::convert_to_type(values[i],DOUBLETYPE,value))
							cout<<"DOUBLEType: "<<types[i]<<" Value: "<< *((double*)value)<<endl;
							break;
				case VARCHARTYPE:if(pack::convert_to_type(values[i],VARCHARTYPE,value))
							str = (char*)value;
							cout<<"VARCHARType: "<<types[i]<<" Value: "<< str<<endl;
							break;
				case DATETYPE: if(pack::convert_to_type(values[i],DATETYPE,value))
							cout<<"DATEType: "<<types[i]<<" Value: "<< *((int*)value)<<endl;
							break;
				case NULLTYPE: cout<<"Null type encountered."<<endl;
							break;			
			}		
		}
		char* buf;
		int i=0;
		int itmp;
		const int *pi;
		int size,pos = 0;
		vector<string> databuf;
		bool noerror = pack::pack_data(result,buf,size,types);
		if(pack::unpack_data(databuf, types, buf, size)){
			cout<<"Unpack Success "<<endl;
			for(i=0;i<databuf.size();i++)
				cout<<i+1<<". "<<databuf[i]<<endl;
		}
		/*if( noerror && size != 0){
      	        //cout << "Buff: \n"<< buf<<endl;
	     	   for(int i=0;i<result->noOfColumns;i++){	
			   switch( types[i] ){
			
				case INTTYPE:  ival = new int;
							memcpy(ival,&buf[pos],INT_SIZE);
							pos+=INT_SIZE;
							cout<<"Int value: "<<*ival<<endl;
							break;
				case FLOATTYPE:fval = new float;
							memcpy(fval,&buf[pos],FLOAT_SIZE);
							pos+=FLOAT_SIZE;
							cout<<"Float value: "<<*fval<<endl;
							break;
				case SHORTTYPE:	sval = new short;
							memcpy(sval,&buf[pos],SHORT_SIZE);
							pos+=SHORT_SIZE;
							cout<<"Short value: "<<*sval<<endl;
							break;
				case LONGTYPE:	lval = new long;
							memcpy(lval,&buf[pos],LONG_SIZE);
							pos+=LONG_SIZE;
							cout<<"Long value: "<<*lval<<endl;
							break;
				case DOUBLETYPE: dval = new double;
							memcpy(dval,&buf[pos],DOUBLE_SIZE);
							pos+=DOUBLE_SIZE;
							cout<<"Double value: "<<*dval<<endl;
							break;
				case VARCHARTYPE: str = NULL;
							itmp=pos;
							while(buf[itmp] != -57 && itmp < size)
							itmp++;
							str = new char[itmp+1];
							memcpy(str,&buf[pos],(itmp-pos));
							str[itmp+1] = '\0';
							pos+=(itmp-pos + 1);
							cout<<"String value: "<<str<<" Nxt item pos: "<< pos<<endl;
							break;
				case DATETYPE: date = new int;
							memcpy(date,&buf[pos],INT_SIZE);
							pos+=INT_SIZE;
							cout<<"Date value: "<<*date<<endl;
							break;
				} 	
			}
	}*/
	}
	if(result->getAction() == SELECT){
		cout<< "Selecting from table: "<<result->getTable()<<endl;
		cout<< "Column names: "<<endl;
		vector<string> columns = result->getColumns();
		vector<string> condArray = result->getConditionArray();
		/*for(int i=0;i<result->noOfColumns;i++){
			cout<<"Column "<<i<<": "<<columns[i]<<endl;
		}*/
		if(condArray.size()>0){
			cout<<"Condition Array: "<<condArray[0]<<endl;
		}
		
	}
	if(result->getAction() == DELETE){
		cout<< "Delete from table: "<<result->getTable()<<endl;
		vector<string> condArray = result->getConditionArray();
		/*for(int i=0;i<result->noOfColumns;i++){
			cout<<"Column "<<i<<": "<<columns[i]<<endl;
		}*/
		if(condArray.size()>0){
			cout<<"Condition Array: "<<condArray[0]<<endl;
		}
	}
	if(result->getAction() == DROP_TABLE){
		cout<<"Dropping Table: "<<result->getTable()<<endl;
	}
	if(result->getAction() == DROP_DATABASE){
		cout<<"Dropping Database: "<<result->getDatabase()<<endl;
	}
		
	/*if(result->parseError){
		//cout<<"Error String returned: "<<result->errorstr<<endl;
		if(strcmp(result->errorstr,"syntax error, unexpected ';'")!=0){
			//cout<<"caught unexpected ';'"<<endl;
			getResult(";");
		}	
	}*/
	clearResult();
	}while(1);
	return 0;
}
