#ifndef PARSEDQUERY_H
#define PARSEDQUERY_H
#include <string>
#include <vector>
#include <malloc.h>
#include <cctype>
#include <cstdlib>
#include <iostream>
#include "../Common/DataType.h"
#include "../Common/ActionCodes.h"
#include "./infix2postfix.h"

#define DEFAULT 0
#define RANGE 1
#define RANDOM 2

using namespace std;

struct defaultValue{
	bool isDefault;
	string value;
};	

 class parsedQuery{
	
	private:
	//private variables
	int action;			// Possible actions: Create, Use, Insert, Select,
	string table;			// Table name
	vector<string> columns;	// String of Column names
	string database;		// Database name
	vector<int> types;		// String of Type names
	vector<string> datavalues; //String of Data values stored as string
	void  **values;		// Array of void pointers to each value of arbitary type
	string index_name;	//Specifies Index name 
	vector<bool> notNull;	// Container of indices mapping to columns which indicates nullable columns
	vector<bool> primaryKey;	// Container of indices mapping to columns which indicates primary key columns
	vector<bool> unique;		// Container of indices mapping to columns which indicates unique columns
	vector<int>  maxsize;		//Contains max size for each column
	vector<string> order_column_names;	// Container of Column names by which the Select query must be ordered
	vector<bool> desc_ordering;	// Specifies which of the ordering colunms are supposed to be in descending order
	vector< string > condition_arr; //Contains the operands and operator of condition array in post-order
	vector< string > delete_cols; // Specifies the Columns to Delete
	vector< defaultValue > defaultValues;
	int recordType;	//When action is ADD_RECORDS, Can take values - RANDOM, DEFAULT, RANGE
	int start_range;	//When recordType is RANGE, specifies the start of range values
	int noOfRecords;	//Specifies no of records to add, When action is ADD_RECORDS
	string filename;	//Specifies path of the text file containing all the queries
	 vector<int> updateExprIndex; //Gives index of conditionArray for the column which are to be set to infix expressions, else -1
	 vector< vector<string> > conditionArrays; //Holds the list of condition arrays for each of the update columns

	//private methods
	void** 		getValues();
	
	public:
	//public variables
	int noOfColumns;		// Number of Columns for Create table, and Select
	bool createTable;		// indicates if action is create table
	bool createDatabase;	// indicates if action is create database
	bool drop_table;		// indicates if action is drop table
	bool desc_table;		// indicates if action is desc table
	bool drop_database;	// indicates if action is drop database
	bool delete_table;		// indicates if action is delete table
	bool use;				// indicates if action is use
	bool insert;			// indicates if action is insert
	bool select;			// indicates if action is select
	bool create_index;		// indicates if action is create index
	bool showDatabase;	// indicates if action is show database
	bool showTables;		// indicates if action is show tables
	bool alterTable;		// indicates if action is alter table add/delete column
	bool updateTable;		// indicates if action is update table
	bool sourceFile;		// indicates if action is source file
	char* errorstr;		// contains error string
	bool conditionExists;	//Specifies if condition is given
	bool orderBy;		//Specifies that order by has been given by user
	
	bool parseError;
	
	//constructor
	parsedQuery(){
		values = NULL;
		noOfColumns = 0;
		createTable = false;
		createDatabase = false;
		showDatabase = false;
		showTables = false;
		drop_database = false;
		drop_table = false;
		desc_table = false;
		use = false;
		insert = false;
		select = false;
		create_index = false;
		errorstr = NULL;
		conditionExists = false;
		parseError = false;
		orderBy = false;
		recordType = DEFAULT;
		start_range = -1;
		noOfRecords = 0;
		filename.clear();
	}
	
	//public methods
	int			getAction();
	string		getTable();
	string		getIndexName();
	vector<string> 	getColumns();
	string 		getDatabase();
	vector<int> 	getTypes();
	vector<string> 	getDataValues();
	int 			getNoOfColumns();
	vector<bool> 	getNotNulls();
	vector<bool> 	getPKs();
	vector<bool> 	getUnique();
	vector<defaultValue>	getDefaultValues();
	vector<int>		getMaxSize();
	vector<string>	getConditionArray();
	vector< vector<string> > getConditionArrays();
	vector<string>	getDeleteCols();
	vector<string>	getOrderByCols();
	vector<bool>	getDescOrder();
	string 		getSourceFile();
	int 			getRecordType();
	int 			getStartRange();
	int			getNoOfRecords();
	vector<int>	getUpdateExprIndex();
	int			getNoConditionArrays();
	void 		setAction(int act);
	void 		setTable(string tabl);
	void		setIndexName(string index);
	void 		setColumns(vector<string> col);
	void 		setConditionArray(vector<string> post_stk);
	void 		setConditionArray(char *strarr);
	void 		setDatabase(string db);
	void 		setTypes(vector<int> typ);
	void 		setValues(vector<string> valu);
	void 		setDataValues(vector<string> values);
	void 		setNoOfColumns(int nocol);
	void 		setNotNulls(vector<bool> nonull);
	void 		setPKs(vector<bool> pks);
	void 		setUnique(vector<bool> uniqueC);
	void		setOrderByCols(vector<string> orderCols);
	void		setDescOrder(vector<bool> descCols);
	void		setDefaultValues(vector< defaultValue > defaultV);
	void 		setMaxSize(vector<int> maxSize);
	void		setParseError(){parseError = true;}
	void		setDeleteCols(vector<string> deleteCols);
	void		setRecordType(int type);
	void 		setStartRange(int range);
	void		setNoOfRecords(int no);
	void 		setSourceFile(string file);
	void		setUpdateExprIndex(vector<int> updateExprI);
	void		setConditionArrays(char *strarr);
	bool		isParseError(){return parseError;}
	
	~parsedQuery(){
		
		//free (*values);
		//noOfColumn = 0;
		orderBy = false;
		action = 0;
		recordType = DEFAULT;
		start_range = -1;
		noOfRecords = 0;		
		values = NULL;
		vector<string>().swap(columns);
		vector<bool>().swap(notNull);
		vector<bool>().swap(primaryKey);
		vector<bool>().swap(unique);
		vector<int>().swap(types);
		vector<string>().swap(datavalues);
		errorstr = NULL;
		//	vector<string>	opt_unique_array;
		//	vector<string>	opt_pk_array;
		vector<string>().swap(order_column_names);
		vector<bool>().swap(desc_ordering);
		vector<string>().swap(condition_arr);
		delete_cols.clear();
		defaultValues.clear();
		updateExprIndex.clear();
		conditionArrays.clear();
		//condArray.swap();
	}
	
};

	int parsedQuery::getAction(){
		return action;
	}

	string parsedQuery::getTable(){
		return table;
	}
	
	vector<string> parsedQuery::getColumns(){
		return columns;
	}
	
	vector<string> parsedQuery::getConditionArray(){
		return condition_arr;
	}
	
	string parsedQuery::getDatabase(){
		return database;
	}
	
	vector<int> parsedQuery::getTypes(){
		return types;
	}

	vector<string> parsedQuery::getDataValues(){
		return datavalues;
	}
	
	string parsedQuery::getIndexName(){
		return index_name;
	}
	
	int parsedQuery::getNoOfColumns(){
		return noOfColumns;
	}
	
	vector<bool> parsedQuery::getNotNulls(){
		return notNull;
	}

	vector<bool> parsedQuery::getPKs(){
		return primaryKey;
	}
	
	vector<bool> parsedQuery::getUnique(){
		return unique;
	}

	vector<defaultValue>	parsedQuery::getDefaultValues(){
		return defaultValues;
	}
	
	vector<int> parsedQuery::getMaxSize(){
		return maxsize;
	}

	vector< string > parsedQuery::getDeleteCols(){
		return delete_cols;
	}
	
	vector<string> parsedQuery::getOrderByCols(){
		return order_column_names;
	}
	
	vector<bool> parsedQuery::getDescOrder(){
		return desc_ordering;
	}		
	
	int parsedQuery::getRecordType(){
		return recordType;
	}
	
	int parsedQuery::getNoOfRecords(){
		return noOfRecords;
	}
	
	string parsedQuery::getSourceFile(){
		return filename;
	}

	vector<int>	parsedQuery::getUpdateExprIndex(){
		return updateExprIndex;
	}

	vector< vector<string> > parsedQuery::getConditionArrays(){
		return conditionArrays;
	}
	
	int parsedQuery::getNoConditionArrays(){
		return conditionArrays.size();
	}
	
	int parsedQuery::getStartRange(){
		return start_range;
	}
		
	void parsedQuery::setAction(int act){
		action = act;
	}
	
	
	void parsedQuery::setTable(string tabl){
		table = tabl;
	}
	
	void parsedQuery::setIndexName(string index){
		index_name = index;
	}
		 
	void parsedQuery::setColumns(vector<string> col){
		columns = col;
	}
	
	void parsedQuery::setConditionArray(vector<string> carr){
		condition_arr = carr;
	}
	
	void parsedQuery::setConditionArray(char *strarr){
		string infix_str(strarr);
		string post_str;
		vector< string > post_stk;
		infix2postfix( infix_str, post_str, post_stk);
		condition_arr = post_stk;
		if(condition_arr.size() > 0)
		conditionExists = true;
		//CONDITION_ARRAY_TRACE && cout<<"Postfix String: "<<post_str<<endl;
		if(CONDITION_ARRAY_TRACE){
			cout<<"Postfix Expr"<<endl;
			for (int j=0;j<post_stk.size();j++){
					cout<< "\t" << post_stk[j]<<endl;
			}
  		}
	}
	
	void parsedQuery::setConditionArrays(char *strarr){
		string infix_str(strarr);
		string post_str;
		vector< string > post_stk;
		infix2postfix( infix_str, post_str, post_stk);
		//cout<<"Setting postfix string: "<<post_str<<endl;
		conditionArrays.push_back(post_stk);
		//CONDITION_ARRAY_TRACE && cout<<"Postfix String: "<<post_str<<endl;
		if(CONDITION_ARRAY_TRACE){
			cout<<"Update Postfix Expr:"<<conditionArrays.size()<<endl;
			for (int j=0;j<post_stk.size();j++){
					cout<< "\t" << post_stk[j]<<endl;
			}
  		}
	}
	
	void parsedQuery::setDatabase(string db){
		database = db;
	}
	
	void parsedQuery::setTypes(vector<int> typ){
		types = typ;
	}
	void	parsedQuery::setDataValues(vector<string> values){
		datavalues = values;
	}
		
	void parsedQuery::setValues(vector<string> valu){
		int i=0;
		int size = 0;
		int *typeCheck = (int*)calloc(size,sizeof(int));
		size = valu.size();
		i=0;
		
		values = (void**)calloc(noOfColumns,sizeof(void*));
		//values = new void*[noOfColumns];
		
		//setting values when types have been defined by lexer
		int *itmp; short *stmp; float *ftmp; long *ltmp; double *dtmp; char *ctmp;
		int errnum = 0;
		char remove = '/';
		string str;
		int ival; short sval; float fval; long lval; double dval;  
		cout<<"noOfColumns: "<<size<<endl;
		for(i=0;i<size;i++){
	
			switch(types[i]){
					case INTTYPE:	cout<<"Converting from string: "<<valu[i]<<" to int: ";
								ival = atoi((valu[i]).c_str());
								itmp = &ival;
								cout<<*itmp<<endl;
								
								if(ival == 0) errnum = 1;
								values[i] = (void*)new int(ival);
								break;
					case SHORTTYPE:	cout<<"Converting from string: "<<valu[i]<<" to short: ";
								sval = (short)atoi((valu[i]).c_str());
								stmp = &sval;
								cout<<*stmp<<endl;
								
								if(sval == 0) errnum = 1;
								values[i] = (void*)new short(sval);
								break;

					case DATETYPE: cout<<"Converting from string: "<<valu[i]<<" to Date: ";
								str = valu[i];
								for(string::iterator it = str.begin(); it!=str.end();){
							     	 if( (*it) == remove )
							     	    it = str.erase( it );
								      else
								      it++;
								}
								ival = atoi(str.c_str());
								itmp = &ival;
								cout<<*itmp<<endl;
								if(ival == 0) errnum = 1;
								values[i] = (void*)new int(ival); 
								break;
							
			     	case FLOATTYPE: cout<<"Converting from string: "<<valu[i]<<" to Float: "; 
			     				fval = (float)atof((valu[i]).c_str());
	         						ftmp = &fval;
	         						cout<<*ftmp<<endl;
								if(fval == 0) errnum = 1;
								values[i] = (void*)new float(fval);//(void*)ftmp;
								//values[i] = fval;
								break;
							
					case LONGTYPE: cout<<"Converting from string: "<<valu[i]<<" to Long: ";
								lval = atol((valu[i]).c_str());
								ltmp = &lval;
								cout<<*ltmp<<endl;
								if(lval == 0) errnum = 1;
								values[i] = (void*)new long(lval);
								break;
														
					case DOUBLETYPE: cout<<"Converting from string item:"<<i<<" "<<valu[i]<<" to Double: ";
								  dval = strtod((valu[i]).c_str(), NULL);
								  dtmp = &dval;
								  cout<<dval<<endl;
								  if(dval == 0) errnum = 1;
								  values[i] = (void*)new double(dval);
								  break;
							
					default:  cout<<"string item:"<<i<<endl;
							ctmp = (char *)((valu[i]).c_str());
							values[i] = (void*)ctmp;			
				}	
		    }
		}
		
	void	parsedQuery::setNoOfColumns(int nocol){
		noOfColumns = nocol;
	}
	
	void	parsedQuery::setNotNulls(vector<bool> nonull){
		notNull = nonull;
	}	
	
	void	parsedQuery::setPKs(vector<bool> pks){
		primaryKey = pks;	
	}
	
	void	parsedQuery::setMaxSize(vector<int> maxSize){
		maxsize = maxSize;	
	}
	
	void	parsedQuery::setUnique(vector<bool> uniqueC){
		unique = uniqueC;
	}
	
	void	parsedQuery::setOrderByCols(vector<string> orderCols){
		order_column_names = orderCols;
	}
	
	void	parsedQuery::setDescOrder(vector<bool> descCols){
		desc_ordering = descCols;
	}
	
	void	parsedQuery::setDefaultValues(vector< defaultValue > defaultV){
		defaultValues = defaultV;
	}
	
	void	parsedQuery::setDeleteCols(vector<string> deleteCols){
		delete_cols = deleteCols;
	}
	
	void	parsedQuery::setRecordType(int type){
		recordType = type;
	}
	
	void 	parsedQuery::setStartRange(int range){
		start_range = range;
	}
		
	void	parsedQuery::setNoOfRecords(int no){
		noOfRecords = no;
	}
	
	void	parsedQuery::setSourceFile(string file){
		filename = file;
	}
	
	void	parsedQuery::setUpdateExprIndex(vector<int> updateExprI){
		updateExprIndex = updateExprI;
	}
#endif
