
#ifndef INFIX2POSTFIX
#define INFIX2POSTFIX

#include <stdio.h>  
#include <string.h>  
#include <ctype.h>  
#include <iostream>
#include <sstream>
#include <string.h>
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include "../Common/Settings.h"
#include "/usr/include/boost/regex.hpp"

#define MAXINSIZE 255

using namespace std;

bool isOperator(string word){
	bool success = false;
	if( !word.compare("+") || !word.compare("-") || !word.compare("*") || !word.compare("/") || !word.compare(">") || !word.compare("<") || !word.compare("<=") || !word.compare(">=") || !word.compare("!=") || !word.compare("AND") || !word.compare("OR") || !word.compare("LIKE") || !word.compare("NOTLIKE") || !word.compare("%") || !word.compare("=") || !word.compare("(") || !word.compare(")") || !word.compare("!"))
	success = true;
	return success;
}

string removeBSlash(string str){

	char remove='/';
	string str1(str);
	for(string::iterator it = str1.begin(); it!=str1.end();){
		if( (*it) == remove )
			it = str1.erase( it );
		else
			it++;
		}
	return str1;	
}

bool isNumber(string word, double &num){
	//cout<<"Checking if: "<<word<<" is number"<<endl;
	bool success = false,decimal = false;
	int i=0,c;
	/*if(word[0] == '\'' && word[word.size() - 1] == '\''){
		word.erase(0,1);
		word.erase(word.size()-1,1);
	}
	else if(word[0] == '"' && word[word.size() - 1] == '"'){
		word.erase(0,1);
		word.erase(word.size()-1,1);
	}*/
	for(i=0;i<word.size();i++){

		if(i==0){
			if(word[i] == '+' || word[i] == '-')
				continue;
		}

		if(!isdigit(word[i]) && word[i] != '.'){
			break;
		}
		else if( word[i] == '.' )
			if(!decimal)
				decimal = true;
			else
				break;
	}
	if(i == word.size() )
		success = true;
	//cout<<"Converting to Double "<<endl;
	
	/*boost::regex re;
	try{
	      // Set up the regular expression for case-sensitive
	      string regexp("[+-]?\[0-9]+(\\.[0-9]+)?([Ee][+-][0-9]+)?");
		re.assign(regexp, boost::regex_constants::basic);
	}
	catch (boost::regex_error& e){
		cout << " is not a valid regular expression: \""<< e.what() << "\"" << endl;
	}
	if (boost::regex_match(word, re)){
		//cout<<"Matches"<<endl;
		num = strtod(word.c_str(),NULL);
		success = true;
	}*/
	if(success == true)
		num = strtod(word.c_str(),NULL);
	return success;
}
	
bool inQoutes(string word){
	bool success = false;
	int length = word.length();
	
	if((word[0] == '\'' || word[0] == '"') && (word[length-1] == '\'' || word[length-1] == '"' )){
		success= true;
	}	
	return success;
}

bool calculateVal(double dval1 , double dval2, string opr, double & res ){
	
	bool success = false;
	//cout<<"Operand1: "<<dval1<<" Operand2: "<<dval2<<" Operator: "<< opr;
	if(!opr.compare("+")){
		res = dval1 + dval2;
		success = true;
	}	
	else if(!opr.compare("-")){
		res = dval1 - dval2;
		success = true;
	}	
	else if(!opr.compare("*")){
		res = dval1 * dval2;
		success = true;
	}	
	else if(!opr.compare("/")){
		if(dval2 != 0)
		res = dval1 / dval2;
		else
		res = 0;
		success = true;
	}	
	else if(!opr.compare("%")){
		res = (int)dval1 % (int)dval2;
		success = true;
	}
	//cout<<" Result: "<<res<<endl;
	return success;
}

int numberOfDecimals(double value){
	int no = 0;
	double d = (value - floor(value)) * 10;
	while( (d - floor(d)) != 0){
		no++;
		d = d - floor(d);
		d = d * 10;
	}
	return no;
}

bool evalMathExpr(string opnd1, string opnd2, string opr,string &result){

	//Evaluating where cond
	stringstream out("");
	double dval1,dval2,res;
	dval1 = dval2  = res = 0;
	bool success = false;
	bool opnd1String = false;
	bool opnd2String = false;	
	size_t pos = string::npos;
	string str("\"");
	char* cstr = new char[ (opnd1 > opnd2)? opnd1.length(): opnd2.length() ];
	
	//cout<<"Opnd1: "<<opnd1<<" , Opnd2: "<<opnd2<<endl;
	if( isNumber(opnd1,dval1) && isNumber(opnd2,dval2) ){
		//cout<<"here1"<<endl;
		success = calculateVal(dval1,dval2,opr,res);
		out << res;
		result = out.str();
	}
	else{
		if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
			opnd1.erase(0,1);
			opnd1.erase(opnd1.size()-1,1);
			opnd1String = true;
		}
		if(opnd2[0] == '"' && opnd2[opnd2.size()-1] == '"'){
			opnd2.erase(0,1);
			opnd2.erase(opnd2.size()-1,1);
			opnd2String = true;
		}
		if(opnd2[0] == '\'' && opnd2[opnd2.size()-1] == '\''){
			opnd2.erase(0,1);
			opnd2.erase(opnd2.size()-1,1);
			opnd2String = true;
		}
		if(opnd1String && opnd2String){
			if(!opr.compare("+")){
				//cout<<"here2"<<endl;			
				opnd1.append(opnd2);
				success = true;
				str.append(opnd1);
				str.append("\"");			
				result = str;
				//cout<<"Result: "<<result<<endl;
			}
			else if(!opr.compare("-")){
				//cout<<"Erasing all occurance of: " << opnd2 << " from " << opnd1<<endl;			
				pos = opnd1.find(opnd2);
				while(pos != string::npos){
					opnd1.erase(pos,opnd2.size());
					pos = opnd1.find(opnd2);
				}
				success = true;
				str.append(opnd1);
				str.append("\"");
				result = str;
				//cout<<"Result: "<<result<<endl;
			}
		}
		else if(opnd1String && !opnd2String && isNumber(opnd2,dval2) ){
			if(isNumber(opnd1,dval1)){
				//cout<<"here3"<<endl;
				success = calculateVal(dval1,dval2,opr,res);
				//cout<<"res: "<<res<<endl;
				//cout<<"No of decimals: "<<numberOfDecimals(res)<<endl;
				if( numberOfDecimals(res) == 0)
					sprintf(cstr,"%.0f",res);
				else
					sprintf(cstr,"%.4f",res);
				result = cstr;
				//cout<<"No of decimals: "<<numberOfDecimals(res)<<endl;
				//cout<<"result: "<<cstr<<endl;
				//result = out.str();
			}
		}
		else if(opnd2String && !opnd1String && isNumber(opnd1,dval1) ){
			if(isNumber(opnd2,dval2)){
				//cout<<"here4"<<endl;
				success = calculateVal(dval1,dval2,opr,res);
				if( numberOfDecimals(res) == 0)
					sprintf(cstr,"%.0f",res);
				else
					sprintf(cstr,"%.4f",res);
				result = cstr;
				//cout<<"result: "<<res<<endl;				
				//out << res;
				//result = out.str();
				//cout<<"Result: "<<result<<endl;
			}
		}
	}
	
	delete cstr;
	return success;
}

string evalComparExpr(string opnd1, string opnd2, string opr){

	double dval1,dval2,res;
	dval1 = dval2  = res = 0;
	bool success = false;
	string result("false");
	int val = 0;
	string strRes;
	//cout<<"Entered evalComparExpr"<<endl;
	CONDITION_ARRAY_TRACE && cout<<"Opnd1: "<<opnd1<<" ,Opnd2: "<<opnd2<<" ,Opr: "<<opr<<endl;
	if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
		opnd1.erase(0,1);
		opnd1.erase(opnd1.size()-1,1);
	}
	if(opnd1[0] == '\'' && opnd1[opnd1.size()-1] == '\''){
		opnd1.erase(0,1);
		opnd1.erase(opnd1.size()-1,1);
	}
	if(opnd2[0] == '"' && opnd2[opnd2.size()-1] == '"'){
		opnd2.erase(0,1);
		opnd2.erase(opnd2.size()-1,1);
	}
	if(opnd2[0] == '\'' && opnd2[opnd2.size()-1] == '\''){
		opnd2.erase(0,1);
		opnd2.erase(opnd2.size()-1,1);
	}
	if(isNumber(opnd1,dval1) && isNumber(opnd2,dval2)){
		if( !opr.compare("<") && dval1 < dval2 )
			result = "true";
		else if( !opr.compare(">") && dval1 > dval2)
			result = "true";
		else if( !opr.compare("=") && dval1 == dval2)
			result = "true";
		else if( !opr.compare(">=") && dval1 >= dval2)
			result = "true";
		else if( !opr.compare("<=") && dval1  <= dval2)
			result = "true";
		else if( !opr.compare("!=") && dval1  != dval2)
			result = "true";
	}
	else{
	
		if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
			opnd1.erase(0,1);
			opnd1.erase(opnd1.size()-1,1);
		}
		if(opnd2[0] == '"' && opnd2[opnd2.size()-1] == '"'){
			opnd2.erase(0,1);
			opnd2.erase(opnd2.size()-1,1);
		}
		 val = opnd1.compare(opnd2);
		 if( !opr.compare(">") && val > 0)
		 	result = "true";
		 else if( !opr.compare("<") && val < 0)
		 	result = "true";
		 else if( !opr.compare("=") && val == 0)
		 	result = "true";
		 else if( !opr.compare(">=") && val >= 0)
		 	result = "true";
		 else if( !opr.compare("<=") && val <= 0)
		 	result = "true";
		 else if( !opr.compare("!=") && val != 0)
		 	result = "true";
		}
	if( !result.compare("true") )
		strRes = "1";
	else
		strRes = "0";
	return strRes;
}

string evalLogicalOperator(string opnd1,string opnd2,string oper){
	bool operand1 = true;
	bool operand2 = true;
	int result = 0;
	string strRes;
	if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
		opnd1.erase(0,1);
		opnd1.erase(opnd1.size()-1,1);
	}
	if(opnd2[0] == '"' && opnd2[opnd2.size()-1] == '"'){
		opnd2.erase(0,1);
		opnd2.erase(opnd2.size()-1,1);
	}
	if(!opnd1.compare("0"))
		operand1 = false;
	if(!opnd2.compare("0"))
		operand2 = false;
	if(!oper.compare("AND"))
	result = (operand1 && operand2)?1:0;
	else if(!oper.compare("OR"))
	result = (operand1 || operand2)?1:0;
	
	CONDITION_ARRAY_TRACE && cout<<"Result  of("<<operand1<<" "<<oper<<" "<<operand2<<"):"<<result<<endl;
	
	strRes = ( result == 0) ? "0" : "1";
	return strRes;
}

string evalNotOperator(string opnd1,string oper){
	bool operand1 = true;
	string strRes;
	int result = 0;
	
	if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
		opnd1.erase(0,1);
		opnd1.erase(opnd1.size()-1,1);
	}
	
	if(!opnd1.compare("0"))
		operand1 = false;
	
	result = ( !operand1 )?1:0;	
	strRes = ( result == 0) ? "0" : "1";
	return strRes;
}

string evalLikeOperator(string opnd1, string opnd2, string oper){
	bool operand1 = true;
	bool operand2 = true;
	int result = 0;
	string strRes("0");
	size_t pos = string::npos;
	
	if(opnd1[0] == '"' && opnd1[opnd1.size()-1] == '"'){
		opnd1.erase(0,1);
		opnd1.erase(opnd1.size()-1,1);
	}
	if(opnd2[0] == '"' && opnd2[opnd2.size()-1] == '"'){
		opnd2.erase(0,1);
		opnd2.erase(opnd2.size()-1,1);
	}
	
	//cout<<"Opnd1: "<<opnd1<<" Opnd2: "<<opnd2<<endl;
		if(opnd2[0] == '\'' && opnd2[opnd2.size()-1] == '\''){
			//Operand in S Quotes, so Wild characters dont lose their meaning
			//Wild Characters = % matches any no. of characters or no chars
			//Wild Characters = _ matches single char
			//replacing % and _ with * and 
			opnd2.erase(0,1);
			opnd2.erase(opnd2.size()-1,1);
			result = 0;
			/*
			pos = opnd2.find('%',0);
			while(pos != string::npos){
				opnd2.replace(pos,1,".*");
				pos = opnd2.find('%',pos+1);
			}
			pos = string::npos;
			pos = opnd2.find('_',0);
			while(pos != string::npos){
				opnd2[pos] = '.';
				pos = opnd2.find('_',pos+1);
			}
			*/
			//cout<<"Regex: "<<opnd2<<endl;
			boost::regex re;
			try{
			       // Set up the regular expression for case-sensitive
			       re.assign(opnd2, boost::regex_constants::basic);
			}
			catch (boost::regex_error& e){
			       cout << " is not a valid regular expression: \""<< e.what() << "\"" << endl;
			       if(!opnd1.compare(opnd2))
			       	result = 1;
			       else
			       	result = 0;
			       if(!oper.compare("NOTLIKE"))
			       	result = !result;
			       	
			       strRes = ( result == 0) ? "0" : "1";	
			       return strRes;	
			}
			if (boost::regex_match(opnd1, re)){
				//cout<<"Matches"<<endl;
				result = 1;
			}
		}
		else{
			//Operand in D Quotes, so Wild characters lose their meaning
			//cout<<"In double qoutes"<<endl;
			if(opnd2[0] == '"' && opnd2[opnd2.size() -1] == '"'){
				opnd2.erase(0,1);
				opnd2.erase(opnd2.size()-1,1);
			}	
			if(!opnd1.compare(opnd2))
		       	result = 1;
		       else
		       	result = 0;
		       //cout<<"Compared "<<opnd1<<" and "<<opnd2<<" Result: "<<result<<endl;	
		}
	
       if(!oper.compare("NOTLIKE"))
       	result = !result;
      	strRes = ( result == 0) ? "0" : "1";
       return strRes;
}
bool isComparisionOperator(string item){
	bool success = false;
	if( !item.compare("<") || !item.compare(">") || !item.compare("=") || !item.compare(">=") || !item.compare("<=") || !item.compare("!="))
			success = true;
	return success;
}

bool isMathOperator(string item){
	bool success = false;
	if( !item.compare("+") || !item.compare("-") || !item.compare("*") || !item.compare("/") || !item.compare("%"))
		success = true;
	return success;
}
/*bool isOperand(string word){
	
	
	bool success = false;
	char* cstr = (char*)word.c_str();
	string sre("[a-zA-Z0-9][a-zA-Z0-9@_#!$\"]*[a-zA-Z0-9@_#!$']");
	string sre_double("[a-zA-Z0-9\"][a-zA-Z0-9@_#!$']*[a-zA-Z0-9@_#!$\"]");

	boost::regex re;
	
	try{
         // Set up the regular expression for case-sensitive
         re.assign(sre, boost::regex_constants::basic);
      }
      catch (boost::regex_error& e){
         cout << sre << " is not a valid regular expression: \""<< e.what() << "\"" << endl;
         return false;
      }
      
	if (boost::regex_match(word, re)){
		if( !word.compare("AND") || !word.compare("OR") || !word.compare("NOT") || !word.compare("LIKE") || !word.compare("BETWEEN") || !word.compare("IN") )
			success = false;
		else
		success = true;	
	}
	
	//else if(boost::regex_match(word, re1)){
	//	if( !word.compare("AND") || !word.compare("OR") || !word.compare("NOT") || !word.compare("LIKE") || !word.compare("BETWEEN") || !word.compare("IN") )
	//		success = false;
	//	else
	//	success = true;
	}
	
	return success;
}*/

int priority(string e) 
{  
       int pri = 0;
       if(!e.compare("!"))
      	pri = 6;
       else if(!e.compare("*") || !e.compare("/") || !e.compare("%"))  
           pri = 5;
       else if(!e.compare("+") || !e.compare("-"))  
           pri = 4;  
    	  else if(!e.compare("<") || !e.compare("<=") || !e.compare("=") || !e.compare(">=") || !e.compare(">") || !e.compare("!=") || !e.compare("=") || !e.compare("LIKE"))
    	  	pri = 3;
    	else if(!e.compare("AND"))
      	pri = 2;
       else if(!e.compare("OR"))
      	pri = 1;  
       return pri;  
}

void infix2postfix(string instr,string &poststr, vector<string> &stk){
	
	stack<int, vector<string> > ele_stk;
	int index = 0;
	int i =0,j;
	char* to_str;
	string word;
	string stack_top;
	char pre_char;
	string qword;
	int t;
	
	//size_t i,j;
	
	//Repeat for each word found in expression
	while(i < instr.length() ){
		
		//Literal values between Double quotes
		if( instr[i] == '"'  && i<instr.length()){
			qword.clear();
			j = i + 1;
			pre_char = ' ';
			t = 0;
			qword.append(1,instr[i]);t++;		
			while( j<instr.length() ){
				if( instr[j] == '"' && pre_char != '\\' ){
					qword.append(1,'"');
					break;
				}
				else if( instr[j] == '"' && pre_char == '\\')
					qword.erase(t-1,1);
				qword.append(1,instr[j]);
				pre_char = instr[j];
				j++;t++;
			}
			word = qword;
			//cout<<"After removing '\' word:"<<word<<endl;
			goto check_word;
		}
		
		//Literal values btw single qoute
		else if( instr[i] == '\'' && i<instr.length()){
			qword.clear();
			j = i + 1;
			pre_char = ' ';
			t = 0;
			qword.append(1,instr[i]);t++;
			while( j<instr.length() ){
				if( instr[j] == '\'' && pre_char != '\\' ){
					qword.append(1,'\'');
					break;
				}
				else if( instr[j] == '\'' && pre_char == '\\')
					qword.erase(t-1,1);
				qword.append(1,instr[j]);
				pre_char = instr[j];
				j++;t++;
			}
			word = qword;
			//cout<<"After removing '\' word:"<<word<<endl;			
			goto check_word;
		}
		
		//space or tab - go nxt
		while( (instr[i] == ' ' | instr[i] == '\t') && i<instr.length() )
		i++;
		
		j = i;
		//get next word starting from present index
		while( instr[j] != ' ' && instr[j] != '\t' && j < instr.length() )
			j++;
	
		word.clear();
		word = instr.substr( i , j-i );
		
		
		//is Operand
		check_word:	
		if( isOperator(word) ){
			CONDITION_ARRAY_TRACE && cout<<"\t\tOperator: "<<word<<endl;
			
			if(!word.compare("(")){
				poststr.append(stack_top);
				ele_stk.push("(");
			}	
			else if(!word.compare(")")){
				stack_top = ele_stk.top();
				ele_stk.pop();
				while(stack_top.compare("(")){
					poststr.append(stack_top);
					stk.push_back(stack_top);
					stack_top = ele_stk.top();
					ele_stk.pop();
				}
			}
			else{
				//operator = + - * ... 
				if(ele_stk.size() == 0)
					ele_stk.push(word);
				else{
					stack_top = ele_stk.top();
					while(priority(stack_top) >= priority(word)){
						CONDITION_ARRAY_TRACE && cout<<"top element: "<<stack_top<<endl;
						stk.push_back(stack_top);
						poststr.append(stack_top);
						ele_stk.pop();
						if(ele_stk.empty())
						break;
						else
						stack_top = ele_stk.top();
					}
					ele_stk.push(word);
				}
			}
		}
		else{
		CONDITION_ARRAY_TRACE && cout<<"\tOperand: "<<word<<endl;
		poststr.append(word);
		stk.push_back(word);
		}
		

		i = j + 1;
	}
	while(!ele_stk.empty()){
		stack_top = ele_stk.top();
		ele_stk.pop();
		stk.push_back(stack_top);
		poststr.append(stack_top);
	}
}

/*
int main(){


	char in[MAXINSIZE];
	string poststr;
	string input;
	vector<string> stk;

	cout<<"Enter infix expression:";
	cin.getline(in,MAXINSIZE);
	//cout<<endl<<"Entered in expression:"<<in<<endl;
	input = in;
	infix2postfix(in,poststr,stk);
	cout<<"Postfix Expr:"<<poststr<<endl;
	for (int i=0;i<stk.size();i++)
  	{
     		cout<< " " << stk[i]<<endl;
  	}
	
	return 0;
}
*/
#endif
