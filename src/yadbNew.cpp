#include "BufferManager/buffermanager.h"
//#include "BufferManager/Frame.h"
#include <iostream>
#include <cstdio>
#include "Common/Settings.h"
#include "Common/Priorities.h"

using namespace std;

void getIntInput(int* temp){
    while(!(std::cin >> *temp))
    {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                cout << "Invalid Input\nRe-enter : ";
    }
}

void getCharInput(char* temp){
    while(!(std::cin >> *temp))
    {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                cout << "Invalid Input\nRe-enter : ";
    }
}

int main(int argc, char* argv){

	settings::init();

        BufferManager bm;
        //frame* temp;
	char* schema;
	schema = new char[FILENAME_SIZE];

	char buffer[PAGE_SIZE_IN_BYTES];

        int choice = 1;
        int pageNo = -1;
        char toWrite = 'a';
	int verbose = 1;

	//1 pages
	if(!bm.init((sizeof(Frame) + PAGE_SIZE_IN_BYTES) * 3)){
		cout << "Cannot initialize buffer\n";	
		exit(0);
	}

	//cout << "Schema File : ";
	//fflush(stdin);
	//gets(schemafile);
	sprintf(schema,"%s","intialschema");
	bm.setSchemaFile(schema);

        while(true){

            cout << "\n\n1 : Set verbose mode \n";
            cout << "2 : Read a page \n";
            cout << "3 : Write a page\n";
	    cout << "4 : Commit Buffer\n";
	    cout << "5 : Change Schema File\n";
	    cout << "6 : BufferManager Shutdown\n";
            cout << "0 : Exit\n";
            cout << "Choice : ";

            if(!(std::cin >> choice))
            {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
                //cout << "Invalid choice\nChoice : ";
                choice = -1;
                break;
            }


            switch(choice){

                case 1 :    cout << "Mode ( 0=Off, 1=On): ";
			    getIntInput(&verbose);
			    VERBOSE = verbose;
			    break;

                case 2 :    cout << "Page number to read : ";                            
                            getIntInput(&pageNo);
                            if(bm.read(pageNo, buffer, DIR_PAGE_PRIORITY)){
							    cout << "Page " << pageNo << " contents :\n";
							    for(int i=0; i < PAGE_SIZE_IN_BYTES; i++){
									cout << buffer[i];				
								}
				    			cout << endl;
							}else{
								cout << "Read Error\n";
							}

                            break;

                case 3 :    cout << "Page number to write : ";
                            getIntInput(&pageNo);

			    //temp = bm.read(pageNo);
			    //cout << "Frame number to write : ";
			    //int frameNo;
			    //getIntInput(&frameNo);
			    //temp = &bufferPool[frameNo]);

                            cout << "Dummy character to pad : ";
                            getCharInput(&toWrite);
                           

                            for(int i=0; i < PAGE_SIZE_IN_BYTES; i++){
                                buffer[i] = toWrite;
                            }
			    
                            bm.write(pageNo,buffer, DIR_PAGE_PRIORITY);
                            break;

		case 4 :    bm.commitBuffer();
			    break;

		case 5 :    fflush(stdin);
			    cout << "Schema File : ";
			    //gets(schemafile);
			    cin >> schema;
			    bm.setSchemaFile(schema);
			    break;

		case 6 :    bm.shutdown();
			    break;

                case 0 :    exit(0);
                            break;

                default :   cout << "Invalid choice\n\n";
                            continue;

            }

        }
}
