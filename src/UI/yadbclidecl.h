#ifndef YADBDECL_CLI_H
#define YADBDECL_CLI_H

class yadbCli{

	private:	
	DataManager dm;
	
	void settingsMode();
	void sqlMode();
	void handleAction(parsedQuery* & pq);
	
	public:
	bool init(int); //ram size to use
	void start();
	yadbCli();
	~yadbCli();
};

#endif
