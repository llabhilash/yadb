using namespace std;

#ifndef YADB_CLI_DEFN_H
#define YADB_CLI_DEFN_H
//==========================================================
yadbCli::yadbCli(){}
//==========================================================
yadbCli::~yadbCli(){}
//==========================================================
bool yadbCli::init(int ramSizeToUseInBytes){

	settings::init();

	if(!dm.init(ramSizeToUseInBytes)){
		std::cout << "Unable to initialize to DataManager\n";
		return false;
	}
	
	return true;

}

//==========================================================
void yadbCli::start(){

	string choice = "";
	int choiceval;

	while(true){
		cout << "\n\n=====YADB=====\n";
		cout << "1 : Settings and Stats\n";
	    	cout << "2 : SQL Mode\n\n";	    	
		cout << "Choice : ";
		
		cin >> choice;
		
		if(choice.compare("quit") == 0  || choice.compare("exit") == 0)
			choiceval = -1;
		else{			
			choiceval = atoi(choice.c_str());	
			if(choiceval < 1 || choiceval > 2){
				cout << "Invalid choice\n";
				continue;
			}
		}
			
		
		switch(choiceval){
		
			case 1: settingsMode();
				break;
			
			case 2: sqlMode();
				break;
				
			case -1: if(dm.shutdown()){
					cout << "Bye\n";
					exit(0);
				}else{
					cout << "Unable to shutdown DataManager\n";
				}
				break;
				
			default: break;
		}
	
	}
	

}
//==========================================================
void yadbCli::settingsMode(){

	string choice = "";
	int choiceval;
	string mode = "";
	cout << endl;		
	
	
	while(true){
		cout << "\n\n=====SETTINGS and STATS=====\n";
		cout << "1  : Set Verbose Mode\n";
		cout << "2  : Set Condition Array Trace\n";
		cout << "3  : Set Parser Function trace\n";
		cout << "4  : Set Buffer-Manager Settings\n";
		cout << "5  : Set Data-Manager Function trace\n";		
		cout << "6  : Set Indexer Function trace\n";		
		//cout << "7  : Set Buffer Manager Size\n";
		//cout << "8  : Show Buffer Manager Statistics\n";
		//cout << "9  : Commit Buffer Manager\n";
		//cout << "10 : ReplacementPolicy\n\n";
		cout << "0  : Back\n\n";
	    	cout << "Choice : ";
		
		cin >> choice;
		
		if(choice.compare("quit") == 0  || choice.compare("exit") == 0|| choice.compare("0") == 0)
			choiceval = -112918329;
		else{
			choiceval = atoi(choice.c_str());
			if(choiceval < 1 || choiceval > 6){
				cout << "Invalid choice\n";
				continue;
			}
		}
		
		
		switch(choiceval){
		
			case 1: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				VERBOSE = atoi(mode.c_str());
				break;
				
			case 2: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				CONDITION_ARRAY_TRACE = atoi(mode.c_str());				
				break;				

			case 3: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				PARSER_FUNC_TRACE = atoi(mode.c_str());				
				break;
				
			/*case 4: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				BM_FUNC_TRACE = atoi(mode.c_str());
				break;
			*/
			case 4:
				dm.showBMWorking();
				break;
				
			case 5: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				DM_FUNC_TRACE = atoi(mode.c_str());
				break;

			case 6: cout << "Mode ( 0=Off, 1=On): ";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				INDEX_FUNC_TRACE = atoi(mode.c_str());
				break;
			
			/*
			case 7: cout << "Set new size (KB): ";
				
				cin >> mode;

				if( atoi(mode.c_str()) <= 0 ){
					cout << "\nInvalid Input\n";
					continue;
				}
				if(!dm.init(atoi(mode.c_str()))){
					cout << "Cannot initialize buffer\n";
				}
				break;
		
			case 8:	dm.showBMHitRatio(); 
				break;
			
			case 9:	if(!dm.commitBM()){
					cout << "Cannot commit buffer\n";
				}
				break;

			case 10:
				cout << "\n0: LRU\n1: Product of priority, hit count, time\n2: MRU\n\npolicy :";
				
				cin >> mode;
				if( ( atoi(mode.c_str()) != 1  ) && (atoi(mode.c_str()) != 0) && (atoi(mode.c_str()) != 2) ){
					cout << "\nInvalid Input\n";
					continue;
				}
				REPLACEMENT_POLICY = atoi(mode.c_str());
				break;
			*/

			case -112918329: return;			
				break;
		}
	}
}

//==========================================================
void yadbCli::sqlMode(){
	parsedQuery* result = NULL;
	parsedQuery* tmpres = NULL;
	int lineno = 0;
	string sql, sql_tmp;
	char* action=NULL;
	char* delimit = (char*)" ";
	char c;
	char* cstr = new char[500];
	int i=0, j=0;
	bool query_com = true;
	int inSQuotes = 0;
	int inDQuotes = 0;
	bool sQuote = false;
	bool dQuote = false;
	char prev_char;
	time_t starttime,endtime;
	double clock_start, clock_end;
	timeval start,end;
	double diffintime;
	double secs;
	double mins;
	int isecs;
	cout << endl << endl;
	string line;
	
	//consume \n	
	getline(cin,sql,'\n');
	
	while(true){
		if(query_com){
			i=0;
			inSQuotes = 0;
			inDQuotes = 0;
			delete[] cstr;
			prev_char = ' ';
			sQuote = false;
			dQuote = false;
			cstr = new char[500];
			cout << "\n{YaDB-SQL} > ";
		}
		else
			cout << "           > ";
		/*
		j=0;
		sql_tmp.clear();
		getline(cin,sql_tmp,'\n');
		while( j < sql_tmp.size() ){
			c = sql_tmp[i];
		*/
		while( (c = getchar()) != '\n'){	
			if( c == '\'' && prev_char != '\\' && !dQuote){
				if(sQuote)
					sQuote = false;
				else
					sQuote = true;
			}
			/*else if(c == '\'' && prev_char == '\\'){
				i--; //	this takes the query back by one char, so the previous char(\) will be omitted
				    //ex - xyz\'s will become xyz's
			}*/
			else if(c == '"' && prev_char != '\\'  && !sQuote){
				if(dQuote)
					dQuote = false;
				else
					dQuote = true;
			}
			/*else if(c == '"' && prev_char == '\\'){
				i--; //	this takes the query back by one char, so the previous char(\) will be omitted
				    //ex - xyz\'s will become xyz's		
			}*/
			else if(c == ';' && ( !sQuote && !dQuote ) )
				query_com = true;
			else
				query_com = false;
			prev_char = c;
			cstr[i] = c;
			i++;
			//j++;
		}
		
		cstr[i] = '\0';
		
		if( strcmp(cstr,"quit") == 0 || strcmp(cstr,"exit") == 0){
			cout << endl;
			delete[] cstr;
			return;
		}

		
		//replacing newline with space
		//cstr[i] =' ';
		//i++;
		
		if(!query_com || strchr(cstr,';') == NULL)
			continue;
		
		//cstr[i] = '\n';
		//i++;
		cstr[i] = '\0';
		sql = cstr;
		//cout<<"Sql: "<<cstr<<endl;
		
		if(sql.compare("") == 0){
			continue;
		}
		
		/*
		action = NULL;
		string tempsql = sql;
		action = strtok((char*)tempsql.c_str(),delimit);
		string tempaction(action);
		
		if(tempaction.compare("system") == 0){
			//system command.. run it
			if(sql.size()>6){
				string temp(&sql.c_str()[7]);
				//cout << "Command : " << temp << "\n";
				system(temp.c_str());
			}
			
			continue;
		}
		*/
		
		if(sql.compare("clear;") == 0){
			system("clear");
			continue;		
		}
		
		/*if(sql.compare("show columns;") == 0){
			dm.handleShowColumns((*result));
			continue;
		}*/
		
		//cout << "Input SQL : " << sql << endl;
		
		if(result!= NULL){
			//delete result;
			result = NULL;
		}
			
		//result = (parsedQuery*)new parsedQuery;
		result = getResult(sql);
		
		if(result->isParseError()){
			cout << "\n\nThere is a syntax error in the input SQL statement\n";
			continue;
		}
		ifstream sqlSourceFile;
		//Handle different Actions
		switch(result->getAction()){
							
			case SOURCE_FILE	:	//Read file pq->getSourceFile 
							starttime = time( NULL );
							sqlSourceFile.open( (pq->getSourceFile()).c_str() );
							if (sqlSourceFile.is_open())
							{
								lineno = 0;
								while (sqlSourceFile.good()){
									if( sqlSourceFile.get() != EOF ){
									sqlSourceFile.unget();
									tmpres = NULL;
									getline (sqlSourceFile,line);
									lineno++;
									tmpres = getResult(line);
									if(tmpres->isParseError()){
										cout << "\n\nThere is a syntax error in the SQL statement in line: "<<lineno<<" from the File: "<<pq->getSourceFile()<<"\n";
										break;
									}
									
									if((lineno%500) == 0)
										cout << "No of Queries ::::::::::::::::::::::::     " << lineno << endl;

									handleAction(tmpres);
									}
								}
								sqlSourceFile.close();
							}
  							else
								cout << "Unable to open file: " << pq->getSourceFile() << endl; 
							endtime = time(NULL);	
							break;
								
			default:	//starttime = time(NULL);
					gettimeofday(&start,NULL);
					handleAction(pq);
					//endtime = time(NULL);
					gettimeofday(&end,NULL);
					break;									
		}
		
		diffintime = (double)difftime(endtime,starttime);
		diffintime = fabs(diffintime);
		secs = diffintime;
		//mins = 0;
		//if(secs > 59){
			mins = (int)(secs / 60);
			isecs = (long int)secs % 60;
		//}
		
		//if(mins == 0)	
		//	printf("Time taken : %.2lf secs\n",secs);
		//else
		//printf("Time taken : %d mins %d secs\n", (int)mins,isecs);
		diffintime = end.tv_sec*1000+ end.tv_usec/1000 - start.tv_sec*1000 - start.tv_usec/1000;
		printf("Time Elapsed: %.3lfs\n",  diffintime/1000);
		//printf("Time taken : %.2lf secs\n",secs);	
		
	}
	
	delete[] cstr;
}
//==========================================================

void yadbCli::handleAction(parsedQuery* & result){
	
	DBHeader* dbheader = NULL;
		//diffintime = 0;
	
		switch(pq->getAction()){
			
			case SHOW_DATABASE		:	dm.handleShowSchemas();
								break;
			
			case CREATE_TABLE		:	dm.handleCreateTable((*result));
								break;
			
			case USE			:	dm.handleUseSchema((*result));
								break;
							
			case SHOW_TABLES		:	dm.handleShowTables((*result));
								break;
							
			case CREATE_DATABASE		:       dm.handleCreateSchema((*result));
								break;
			
			case SELECT			:	dm.handleSelect((*result));
								break;
		     
			case INSERT			:       dm.handleInsert((*result));
								break;
		                                        
			case DROP_TABLE			:       dm.handleDropTable((*result));
								break;
		                  
			case DROP_DATABASE		:       dm.handleDropSchema((*result));
								break;
		      	
			case DESC_TABLE			:	dm.handleDescTable((*result));
				        			break;
		        				
			case DELETE			:	dm.handleDelete((*result));
		        					break;
		
			case CREATE_INDEX		:	dm.handleCreateIndex((*result));
								break;
							
			case ALTER_TABLE		:	dm.handleAlterTable((*result));
								break;				
				
			case UPDATE_TABLE		:	dm.handleUpdate((*result));
								break;
							
			case ADD_RECORDS		:	dm.handleAddRecordes((*result));
								break;
			
			case DROP_INDEX			:	dm.handleDropIndex((*result), false, dbheader);
								break;
								
			case SEARCH_INDEX		:	dm.handleSearchIndex((*result));
								break;
								
			case TRAVERSE_INDEX		:	dm.handleTraverseIndex((*result));
								break;								
								
			case BUFFER_MANAGER	:	dm.showBMWorking();
								break;
								
		}
		
}

#endif
