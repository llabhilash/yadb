#ifndef ERROR_CODES
#define ERROR_CODES
#define NO_ERROR 0
#define INVALID 1
#define READ_ERROR 2
#define WRITE_ERROR 3
#define SEEK_ERROR 4
#define COMMIT_ERROR 5
#define FILEOPEN_ERROR 6
#define FILECLOSE_ERROR 7
#define HEAP_EXPAND_ERROR 8
#define DIRPAGE_READ_ERROR 9
#define DIRENTRY_READ_ERROR 10
#define DATABASEPAGE_WRITE_ERROR 11
#define FREEPAGE_READ_ERROR 12
#define USE_SCHEMA_ERROR 13
#define DATABASEPAGE_READ_ERROR 14
#define RECORD_READ_ERROR 15
#define RECORD_INSERT_ERROR 16
#define INVALIDTABLENAME 17
#define TOTALCOL_MISMATCH 18
#define COLTYPE_MISMATCH 19
#define PACK_ERROR 20
#define UNPACK_ERROR 21
#define CONVERT_ERROR 22
#define DROP_TABLE_ERROR 23
#define DROP_SCHEMA_ERROR 24
#define SCHEMAFILE_DELETE_ERROR 25
#define CONDITION_ERROR 26
#define COLUMN_NOT_DEFINED 27
#define COLUMN_REDEFINITION 28
#define NOT_NUMBER_ERROR 29
#define SIZE_MISMATCH 30
#define COLUMN_EXISTS 31
#define DATAMANAGER_DELETE_RECORD_ERROR 32;
#define DATAMANAGER_INSERT_RECORD_INTO_DATAPAGE_ERROR 33;
#define UPDATE_RECORD_FROM_DATAPAGE_ERROR 34;
#define COLUMN_NULLABLE 35;
#define COLUMN_NOT_NULLABLE 36;
#define INSERT_RECORDS_TO_TABLE 37;
#define DROP_INDEX_ERROR 38;
#define NOT_UNIQUE_ERROR 39;
#define PRIMARY_KEY_ERROR 40;
#define INDEX_KEY_NOT_FOUND 41;

#include <iostream>
bool printErrorCode(int errCode){
	using namespace std;
	switch(errCode){
		case 0 : cout << "NO_ERROR\n";
			 break;

		case 1 : cout << "INVALID\n";
			 break;

		case 2 : cout << "READ_ERROR\n";
			 break;

		case 3 : cout << "WRITE_ERROR\n";
			 break;

		case 4 : cout << "SEEK_ERROR\n";
			 break;

		case 5 : cout << "COMMIT_ERROR\n";
			 break;

		case 6 : cout << "FILEOPEN_ERROR\n";
			 break;

		case 7 : cout << "FILECLOSE_ERROR\n";		
			 break;
			 
		case 8 : cout << "HEAP_EXPAND_ERROR\n";		
			 break;
			 
	        case 9 : cout << "DIRPAGE_READ_ERROR\n";
	                 break;
	                 
	        case 10: cout << "DIRENTRY_READ_ERROR\n";
	                 break;
	                 
	        case 11: cout << "DATABASEPAGE_WRITE_ERROR\n";
	                 break;
	                 
	        case 12: cout <<"FREEPAGE_READ_ERROR\n";
	                 break;
	                 
	        case 13: cout <<"USE_SCHEMA_ERROR\n";
	                 break;
	                 
	        case 14: cout << "DATABASEPAGE_READ_ERROR\n";
	                 break;
	                 
	        case 15: cout << "RECORD_READ_ERROR\n";
	                 break;
	                 
	        case 16: cout<<"RECORD_INSERT_ERROR\n";
	                 break;
	                
	        case 17: cout<<"INVALIDTABLENAME\n";
	                 break;
	                
	        case 18: cout<<"TOTALCOL_MISMATCH\n";
	                 break;
	                
	        case 19: cout<<"COLTYPE_MISMATCH\n";
	                 break;
	                
	        case 20: cout<<"PACK_ERROR\n";
	                 break;
	              
	        case 21: cout<<"UNPACK_ERROR\n";
	                 break;
	                
	        case 22: cout<<"CONVERT_ERROR\n";
	        	 break;
	        
	        case 23: cout<<"DROP_TABLE_ERROR\n";
	        	 break;

	        case 24: cout<<"DROP_SCHEMA_ERROR\n";
	        	 break;
	        
	        case 25: cout<<"SCHEMAFILE_DELETE_ERROR\n";
	        	 break;
	        	
	        case 26: cout<<"CONDITION_ERROR\n";
	        	 break;
	        	 
	        case 27: cout<<"COLUMN DOES NOT EXIST ERROR\n";
	        	 break;
	        	    	 	        	 
	        case 28: cout<<"COLUMN_REDIFINITION ERROR FROM WHERE CONDITION \n";
	        	 break;
	        	 
	        case 29: cout<<"NOT NUMBER ERROR FROM WHERE CONDITION\n";	 
	        	break;
	        	
		case 30: cout<<"SIZE MISMATCH\n";	 
	        	break;	        	
	        	
		case 31: cout<<"COLUMN ALREADY EXISTS\n";	 
	        	break;
	       
	       	case 32: cout<<"DATAMANAGER_DELETE_RECORD_ERROR\n";
	       		break;
	       	
	       	case 33: cout<<"DATAMANAGER_INSERT_RECORD_INTO_DATAPAGE_ERROR\n";
	       		break;	
	       	
	       	case 34: cout<<"UPDATE_RECORD_FROM_DATAPAGE_ERROR\n";
	       		break;
	       
	       	case 35:cout<<"COLUMN should be NULLABLE\n";
	       		break;
	       
	       	case 36:cout<<"Trying to add null values to column that is NOT_NULLABLE\n";
	       		break;	
	       
	       	case 37:cout<<"INSERT_RECORDS_TO_TABLE\n";
	       		break;

	       	case 38: cout<<"DROP_INDEX_ERROR\n";
	        	break;
	       
	       	case 39: cout<<"NOT_UNIQUE_ERROR\n";
	        	break;

	       	case 40: cout<<"PRIMARY_KEY_ERROR\n";
	        	break;
	        case 41:cout<<"INDEX_KEY_NOT_FOUND\n";
	        	break;
	}

	return true;
}

#endif
