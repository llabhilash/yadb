#ifndef DATATYPE_H
#define DATATYPE_H

#define VARCHARTYPE 1
#define INTTYPE 2
#define DATETYPE 3
#define FLOATTYPE 4
#define LONGTYPE 5
#define DOUBLETYPE 6
#define SHORTTYPE 7
#define TEXTTYPE 8
#define BOOLTYPE 9
#define NULLTYPE 10
#define VARCHAR_SIZE 255
#define INT_SIZE 4
#define DATE_SIZE 4
#define FLOAT_SIZE 4
#define LONG_SIZE 4
#define DOUBLE_SIZE 8
#define SHORT_SIZE 2
#define TEXT_SIZE 255
#define BOOL_SIZE 1
#define NULL_SIZE 0

int getTypeSize(int dataType){
	int size = 0;
	switch(dataType){
					case INTTYPE:
	 							size = INT_SIZE;
								break;
					case DATETYPE:
								size = DATE_SIZE;
								break;
					case FLOATTYPE:
								size = FLOAT_SIZE;
								break;
					case LONGTYPE:
								size = LONG_SIZE;
								break;
					case DOUBLETYPE:
								size = DOUBLE_SIZE;
								break;
					case VARCHARTYPE:
								size = VARCHAR_SIZE;
								break;
					case SHORTTYPE:
								size = SHORT_SIZE;
								break;
					case BOOLTYPE:
								size = BOOL_SIZE;
								break;
					case NULLTYPE:
								size = NULL_SIZE;
								break;
										
					}
	return size;
}

/*
string getTypeName(int dataType){

	string ret("");
	switch(dataType){
				case INTTYPE:
 							ret = "INT";
							break;
				case DATETYPE:
							ret = "DATE";
							break;
				case FLOATTYPE:
							ret = "FLOAT";
							break;
				case LONGTYPE:
							ret = "LONG";
							break;
				case DOUBLETYPE:
							ret = "DOUBLE";
							break;
				case VARCHARTYPE:
							ret = "VARCHAR";
							break;
				case SHORTTYPE:
							ret = "SHORT";
							break;
				case BOOLTYPE:
							ret = "BOOL";
							break;			
	}
	return ret;
}*/
#endif
