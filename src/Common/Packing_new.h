//Record Packing and Unpacking

#include "../Common/DataType.h"
#include <string.h>
#include <malloc.h>
#include <sstream>

#ifndef PACKING_H
#define PACKING_H

class pack{

//private methods
private:

//public methods
public:
//constructor
static bool convert_to_type(string str, int type,void* &var);
//static bool pack_data(parsedQuery* pq, char* &buf, int &set_size, vector<int> types);
static bool pack_data(vector<string> values, char* &buf, int &set_size, vector<int> types);
static bool unpack_data(vector<string> &datavalues,vector<int> types, char* buf, int size);

};

bool pack::convert_to_type(string str, int type,void* &var){
	bool error = false;
	string str1;
	char remove = '/';
	char* ctmp;
	int i=0;
	bool boolval;
	var = NULL;
	//cout<<"In string data: "<<str<<endl;
	int ival; short sval; float fval; long lval; double dval;
	if(!str.compare("NULL")){
		var = (char *)((str).c_str());
		return true;
	}
	boolval =false;
	switch(type){
					case INTTYPE:	//cout<<"Converting from string: "<<str<<" to int: ";
								ival = atoi((str).c_str());
								//cout<<ival<<endl;
								//if(ival == 0 && str.compare("0")) error = true;
								var = (void*)new int(ival);
								break;
					case SHORTTYPE://	cout<<"Converting from string: "<<str<<" to short: ";
								sval = (short)atoi((str).c_str());
								//cout<<sval<<endl;
								
								if(sval == 0 && str.compare("0")) error = true;
								var = (void*)new short(sval);
								break;

					case DATETYPE: //cout<<"Converting from string: "<<str<<" to Date: ";
								str1 = str;
								for(string::iterator it = str1.begin(); it!=str1.end();){
							     	 if( (*it) == remove )
							     	    it = str1.erase( it );
								      else
								      it++;
								}
								ival = atoi(str1.c_str());
								//cout<<ival<<endl;
								if(ival == 0 && str.compare("0")) error = true;
								var = (void*)new int(ival); 
								break;
							
			     	case FLOATTYPE: //cout<<"Converting from string: "<<str<<" to Float: "; 
			     				fval = (float)atof((str).c_str());
	         						//cout<<fval<<endl;
	         						
								if(fval == 0 && str.compare("0") ) error = true;
								var = (void*)new float(fval);//(void*)ftmp;
								//values[i] = fval;
								break;
							
					case LONGTYPE: //cout<<"Converting from string: "<<str<<" to Long: ";
								lval = atol((str).c_str());
								//cout<<lval<<endl;
								if(lval == 0 && str.compare("0")) error = true;
								var = (void*)new long(lval);
								break;
														
					case DOUBLETYPE: //cout<<"Converting from string "<<str<<" to Double: ";
								  dval = strtod((str).c_str(), NULL);
								  //cout<<dval<<endl;
								  if(dval == 0 && str.compare("0")) error = true;
								  var = (void*)new double(dval);
								  break;
					case BOOLTYPE:	 //cout<<"Converting from string "<<str<<" to Bool: ";
								boolval = false;
								 if(!str.compare("1")){
								 boolval = true;
								 }
								  var = (void*)new bool(boolval);
								  break;
							
					case VARCHARTYPE:  //cout<<"string item:"<<endl;
									var = (char *)((str).c_str());
									break;
							
				}
				return (error==false)?true:false;
}

//Pack data into buf, size of buf into set_size, return true or false on success and faliure respectively
//bool pack::pack_data(parsedQuery* pq, char* &buf, int &set_size, vector<int> types){
bool pack::pack_data(vector<string> values, char* &buf, int &set_size, vector<int> types){
	int i = 0;
	int size = 0;
	int var_size = 0;
	char* cstr;
	string strtmp;	
	int *ival = NULL; float *fval  = NULL; long *lval = NULL; double *dval = NULL; int *date = NULL; char* str = NULL;short* sval = NULL;
	//vector<string> values = pq->getDataValues();
	//vector<int> types = pq->getTypes();
	short* pointers = new short[types.size()+1];
	
	//one pointer(size = short) for each column + end pointer
	size = ((types.size() * SHORT_SIZE) + SHORT_SIZE);
	
	for(i=0;i<types.size();i++){
		
		switch(types[i]){
			case INTTYPE:  pointers[i] = size;
						size += INT_SIZE;
						break;
			case FLOATTYPE:pointers[i] = size;
						size += FLOAT_SIZE;
						break;
			case SHORTTYPE:pointers[i] = size;
						size += SHORT_SIZE;
						break;
			case LONGTYPE:	pointers[i] = size;
						size += LONG_SIZE;
						break;
			case DOUBLETYPE:pointers[i] = size;
						size += DOUBLE_SIZE;
						break;
			case VARCHARTYPE:pointers[i] = size;
						 strtmp = values[i];
						size += strtmp.length();
						break;
			case DATETYPE: pointers[i] = size;
						size += DATE_SIZE;
						break;
			case BOOLTYPE: pointers[i] = size;
						size += BOOL_SIZE;
						break;			
			case NULLTYPE: pointers[i] = size;
						break;						
		}
	}
	pointers[i] = size;
	//cout<<"Buf Size: "<<size<<endl;
	set_size = size;
	buf = new char[size];
	int pos = 0;
	//Adding pointers at being of data values
	for(i=0;i<types.size();i++){
		memcpy(&buf[pos],&(pointers[i]),SHORT_SIZE);
		pos+= SHORT_SIZE;
	}
	memcpy(&buf[pos],&(pointers[i]),SHORT_SIZE);
	pos += SHORT_SIZE;
	
	void* _value;
	bool error = false;
	bool* boolptr;
	bool boolval;
	//Adding data values at end of pointer info	
	for(i=0;i<types.size();i++){	
		switch( types[i] ){
			
			case INTTYPE:  if(convert_to_type(values[i],INTTYPE,_value)){
						ival = (int *)_value;
						memcpy(&buf[pos], ival, INT_SIZE);
						pos+=INT_SIZE;
						ival = NULL;
						//if( _value != NULL )
						//delete (int*)_value;				
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						}
						else
						error = true;	
						//buf = memcpy(buf, ival, INT_SIZE);
						break;
			case FLOATTYPE:if(convert_to_type(values[i],FLOATTYPE,_value)){
						fval = (float *)_value;
						memcpy(&buf[pos], fval, FLOAT_SIZE);
						pos+=FLOAT_SIZE;
						fval = NULL;
						delete (float*)_value;						
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						}
						else
						error = true;												
						//buf = memcpy(buf, fval, FLOAT_SIZE);
						break;
			case SHORTTYPE:if(convert_to_type(values[i],SHORTTYPE,_value)){
						sval = (short *)_value;
						memcpy(&buf[pos], sval, SHORT_SIZE);
						pos+=SHORT_SIZE;
						sval = NULL;
						delete (short*)_value;
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;	
						}
						else
						error = true;
						//buf = memcpy(buf, sval, SHORT_SIZE);
						break;
			case BOOLTYPE:if(convert_to_type(values[i],BOOLTYPE,_value)){
						boolptr = (bool *)_value;
						/*boolval = false;
						if(*boolptr == true)
						boolval = true;*/
						PARSER_FUNC_TRACE && cout<<"Bool value: "<<*(boolptr)<<endl;
						memcpy(&buf[pos], boolptr, BOOL_SIZE );
						pos+= (BOOL_SIZE);
						delete (bool*)_value;
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						boolptr = NULL;
						}
						else
						error = true;
						//buf = memcpy(buf, sval, SHORT_SIZE);
						break;
			case LONGTYPE:	if(convert_to_type(values[i],LONGTYPE,_value)){
						lval = (long *)_value;
						memcpy(&buf[pos], lval, LONG_SIZE);
						pos+=LONG_SIZE;
						lval = NULL;
						delete (long*)_value;						
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						}
						else
						error = true;
						//buf = memcpy(buf, lval, LONG_SIZE);
						break;
			case DOUBLETYPE: if(convert_to_type(values[i],DOUBLETYPE,_value)){
						dval = (double *)_value;
						memcpy(&buf[pos], dval, DOUBLE_SIZE);
						pos+=DOUBLE_SIZE;
						dval = NULL;
						delete (double*)_value;						
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						}
						else
						error = true;				
						break;
			case VARCHARTYPE:if(convert_to_type(values[i],VARCHARTYPE,_value)){
						strtmp = (char *)_value;
						PARSER_FUNC_TRACE && cout<<"String packed: "<<strtmp<<" Size: "<<strtmp.length()<<endl;
						memcpy(&buf[pos], strtmp.c_str(), strtmp.length() );
						pos+= (strtmp.length()  );
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						strtmp = "";
						}
						else
						error = true;
						break;
			case DATETYPE: if(convert_to_type(values[i],DATETYPE,_value)){
						date = (int *)_value;
						memcpy(&buf[pos], date, DATE_SIZE);
						pos+=DATE_SIZE;
						date = NULL;
						delete (int*)_value;						
						PARSER_FUNC_TRACE && cout<<"Pos: "<< pos <<endl;
						}
						else
						error = true;
						break;
			case NULLTYPE: 		break;			
			}
	}//end of for loop on noOfColuumns	
	
	delete[] (short*)pointers;
	return (error==false)?true:false;
}//end pack_data()

//Unpack buf into datavalues and send vector, return true or false on success and faliure respectively
bool pack::unpack_data(vector<string> &datavalues,vector<int> types, char* buf, int size){
	int datasize = 0;
	bool error = false;
	int *ival = NULL; float *fval = NULL; long *lval = NULL; double *dval = NULL; int *date = NULL; char* str = NULL;short* sval = NULL;
	string strtmp;
	int noOfColumns;
	short cur = 0;
	short* index = new short;
	int i = 0;
	short* start_data = new short;
	int varchar_size = 0;
	short* nxtindex = new short;
	bool* boolval;
	//get starting of data values
	memcpy(start_data,&buf[cur],SHORT_SIZE);
	noOfColumns = (*start_data)/2 - 2;// - 2 for end pointer
	PARSER_FUNC_TRACE && cout << "No of Columns: " << noOfColumns+1 << endl;
	
	while(cur<*start_data-2){
		//get the current index of data
		memcpy(index,&buf[cur],SHORT_SIZE);
		PARSER_FUNC_TRACE && cout<<"Curr index: "<<*index<<endl;
		cur += SHORT_SIZE;
		stringstream out("");
		memcpy(nxtindex,&buf[cur],SHORT_SIZE);
		PARSER_FUNC_TRACE && cout<<"Nxt index: "<<*nxtindex<<endl;
		datasize = *nxtindex - *index;
		//null entry
		if(datasize == 0){
			cout<<"Null entry"<<endl;
			datavalues.push_back("NULL");
			i++;
			continue;
		}
				
		//check for type
		switch( types[i] ){
		
				case INTTYPE:  ival = new int;
							memcpy(ival,&buf[*index],INT_SIZE);
							PARSER_FUNC_TRACE &&  cout<<"Int value: "<<*ival<<endl;
							out << *ival;
							strtmp = out.str();
							datavalues.push_back(strtmp);
							delete ival;
							break;
				case FLOATTYPE:fval = new float;
							memcpy(fval,&buf[*index],FLOAT_SIZE);
							PARSER_FUNC_TRACE && cout<<"Float value: "<<*fval<<endl;
							out << *fval;
							strtmp = out.str();
							out.flush();
							datavalues.push_back(strtmp);							
							delete fval;
							break;
				case SHORTTYPE:	sval = new short;
							memcpy(sval,&buf[*index],SHORT_SIZE);
							PARSER_FUNC_TRACE && cout<<"Short value: "<<*sval<<endl;
							out << *sval;
							strtmp = out.str();
							datavalues.push_back(strtmp);							
							delete sval;
							break;
				case BOOLTYPE:	boolval = NULL;
							boolval = new bool;
							memcpy(boolval,&buf[*index],BOOL_SIZE);
							//boolval[BOOL_SIZE] = '\0';							
							//strtmp = "false";
							PARSER_FUNC_TRACE && cout<<"Bool value: "<<*boolval<<endl;
							//if(boolval)
							//strtmp = "true";
							out << *boolval;
							strtmp = out.str();
							datavalues.push_back(strtmp);
							delete boolval;
							break;
				case LONGTYPE:	lval = new long;
							memcpy(lval,&buf[*index],LONG_SIZE);
							PARSER_FUNC_TRACE && cout<<"Long value: "<<*lval<<endl;
							out << *lval;
							strtmp = out.str();
							datavalues.push_back(strtmp);
							delete lval;
							break;
				case DOUBLETYPE: dval = new double;
							memcpy(dval,&buf[*index],DOUBLE_SIZE);						
							PARSER_FUNC_TRACE && cout<<"Double value: "<<*dval<<endl;
							out << *dval;
							strtmp = out.str();
							datavalues.push_back(strtmp);
							delete dval;
							break;
				case VARCHARTYPE: str = NULL;
							memcpy(nxtindex,&buf[cur],SHORT_SIZE);
							PARSER_FUNC_TRACE && cout<<"Nxt index: "<<*nxtindex<<endl;
							varchar_size = *nxtindex-*index;	
							//cout<<"Error here: due to varchar_size:"<<varchar_size<<endl;
							
							str = new char[varchar_size + 1];
							memcpy(str,&buf[*index],(varchar_size));
							str[varchar_size] = '\0';
							PARSER_FUNC_TRACE && cout<<"String value: "<<str<<endl;
							strtmp = str;
							datavalues.push_back(strtmp);
							delete[] str;
							break;
				case DATETYPE: date = new int;
							memcpy(date,&buf[*index],INT_SIZE);
							PARSER_FUNC_TRACE && cout<<"Date value: "<<*date<<endl;
							out << *date;
							strtmp = out.str();
							//strtmp.insert(4,"/",2);
							//strtmp.insert(8,"/",2);
							datavalues.push_back(strtmp);
							delete date;
							break;
				case NULLTYPE:  	datavalues.push_back("NULL");
							break;			
				}//end switch
				i++;
	}//end while

	//clean up
	
	if(index!=NULL)
		delete index;
	if(start_data!=NULL)
		delete start_data;
	if(nxtindex!=NULL)
		delete nxtindex;

	//no error checking
	return (error==false)?true:false;
}

#endif
