#include <cstring>
#ifndef SETTINGS_H
#define SETTINGS_H
class settings{
	public:
	static bool verbose;
	static int pageSizeInBytes;
	static bool bmfunctrace;
	static bool dmfunctrace;
	static bool indexfunctrace;
	static bool parserfunctrace;
	static bool conditionarray;
	static int replacementPolicy;
	static int filenamesize;
	static int pathsize;
	static char* path;	

	static void init(){
		verbose = false;
		pageSizeInBytes = 8192;
		bmfunctrace = false;
		dmfunctrace = false;
		indexfunctrace = false;
		parserfunctrace = false;
		conditionarray = false;
		filenamesize = 256;		
		replacementPolicy = 0;
		pathsize = 6;
		if(path != NULL)
			delete[] path;
		path = new char[pathsize];
		sprintf(path,"%s","Data/");
		
	}
};

bool settings::verbose;
int settings::pageSizeInBytes;
bool settings::bmfunctrace;
bool settings::dmfunctrace;
bool settings::indexfunctrace;
bool settings::parserfunctrace;
bool settings::conditionarray;
int settings::filenamesize;
int settings::pathsize;
int settings::replacementPolicy;
char* settings::path;

#define VERBOSE settings::verbose
#define PAGE_SIZE_IN_BYTES settings::pageSizeInBytes
#define BM_FUNC_TRACE settings::bmfunctrace
#define DM_FUNC_TRACE settings::dmfunctrace
#define INDEX_FUNC_TRACE settings::indexfunctrace
#define PARSER_FUNC_TRACE settings::parserfunctrace
#define CONDITION_ARRAY_TRACE settings::conditionarray
#define FILENAME_SIZE settings::filenamesize
#define FILENAME_PATH settings::path
#define PATH_SIZE settings::pathsize
#define REPLACEMENT_POLICY settings::replacementPolicy
#endif
