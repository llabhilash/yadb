//freepagedecl.h
//This file contains the declaration of the class FreePage.
/////////////////////////////////////////////////////////////////////////////////



#ifndef FREEPAGE_DECL_H
#define FREEPAGE_DECL_H
class FreePage{
	public:
		char *pageData;
		FreePage(char* &);
		int getNextFreePage();
		bool setNextFreePage(int );
};
#endif
