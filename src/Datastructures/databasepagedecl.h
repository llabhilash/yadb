//databasepagedecl.h
//This file contains declarations for class DataBasePage.
/////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef DATABASEPAGEDECL_H
#define DATABASEPAGEDECL_H

class DataBasePage{
        private:
	        bool setTotalFreeSpace(short );
	        bool defragmentPage();
	public:
	        char *pageData;
	        DataBasePage();
	        DataBasePage(char* &);
	        void init();
	        short getTotalFreeSpace();
	        SlotDirEntry getCFSTuple();
	        bool setCFSTuple(SlotDirEntry );
	        short getNoOfSlots();
	        bool setNoOfSlots(short );
	        short getTotalFreeSlots();
	        bool setTotalFreeSlots(short );
	        bool incrementTotalFreeSlots();
	        bool decrementTotalFreeSlots();
	        bool insertRecord(char * recPtr, int reclen, RID& rid);
	        bool deleteRecord(const RID &);
	        bool updateRecord(char * recPtr, int reclen, RID rid);
	        bool firstRecord(RID& firstRid);
	        bool nextRecord(RID& curRid, RID& nextRid);
	        bool getRecord(RID rid, char *&recPtr, int& recLen);
	        bool returnRecord(RID& rid, char*& recPtr, int& recLen);
	        bool getRecordsForRIDs(vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs);
	        bool returnOffset(RID rid, int& offset);
	        bool isEmpty();
	        bool insertSlotDirEntry(SlotDirEntry ,short );
	        SlotDirEntry getSlotDirEntry(short );
	        void dump();
};

#endif
