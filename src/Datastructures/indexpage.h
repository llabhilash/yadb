//indexpage.h
//This file contains the declaration and definitions of class index,which is a generic class for both 
//LeafPage and BranchPage used in indexing.Both LeafPage and BranchPage inherits the methods this class.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef INDEXPAGE_H
#define INDEXPAGE_H

#define PAGETYPE_START_POS (PAGE_SIZE_IN_BYTES -1 -sizeof(short) + 1)
#define FANOUT_START_POS (PAGETYPE_START_POS -sizeof(short))
#define TOTALKEYS_START_POS (FANOUT_START_POS -sizeof(short))
#define KEYSIZE_START_POS (TOTALKEYS_START_POS -sizeof(short))
#define INDEXER_ROOT_START_POS  (KEYSIZE_START_POS-sizeof(int))

class IndexPage{
	public :
	char * pageData;
	IndexPage(char* &pageData);
	bool setPageType(const short &pageType);
	short getPageType();
	bool setMaximumNodeEntries(const short  &maxValue);
	short getMaximumNodeEntries();
	bool setTotalKeysOccupied(const short &totalKeys);
	short getTotalKeysOccupied();
	bool setKeySize(const short &size);
	short getKeySize();
	bool setIndexerRoot(const int &pageNo);
	int getIndexerRoot();
	bool dump();
};

IndexPage::IndexPage(char* &data){
	pageData = data;
}


//setPageType(const short &pageType)-This method is used to set the type of the page.i.e., whether it is a leafnode.
//or a intermidiate/root node
bool IndexPage::setPageType(const short &pageType){
	memcpy(&pageData[PAGETYPE_START_POS],(char *)&pageType,sizeof(short));
	return true;
}

//getPageType()-This method returns the type of the page i.e, whether its a leaf node or an intermidiate node.
short IndexPage::getPageType(){
	short pageType;
	memcpy((char *) &pageType,&pageData[PAGETYPE_START_POS],sizeof(short));
	return pageType;
}


//setMaximumNodeEntries(const short  &maxValue)- This method sets the maximum number of node entries i.e.,fanout.
bool IndexPage::setMaximumNodeEntries(const short  &maxValue){
	memcpy(&pageData[FANOUT_START_POS],(char *) &maxValue,sizeof(short));
	return true;
}

//getMaximumNodeEntries()-This method returns the fanout of the node.
short IndexPage::getMaximumNodeEntries(){
	short fanout;
	memcpy((char *) &fanout,&pageData[FANOUT_START_POS],sizeof(short));
	return fanout;
}


//setTotalKeysOccupied(const short &totalKeys)-This method sets the total Number of keys currently present in that node.
bool IndexPage::setTotalKeysOccupied(const short &totalKeys){
	memcpy(&pageData[TOTALKEYS_START_POS],(char *) &totalKeys,sizeof(short));
	return true;
}


//getTotalKeysOccupied()-This method returns the total number of keys currently present in that node
short IndexPage::getTotalKeysOccupied(){
	short totalKeys;
	memcpy((char *) &totalKeys,&pageData[TOTALKEYS_START_POS],sizeof(short));
	return true;
}


//setKeySize(const short &size)-This method sets the size of the indexkey.In case of composite key it should give the 
//size of the key after the key has been packed.
bool IndexPage::setKeySize(const short &size){
	memcpy(&pageData[KEYSIZE_START_POS],(char *) &size,sizeof(short));
	return true;
}


//getKeySize()-This method returns the size of the key used in indexing.
short IndexPage::getKeySize(){
	short keySize;
	memcpy((char *) &keySize ,&pageData[KEYSIZE_START_POS],sizeof(short));
	return keySize;
}


//setIndexerRoot(const int &pageNo)-This method gets the rootpage number of the B+ tree index.
bool IndexPage::setIndexerRoot(const int &pageNo){
	memcpy(&pageData[INDEXER_ROOT_START_POS],(char *)&pageNo,sizeof(int));
	return true;
}


//getIndexerRoot()-This method returns the rootpage number of the B+ tree index.
int IndexPage::getIndexerRoot(){
	int pageNo;
	memcpy((char *) &pageNo,&pageData[INDEXER_ROOT_START_POS],sizeof(int));
	return pageNo;

}


//dump()-this method dumps the contents of the index page.
bool IndexPage::dump(){
	cout<<"###################IndexPage###########################" << endl;
	cout << "Page Type : " << getPageType() << "   (INDEXHEADER)"<<endl;
	cout << "Indexer Root :" << getIndexerRoot() << endl;
	cout << "Maximum Node Entries (fanout) " << getMaximumNodeEntries() <<endl;
	cout << "KeySize : " << getKeySize() << endl;
	cout << "#####################################################" << endl;
}
#endif
