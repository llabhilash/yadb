#ifndef DIRPAGE_DECL_H
#define DIRPAGE_DECL_H

class DirectoryPage{
	
   private:
		//int getMaxNoOfDEs();

	public:
	        char *pageData;
	        DirectoryPage(char* &);
	        int getNextDirectoryPage();
	        bool setNextDirectoryPage(int );
	        short getNoOfDirectoryEntries();
	        bool setNoOfDirectoryEntries(short );
	        short getLatestFreeSpace();
	        bool setLatestFreeSpace(short );
	        bool setLastModifiedDE(const DirectoryEntry &dirEntry);
	        bool getLastModifiedDE(DirectoryEntry &dirEntry);
	        void init();
	        bool getFirstDirectoryEntry(DirectoryEntry &);
	        bool getNextDirectoryEntry(DirectoryEntry &cur,DirectoryEntry &next);
	        bool updateDirectoryEntry(DirectoryEntry &, bool isDelete);
	        bool insertDirectoryEntry(DirectoryEntry &);
	        bool isAnyDEEmpty();
	        void refreshMaxFreeSpace();
	        int getMaxNoOfDEs();
	        void dump();
};

#endif
