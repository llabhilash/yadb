//dbheaderdecl.h
//This file contains the declaration of the class DBHeader.
////////////////////////////////////////////////////////////////////////////////////


#ifndef DBHEADER_DECL_H
#define DBHEADER_DECL_H

class DBHeader{
	public:
		char *pageData;
		DBHeader(char* &);
		int getNoOfPages();
		bool setNoOfPages(int );
		int getLastPage();
		bool setLastPage(int );
		int getFreePagePointer();
		bool setFreePagePointer(int );
		int getSystemTablePointer();
		bool setSystemTablePointer(int );
		int getSystemColumnPointer();
		bool setSystemColumnPointer(int );
		int getSystemIndexPointer();
		bool setSystemIndexPointer(int);
		void dump();
};

#endif
