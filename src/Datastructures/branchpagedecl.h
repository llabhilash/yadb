//branchpagedecl.h
//This file contains the class declaration for Branch/Root node.
//////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BRANCHPAGEDECL_H
#define BRANCHPAGEDECL_H


class BranchPage{
	public:
		char *pageData;
	        BranchPage(char* &pageData);
		bool setPageType(const short &pageType);
		short getPageType();
		bool setMaximumNodeEntries(const short  &maxValue);
		short getMaximumNodeEntries();
		bool setTotalKeysOccupied(const short &totalKeys);
		short getTotalKeysOccupied();
		bool setKeySize(const short &size);
		short getKeySize();
		bool insert(const vector<int> &keyType ,char* const &key ,const IndexNode &node ,bool &split,int &pos);
		bool split(const vector<int> &keyType, IndexNode &node ,char* &branchleaf,char* const &key);
		bool dump(const vector<int> &keyType,const int &pos);
		bool traverseDump(const vector<int> &keyType);
		bool compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes,
		                    const int &keySize1, const int &keySize2, bool &result,int);
		bool findNextNode(const vector<int> &keyType ,char* const &key,int &pageNo,int);
		bool getAllChildPageNumbers(vector<int> &pageNo);
		bool getNextSibling(int pageNo , int &nextPage);
		bool getPrevSibling(int pageNo ,int &prevPage);
		int getFirstChild(char* &key);
		int getLastChild(char* &key);
		bool deleteNode(const vector<int> &keyType ,const int &pageNo ,bool &merge);
		bool removeNode(const vector<int> &keyType ,const int &pageNo);
		bool adjustFromLeftSibling(BranchPage &branchPage,const vector<int> &keyType,
		                                                 char* &newKey, char* const &key);
		bool adjustFromRightSibling(BranchPage &branchPage,const vector<int> &keyType,
		                                                  char* &newKey,char* const &key);
		bool getKeyBetweenNodes(int leftPage,int rightPage,char* &key);
		bool replaceKeyBetweenNodes(int leftPage,int rightPage,char* const &key);
		bool removeFirstNode(char* &key);
		bool merge(BranchPage &branchPage , const vector<int> &keyType,char* const &key);
};


#endif
