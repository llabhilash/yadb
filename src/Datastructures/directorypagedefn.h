//Directorypagedefn.h
//function definitions of all the methods of class DirectoryPage
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef DIRPAGE_DEFN_H
#define DIRPAGE_DEFN_H
#define DE_START_POS (8 + sizeof(DirectoryEntry))

using namespace std;

//Constructor for the class DirectoryPage when a data(page data which is obtained from the BufferManager)
// is passed.
DirectoryPage::DirectoryPage(char* &data){
	//pageData = new char[PAGE_SIZE_IN_BYTES];
	pageData = data;
}

//Maximum number of DEs possible
int DirectoryPage::getMaxNoOfDEs(){
	
	//<NextPageNo = int><NoOfDEs = short><DE1><DE2>
	return (int)(PAGE_SIZE_IN_BYTES - DE_START_POS)/(sizeof(DirectoryEntry));
}


//Return true if there any DE with pageNo == -1, or there is room for more DE
bool DirectoryPage::isAnyDEEmpty(){

	//check if noOfDEs < maxNoOfDEs
	if(getNoOfDirectoryEntries() < getMaxNoOfDEs()){
		return true;
	}
	
	//DEs start from byte DE_START_POS <NextPageNo><NoOfDEs><DE1><DE2>
	
	DirectoryEntry tempDE;
	//iterate over noOfDEs. Find first DE where the pageNo != -1
	short startPos = DE_START_POS;
	bool found= false;		


	for(int i=0; i < getNoOfDirectoryEntries(); i++){
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));
		
		//get next DE until tempDE.pageNo != curDE.pageNo. Then get the next page with pageNo != -1
		if(tempDE.pageNo != -1){
			startPos+=sizeof(DirectoryEntry);				
			continue;
						
		}
		
		//atleast one DE which can be used	
		return true;
		
	}
	
	
	//unable to find such a DE
	return false;	
	
}


//getNextDirectoryPage()-Returns the pageNumber of the next DirectoryPage if it exists ,else returns -1 
//indicating this is the last DirectoryPage of that link of Directory Pages.
int DirectoryPage::getNextDirectoryPage(){

	//First four bytes in the DirPage is the Next page Number
    int nextDP;
	memcpy((char *)&nextDP,pageData,sizeof(int));
	return nextDP;
}


//setNextDirectoryPage(int pageNo)-This method sets the Next DirectoryPage to pageNo.
bool DirectoryPage::setNextDirectoryPage(int pageNo){
	
	//First four bytes in the DirPage is the Next page Number
    memcpy(pageData,&pageNo,sizeof(int));	
	return true;
}


//getNoOfDirectoryEntries()-Return the total number of DirectoryEntries(DE) in that DirectoryPage.
short DirectoryPage::getNoOfDirectoryEntries(){

	//Bytes 4-5 has total number of DEs
	short totDEs;

	memcpy((char *)&totDEs,&pageData[4],sizeof(short));
	return totDEs;
	
}


//setNoOfDirectoryEntries(short totalDEs)-This method sets/resets the total Number of DE's to totalDEs.
bool DirectoryPage::setNoOfDirectoryEntries(short totDEs){

	//Bytes 4-5 has total number of DEs
    memcpy(&pageData[4],&totDEs,sizeof(short));	
	return true;
}


short DirectoryPage::getLatestFreeSpace(){
	//Bytes 6-7 has LatestFreeSpace.
	short freeSpace;
	memcpy((char *)&freeSpace,&pageData[6],sizeof(short));
	return freeSpace;
}


bool DirectoryPage::setLatestFreeSpace(short freeSpace){
	//Bytes 6-7 has total number of DEs
        memcpy(&pageData[6],&freeSpace,sizeof(short));	
	return true;
}


bool DirectoryPage::setLastModifiedDE(const DirectoryEntry &dirEntry){
	//Bytes 8-  contains last modified DE.
	memcpy(&pageData[8],&dirEntry,sizeof(DirectoryEntry));	
	return true;
}


bool DirectoryPage::getLastModifiedDE(DirectoryEntry &dirEntry){
	memcpy((char *)&dirEntry,&pageData[8],sizeof(DirectoryEntry));	
	return true;
}



//intializes the page when empty, typically when dealing with a new page
void DirectoryPage::init(){

	//set next dp page as -1	
	setNextDirectoryPage(-1);
	//set NoOfDEs to 0
	setNoOfDirectoryEntries(0);
	setLatestFreeSpace(-1);
}

//Sets the destn with FirstDE value. Returns TRUE if a DE is present. Returns False otherwise
bool DirectoryPage::getFirstDirectoryEntry(DirectoryEntry &destn){

	//return false if no of DEs = 0
	if(getNoOfDirectoryEntries() == 0){
		return false;
	}
	
	//DEs start from byte DE_START_POS <NextPageNo><NoOfDEs><DE1><DE2>
	
	DirectoryEntry tempDE;
	//iterate over noOfDEs. Find first DE where the pageNo != -1
	short startPos = DE_START_POS;
	
	for(int i=0; i < getNoOfDirectoryEntries(); i++){
		
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));		
		
		//if pageNo != -1, copy tempDE data to destn, return true
		if(tempDE.pageNo != -1){
			destn.pageNo = tempDE.pageNo;
			destn.freeSpace = tempDE.freeSpace;
			return true;
		}
		
		startPos+=sizeof(DirectoryEntry);
		
	}	
	
	//all page nos = -1
	return false;	
	
}


//refresh DP.
void DirectoryPage::refreshMaxFreeSpace(){
       DirectoryEntry curDE,tempDE;
       short startPos = DE_START_POS;
       curDE.freeSpace = -1;
       for(int i = 0; i < getNoOfDirectoryEntries(); i++){
	       memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));
	       if(curDE.freeSpace  <=  tempDE.freeSpace  && tempDE.pageNo != -1){
		       curDE = tempDE;
	       }
       }
       setLatestFreeSpace(curDE.freeSpace);
       setLastModifiedDE(curDE);
}


//Get the next DE to cur DE
bool DirectoryPage::getNextDirectoryEntry(DirectoryEntry &curDE,DirectoryEntry &nextDE){

	//return false if no of DEs = 0
	if(getNoOfDirectoryEntries() == 0){
		return false;
	}
	
	//DEs start from byte DE_START_POS <NextPageNo><NoOfDEs><DE1><DE2>
	
	DirectoryEntry tempDE;
	//iterate over noOfDEs. Find first DE where the pageNo != -1
	short startPos = DE_START_POS;
	bool found= false;
	
	for(int i=0; i < getNoOfDirectoryEntries(); i++){
		
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));
		
		//get next DE until tempDE.pageNo != curDE.pageNo. Then get the next page with pageNo != -1
		if(!found && tempDE.pageNo != curDE.pageNo){
			startPos+=sizeof(DirectoryEntry);				
			continue;
						
		}
		
		found = true;
		
		//get nextDE to curDE and ignore DEs with pageNo == -1
		//if pageNo != -1, copy tempDE data to nextDE, return true
		if(tempDE.pageNo != -1 && tempDE.pageNo != curDE.pageNo){
			nextDE.pageNo = tempDE.pageNo;
			nextDE.freeSpace = tempDE.freeSpace;
			return true;
		}
	
		
		startPos+=sizeof(DirectoryEntry);
		
	}	
	
	//all page nos = -1
	return false;
	
}

//update a DE with the same pageNo
bool DirectoryPage::updateDirectoryEntry(DirectoryEntry &updatedDE, bool isDelete = false){

	//return false if no of DEs = 0
	if(getNoOfDirectoryEntries() == 0){
		return false;
	}

	//isDelete is true when DE has to be 'deleted' as the DataPage it was referring to has been free'd
	
	//DEs start from byte DE_START_POS <NextPageNo><NoOfDEs><DE1><DE2>
	
	DirectoryEntry tempDE, lastModDE;
	//iterate over noOfDEs. Find first DE where the pageNo != -1
	short startPos = DE_START_POS;
	bool found= false;		
	int i;
	
	for(i=0; i < getNoOfDirectoryEntries(); i++){
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));
		
		//get next DE until tempDE.pageNo != curDE.pageNo. Then get the next page with pageNo != -1
		if((tempDE.pageNo != updatedDE.pageNo) && tempDE.pageNo != -1){
			startPos+=sizeof(DirectoryEntry);				
			continue;
						
		}
		break;
	}

	if(i == getNoOfDirectoryEntries()){
		//No such DE
		return false;
	}
		
	//get nextDE to curDE and ignore DEs with pageNo == -1
	//if pageNo != -1, copy tempDE data to nextDE, return true
	if(isDelete){
		//page deleted
		updatedDE.pageNo = -1;
		updatedDE.freeSpace = -1;
	}
	
	//check if update is for record delete..
	if((updatedDE.freeSpace > tempDE.freeSpace)){
		getLastModifiedDE(lastModDE);
		if(lastModDE.pageNo == updatedDE.pageNo){
			//Records delete from the DataBassePage in Last Modified DE.. update the last modified values
			setLatestFreeSpace(updatedDE.freeSpace);
			setLastModifiedDE(updatedDE);
		}else{
			//DE in Last Modified is not the same as the one being updated now..
			//if freespace of this DE is more.. set this as the last modified DE
			if(getLatestFreeSpace() < updatedDE.freeSpace){					
				setLatestFreeSpace(updatedDE.freeSpace);
				setLastModifiedDE(updatedDE);
			}	
		}			
	}
	//write back updatedDE to page			
	memcpy(&pageData[startPos],(char *)&updatedDE,sizeof(DirectoryEntry));			
	getLastModifiedDE(lastModDE);
	if(isDelete && (lastModDE.pageNo == tempDE.pageNo)){
		//reset latest free space and last modified DE... which has been deleted now
		refreshMaxFreeSpace();
	}

	return true;
	
}




//insert new DE to page. return true if successful else false
bool DirectoryPage::insertDirectoryEntry(DirectoryEntry &newDE){

	DM_FUNC_TRACE && cout << "DirPage.insertDE()\n";
	DM_FUNC_TRACE && cout << "DE.pageNo = " << newDE.pageNo << " , DE.freespace = " << newDE.freeSpace << endl;

	//check if noOfDEs == 0, add as first DE, make noOfDEs = 1
	if(getNoOfDirectoryEntries() == 0){
		memcpy(&pageData[DE_START_POS],(char *)&newDE,sizeof(DirectoryEntry));
		setNoOfDirectoryEntries(1);
		return true;
	}
	
	
	//DEs start from byte DE_START_POS <NextPageNo><NoOfDEs><DE1><DE2>
	
	DirectoryEntry tempDE;
	//iterate over noOfDEs. Find first DE where the pageNo != -1
	short startPos = DE_START_POS;
	
	//find a DE with page -1
	for(int i=0; i < getNoOfDirectoryEntries(); i++){
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));
		
		//get next DE until tempDE.pageNo != -1
		if(tempDE.pageNo != -1){
			startPos+=sizeof(DirectoryEntry);				
			continue;
						
		}
		
		//insert new DE here
		//DM_FUNC_TRACE && cout << "IN for LOOP.....DE Size : " << sizeof(DirectoryEntry) << endl;
		DM_FUNC_TRACE && cout << "Inserting new DE @  " << startPos <<endl;
		memcpy(&pageData[startPos],(char *)&newDE,sizeof(DirectoryEntry));
		
		return true;
		
	}
	
	//No DE with pageNo == -1
	
	//check if noOfDEs == maxNoOfDEs, yes => no space
	if(getNoOfDirectoryEntries() == getMaxNoOfDEs()){
		//no space
		return false;
	}
	
	//add as last DE, increment noOfDEs	
	//increment startPos, now points to next DE loc
	//startPos+=sizeof(DirectoryEntry);
	//DM_FUNC_TRACE && cout << "NOT in LOOP.........DE Size : " << sizeof(DirectoryEntry) << endl;
	DM_FUNC_TRACE && cout << "Inserting new DE @  " << startPos <<endl;
	memcpy(&pageData[startPos],(char *)&newDE,sizeof(DirectoryEntry));
	//increment noOfDEs
	setNoOfDirectoryEntries(getNoOfDirectoryEntries() + 1);
	return true;
	
}


//dump()-This method dumps the contents of directory page.
void DirectoryPage::dump(){
	cout<<"============== Directory Page ================\n";
	cout<<"Next Directory Page :"<<getNextDirectoryPage()<<endl;
	cout<<"Maximum DEs :"<<getMaxNoOfDEs()<<endl;
	cout<<"Total No of DEs :"<<getNoOfDirectoryEntries()<<endl;
	cout<<"Directories Entries :\n";	
	DirectoryEntry tempDE;
	short startPos = DE_START_POS;
	for(int i=0; i < getNoOfDirectoryEntries(); i++){
		//retrieve ith DE
		memcpy((char *)&tempDE,&pageData[startPos],sizeof(DirectoryEntry));		
		cout<<"PageNo :"<<tempDE.pageNo<<" Free Space :"<<tempDE.freeSpace<<endl;
		startPos+=sizeof(DirectoryEntry);
	}
	cout<<"==============================================\n";
}



#endif
