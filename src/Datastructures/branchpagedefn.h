//branchpagedefn.h
//This file contains the function definations of methods of class BranchPage.
//////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BRANCHPAGEDEFN_H
#define BRANCHPAGEDEFN_H

#define PAGETYPE_START_POS (PAGE_SIZE_IN_BYTES -1 -sizeof(short) + 1)
#define FANOUT_START_POS (PAGETYPE_START_POS -sizeof(short))
#define TOTALKEYS_START_POS (FANOUT_START_POS -sizeof(short))
#define KEYSIZE_START_POS (TOTALKEYS_START_POS -sizeof(short))

using namespace std;

//BranchPage(char* &data)-Constructor for the class LeafPage.
BranchPage::BranchPage(char* &data){
	pageData = data;
}


//setPageType(const short &pageType)-This method is used to set the type of the page.i.e., whether it is a leafnode.
//or a intermidiate/root node
bool BranchPage::setPageType(const short &pageType){
	memcpy(&pageData[PAGETYPE_START_POS],(char *)&pageType,sizeof(short));
	return true;
}

//getPageType()-This method returns the type of the page i.e, whether its a leaf node or an intermidiate node.
short BranchPage::getPageType(){
	short pageType;
	memcpy((char *) &pageType,&pageData[PAGETYPE_START_POS],sizeof(short));
	return pageType;
}

//setMaximumNodeEntries(const short  &maxValue)- This method sets the maximum number of node entries i.e.,fanout.
bool BranchPage::setMaximumNodeEntries(const short  &maxValue){
	memcpy(&pageData[FANOUT_START_POS],(char *) &maxValue,sizeof(short));
	return true;
}

//getMaximumNodeEntries()-This method returns the fanout of the node.
short BranchPage::getMaximumNodeEntries(){
	short fanout;
	memcpy((char *) &fanout,&pageData[FANOUT_START_POS],sizeof(short));
	return fanout;
}


//setTotalKeysOccupied(const short &totalKeys)-This method sets the total Number of keys currently present in that node.
bool BranchPage::setTotalKeysOccupied(const short &totalKeys){
	memcpy(&pageData[TOTALKEYS_START_POS],(char *) &totalKeys,sizeof(short));
	return true;
}


//getTotalKeysOccupied()-This method returns the total number of keys currently present in that node
short BranchPage::getTotalKeysOccupied(){
	short totalKeys;
	memcpy((char *) &totalKeys,&pageData[TOTALKEYS_START_POS],sizeof(short));
	return totalKeys;
}


//setKeySize(const short &size)-This method sets the size of the indexkey.In case of composite key it should give the 
//size of the key after the key has been packed.
bool BranchPage::setKeySize(const short &size){
	memcpy(&pageData[KEYSIZE_START_POS],(char *) &size,sizeof(short));
	return true;
}


//getKeySize()-This method returns the size of the key used in indexing.
short BranchPage::getKeySize(){
	short keySize;
	memcpy((char *) &keySize ,&pageData[KEYSIZE_START_POS],sizeof(short));
	return keySize;
}


//bool getAllChildPageNumbers(vector<int> &pageNo)-This methods sets the pageNo with the page Numbers of the children 
//of this node.
bool BranchPage::getAllChildPageNumbers(vector<int> &pageNo){
        int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *page = NULL;
	int childPage;
	for(i = 0 ; i < (getTotalKeysOccupied()  ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(page != NULL){
			delete [] page;
		}
		page = new char[sizeof(int)];
		memcpy(page,record,sizeof(int));
		memcpy((char *) &childPage,page,sizeof(int));
		pageNo.push_back(childPage);
		
	}
	if(page != NULL){
		delete [] page;
	}
	page = new char[sizeof(int)];
	memcpy(&page[0],&record[sizeof(int) + getKeySize()],sizeof(int));
	memcpy((char *) &childPage,&page[0],sizeof(int));
	pageNo.push_back(childPage);
	if(page != NULL){
		delete [] page;
	}
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//getNextSibling(int pageNo,int &nextPageNo)-This method returns the page number of the sibling next to node "pageNo".
bool BranchPage::getNextSibling(int pageNo,int &nextPageNo){
	int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *page = NULL;
	int childPage;
	int sibling;
	bool ret = false;
        for(i = 0 ; i <= (getTotalKeysOccupied() -1 ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(page != NULL){
			delete [] page;
		}
		page = new char[sizeof(int)];
		memcpy(page,record,sizeof(int));
		memcpy((char *) &childPage,page,sizeof(int));
		if(childPage == pageNo){
			memcpy(page,&record[sizeof(int) + getKeySize()],sizeof(int));
			memcpy((char *) &sibling,page,sizeof(int));
			nextPageNo = sibling;
			ret = true;
			break;
		}
	}
	if(page != NULL){
		delete [] page;
	}
	if(record != NULL){
		delete [] record;
	}
	return ret;
}


//getPrevSibling(int pageNo,int prevPage)-This method sets the prevPage to the previous page number to "pageNo" 
//                                         and returns true else returns false if previous page doesnt exist.
bool BranchPage::getPrevSibling(int pageNo,int &prevPage){
        int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *page = NULL;
	int childPage;
	int sibling;
	bool ret = false;
        for(i = 0 ; i <= (getTotalKeysOccupied() -1 ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(page != NULL){
			delete [] page;
		}
		page = new char[sizeof(int)];
		memcpy(page,&record[sizeof(int) + getKeySize()],sizeof(int));
		memcpy((char *) &childPage,page,sizeof(int));
		if(childPage == pageNo){
			memcpy(page,record,sizeof(int));
			memcpy((char *) &sibling,page,sizeof(int));
			prevPage = sibling;
			ret = true;
			break;
		}
	}
	if(page != NULL){
		delete [] page;
	}
	if(record != NULL){
		delete [] record;
	}
	return ret;

}


//insert(const vector<int> &keyType , const char* &key ,const RID &rid ,IndexNode &node)-This method inserts the
//tuple <rid ,key > into the leafpage in an indexed manner(sorted order).
//Here keyType is a vector containing the datatype of a key in case of composite key its consists of multiple rows of
//datatype arranged inthe order as it is to be indexed.Key represents the packed version of the key.
bool BranchPage::insert(const vector<int> &keyType , char* const &key ,const IndexNode &node ,bool &split,int &pos){
        INDEX_FUNC_TRACE && cout << "BranchPage.insert()" << endl;
	int i;
	short totalKeys;
	bool result = false;
	int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
	
	if(getTotalKeysOccupied() == getMaximumNodeEntries()){
	//Split the BranchNode into Two.
		split = true;
		return false;
	}
	for(i = (getTotalKeysOccupied() -1 ) ; i >= 0 ; i --){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result,-1);
		if(result){
			break;
		}
		memcpy(&pageData[(i + 1) * (sizeof(int) + getKeySize())],record,recordSize);
	}
	if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	//construct the tuple<pageid,keyvalue,pageid>
	memcpy(record,(char *) &node.leftPtr ,sizeof(int));
	memcpy(&record[sizeof(int)],key,getKeySize());
	memcpy(&record[sizeof(int) + getKeySize()],(char *) &node.rightPtr,sizeof(int));
	
	memcpy(&pageData[(i + 1) * (sizeof(int) + getKeySize())],record,recordSize);
	
	totalKeys = getTotalKeysOccupied();
	setTotalKeysOccupied(totalKeys + 1);
	
	if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	//set split = false indicating no need for split.
	//Also set pos = i + 1 where the new value is inserted.
	split = false;
	pos = i + 1;
	return true;
}


//dump();
bool BranchPage::dump(const vector<int> &keyTypes,const int &pos){
	int i,j;
	int err = NO_ERROR;
	short totalKeys;
	bool result = false;
	int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int ptr;
        vector<string> recAttributes;
	cout <<"####################Branch Node Values###############################"<<endl;
        
        for(i = 0 ; i < getTotalKeysOccupied();i ++){
		
		//print only first and last key
		if(i!=0 && i!=(getTotalKeysOccupied()-1) && i!=pos)
			continue;

	        if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy((char *) &ptr,record,sizeof(int));
		cout << "Left Pointer  :" << ptr <<endl;
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		memcpy((char *) &ptr,&record[sizeof(int) + getKeySize()],sizeof(int));
		cout << "Right Pointer  :" << ptr <<endl;
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	        }
	        for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
	       }
	       recAttributes.clear();
	       cout << endl;
	}
        cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete [] record;
	if(nodeKeys != NULL)
		delete [] nodeKeys;
		
	return true;
}



//TraverseDump();
bool BranchPage::traverseDump(const vector<int> &keyTypes){
	int i,j;
	int err = NO_ERROR;
	short totalKeys;
	bool result = false;
	int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int ptr;
        int num = 10;
        vector<string> recAttributes;
	cout <<"####################Branch Node Values###############################"<<endl;
        cout << "Page Type : " << getPageType() << "   (BRANCHNODE)"<<endl;
	cout << "Maximum Node Entries (fanout) " << getMaximumNodeEntries() <<endl;
	cout << "Total Keys occupied :" << getTotalKeysOccupied() << endl;
	cout << "KeySize : " << getKeySize() << endl;
	cout << "Enter the Number of Keys to be displayed \n";
	cin >> num;
        for(i = 0 ; i < getTotalKeysOccupied() && i < num;i ++){
		

	        if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy((char *) &ptr,record,sizeof(int));
		cout << "Left Pointer  :" << ptr <<endl;
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		memcpy((char *) &ptr,&record[sizeof(int) + getKeySize()],sizeof(int));
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	        }
	        for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
		}
		cout << "\nRight Pointer  :" << ptr <<endl;
	       recAttributes.clear();
	       cout << endl;
	}
        cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete [] record;
	if(nodeKeys != NULL)
		delete [] nodeKeys;
		
	return true;
}

//bool split(const vector<int> &keyType, IndexNode &node ,char* &branchleaf,const char* &key)-This method creates
// the new branch node whenever a split is necessary.
//This method fills half of the current nodes keys to the new branch.
bool BranchPage::split(const vector<int> &keyType, IndexNode &node ,char* &branchleaf,char* const &key){
	int i;
	int pos = 0;
	short totalKeys;
	bool result = false;
        int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        bool split = false;
        bool inserted = false;
        IndexNode n1;
	
	BranchPage newPage = BranchPage(branchleaf);
	newPage.setPageType(getPageType());
	newPage.setMaximumNodeEntries(getMaximumNodeEntries());
	newPage.setTotalKeysOccupied(0);
	newPage.setKeySize(getKeySize());
	
	for(i = (getMaximumNodeEntries() -1) ; i >= (getMaximumNodeEntries() +1 )/2 ; i --){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		memcpy((char *) &n1.leftPtr,record ,sizeof(int));
		memcpy((char *) &n1.rightPtr,&record[sizeof(int) + getKeySize()],sizeof(int));
		n1.key = nodeKeys;
		if(!inserted)	
			compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result,-1);
		if(result && (!inserted)){
			newPage.insert(keyType ,key ,node,split,pos);
			inserted = true;
		}
		newPage.insert(keyType ,nodeKeys,n1,split,pos);
		
		memcpy(&pageData[ (i * (sizeof(int) + getKeySize())) + sizeof(int)],"",(sizeof(int) + getKeySize()));
		totalKeys = getTotalKeysOccupied();
		setTotalKeysOccupied(totalKeys - 1);
	}
	if(!inserted)
		insert(keyType,key,node,split,pos);
	if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	memcpy(record ,branchleaf,recordSize);
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	memcpy(node.key,nodeKeys,getKeySize());
	memmove(branchleaf,&branchleaf[sizeof(int) + getKeySize()],(newPage.getTotalKeysOccupied() - 1)*recordSize);
	totalKeys = newPage.getTotalKeysOccupied();
	newPage.setTotalKeysOccupied(totalKeys - 1);
	INDEX_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$Dump of new Branch$$$$$$$$$$$$$$$" << endl;
	INDEX_FUNC_TRACE && newPage.dump(keyType,pos);
	if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	return true;	
}


//adjustFromLeftSibling()-this method tries to redistribute keys from the left sibling if possible
bool BranchPage::adjustFromLeftSibling(BranchPage &branchPage,const vector<int> &keyType,char* &newKey,char* const &key){
	char *record = NULL;
	bool split;
	int lPage;
	int rPage;
	int pos;
	IndexNode node;
	if(branchPage.getTotalKeysOccupied() <= ((getMaximumNodeEntries() + 1)/2)){
		return false;
	}
	lPage = branchPage.getLastChild(newKey);
	branchPage.deleteNode(keyType ,lPage ,split);
	rPage = getFirstChild(record);
	node.leftPtr = lPage;
	node.rightPtr = rPage;
	insert(keyType,key,node,split,pos);
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//adjustFromRightSibling()-this method tries to redistribute keys from the right sibling if possible
bool BranchPage::adjustFromRightSibling(BranchPage &branchPage,const vector<int> &keyType,char* &newKey,char* const &key){
	char *record = NULL;
	bool split;
	int lPage;
	int rPage;
	int pos;
	IndexNode node;
	if(branchPage.getTotalKeysOccupied() <= ((getMaximumNodeEntries() + 1)/2)){
		return false;
	}
	rPage = branchPage.getFirstChild(newKey);
	branchPage.removeFirstNode(record);
	if(record != NULL){
		delete [] record;
	}
	lPage = getLastChild(record);
	node.leftPtr = lPage;
	node.rightPtr = rPage;
	insert(keyType,key,node,split,pos);
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//merge()-this method merges two nodes.
bool BranchPage::merge(BranchPage &branchPage , const vector<int> &keyType,char* const &key){
        int lChild;
        int rChild;
        IndexNode node;
	char *newKey = NULL;
	bool split;
	int pos;
	if(newKey != NULL){
		delete [] newKey;
	}
	lChild = getLastChild(newKey);
	if(newKey != NULL){
		delete [] newKey;
	}
	rChild = branchPage.getFirstChild(newKey);
	node.leftPtr = lChild;
	node.rightPtr = rChild;
	//if(newKey != NULL){
	//	delete [] newKey;
	//}
	//newKey = new char[getKeySize()];
	//memcpy(newKey,key,getKeySize());
	for(int i = 0 ; i <= (branchPage.getTotalKeysOccupied()) ; i ++){
		if(i == 0)
			insert(keyType,key,node,split,pos);
		else
			insert(keyType,newKey,node,split,pos);
		if(i == branchPage.getTotalKeysOccupied())
			break;
		lChild = rChild;
		branchPage.getNextSibling(lChild,rChild);
		if(newKey != NULL){
			delete [] newKey;
		}
		if(!branchPage.getKeyBetweenNodes(lChild,rChild,newKey)){
			cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" << endl;
		}
		node.leftPtr = lChild;
		node.rightPtr = rChild;
	}
	//insert(keyType,newKey,node,split,pos);
	if(newKey != NULL){
		delete [] newKey;
	}
	return true;
}



//getKeyBetweenNodes(int leftPage,int rightPage,char* &key)-this method sets the key with the value present between the
//                                                    leftPage and rightPage i.e., <leftPage><key_1><rightPage>  then
//                                                    key = key_1;
bool BranchPage::getKeyBetweenNodes(int leftPage,int rightPage,char* &key){
        int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *lPage = NULL;
	char *rPage = NULL;
	int rChild;
	int lChild;
	bool ret = false;
	 for(i = 0 ; i <= (getTotalKeysOccupied() -1 ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(lPage != NULL){
			delete [] lPage;
		}
		lPage = new char[sizeof(int)];
		if(rPage != NULL){
			delete [] rPage;
		}
		rPage = new char[sizeof(int)];
		memcpy(lPage,record,sizeof(int));
		memcpy(rPage,&record[sizeof(int) + getKeySize()],sizeof(int));
		memcpy((char *) &lChild,lPage,sizeof(int));
		memcpy((char *) &rChild,rPage,sizeof(int));
		if((lChild == leftPage) && (rChild == rightPage)){
			key = new char[getKeySize()];
			memcpy(key,&record[sizeof(int)],getKeySize());
			ret = true;
			break;
		}
	}
	if(record != NULL){
		delete [] record;
	}
	if(lPage != NULL){
		delete [] lPage;
	}
	if(rPage != NULL){
		delete [] rPage;
	}
	return ret;
}


//replaceKeyBetweenNodes(int leftPage,int rightPage,char* const &key) - this method replaces the key present in between 
//                                                                      the leftPage and right Page with "key".
bool BranchPage::replaceKeyBetweenNodes(int leftPage,int rightPage,char* const &key){
	int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *lPage = NULL;
	char *rPage = NULL;
	int rChild;
	int lChild;
	bool ret = false;
	 for(i = 0 ; i <= (getTotalKeysOccupied() -1 ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(lPage != NULL){
			delete [] lPage;
		}
		lPage = new char[sizeof(int)];
		if(rPage != NULL){
			delete [] rPage;
		}
		rPage = new char[sizeof(int)];
		memcpy(lPage,record,sizeof(int));
		memcpy(rPage,&record[sizeof(int) + getKeySize()],sizeof(int));
		memcpy((char *) &lChild,lPage,sizeof(int));
		memcpy((char *) &rChild,rPage,sizeof(int));
		if((lChild == leftPage) && (rChild == rightPage)){
			memcpy(&pageData[(i * (sizeof(int) + getKeySize())) + sizeof(int)],key,getKeySize());
			ret = true;
			break;
		}
	}
	if(record != NULL){
		delete [] record;
	}
	if(lPage != NULL){
		delete [] lPage;
	}
	if(rPage != NULL){
		delete [] rPage;
	}
	return ret;
}


//getFirstChild()-this method returns the pageNo of the first child and sets the key to the value of the 
//       key present beside the firstchild ie., <firstpage><key_1><secondpage>.. key = key_1 and firstpage is returned.
int BranchPage::getFirstChild(char* &key){
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = new char[recordSize];
	int child;
	memcpy(record,pageData,recordSize);
	memcpy((char *) &child,record,sizeof(int));
	key = new char[getKeySize()];
	memcpy(key,&record[sizeof(int)],getKeySize());
	if(record != NULL){
		delete [] record;
	}
	return child;
}


//getLastChild() -this method returns the pageNo of the last child. and sets the key to the value of the 
//       key present beside the lastchild ie., .......<page><key_1><lastpage> key = key_1 and lastpage is returned.
int BranchPage::getLastChild(char* &key){
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	int child;
	record = new char[recordSize];
	memcpy(record,&pageData[(getTotalKeysOccupied() -1 ) * (sizeof(int) + getKeySize())],recordSize);
	memcpy((char *) &child,&record[sizeof(int) + getKeySize()],sizeof(int));
	key = new char[getKeySize()];
	memcpy(key,&record[sizeof(int)],getKeySize());
	if(record != NULL){
		delete [] record;
	}
	return child;
}


//deleteNode()-this method removes the entry pageNo from the current Node ,it also removes the key present towards the
//              right of this pageNo i.e., <page1><key><page2> in this case if page2 == pageNo then it removes the
//              entry "<key><page2>". 
bool BranchPage::deleteNode(const vector<int> &keyType ,const int &pageNo ,bool &merge){
        removeNode(keyType,pageNo);
	if(getTotalKeysOccupied() >= ((getMaximumNodeEntries() + 1)/2)){
		merge = false;
	}else{
		merge = true;
	}
	return true;
}


//this method removes the first node and the key beside it.
bool BranchPage::removeFirstNode(char* &key){
	int recordSize = 2*(sizeof(int)) + getKeySize();
	key = new char[getKeySize()];
	memcpy(key,&pageData[sizeof(int)],getKeySize());
	memmove(pageData,&pageData[sizeof(int) + getKeySize()],(getTotalKeysOccupied() - 2)*recordSize);
	setTotalKeysOccupied(getTotalKeysOccupied() - 1);
	return true;
}


//removeNode()-this method removes the entry pageNo from the current Node ,it also removes the key present towards the
//              right of this pageNo i.e., <page1><key><page2> in this case if page2 == pageNo then it removes the
//              entry "<key><page2>".  
bool BranchPage::removeNode(const vector<int> &keyType ,const int &pageNo){
	int i;
	int recordSize = 2*(sizeof(int)) + getKeySize();
	char *record = NULL;
	char *page = NULL;
	int childPage;
	int sibling;
	bool ret = false;
        for(i = 0 ; i <= (getTotalKeysOccupied() -1 ) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(page != NULL){
			delete [] page;
		}
		page = new char[sizeof(int)];
		memcpy(page,&record[sizeof(int) + getKeySize()],sizeof(int));
		memcpy((char *) &childPage,page,sizeof(int));
		if(childPage == pageNo){
			memmove(&pageData[i * (sizeof(int) + getKeySize()) + sizeof(int)],
			                      &pageData[(i + 1) * (sizeof(int) + getKeySize()) + sizeof(int)],
                                                      ((getTotalKeysOccupied() - 1) - i)*(sizeof(int) + getKeySize()));
                        setTotalKeysOccupied(getTotalKeysOccupied() - 1);
			ret = true;
			break;
		}
	}
	if(page != NULL){
		delete [] page;
	}
	if(record != NULL){
		delete [] record;
	}
	return ret;
	
}



//findNextNode(const vector<int> &keyType ,char* const &key,int &pageNo)-This method returns pointer to nextNode.
//i.e., its sets the pageNo to the next node.
bool BranchPage::findNextNode(const vector<int> &keyType ,char* const &key,int &pageNo,int totalKeyValues = -1){
        INDEX_FUNC_TRACE && cout << "BranchPage.findNextNode() " << endl;
	int i;
	short totalKeys;
	bool result = false;
	bool result1 = false;
        int recordSize = 2*(sizeof(int)) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        
        if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	memcpy(record ,pageData,recordSize);
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	
        compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result,totalKeyValues);
        if(!result){
	        memcpy((char *) &pageNo,record ,sizeof(int));
	        if(record != NULL){
			delete [] record;
		}
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
	        return true;
        }
        
        
        if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	memcpy(record ,&pageData[(getTotalKeysOccupied() -1) * (sizeof(int) + getKeySize())],recordSize);
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
	
        compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result,totalKeyValues);
        if(result){
	        memcpy((char *) &pageNo,&record[sizeof(int) + getKeySize()] ,sizeof(int));
	        if(record != NULL){
			delete [] record;
		}
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
	        return true;
        }
        
        
        for(i = 0 ; i < (getTotalKeysOccupied() -1); i ++){
	        if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result,totalKeyValues);
		
		
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ (i + 1)* (sizeof(int) + getKeySize())],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(int)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),result1,totalKeyValues);
                if(result && (!result1)){
	                memcpy((char *) &pageNo,record ,sizeof(int));
	                if(record != NULL){
				delete [] record;
			}
			if(nodeKeys != NULL){
				delete [] nodeKeys;
			}
		        return true;
                }
        }
        if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
        return false;
}


//Used for comparision of keys
bool BranchPage::compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes, const int &keySize1, const int &keySize2, bool &result,int totalKeys = -1){

	//return true if key1 is >= key2

	if(totalKeys == -1)
		totalKeys = keyTypes.size();
	//INDEX_FUNC_TRACE && cout << "BranchPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	
	vector<string> recAttributes1, recAttributes2;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	if(!pack::unpack_data(recAttributes2, keyTypes, key2, keySize2)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	for(int i = 0; i < totalKeys; i++){
	
		//convert both attributes
		if( !pack::convert_to_type(recAttributes1[i], keyTypes[i], att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(recAttributes2[i], keyTypes[i], att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
		/*
		if(keyTypes[i] == VARCHARTYPE){
			toReturn = strcmp((char*)att_value1, (char*)att_value2);
			//if unequal.. stop
			if(toReturn != 0)
				goto ret;
		}else{
			if()		
		}
		*/
		switch(keyTypes[i]){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;


								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							
		 							//unequal.. stop
		 							goto ret;
		 						}
		 						
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
		 								 						
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;	 						
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
					case VARCHARTYPE:								
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 						
		 						//delete (char*)att_val1;
		 						//delete (char*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)			 						
			 						goto ret;
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;			
		}		
	}
	
	
	ret:
	//INDEX_FUNC_TRACE && printErrorCode(err);
	if(toReturn < 0)
		result = false;
	else
		result = true;
	return err==NO_ERROR?true:false;	
}
#endif
