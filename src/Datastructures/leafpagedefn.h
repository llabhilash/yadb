//leafpagedefn.h
//This includes function definition of all methods of class LeafPage
////////////////////////////////////////////////////////////////////////////////////

#ifndef LEAFPAGEDEFN_H
#define LEAFPAGEDEFN_H

#define PAGETYPE_START_POS (PAGE_SIZE_IN_BYTES -1 -sizeof(short) + 1)
#define FANOUT_START_POS (PAGETYPE_START_POS -sizeof(short))
#define TOTALKEYS_START_POS (FANOUT_START_POS -sizeof(short))
#define KEYSIZE_START_POS (TOTALKEYS_START_POS -sizeof(short))
#define NEXTLEAF_START_POS (KEYSIZE_START_POS - sizeof(int))
#define PREVLEAF_START_POS (NEXTLEAF_START_POS - sizeof(int))


using namespace std;

//LeafPage(char* &data)-Constructor for the class LeafPage.
LeafPage::LeafPage(char* &data){
	pageData = data;
}

//setPageType(const short &pageType)-This method is used to set the type of the page.i.e., whether it is a leafnode.
//or a intermidiate/root node
bool LeafPage::setPageType(const short &pageType){
	memcpy(&pageData[PAGETYPE_START_POS],(char *)&pageType,sizeof(short));
	return true;
}

//getPageType()-This method returns the type of the page i.e, whether its a leaf node or an intermidiate node.
short LeafPage::getPageType(){
	short pageType;
	memcpy((char *) &pageType,&pageData[PAGETYPE_START_POS],sizeof(short));
	return pageType;
}

//setMaximumNodeEntries(const short  &maxValue)- This method sets the maximum number of node entries i.e.,fanout.
bool LeafPage::setMaximumNodeEntries(const short  &maxValue){
	memcpy(&pageData[FANOUT_START_POS],(char *) &maxValue,sizeof(short));
	return true;
}

//getMaximumNodeEntries()-This method returns the fanout of the node.
short LeafPage::getMaximumNodeEntries(){
	short fanout;
	memcpy((char *) &fanout,&pageData[FANOUT_START_POS],sizeof(short));
	return fanout;
}


//setTotalKeysOccupied(const short &totalKeys)-This method sets the total Number of keys currently present in that node.
bool LeafPage::setTotalKeysOccupied(const short &totalKeys){
	memcpy(&pageData[TOTALKEYS_START_POS],(char *) &totalKeys,sizeof(short));
	return true;
}


//getTotalKeysOccupied()-This method returns the total number of keys currently present in that node
short LeafPage::getTotalKeysOccupied(){
	short totalKeys;
	memcpy((char *) &totalKeys,&pageData[TOTALKEYS_START_POS],sizeof(short));
	return totalKeys;
}


//setKeySize(const short &size)-This method sets the size of the indexkey.In case of composite key it should give the 
//size of the key after the key has been packed.
bool LeafPage::setKeySize(const short &size){
	memcpy(&pageData[KEYSIZE_START_POS],(char *) &size,sizeof(short));
	return true;
}


//getKeySize()-This method returns the size of the key used in indexing.
short LeafPage::getKeySize(){
	short keySize;
	memcpy((char *) &keySize ,&pageData[KEYSIZE_START_POS],sizeof(short));
	return keySize;
}


//setNextLeaf(const int &pageNo) - This method sets the page number of next leaf to pageNo w.r.t. this leaf.
bool LeafPage::setNextLeaf(const int &pageNo){
	memcpy(&pageData[NEXTLEAF_START_POS],(char *) &pageNo,sizeof(int));
	return true;
}


//getNextLeaf() - This method returns the page number of the next leaf w.r.t. this leaf.
int LeafPage::getNextLeaf(){
	int nextLeaf;
	memcpy((char *) &nextLeaf ,&pageData[NEXTLEAF_START_POS],sizeof(int));
	return nextLeaf;
}


//setPrevLeaf() - This method sets the page number of prev leaf to pageNo w.r.t. this leaf.
bool LeafPage::setPrevLeaf(const int &pageNo){
	memcpy(&pageData[PREVLEAF_START_POS],(char *) &pageNo,sizeof(int));
	return true;
}
		

//getPrevLeaf() - This method returns the page number of the prev leaf w.r.t. this leaf.		
int LeafPage::getPrevLeaf(){
	int prevLeaf;
	memcpy((char *) &prevLeaf ,&pageData[PREVLEAF_START_POS],sizeof(int));
	return prevLeaf;
}
		
		
//insert(const vector<int> &keyType , const char* &key ,const RID &rid ,IndexNode &node)-This method inserts the
//tuple <rid ,key > into the leafpage in an indexed manner(sorted order).
//Here keyType is a vector containing the datatype of a key in case of composite key its consists of multiple rows of
//datatype arranged inthe order as it is to be indexed.Key represents the packed version of the key.
bool LeafPage::insert(const vector<int> &keyType ,char* const &key ,const RID &rid ,bool &split,int &pos){
        int i;
        short totalKeys;
        bool result = false;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int cmpResult;
        
	if(getTotalKeysOccupied() == getMaximumNodeEntries()){
	//Split the LeafNode into Two.
		split = true;
		return false;
	}
	for(i = (getTotalKeysOccupied() - 1 ) ; i >= 0 ; i --){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(&record[0] ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy(&nodeKeys[0] ,&record[sizeof(RID)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),cmpResult,result,-1);
		if(result){
			break;
		}
		memcpy(&pageData[(i + 1) * recordSize],&record[0],recordSize);
		//if(INDEX_FUNC_TRACE &&( i > (getTotalKeysOccupied() - 5)))
		//	cout << "Moving record : " << i << " from pageData["<< i * recordSize <<"] to pageData[" << (i+1)*recordSize << "]" << endl;
	}
	if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	//construct tuple<rid,key>
	memcpy(&record[0],(char *) &rid,sizeof(RID));
	memcpy(&record[sizeof(RID)],&key[0],getKeySize());
	
	memcpy(&pageData[(i + 1) * recordSize],&record[0],recordSize);
	totalKeys = getTotalKeysOccupied();
	setTotalKeysOccupied(totalKeys + 1);
	
        if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	//set split = false indicating no need for split.
	//Also set pos to i + 1 which represents where new key is inserted.
	split = false;
	pos = i + 1;
	return true;
}



//split(IndexNode &node ,char* &newleaf)-This method creates the new leaf node whenever a split is necessary.
//This method fills half of the current nodes keys to the new leaf.
bool LeafPage::split(const vector<int> &keyType, IndexNode &node ,char* &newleaf,char* const &key ,const RID &rid,
const int &pageNo){
	int i;
	int pos =0;
	short totalKeys;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        bool split = false;
        bool result = false;
        bool inserted = false;
        int cmpResult;
        
        RID curRID;
	
	LeafPage newPage = LeafPage(newleaf);
	newPage.setPageType(getPageType());
	newPage.setMaximumNodeEntries(getMaximumNodeEntries());
	newPage.setTotalKeysOccupied(0);
	newPage.setKeySize(getKeySize());
	newPage.setPrevLeaf(pageNo);
	newPage.setNextLeaf(getNextLeaf());
	for(i = (getMaximumNodeEntries() -1) ; i >= (getMaximumNodeEntries() +1 )/2 ; i --){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		memcpy((char *) &curRID,record,sizeof(RID));
		if(!inserted)
			compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),cmpResult,result,-1);
		if(result && (!inserted)){
			newPage.insert(keyType ,key ,rid ,split,pos);
			inserted = true;
		}
		newPage.insert(keyType ,nodeKeys,curRID ,split,pos);
		
		memcpy(&pageData[i * recordSize] ,"",recordSize);
		totalKeys = getTotalKeysOccupied();
		setTotalKeysOccupied(totalKeys - 1);
	}
	if(!inserted)
		insert(keyType,key,rid,split,pos);
	if(record != NULL){
		delete [] record;
	}
	record = new char[recordSize];
	INDEX_FUNC_TRACE && cout << "Total keys ........." << (getTotalKeysOccupied() -1) << endl;
	memcpy(record ,newleaf,recordSize);
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	nodeKeys = new char[getKeySize()];
	
	memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
	memcpy(node.key,nodeKeys,getKeySize());
	//INDEX_FUNC_TRACE && cout << "Key passed to upper level " << node.key << ", "<< nodeKeys << endl;
	//dump to see contents of new leaf page.
	INDEX_FUNC_TRACE && cout << "$$$$$$$$$$$$$$$Dump of new leaf$$$$$$$$$$$$$$$" << endl;
	INDEX_FUNC_TRACE && newPage.dump(keyType,pos);
	
	/*if(inserted)
		INDEX_FUNC_TRACE && newPage.dump(keyType,pos);		//inserted in new page
	else
		INDEX_FUNC_TRACE && dump(keyType,pos);			//inserted in old page  */

	if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	
	return true;
}

//deleteKey() - This method deletes the entry 'key' present in the leaf node and adjusts the structure of B+ tree
//              so that order of the tree is maintained.In case of any merge it sets the value of merge to true.
bool LeafPage::deleteKey(const vector<int> &keyType,char* const &key,bool &merge ,RID &rid){
        int pos;
        
	if(!(findKey(keyType ,key ,pos ,rid))){
		return false;
	}
	removeKey(pos);
	if(getTotalKeysOccupied() >= ((getMaximumNodeEntries() + 1)/2)){
		merge = false;
	}else{
		merge = true;
	}
	return true;
}


//getKey(int pos,char *&key)-this method sets the key to the value present in the pos "pos"
bool LeafPage::getKey(int pos,char* &key,RID &rid){
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
	int i = getTotalKeysOccupied();
	if(pos > (i -1)){
		return false;
	}
	record = new char[recordSize];
	memcpy(record ,&pageData[ pos * recordSize],recordSize);
	memcpy((char *) &rid,record,sizeof(RID));
	key = new char[recordSize];
	memcpy(key,&record[sizeof(RID)],getKeySize());
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//adjustFromLeftSibling()-this method tries to redistribute keys from the left sibling if possible
bool LeafPage::adjustFromLeftSibling(LeafPage &leafPage,const vector<int> &keyType,char* &key){
        char *record = NULL;
        RID rid;
        bool split;
        int pos = 0;
	if(leafPage.getTotalKeysOccupied() <= ((getMaximumNodeEntries() + 1)/2)){
		return false;
	}
	leafPage.getKey(leafPage.getTotalKeysOccupied() -1,record,rid);
	leafPage.removeKey((int)leafPage.getTotalKeysOccupied() -1);
	key = new char[getKeySize()];
	memcpy(key,record,getKeySize());
	insert(keyType ,key ,rid ,split,pos);
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//adjustFromRightSibling()-this method tries to redistribute keys from the right sibling if possible.
bool LeafPage::adjustFromRightSibling(LeafPage &leafPage,const vector<int> &keyType,char* &key){
	INDEX_FUNC_TRACE && cout << "adjustFromRightSibling()" << endl;
	char *record = NULL;
        RID rid;
        bool split;
        int pos = 0;
	if(leafPage.getTotalKeysOccupied() <= ((getMaximumNodeEntries() + 1)/2)){
		return false;
	}
	leafPage.getKey(0,record,rid);
	leafPage.removeKey(0);
	insert(keyType,record,rid,split,pos);
	leafPage.getKey(0,key,rid);
	if(record != NULL){
		delete [] record;
	}
	return true;
}


//removeKey()- This method removes the key present in the position 'pos'.
bool LeafPage::removeKey(int pos){
     int recordSize = sizeof(RID) + getKeySize();
     if(pos == (getTotalKeysOccupied() -1 )){
	     setTotalKeysOccupied(getTotalKeysOccupied() - 1);
	     return true;
     }
     memmove(&pageData[pos * recordSize],&pageData[(pos + 1)*recordSize],
                                                       ((getTotalKeysOccupied() - 1) - pos)*recordSize);
     setTotalKeysOccupied(getTotalKeysOccupied() - 1);
     return true; 
}


//merge()-this method merges the two leaf nodes .
bool LeafPage::merge(LeafPage &leafPage ,const vector<int> &keyType){
	char *record = NULL;
        RID rid;
        bool split;
        int i;
        int pos = 0;
        for(i = 0 ; i <= (leafPage.getTotalKeysOccupied() -1) ; i ++){
		if(record != NULL){
			delete [] record;
		}
		leafPage.getKey(i,record,rid);
		if(! (insert(keyType,record,rid,split,pos))){
		        if(record != NULL){
				delete [] record;
			}
			return false;
		}
        }
        setNextLeaf(leafPage.getNextLeaf());
        if(record != NULL){
		delete [] record;
	}
        return true;
}


//findKey()- This method checks whether the given key is present in this node or not .If its present its sets the
//           value of pos to the key position in the leaf node and returns true.If its not present it returns false.
bool LeafPage::findKey(const vector<int> &keyType ,char * const &key ,int &pos ,RID &rid){
        int i;
        short totalKeys;
        bool result = false;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int cmpResult;
        bool find = false;
        
	for(i = (getTotalKeysOccupied() - 1 ) ; i >= 0 ; i --){
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		compareGTE(key,nodeKeys,keyType,getKeySize(),getKeySize(),cmpResult,result,-1);
		if(cmpResult == 0){
			find = true;
			pos = i;
			memcpy((char *) &rid,record,sizeof(RID));
			break;
		}
	}
	if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	return find;

}


//getRIDs()- This method is used to return all the RIDs that satisfy the conditions (conditions are infered from 
// the operatorCode and values).RIDs satisfying the condition are set in the vector allrids.
bool LeafPage::getRIDs(const vector<int> &keyType ,vector<int> columnPos,vector<int > operatorCode,
		vector<string> values,vector<RID> &allrids,int totalKeyValues,const bool &firsttime,bool &isResultsStarted){
        int i;
        short totalKeys;
        bool result = false;
        int recordSize = sizeof(RID) + getKeySize();
        char *record = NULL;
        char *nodeKeys = NULL;
        int cmpResult;
        RID rid;
        //bool find = false;
        bool kontinue = false;
        bool flag = true;
	for(i = (getTotalKeysOccupied() - 1 ) ; i >= 0 ; i --){
		//cout << "inside loop" << endl;
		result = false;
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		compare(nodeKeys,keyType,getKeySize(),columnPos,operatorCode,values,result,kontinue,totalKeyValues);
		if(result == true){
			isResultsStarted = true;
			//flag = true;
			memcpy((char *) &rid,record,sizeof(RID));
			allrids.push_back(rid);
		}else{
			flag = false;
			if(isResultsStarted == false){
				flag = true;
				if(firsttime){
					continue;
				}
				if(kontinue){
					continue;
				}else{
					break;
				}
			}//else{
				//if(firsttime){
				//	continue;
				//}
				//kontinue = false;
				//break;
			//}
		}
	}
	if(record != NULL){
		delete [] record;
	}
	if(nodeKeys != NULL){
		delete [] nodeKeys;
	}
	if((isResultsStarted == true) && (flag == false))
		kontinue = false;
	if(firsttime)
		kontinue = true;
	return kontinue;

}
//dump();
bool LeafPage::dump(const vector<int> &keyTypes,const int &pos){
	int i;
	int err = NO_ERROR;
	int recordSize = sizeof(RID) + getKeySize();
	int j;
        char *record = NULL;
        char *nodeKeys = NULL;
	vector<string> recAttributes;
	cout << "#######################LEAF NODE##################################" << endl;
	cout << "Page Type : " << getPageType() << "   (LEAFNODE)"<<endl;
	cout << "Maximum Node Entries (fanout) " << getMaximumNodeEntries() <<endl;
	cout << "Total Keys occupied :" << getTotalKeysOccupied() << endl;
	cout << "KeySize : " << getKeySize() << endl;
	cout << "Next Page Number :  " << getNextLeaf()<< endl;
	cout << "Prev Page Number :  " <<  getPrevLeaf() << endl;
	cout <<"                     ###Leaf Node Values##                         "<<endl;
	for(i = 0 ; i < getTotalKeysOccupied();i++){

		//print only first and last key
		if(i!=0 && i!=(getTotalKeysOccupied() - 1) && i!=pos)
			continue;

	        //cout << "in pack" << endl;
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	       }
	       //cout << "Key Start Positon = " << i * recordSize << endl;
	       cout << "Key Value/s =  ";
	       for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
	       }
	       recAttributes.clear();
	       cout << endl;
	}
	cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete [] record;
	if(nodeKeys != NULL)
		delete [] nodeKeys;
		
	return true;
}

//TraverseDump();
bool LeafPage::traverseDump(const vector<int> &keyTypes){
	int i;
	int err = NO_ERROR;
	int recordSize = sizeof(RID) + getKeySize();
	int j;
	int num = 10;
        char *record = NULL;
        char *nodeKeys = NULL;
	vector<string> recAttributes;
	cout << "#######################LEAF NODE##################################" << endl;
	cout << "Page Type : " << getPageType() << "   (LEAFNODE)"<<endl;
	cout << "Maximum Node Entries (fanout) " << getMaximumNodeEntries() <<endl;
	cout << "Total Keys occupied :" << getTotalKeysOccupied() << endl;
	cout << "KeySize : " << getKeySize() << endl;
	cout << "Next Page Number :  " << getNextLeaf()<< endl;
	cout << "Prev Page Number :  " <<  getPrevLeaf() << endl;
	cout << "Enter the Number of Keys to be displayed \n";
	cin >> num;
	cout <<"                     ###Leaf Node Values##                         "<<endl;
	
	for(i = 0 ; i < getTotalKeysOccupied() && i < num;i++){

		//cout << "in pack" << endl;
		if(record != NULL){
			delete [] record;
		}
		record = new char[recordSize];
		memcpy(record ,&pageData[ i * recordSize],recordSize);
		if(nodeKeys != NULL){
			delete [] nodeKeys;
		}
		nodeKeys = new char[getKeySize()];
		
		memcpy(nodeKeys ,&record[sizeof(RID)],getKeySize());
		if(!pack::unpack_data(recAttributes, keyTypes, nodeKeys,getKeySize())){
			err = UNPACK_ERROR;
			goto ret;
	       }
	       //cout << "Key Start Positon = " << i * recordSize << endl;
	       cout << "Key Value/s =  ";
	       for(j = 0 ; j < recAttributes.size() ; j ++){
		       cout << recAttributes[j] << ",   ";
	       }
	       recAttributes.clear();
	       cout << endl;
	}
	cout <<"###################################################################"<<endl;
	ret:
	if(record != NULL)
		delete [] record;
	if(nodeKeys != NULL)
		delete [] nodeKeys;
		
	return true;
}


//Used for comparision of keys
bool LeafPage::compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes, const int &keySize1, const int &keySize2,int &cmpResult, bool &result ,int totalKeys = -1){

	//return true if key1 is >= key2

	if(totalKeys == -1)
		totalKeys = keyTypes.size();
	//INDEX_FUNC_TRACE && cout << "LeafPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	
	//string dummy = "";
	
	vector<string> recAttributes1, recAttributes2;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	if(!pack::unpack_data(recAttributes2, keyTypes, key2, keySize2)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	for(int i = 0; i < totalKeys; i++){
	
		//convert both attributes
		if( !pack::convert_to_type(recAttributes1[i], keyTypes[i], att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(recAttributes2[i], keyTypes[i], att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
		/*
		if(keyTypes[i] == VARCHARTYPE){
			toReturn = strcmp((char*)att_value1, (char*)att_value2);
			//if unequal.. stop
			if(toReturn != 0)
				goto ret;
		}else{
			if()		
		}
		*/
		switch(keyTypes[i]){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;


								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							
		 							//unequal.. stop
		 							goto ret;
		 						}
		 						
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
		 								 						
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;	 						
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
					case VARCHARTYPE:	
								//cout << "Enter \n";			
						//cout << (char *) att_val1 << ","<< (char *) att_val2 <<endl;
						//		cin >> dummy;				
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 					
		 						//delete (char*)att_val1;
		 						//delete (char*)att_val2;
                                                  //              cin >> dummy;
		 						//unequal.. stop		 						
		 						if(toReturn != 0)			 						
			 						goto ret;
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;

		 						//unequal.. stop		 						
		 						if(toReturn != 0)
			 						goto ret;
								
								break;			
		}		
	}
	
	
	ret:
	//INDEX_FUNC_TRACE && printErrorCode(err);
	cmpResult = toReturn;
	if(toReturn <= 0)
		result = false;
	else
		result = true;
	return err==NO_ERROR?true:false;	
}
		



//compareFunction()-This method is used to compare two attributes. Based on the operator(OperatorCode), this method 
//sets to either true or false.
bool LeafPage::compareFunction(string attribute1,string attribute2,int keyType,int operatorCode,bool &result){
		//convert both attributes
		int err = NO_ERROR;
		int toReturn;

		void* att_val1 = NULL;
		void* att_val2 = NULL;
		if( !pack::convert_to_type(attribute1, keyType, att_val1)){
			err = CONVERT_ERROR;
			goto ret;
		}
		
		if( !pack::convert_to_type(attribute2, keyType, att_val2)){
			err = CONVERT_ERROR;
			goto ret;
		}
	
		switch(keyType){
					
					case DATETYPE:		//handle date as INT
					case INTTYPE:
	 							if(*(int*)att_val1 == *(int*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(int*)att_val1 < *(int*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						//cout << "Lesser "<<*(int*)att_val1<<"  "<<*(int*)att_val2<<"  " << toReturn << "   "<<operatorCode<<endl;
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:
		 							//cout << "Here I am" <<endl;
		 							if(toReturn == 1){
		 											result = true;
		 										}else{
		 										
		 											result = false;
		 										}
		 										break;
		 							case LESSER:
		 						//cout << "This is me" <<endl;
		 							if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:
		 							
		 							if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						
		 						delete (int*)att_val1;
		 						delete (int*)att_val2;
								break;
								
					case FLOATTYPE:
								if(*(float*)att_val1 == *(float*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(float*)att_val1 < *(float*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 						}
		 						
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn == 1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						delete (float*)att_val1;
		 						delete (float*)att_val2;
			
								break;
								
					case LONGTYPE:
								if(*(long*)att_val1 == *(long*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(long*)att_val1 < *(long*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 							
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn == 1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						
		 						delete (long*)att_val1;
		 						delete (long*)att_val2;
		 						
								break;
								
					case DOUBLETYPE:
								if(*(double*)att_val1 == *(double*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(double*)att_val1 < *(double*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn == 1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						delete (double*)att_val1;
		 						delete (double*)att_val2;

		 						break;
								
					case VARCHARTYPE:	
								//cout << "Enter \n";			
						//cout << (char *) att_val1 << ","<< (char *) att_val2 <<endl;
						//		cin >> dummy;				
								toReturn = strcmp((char*)att_val1, (char*)att_val2);
		 					
		 						//delete (char*)att_val1;
		 						//delete (char*)att_val2;
                                                  //              cin >> dummy;
		 						//unequal.. stop		 						
		 						
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn > 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn < 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn < 0)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn > 0)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
								break;
								
					case SHORTTYPE:
								if(*(short*)att_val1 == *(short*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(short*)att_val1 < *(short*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn == 1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						delete (short*)att_val1;
		 						delete (short*)att_val2;
								break;
								
					case BOOLTYPE:
								if(*(bool*)att_val1 == *(bool*)att_val2)
	 								toReturn = 0;
	 							else{
		 							if(*(bool*)att_val1 < *(bool*)att_val2)
		 								toReturn = -1;
		 							else
		 								//its greater
		 								toReturn = 1;
		 							}
		 						
		 						switch(operatorCode){
		 							case EQUALS:  if(toReturn == 0){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 											
		 							case GREATER:if(toReturn == 1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSER:if(toReturn == -1){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case LESSTHANEQUAL:if((toReturn == 0) ||(toReturn == -1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 							case GREATERTHANEQUAL:if((toReturn == 0)||(toReturn == 1)){
		 											result = true;
		 										}else{
		 											result = false;
		 										}
		 										break;
		 						
		 						}
		 						delete (bool*)att_val1;
		 						delete (bool*)att_val2;
								break;			
		}
		
	ret:	
	return err==NO_ERROR?true:false;	
}


//compare()- This method is used to compare a key(Key may inturn contain many attributes) against the values
//for all the operators specified int the OperatorCode. This method sets the result to either true or false.
//It also sets the value of kontinue which can be used to check if further iterations in the leaf page for next key
//is required or not.
bool LeafPage::compare(char* const &key1, const vector<int> &keyTypes, const int &keySize1,const vector<int> columnPos, const vector<int> operatorCode,const vector<string> values,bool &result,bool &kontinue, int totalKeys = -1){

	//return true if key1 is >= key2
	int j = 0;
	if(totalKeys == -1)
		totalKeys = keyTypes.size();
	//INDEX_FUNC_TRACE && cout << "LeafPage.compareGTE()\n";
	int err = NO_ERROR;

	void* att_val1 = NULL;
	void* att_val2 = NULL;
	int i = 0;
	// -1 => less than
	// 0 => equal
	// 1 => greater
	int toReturn = 0;
	result = true;
	//string dummy = "";
	
	vector<string> recAttributes1;

	//unpack both keys	
	if(!pack::unpack_data(recAttributes1, keyTypes, key1, keySize1)){
		err = UNPACK_ERROR;
		goto ret;
	}
	
	
	//cout << "Total Keys  " << totalKeys <<"       "<<"total column Pos"<<columnPos.size()<< endl;
	//for(int i = 0; i < recAttributes1.size(); i++){
		//cout << "outerloop" << endl;
		for (j = 0 ;  j < columnPos.size()  ; j++){
			i = columnPos[j];
			//cout << "inner loop" << endl;
			compareFunction(recAttributes1[i],values[j],keyTypes[i],operatorCode[j],result);
			//cout << result << endl;
			if(i == 0){
				if(result)
					kontinue = true;
			}
			if(result == false)
				break;
			//if((j + 1) >= columnPos.size()){
			//	break;
			//}
		}
		//if((j + 1) >= columnPos.size()){
		//		break;
		//	}
		
	//}
	
	
	ret:
	return (err==NO_ERROR)?true:false;	
}	


#endif
