//
//Contains all structures needed to enter into the pages
////////////////////////////////////////////////////////////////////////////////////////



#ifndef PAGEDATAENTRIES_H
#define PAGEDATAENTRIES_H

#define MAX_TABLENAME_SIZE 64
#define MAX_COLUMNNAME_SIZE 64
#define MAX_DATATYPE_SIZE 255

//RID :used to store the tuple<pageNo,slot no> (Record ID)
struct RID{
	int pageNo;
	short slotNo;
	void dump(){
		cout << "========================RID====================" << endl;
		cout << "page No : " << pageNo << endl;
		cout << "Slot No : " << slotNo << endl;
		cout << "===============================================" << endl;
	}
};


//SlotDirectoryEntry :used to store the tuple<record offset,record length>
//                     in the DB page.
struct SlotDirEntry{
	short offset;
	short length;
	void dump(){
		cout << "=======================Slot Dir Entry ==================" << endl;
		cout << "Offset : " << offset << endl;
		cout << "length : " << length << endl;
		cout << "========================================================" << endl;
	}
};


//DirectoryEntry :used to store the tuple<Page No. ,Total freespace in that page>
//                in the Directory Page.
struct DirectoryEntry{
	int pageNo;
	short freeSpace;
	void dump(){
		cout << "===========================Directory Entry=======================" << endl;
		cout << "Page No :  " << pageNo <<endl;
		cout << "FreeSpace : " << freeSpace << endl;
		cout << "=================================================================" << endl;
	}
};


//DBTime :used to store time format in database
struct DBTime{
	short year;
	unsigned char month;
	unsigned char date;
	unsigned char hour;
	unsigned char min;
	unsigned char sec;
};

//IndexNode : used to store a keyValue ,LeftPointer ,Right Pointer
struct IndexNode{
	char *key;
	int leftPtr;
	int rightPtr;
};

//SysTableRecord :used to store all the information related to table like tablename,creation time.....
//                and this structure is inserted as record in the SYSTABLE
struct SysTableRecord{
	char tableName[MAX_TABLENAME_SIZE];//Table Name.
	int totalRows;//total number of rows in the table.
	//DBTime createTime;//table creation time.
	//DBTime updateTime;//table updation time.
	int startPage;//table content's start page.
	int totalDP;//total number of directory page.
	int totalColumns;//total number ofcolumns in this table;
	
	void dump(){
		using namespace std;
		cout << "\n=======SYS_TABLE_RECORD=======\n";		
		cout << "Table Name     : " << tableName <<endl;
		cout << "No Of Rows     : " << totalRows << endl;
		/*cout << "Create Time    : " << endl;
		cout << "Update Time    : " << endl;*/
		cout << "Start DirPage  : " << startPage << endl;
		cout << "Total No Of DPs: " << totalDP << endl;
		cout << "No Of Columns  : " << totalColumns << endl;
		cout << "================================\n";
	}
};


//SysColumnRecord :used to store all the information related to column like column_name,columnposition,tablename.....
//                 and this structure is inserted as record in the SYSCOLUMN
struct SysColumnRecord{
	char columnName[MAX_COLUMNNAME_SIZE];//column name.
	char tableName[MAX_TABLENAME_SIZE];//table name.
	short colPos;//position of the column in the table.
	int dataType;//specifies the datatype of the column.
	bool isNotNull;//whether the column can contain null values.
	bool isPrimary;
	bool isUnique;
	bool isDeleted;
	int maxSize;//specifies the maximum size of the column.
	char defaultvalue[MAX_DATATYPE_SIZE];
	
	void dump(){
		using namespace std;
		string disp("NO");
		cout << "\n=======SYS_COLUMN_RECORD=======\n";
		cout << "Column Name : " << columnName <<endl;
		cout << "Table Name  : " << tableName <<endl;
		cout << "Position    : " << colPos << endl;
		cout << "Data Type   : " << getTypeName(dataType) << endl;
		//cout << "Data Type   : " << dataType << endl;
		(isNotNull)?disp="YES":disp="NO";
		cout << "Not Null    : " << disp << endl;
		(isPrimary)?disp="YES":disp="NO";
		cout << "Primary Key : " << disp << endl;
		(isUnique)?disp="YES":disp="NO";
		cout << "Unique      : " << disp << endl;
		cout << "Max Size    : " << maxSize << endl;
		cout << "Default     : " << defaultvalue << endl;
		cout << "==============================\n";
	}
	
	
	public:
	string getTypeName(int dataType){

		string ret("");
		switch(dataType){
					case INTTYPE:
	 							ret = "INT";
								break;
					case DATETYPE:
								ret = "DATE";
								break;
					case FLOATTYPE:
								ret = "FLOAT";
								break;
					case LONGTYPE:
								ret = "LONG";
								break;
					case DOUBLETYPE:
								ret = "DOUBLE";
								break;
					case VARCHARTYPE:
								ret = "VARCHAR";
								break;
					case SHORTTYPE:
								ret = "SHORT";
								break;
					case BOOLTYPE:
								ret = "BOOL";
								break;			
		}
		return ret;
	}
};


//SysIndexRecord: used to store all information related to index like index name, table name,...
struct SysIndexRecord{
	
	char indexName[MAX_TABLENAME_SIZE];//index name
	char tableName[MAX_TABLENAME_SIZE];//table name	
	short colOrigPos;//position of the column in the table.
	short colIndexPos;//position of the column in the index.
	int startPage;//starting DP for this index
	short totalIndexCols;//number of columns in the index.
	
	void dump(){
		using namespace std;
		cout << "\n=======SYS_INDEX_RECORD========\n";		
		cout << "Index Name 		: " << indexName <<endl;
		cout << "Table Name		: " << tableName <<endl;
		cout << "Col Orig Position	: " << colOrigPos << endl;
		cout << "Col Index Position	: " << colIndexPos << endl;
		cout << "Start Page (DP)	: " << startPage << endl;
		cout << "No of Index Cols	: " << totalIndexCols << endl;
		cout << "\n===============================\n";
	}
};

#endif
