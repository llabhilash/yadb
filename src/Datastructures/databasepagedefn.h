//databasepagedefn.h
//function definitions of all the methods of class DataBasePage.
///////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef DATABASEPAGEDEFN_H
#define DATABASEPAGEDEFN_H



#ifndef SLOT_DIR_SIZE
#define SLOT_DIR_SIZE 4
#endif
#ifndef SLOTDIR_ENTRIES_START_POS
#define SLOTDIR_ENTRIES_START_POS 10
#endif

using namespace std;
/*
//Constructor for the class DataBasePage when no data is passed.
DataBasePage::DataBasePage(){
	pageData = new char[PAGE_SIZE_IN_BYTES];
}
*/


//Constructor for the class DataBasePage when a data(page data which is obtained from the BufferManager)
// is passed.
DataBasePage::DataBasePage(char* &data){
	//pageData = new char[PAGE_SIZE_IN_BYTES];
	pageData = data;
}


//getTotalFreeSpace()-Returns the total free space of this page.
short DataBasePage::getTotalFreeSpace(){
      //Bytes 0-1 of pageData contains total Free Space.
      short freeSpace;
      memcpy((char *)&freeSpace,pageData,2);
      return freeSpace;
}


//setTotalFreeSpace(short freeSpace)-This method sets/Resets the total free space to freespace.
bool DataBasePage::setTotalFreeSpace(short freeSpace){
        //Bytes 0-1 of pageData contains total Free Space.
        memcpy(pageData,(char *)&freeSpace,2);
        return true;
}


//getCFSPointer()-This method returns the structure SlotDirEntry(offset,length) which contains the 
//                offset of ContextFreePage(CFS) and length of CFS.
SlotDirEntry DataBasePage::getCFSTuple(){
       //Bytes 2-5 contains the CFS tuple (offset ,length).
       SlotDirEntry dirEntry;
       memcpy((char *)&(dirEntry.offset),&pageData[2],2);
       memcpy((char *)&(dirEntry.length),&pageData[4],2);
       return dirEntry;
}
         

//setCFSTuple()- This method sets/Resets the CFS tuple(offset,length) whenever a record is inserted in
//               the CFS area.         
 bool DataBasePage::setCFSTuple(SlotDirEntry dirEntry){
	 //Bytes 2-5 contains the CFS tuple (offset,length)
	 memcpy(&pageData[2],(char *)&(dirEntry.offset),2);
	 memcpy(&pageData[4],(char *)&(dirEntry.length),2);
	 return true;
 }
 
 
//getNoOfSlots()-This method return the total number of slots present in this page.
short DataBasePage::getNoOfSlots(){
         //Bytes 6-7 contains the total Number of Slots present in that page.
         short totalSlots;
         memcpy((char *)&totalSlots,&pageData[6],2);
         return totalSlots;
}


//setNoOfSlots(short totalSlots)-This method sets/Resets the total number of slots to totalSlots.
bool DataBasePage::setNoOfSlots(short totalSlots){
	//Bytes 6-7 contains the total Number of Slots present in that page.
	memcpy(&pageData[6],(char *)&totalSlots,2);
	return true;
}

//getTotalFreeSlots() - This method returns the total free slots left.
short DataBasePage::getTotalFreeSlots(){
         //Bytes 8-9 contains the total Number of Slots present in that page.
         short freeSlots;
         memcpy((char *)&freeSlots,&pageData[8],2);
         return freeSlots;
}


//setTotalFreeSlots() - This method is used to set/reset the total free slot count.
bool DataBasePage::setTotalFreeSlots(short totalSlots){
	//Bytes 8-9 contains the total Number of Slots present in that page.
	memcpy(&pageData[8],(char *)&totalSlots,2);
	return true;
}


//incrementTotalFreeSlots() -This method increment total number of free slots.
bool DataBasePage::incrementTotalFreeSlots(){
	//Bytes 8-9 contains the total Number of Slots present in that page.
	short totalSlots = getTotalFreeSlots() + 1;
	memcpy(&pageData[8],(char *)&totalSlots,2);
	return true;
}

//decrementTotalFreeSlots() -This method decrement total number of free slots.
bool DataBasePage::decrementTotalFreeSlots(){
	//Bytes 8-9 contains the total Number of Slots present in that page.
	short totalSlots = getTotalFreeSlots() - 1;
	memcpy(&pageData[8],(char *)&totalSlots,2);
	return true;
}

//insertRecord(char * recPtr, int reclen, RID& recID)-This method adds new record to the page.It returns TRUE if 
//                                                  it was able to insert a record else returns FALSE.Also it sets
//                                                  the rid  to be the new record RID ,if it returns TRUE.
bool DataBasePage::insertRecord(char * recPtr, int reclen, RID& recID){
	short totalSlots = getNoOfSlots();
	//check if the size of the record is greater than totalFreeSpace.
	if(reclen > getTotalFreeSpace()){
		return false;//indicates that record cannot be inserted.
	}
	//check if this is the first record to be inserted.
	if(totalSlots == 0){
	        //check if the (size of record + size of slot directory entry )is greater than TFS
	        if((reclen + SLOT_DIR_SIZE) > getTotalFreeSpace()){
		        return false;//indicates that record cannot be inserted.
	        }
	        SlotDirEntry CFSTuple,slotDir;
	        CFSTuple = getCFSTuple();
	        short recordOffset = CFSTuple.offset - reclen + 1;//find the offset to insert a record.
	        //construct slotDirectory for the new record.
	        slotDir.offset = recordOffset;
	        slotDir.length = reclen;
	        //reset CFSTuple.
	        CFSTuple.offset = (CFSTuple.offset -reclen);
	        CFSTuple.length = (CFSTuple.length - reclen - SLOT_DIR_SIZE);
	        //reset TFS.
	        short totalFS = getTotalFreeSpace();
	        totalFS = (totalFS - reclen -SLOT_DIR_SIZE);
	        //reset total Number of Slots.
	        totalSlots = totalSlots + 1;
	        //Reset TFS ,CFS tuple ,total No of Slots. 
	        setTotalFreeSpace(totalFS);
	        setCFSTuple(CFSTuple);
	        setNoOfSlots(totalSlots);
	        //insert Slot Directory Entry at byte offset starting at 8
	        insertSlotDirEntry(slotDir,SLOTDIR_ENTRIES_START_POS);
	        //insert record into pageData.
	        memcpy(&pageData[recordOffset],recPtr,reclen);
	        //set recID
	        recID.slotNo = 0;
		return true;
	}
	
        //Check for the slotDirEntries, if there is any unused place is left 
        //If any unused slot is found ,check if new record can be placed.
        //If a record can be inserted.Then choose the best-fit one.
        //If no slot can be found then use the CFS to store record and try to use any unsed slot
        //with slotDirEnrty.length being minimum. update CFS ,TFS ,slotDirEntry accordingly
        //If no unused slot is found expand slot directory.
        SlotDirEntry curSlot;
        //curSlot.length = 0;
        curSlot.length = PAGE_SIZE_IN_BYTES + 1;//initialize it to some value,so that comparision between
                              //slots can be made in terms of length to get best fit.
        int slotNo = -1;//initialize to some value.                      
        
        if(getTotalFreeSlots() > 0){
        
		for(int i = 0 ; i < totalSlots ; i++){
		        short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
		        SlotDirEntry temp = getSlotDirEntry(dirOffset);
		        if((temp.length < 0) && ((-temp.length) >= reclen) && ((-temp.length) < curSlot.length)){
			        slotNo = i;
			        curSlot.length  = -temp.length;
			        curSlot.offset = temp.offset;
		        }
	        }
	        //Check if there is any unused best slot which is capable of storing the reocrd is found.
	        if(slotNo >= 0){
		        //Unused Best Slot is found.
		        //Reset TFS value.
		        short totalFS = getTotalFreeSpace();
		        totalFS = totalFS - reclen;//calculate new TFS
		        //Reset slotDirectory for new record.
		        curSlot.length = reclen;
		        //Reset TFS and slotDirectoryEntry.
		        setTotalFreeSpace(totalFS);
		        decrementTotalFreeSlots();
		        insertSlotDirEntry(curSlot , (short)(SLOTDIR_ENTRIES_START_POS + (slotNo * SLOT_DIR_SIZE)));
		        //insert record into pagedata.
		        memcpy(&pageData[curSlot.offset],recPtr,reclen);
		        //set recID.
		        recID.slotNo = slotNo;
		        return true;
	        }
	        //No unused best slots were found.
	        //Check if CFS can accomodate this record.
	        
	        SlotDirEntry cfsTuple;
	        cfsTuple = getCFSTuple();
	        
	        if(cfsTuple.length < reclen){
	        	//DeFragment the page.
	        	//and then try and insert
	        	defragmentPage();
	        	cfsTuple = getCFSTuple();	        	
	        }

	        if(cfsTuple.length > reclen){
		        slotNo = -1;
		        curSlot.length = PAGE_SIZE_IN_BYTES + 1;
		        //check if there are any unused best slots 
		        for(int i = 0 ; i < totalSlots ; i ++){
			        short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
			        SlotDirEntry temp = getSlotDirEntry(dirOffset);
			        if((temp.length <= 0) && ((-temp.length) < curSlot.length)){
				        slotNo = i;
				        curSlot.length  = -temp.length;
				        curSlot.offset = temp.offset;
			        }    
		        }
		        //check if there are any unused best slots
		        if(slotNo >= 0){
		                short recordOffset = cfsTuple.offset - reclen + 1;//find the offset to insert a record.
	                        //construct slotDirectory for the new record.
	                        curSlot.offset = recordOffset;
	                        curSlot.length = reclen;
	                        //reset CFSTuple.
	                        cfsTuple.offset = (cfsTuple.offset -reclen);
	                        cfsTuple.length = (cfsTuple.length - reclen);
	                        //reset TFS.
	                        short totalFS = getTotalFreeSpace();
	                        totalFS = (totalFS - reclen);
	                        //Reset TFS ,CFS tuple . 
	                        setTotalFreeSpace(totalFS);
	                        setCFSTuple(cfsTuple);
	                        decrementTotalFreeSlots();
	                        //insert Slot Directory Entry at byte offset starting at 8
	                        insertSlotDirEntry(curSlot,(short)(SLOTDIR_ENTRIES_START_POS +(slotNo * SLOT_DIR_SIZE)));
	                        //insert record into pageData.
	                        memcpy(&pageData[recordOffset],recPtr,reclen);
	                        //set recID
	                        recID.slotNo = slotNo;
			        return true;
		        }
		        
		        
	        }
        
        }
        
        
         //If there are no unused slots.
        //Check if a new record and new slot Directory Entry can be inserted.
        SlotDirEntry cfsTuple;
	cfsTuple = getCFSTuple();
        
        if(cfsTuple.length < (reclen + SLOT_DIR_SIZE)){
	        	//DeFragment the page.
	        	//and then try and insert
	        	defragmentPage();
	        	cfsTuple = getCFSTuple();	        	
	}
	
        
        if(cfsTuple.length >= (reclen + SLOT_DIR_SIZE)){
	        //Space for new record and new slot Directory Entry is available.
                SlotDirEntry CFSTuple,slotDir;
	        CFSTuple = getCFSTuple();
	        short recordOffset = CFSTuple.offset - reclen + 1;//find the offset to insert a record.
	        //construct slotDirectory for the new record.
	        slotDir.offset = recordOffset;
	        slotDir.length = reclen;
	        //reset CFSTuple.
	        CFSTuple.offset = (CFSTuple.offset -reclen);
	        CFSTuple.length = (CFSTuple.length - reclen - SLOT_DIR_SIZE);
	        //reset TFS.
	        short totalFS = getTotalFreeSpace();
	        totalFS = (totalFS - reclen - SLOT_DIR_SIZE);
	        //reset total Number of Slots.
	        totalSlots = totalSlots + 1;
	        //Reset TFS ,CFS tuple ,total No of Slots. 
	        setTotalFreeSpace(totalFS);
	        setCFSTuple(CFSTuple);
	        setNoOfSlots(totalSlots);
	        //insert Slot Directory Entry at byte offset starting at 8
	        insertSlotDirEntry(slotDir,SLOTDIR_ENTRIES_START_POS + ((totalSlots-1)*SLOT_DIR_SIZE));
	        //insert record into pageData.
	        memcpy(&pageData[recordOffset],recPtr,reclen);
	        //set recID
	        recID.slotNo = totalSlots -1;
		return true;
        }
        

}

//defragmentPage()- This method is used to defragment the database page. 
bool DataBasePage::defragmentPage(){

	//TFS will be the same in the new page
	//No of Slots will be the same	
	//Make empty slots as those with length 0
	//add records for non empty slots

	//short recordNewOffset;
	DM_FUNC_TRACE && cout << "\n###################### DeFragmentPage() ######################### \n"<<endl;
	char* newdbpagebuf = new char[PAGE_SIZE_IN_BYTES];
	DataBasePage newDBPage(newdbpagebuf);
	newDBPage.init();
	
	SlotDirEntry newCFSTuple, newSlotDE;
	newCFSTuple = newDBPage.getCFSTuple();
	
	//set new TFS as the same as the old TFS
	newDBPage.setTotalFreeSpace(getTotalFreeSpace());
	
	//set number of slots as same
	newDBPage.setNoOfSlots(getNoOfSlots());
	newDBPage.setTotalFreeSlots(getTotalFreeSlots());
	
	for(int i = 0 ; i < getNoOfSlots() ; i ++){
	        short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
	        SlotDirEntry temp = getSlotDirEntry(dirOffset);
	        if(temp.length > 0){
	        	//add record
	        	
	        	//record length is the same
	        	newSlotDE.length = temp.length;
	        	
	        	//find record new offset
	        	//recordNewOffset = newCFSTuple.offset - newSlotDE.length + 1;
	        	
	        	newSlotDE.offset = newCFSTuple.offset - newSlotDE.length + 1;
	        	
	        	//make changes to CFS Tuple
	        	newCFSTuple.offset = (newCFSTuple.offset - newSlotDE.length);
		        newCFSTuple.length = (newCFSTuple.length - newSlotDE.length - SLOT_DIR_SIZE);
	        	
	        	//copy record from oldpage.offset to newpage[recordnewoffset]
	        	memcpy(&newdbpagebuf[newSlotDE.offset], &pageData[temp.offset], newSlotDE.length);
	        	
	        }else{
	        	//make new length as 0..  offset doesnt matter
	        	newSlotDE.length = 0;
	        	newCFSTuple.length = (newCFSTuple.length - SLOT_DIR_SIZE);
	        }
	        
	        //write back new SlotDE to newDBPage
	        newDBPage.insertSlotDirEntry(newSlotDE, SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE));
        }
	
	//set new CFS
	newDBPage.setCFSTuple(newCFSTuple);
	
	//write back data
	memcpy(&pageData[0], &newdbpagebuf[0], PAGE_SIZE_IN_BYTES);
	
	return true;
	
}


//insertSlotDirEntry(SlotDirEntry dirEntry,short offset)-This method inserts SlotDirEntry starting from position offset.
bool DataBasePage::insertSlotDirEntry(SlotDirEntry dirEntry,short offset){
        //insert dirEntry from bytes offset to (offset + 3)
	memcpy(&pageData[offset],(char *)&(dirEntry.offset),2);
	memcpy(&pageData[offset + 2],(char *)&(dirEntry.length),2);
	return true;
}


//getSlotDirEntry(short offset)-This method returns the SlotDirEntry starting from position offset.
SlotDirEntry DataBasePage::getSlotDirEntry(short offset){
	//retreives SlotDirEntry starting from position offset to (offset + 3).
	SlotDirEntry dirEntry;
	memcpy((char *)&(dirEntry.offset),&pageData[offset],2);
        memcpy((char *)&(dirEntry.length),&pageData[offset + 2],2);
        return dirEntry;
}


//init()-This method initializes the new DataBasePage.It sets the 
//totalfreespace of page to (PAGE_SIZE_IN_BYTES - 2(bytes to store freespace) - 4(bytes to store CFS tuple - 2(bytes 
// to store total number of slots)),
//CFS tuple to (offset ,length ) where offset = (PAGE_SIZE_IN_BYTES -1) since records will begin from end of the page,
// and length = (PAGE_SIZE_IN_BYTES - 2 - 4) which is same as TFS initially.
//total number of slots to 0.
void DataBasePage::init(){
        SlotDirEntry dirEntry;
        short freeSpace = PAGE_SIZE_IN_BYTES - 2 - 4 -2 -2;
        short totalSlots = 0;
        dirEntry.offset = PAGE_SIZE_IN_BYTES -1;
        dirEntry.length = PAGE_SIZE_IN_BYTES - 2 - 4 -2 -2;
	setTotalFreeSpace(freeSpace);
	setTotalFreeSlots(0);
	setCFSTuple(dirEntry);
	setNoOfSlots(totalSlots);
}


//deleteRecord(const RID &) - This method makes the slot Directory offset value to negative indicating that the 
//                            record can no longer be used.
bool DataBasePage::deleteRecord(const RID & RecID){
         short removeSlot = RecID.slotNo;
         short totalSlots = getNoOfSlots();
         //check if that slot exists.
         if(totalSlots < removeSlot){
	         return false;
         }
         short dirOffset = SLOTDIR_ENTRIES_START_POS + (removeSlot * SLOT_DIR_SIZE);
         SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
         //Reset TFS value
         short totalFS = getTotalFreeSpace();
         totalFS = totalFS + dirEntry.length;
         setTotalFreeSpace(totalFS);
         incrementTotalFreeSlots();
         //Change SlotDirEntry offset value to -offset(make negative).
         dirEntry.length = -(dirEntry.length);
         insertSlotDirEntry(dirEntry,dirOffset);
         return true;
}


//firstRecord(RID &firstRID) - This method set the RID.slotNo to the first valid slotNo and returns true.
//                             If no valid slotNo (slotNo storing valid record)exists it returns false.
bool DataBasePage::firstRecord(RID& firstRID){
	//check if there are slots.
	short totalSlots = getNoOfSlots();
	if(totalSlots == 0){
		return false;
	}
	//check each slot directory entry until a valid slot directory entry is found.
	//valid slot directory entry means offset value is not negative.
	for(int i = 0; i < totalSlots ; i ++){
		short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
		SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
		if(dirEntry.length <= 0){
			continue;
		}
		firstRID.slotNo = i;
		return true;
	}
	//No valid Slot dir Entry was found.
	return false;
}


//nextRecord(RID &curRID ,RID &nextDID)-This method sets the nexRID.slotNo to the next valid slotNo if it exists
//                                      and returns true.Else returns false indicating no next Record.
bool DataBasePage::nextRecord(RID& curRID, RID& nextRID){
        //check if curRID.slotNo is the last slot in that page.
	short totalSlots = getNoOfSlots();
	if(curRID.slotNo == (totalSlots -1)){
	        return false;
	}
	//check each slot directory entry until a valid slot directory entry is found.
	//if found set nextRID values and return true,else return false.
	for(int i = (curRID.slotNo + 1) ; i < totalSlots ;i++){
		short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
		SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
		if(dirEntry.length <= 0){
			continue;
		}
		nextRID.slotNo = i;
		return true;
	}
	//No valid Slot dir was found.
	return false;
}


//getRecord(RID recID,char *recPtr,int &recLen)-This method sets the recPtr to the record pointed by the RID recID
//                                              and recLen to the record length of the record pointed by the RID recID
//                                              If it was able to set it return true, else returns false.
bool DataBasePage::getRecord(RID recID, char* &recPtr, int& recLen){
        //check if this is valid RID i.e.,check if the recID.slotNo exits in the page.
	short totalSlots = getNoOfSlots();
	if(recID.slotNo >= totalSlots){
		return false;
	}
	//check if this RID points to valid record i.e., slot offset value should not be negative.
	short dirOffset = SLOTDIR_ENTRIES_START_POS + (recID.slotNo * SLOT_DIR_SIZE);
	SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
	if(dirEntry.length < 0){
		return false;
	}
	recLen = (int )dirEntry.length;	
	recPtr = new char[recLen];
	memcpy(recPtr,&pageData[dirEntry.offset],recLen);
	//std::cout << "Size,Data,offset : " << recLen << " , " << recPtr << " , " << dirEntry.offset << "\n";
	return true;
}


//returnOffset(RID recID ,int &offset)-This method set the offset value to the offset of the record pointed by recID.
//                                     It returns true if everything is valid else false.
bool DataBasePage::returnOffset(RID recID, int& offset){
	//check if this is valid RID i.e.,check if the recID.slotNo exits in the page.
	short totalSlots = getNoOfSlots();
	if(recID.slotNo > totalSlots){
		return false;
	}
	//check if this RID points to valid record i.e., slot offset value should not be negative.
	short dirOffset = SLOTDIR_ENTRIES_START_POS + (recID.slotNo * SLOT_DIR_SIZE);
	SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
	if(dirEntry.length < 0){
		return false;
	}
        offset = dirEntry.offset;
        return true;
}


//dump()-This methods is used to dump the contents of the databasepage.
void DataBasePage::dump(){

	cout<<"============== Database Page ================\n";

	cout<<"Total Free Space :"<<getTotalFreeSpace()<<endl;
	SlotDirEntry sDE;
	int totalSlots = (int) getNoOfSlots();
	sDE = getCFSTuple();
	cout<<"CFS free space :"<<sDE.length<<"  CFS offset :"<<sDE.offset<<endl;
	cout<<"Total No of Slots :"<<getNoOfSlots()<<endl;
	cout<<"Slots :\n";
	for(int i = 0; i < totalSlots ; i ++){
		short dirOffset = SLOTDIR_ENTRIES_START_POS + (i * SLOT_DIR_SIZE);
		SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
		cout<<"Slot No :"<<i<<"  slot offset:"<<dirEntry.offset<<"  slot length :"<<dirEntry.length<<endl;
	}
	cout<<"=============================================\n";
}
//bool DataBasePage::returnRecord(RID& rid, char*& recPtr, int& recLen);
//bool DataBasePage::isEmpty();


bool DataBasePage::updateRecord(char* recPtr, int reclen, RID recID){
	//check if this is valid RID i.e.,check if the recID.slotNo exits in the page.
	short totalSlots = getNoOfSlots();
	if(recID.slotNo >= totalSlots){
		return false;
	}
	
	//check if this RID points to valid record i.e., slot offset value should not be negative.
	short dirOffset = SLOTDIR_ENTRIES_START_POS + (recID.slotNo * SLOT_DIR_SIZE);
	SlotDirEntry dirEntry = getSlotDirEntry(dirOffset);
	
	//check for invalid length	
	if(dirEntry.length < 0 || dirEntry.length != reclen){
		return false;
	}
	
	//recLen = (int )dirEntry.length;	
	//recPtr = new char[recLen];
	
	//writeback to page
	memcpy(&pageData[dirEntry.offset], recPtr, reclen);	
	
	return true;
}


//getRecordForRIDs() - This method is used to get all the records for which RIDs have been mentioned. 
bool DataBasePage::getRecordsForRIDs(vector<string> &allrecs, vector<int> &recsizes, vector<RID> &allRIDs){
	char* recbuf = NULL;	
	int recSize;
	string* tempstring = NULL;
	RID curRID;
	for(int i = 0 ; i < allRIDs.size(); i ++){
		curRID = allRIDs[i];
		if(recbuf != NULL){
			delete[] recbuf;
		}
		getRecord(curRID,recbuf,recSize);
		if(tempstring != NULL)
			delete tempstring;
		tempstring = new string(recbuf,recSize);
		allrecs.push_back((*tempstring));				
		recsizes.push_back(recSize);
	}
	if(recbuf != NULL){
		delete[] recbuf;
	}
	if(tempstring != NULL)
		delete tempstring;
	return true;
}

#endif
