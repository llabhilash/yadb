//dbheaderdefn.h
//This file contains all the function definitions of class DBHeader.
///////////////////////////////////////////////////////////////////////////////////////



#ifndef DBHEADER_DEFN_H
#define DBHEADER_DEFN_H

#define START_BYTE_OF_TOTALPAGES O
#define START_BYTE_OF_LASTPAGE 4
#define START_BYTE_OF_FREEPAGEPTR 8
#define START_BYTE_OF_SYSTABLE 12
#define START_BYTE_OF_SYSCOLUMN 16
#define START_BYTE_OF_SYSINDEX 20


//Constructor for the class DBHeader when a data(page data which is obtained from the BufferManager)
// is passed.
DBHeader::DBHeader(char* &data){
	//pageData = new char[PAGE_SIZE_IN_BYTES];
	pageData = data;
}


//getNoOfPages()-This method returns the total No of Pages containes in that schema(in the current file)
int DBHeader::getNoOfPages(){
        //Bytes 0-3 of DBHeader contains total No. of pages.
	int totalPages;
        memcpy((char *)&totalPages,pageData,4);
        return totalPages;
}


//setNoOfPages(int totalPages)-This method sets/resets the total No. of pages.
bool DBHeader::setNoOfPages(int totalPages){
	//Bytes 0-3 of DBHeader contains total No. of pages.
        memcpy(pageData,(char *)&totalPages,4);
        return true;
}


//getLastPage()-This method returns the Last page No.(i.e.,total pages -1).
int DBHeader::getLastPage(){
	//Bytes 4-7 contains Last Page No.
        int lastPageNo;
        memcpy((char *)&lastPageNo,&pageData[START_BYTE_OF_LASTPAGE],4);
        return lastPageNo;
}


//setLastPage(int lastPage)-This methos sets/resets the last page No to lastpage.
bool DBHeader::setLastPage(int lastPage){
        //Bytes 4-7 contains Last Page No
	memcpy(&pageData[START_BYTE_OF_LASTPAGE],(char *)&lastPage,2);
        return true;
}


//getFreePagePointer()-This method returns the Free page No (Free page pointer).      
int DBHeader::getFreePagePointer(){
	//Bytes 8-11 contains Last Page No.
        int freePageNo;
        memcpy((char *)&freePageNo,&pageData[START_BYTE_OF_FREEPAGEPTR],4);
        return freePageNo;
}


//setFreePagePointer(int freePage)-This method sets/resets the Free page No.
bool DBHeader::setFreePagePointer(int freePage){
	//Bytes 8-11 contains Last Page No.
	memcpy(&pageData[START_BYTE_OF_FREEPAGEPTR],(char *)&freePage,4);
	return true;
}


//getSystemTablePointer()-This method returns the system table pointer i.e., the start page
//                        of system table.
int DBHeader::getSystemTablePointer(){
	//Bytes 12-15 contains system table ptr.
        int sysTablePtr;
        memcpy((char *)&sysTablePtr,&pageData[START_BYTE_OF_SYSTABLE],4);
	return sysTablePtr;
}

//setSystemTablePointer(int sysTabPtr)-This method sets/resets the system table pointer.
bool DBHeader::setSystemTablePointer(int sysTabPtr){
	//Bytes 12-15 contains system table ptr.
	memcpy(&pageData[START_BYTE_OF_SYSTABLE],(char *)&sysTabPtr,4);
	return true;
}


//getSystemColumnPointer()-This method returns the system column pointer i.e., the start page
//                        of system column.
int DBHeader::getSystemColumnPointer(){
	//Bytes 16-19 contains system column ptr.
        int sysColumnPtr;
        memcpy((char *)&sysColumnPtr,&pageData[START_BYTE_OF_SYSCOLUMN],4);
	return sysColumnPtr;
}


//setSystemColumnPointer(int sysColPtr)-This method sets/resets the system column pointer.
bool DBHeader::setSystemColumnPointer(int sysColPtr){
	//Bytes 16-19 contains system column ptr.
	memcpy(&pageData[START_BYTE_OF_SYSCOLUMN],(char *)&sysColPtr,4);
	return true;
}


//getSystemIndexPointer()-This method returns the system index pointer i.e., the start page
//                        of system index.
int DBHeader::getSystemIndexPointer(){
	//Bytes 20-23 contains system index ptr.
        int sysIndexPtr;
        memcpy((char *)&sysIndexPtr,&pageData[START_BYTE_OF_SYSINDEX],4);
	return sysIndexPtr;
}


//setSystemIndexPointer(int sysIndexPtr)-This method sets/resets the system index pointer.
bool DBHeader::setSystemIndexPointer(int sysIndexPtr){
	//Bytes 20-23 contains system index ptr.
	memcpy(&pageData[START_BYTE_OF_SYSINDEX],(char *)&sysIndexPtr,4);
	return true;
}


//dump()-This method dumps the contents of the DBHeader.
void DBHeader::dump(){
	cout<<"============== DBHEADER ================\n";
	cout<<"Total No Pages  :"<<getNoOfPages()<<endl;
        cout<<"Last Page No  :"<<getLastPage()<<endl;
        cout<<"Free Page pointer  :"<<getFreePagePointer()<<endl;
        cout<<"sys tab ptr  :"<<getSystemTablePointer()<<endl;
        cout<<"sys col ptr  :"<<getSystemColumnPointer()<<endl;
        cout<<"sys index ptr  :"<<getSystemIndexPointer()<<endl;
       	cout<<"========================================\n";
}


#endif
