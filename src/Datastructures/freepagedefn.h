//freepagedefn.h
//Includes all the function definitions of class FreePage.
//////////////////////////////////////////////////////////////////////////////////////////


#ifndef FREEPAGE_DEFN_H
#define FREEPAGE_DEFN_H

//Constructor for the class FreePage when a data(page data which is obtained from the BufferManager)
// is passed.
FreePage::FreePage(char* &data){
	//pageData = new char[PAGE_SIZE_IN_BYTES];
	pageData = data;
}


//getNextFreePage()-This method returns the next free page of this page.
//                  A return value 0f -1 indicates no next page.
int FreePage::getNextFreePage(){
	//Bytes 0-3 of FreePage contains NextFreePage No.
        int nextFreePage;
        memcpy((char *)&nextFreePage,pageData,4);
        return nextFreePage;
}


//setNextPage(int pageNo)-This method sets/Resets the next free page No.
bool FreePage::setNextFreePage(int pageNo){
        //Bytes 0-3 of FreePage contains NextFreePage No.
        memcpy(pageData,(char *)&pageNo,4);
        return true;
}

#endif
