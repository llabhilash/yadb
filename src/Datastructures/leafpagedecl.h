//leafpagedecl.h
//This file contains the declaration of the Leaf Page
////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef LEAFPAGEDECL_H
#define LEAFPAGEDECL_H
#include "branchpage.h"
#include<string.h>

class LeafPage{
	public :
	        char *pageData;
	        LeafPage(char* &pageData);
		bool setPageType(const short &pageType);
		short getPageType();
		bool setMaximumNodeEntries(const short  &maxValue);
		short getMaximumNodeEntries();
		bool setTotalKeysOccupied(const short &totalKeys);
		short getTotalKeysOccupied();
		bool setKeySize(const short &size);
		short getKeySize();
		bool setNextLeaf(const int &pageNo);
		int getNextLeaf();
		bool setPrevLeaf(const int &pageNo);
		int getPrevLeaf();
		bool insert(const vector<int> &keyType , char* const &key ,const RID &rid ,bool &split,int &pos);
		bool split(const vector<int> &keyType, IndexNode &node ,char* &newleaf,char* const &key ,const RID &rid,
		                const int &pageNo);
		bool dump(const vector<int> &keyType,const int &pos);
		bool traverseDump(const vector<int> &keyType);
		bool compareGTE(char* const &key1,char* const &key2, const vector<int> &keyTypes, 
		              const int &keySize1, const int &keySize2 ,int &cmpResult, bool &result,int);
		bool deleteKey(const vector<int> &keyType,char* const &key,bool &merge,RID &rid);
		bool findKey(const vector<int> &keyType ,char * const &key ,int &pos ,RID &rid);
		bool removeKey(int pos);
		bool getKey(int index,char* &key,RID &rid);
		bool adjustFromRightSibling(LeafPage &leafPage,const vector<int> &keyType,char* &key);
		bool adjustFromLeftSibling(LeafPage &leafPage,const vector<int> &keyType,char* &key);
		bool merge(LeafPage &leafPage ,const vector<int> &keyType);
		bool getRIDs(const vector<int> &keyType ,vector<int> columnPos,vector<int > operatorCode,
				vector<string> values,vector<RID> &allrids,int totalKeyValues,
						const bool &firsttime,bool &isResultsStarted);
		bool compare(char* const &key1, const vector<int> &keyTypes, const int &keySize1,const vector<int> columnPos, const vector<int> operatorCode,const vector<string> values,bool &result,bool &kontinue, int totalKeys );
		static bool compareFunction(string attribute1,string attribute2,int keyType,int operatorCode,bool &result);	
};


#endif
