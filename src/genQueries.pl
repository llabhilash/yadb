#!/usr/bin/perl

use strict;
use warnings;

open OUTFILE, ">queries.sql";

my $query = "";

print OUTFILE "create table t1 ( v1 int, v2 varchar(10) );\n";
#print OUTFILE "create index i1t1 on t1 (v1,v2);\n";

my $top = 10000;

for(my $i = 0; $i < $top; $i++){
	$query = "insert into t1 values ($i,'".($top-$i)."');\n";
	if(($i % 1000) == 0){
		print $query;
	}
	print OUTFILE $query;
}

close OUTFILE;
