%{
	
    #include <iostream>
    #include <string>
    #include <cctype>
    #include <stdlib.h>
    #include <stdio.h>
    #include "../sql_parser/parsedQuery.h"
    
	//char *myinput;
	char *myinputptr;
	int val;
	int *myinputlim = NULL;
	int initial = 0;
	int *mypos = &initial;
	
	using namespace std;
	extern "C"
	{
        int yyparse(void);
        int yylex(void);  
        
        void yyerror(char *);
	
	}
	string send("");
	parsedQuery *pq = new parsedQuery();
	
	
%}

 %union 
{
        int number;
        char *string;
        float fval;
}


%token <number>INTEGER 
%token CREATE USE SELECT INSERT DATABASE TABLE FROM VALUES OF INTO WHERE LIKE OPERATOR SCHEMA WORD OB CB WHITESPACE

%%

program:
        program expr '\n'         {     
        							char* c;
        							c = new char[10];
        							sprintf(c ,"%d", $<number>2);
        							string s = (const char*)c; 
        							send = s;
        							pq->setAction(send);
        						 }
        | 
        ;

expr:
        INTEGER
        | expr '+' INTEGER            { $<number>$ = $<number>1 + $<number>3; }
         | expr '-' INTEGER           { $<number>$ = $<number>1 - $<number>3; }
 //       | WHITESPACE expr WHITESPACE '+' WHITESPACE INTEGER            { $<number>$ = $<number>2 + $<number>5; }
 //       | WHITESPACE expr WHITESPACE '-' WHITESPACE INTEGER           { $<number>$ = $<number>2 - $<number>5; }
        ;

%%

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
}


parsedQuery *getResult(string input) {

	input.append("\n");
	myinputptr = (char *)input.c_str();
	int size  = input.size();
	myinputlim = &size;
     yyparse();
     
     return pq;
}

/*
int main(){

	string input = "1+2\n";
	myinputptr = (char *)input.c_str();
	//myinputptr = myinput;	
	val = input.size();		
	myinputlim = &val;
	yyparse();
	return 0;
}*/
