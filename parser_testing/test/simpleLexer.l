%{
	
	#undef YY_INPUT
	#define YY_INPUT(b,r,ms) (r = my_yyinput(b,ms))


     #include "calc1.hh"
     #include "string.h"
     #include "stdlib.h"
     void yyerror(char *);
     

// extern char myinput[];
 extern char *myinputptr; /* current poistion in myinput */
 extern int *myinputlim;  /* end of data */
 extern int *mypos;
 extern int *flag;
 
 int my_yyinput(char *buf, int max_size)
 {
 
 	int n = (max_size>*myinputlim-*mypos)?*myinputlim-*mypos:max_size;
 	
 	if(n > 0) {
 		memcpy(buf , myinputptr, n);
 		*mypos += n;
 		myinputptr += n;
 	}	
 	
 	return n;
 }


%}

CREATE create|CREATE
SCHEMA schema|SCHEMA
USE use|USE
INSERT insert|INSERT
SELECT select|SELECT
DATABASE database|DATABASE
TABLE table|TABLE
INTO into|INTO
VALUES values|VALUES
FROM from|FROM
WHERE where|WHERE
OF of|OF
LIKE like|LIKE
COMPARISION [=|+|<=|=>|<|>|!=]
AND and|AND
OR  OR|or
%%

CREATE		{ return CREATE; }
SCHEMA		{ return SCHEMA; }
USE			{ return USE; }
INSERT		{ return INSERT; }
SELECT 		{ return SELECT; }
DATABASE   	{ return DATABASE; }
TABLE		{ return TABLE; }
INTO			{ return INTO; }
VALUES 		{ return VALUES; }
FROM			{ return FROM; }
WHERE		{ return WHERE; }
OF			{ return OF; }
LIKE			{ return LIKE; }
COMPARISION	{ return COMPARISION; }
AND			{ return AND; }
OR			{ return OR; }
\(			{ return OB; }
\)			{ return CB; }
%			{ return PERC; }
'			{ return QUOTES; }
\*			{ return STAR; }

[a-zA-Z]+[0-9]*		{ yylval.str = strdup(yytext); return WORD; }

[0-9]+      		{
               	 yylval.str = strdup(yytext);
               	 return INTEGER;
	    			}
[0-9]*\.[0-9]+	    	{
				 yylval.str = strdup(yytext);
				 return FLOAT;		
				}
				
[-+\n]      { return *yytext; }

[ \t]+       ;       /* skip whitespace */

.           yyerror("Unknown character");

%%
int yywrap(void) {
    return 1;
}
