#include <iostream>
#include <string>
#include "calc1.cc"
#include "../sql_parser/parsedQuery.h"

using namespace std;

int main(){
	string s;// = "1 + 2";
	char input[255];
	cout<<"Enter equation:";
	cin.getline(input,255);
	s = input;
	cout<<s<<endl;
	parsedQuery *result = getResult(s);
	cout << "Result: "<<result->getAction()<<endl;
	return 0;
}
