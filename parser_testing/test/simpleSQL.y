%{
	
    #include <iostream>
    #include <string>
    #include <cctype>
    #include <vector>
    #include <stdlib.h>
    #include <stdio.h>
    #include "../sql_parser/parsedQuery.h"
    
	//char *myinput;
	char *myinputptr;
	int val;
	int *myinputlim = NULL;
	int initial = 0;
	int *mypos = &initial;
	
	using namespace std;
	extern "C"
	{
        int yyparse(void);
        int yylex(void);  
        
        void yyerror(char *);
	
	}
	string send("");
	parsedQuery *pq = new parsedQuery();
	vector<string> datavalues;
	vector<string> columnNames;
	vector<string> columnTypes;
	vector<int>  	notNull;
	vector<int>	pks;
	vector<int>	unique;
	vector<string> condArray;
	
	int noOfColumns = 0;
%}

 %union 
{
        int number;
        char *str;
        float fval;
}

%token INTEGER FLOAT
%token CREATE USE SELECT INSERT DATABASE TABLE FROM VALUES OF INTO WHERE LIKE COMPARISION SCHEMA WORD OB CB COMMA QUOTE PERC STAR
%token AND OR
%%

statement:
        statement sql '\n'         {     
        							char* c;
        							c = new char[10];
        							sprintf(c ,"%d", $2);
        							string s = (const char*)c; 
        							send = s;
        							pq->setAction(send);
        						 }
         
        ;

sql:
        creation
        |usage
        |insertion
        |selection
        ;
creation: 
		CREATE DATABASE WORD	{ 
							  cout<<"Caught Create Database"<<endl;
							  pq->setAction("create");
							  pq->setDatabase($3);
							  pq->createDatabase = true;
        						}
		CREATE TABLE WORD OB base_table_element_commalist CB	{ 
													 cout<<"Caught Create Table"<<endl;
													  pq->createTable = true;
													  pq->setTable($<str>3);
													  pq->setColumns(colunmNames);
													  pq->setTypes(columnTypes);
													  pq->setNotNulls(notNull);
													  pq->setUnique(unique);
													  pq->setPKs(pks);
													}
        	;
base_table_element_commalist: base_table_element
						| base_table_element_commalist COMMA base_table_element
						
		;
base_table_element: column_def
				| table_constraint_def;
		;
column_def: WORD WORD column_def_opt_list	{	
										cout<<"Caught column definition"<<endl;
										columnNames.push_back($<str>1);
										columnTypes.push_back($<str>2);
									 	noOfColumns++; 
									 }
		;
column_def_opt_list: 
			NOT_NULL					{ notNull.push_back(noOfColumns); }
			| UNIQUE					{ unique.push_back(noOfColumns); }
			| PRIMARY KEY				{ pks.push_back(noOfColumns); }
		;	
usage:
		USE WORD				{ 
							  cout<<"Caught Use"<<endl;	
							  pq->setAction("use");
							  pq->setDatabase($<str>2);
							  pq->use = true;
							}
		;
									
insertion: 
		INSERT INTO WORD VALUES OB insertData CB { 
										   pq->insert = true;
										   cout<<"Caught Insert"<<endl;	
										   pq->setAction("insert");
							  			   pq->insert = true;
							  			   pq->setTable($<str>3);
							  			   pq->setValues(datavalues);
							  			  }
		;					  			  
insertData:
		INTEGER				{
							  cout<<"Adding integer"<<endl;
							  datavalues.push_back($<str>1); 
							 }
		WORD					{ 
							  cout<<"Adding word"<<endl;
							  datavalues.push_back($<str>1); 
							}
		| QUOTE WORD QUOTE		{ 
							  cout<<"Adding String"<<endl;
							  datavalues.push_back($<str>2); 
							}
		| WORD COMMA insertData
		| QUOTE WORD QUOTE COMMA insertData 
		| INTEGER COMMA insertdata
		;								  
					  
selection: SELECT columnlist FROM WORD { 
								 pq->select = true;
								 cout<<"Caught Select"<<endl;
								 pq->setAction("select");
								 pq->setTable($<str>3);
								 pq->setColumns(columnNames);
   							    }
		| SELECT columnlist FROM WORD WHERE search_condition {
													 pq->select = true;
													 cout<<"Caught Select"<<endl;
													 pq->setAction("select");
													 pq->setTable($<str>3);
													 pq->setColumns(columnNames);
													 pq->setConditionArray(condArray);
												   }
		;
		
columnlist: STAR				{	columnNames.push_back("*");
							}	
		  | columnnames
		;  		
		
columnnames: WORD				{	columnNames.push_back($<str>1);
							}
		   | columnnames COMMA WORD { columnNames.push_back($<str>3);
		   					   }	
		;

		   			
search_condition: search_condition OR search_condition 	{ }
			   | serach_condition AND search_condition   { }
			   | NOT search_condition				{
			   | OB search_condition CB				{
			   | predicate
		;
		
predicate: compare_predicate 		{ $<str>$ = strdup($<str>1); }
		 | between_predicate	{ $<str>$ = strdup($<str>1); }
		 | like_predicate		{ $<str>$ = strdup($<str>1); }
		;
		
compare_predicate: scalar_exp COMPARISION scalar_exp { 
											strcat($<str>1,$<str>2); 
											strcat($<str>1,$<str>3);
											$<str>$ = strdup($<str>1);
										   }
		;

between_predicate: scalar_exp NOT BETWEEN scalar_exp AND scalar_exp
				| scalar_exp BETWEEN scalar_exp AND scalar_exp
		;		
like_predicate: scalar_exp NOT LIKE QUOTE matchphrase QUOTE
			 scalar_exp LIKE QUOTE matchphrase QUOTE
		;
		
matchphrase: 	WORD
		   | PERC WORD 
		   | WORD PERC
		   | PERC WORD PERC
		;
		   			 
scalar_exp: scalar_exp '+' scalar+exp
		  |scalar_exp '-' scalar_exp
		  |scalar_exp '*' scalar_exp
		  |scalar_exp '/' scalar_exp
		  |OB scalar_exp CB
		  |WORD
 		;
 		
%%

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
}


parsedQuery *getResult(string input) {

	input.append("\n");
	myinputptr = (char *)input.c_str();
	//cout<<" String sent: " <<myinputptr<<endl;
	//cout<<" Size: " << input.size()<<endl;
	//myinputptr = myinput;
	int size  = input.size();
	myinputlim = &size;
     yyparse();
     
     return pq;
}

/*
int main(){

	string input = "1+2\n";
	myinputptr = (char *)input.c_str();
	//myinputptr = myinput;	
	val = input.size();		
	myinputlim = &val;
	yyparse();
	return 0;
}*/
